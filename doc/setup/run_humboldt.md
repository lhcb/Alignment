# Run 3 Alignment instructions
The alignment software has a [gitlab repository](https://gitlab.cern.ch/lhcb/Alignment). It is used to estimate the positions of the detector elements. In order to run the alignment you need to define which detector elements and degrees of freedom you want to use. We refer to this as configuration and it is usually specified in the options that are used to run the alignment.


## Running with DD4hep
### Basic usage
To run an example VELO alignment job, do
```
./Alignment/run gaudiiter.py Alignment/Alignment/Humboldt/options/AlignVPHalvesModules.py -e 100 -n2
```
where `-e` specifies the number of events and `-n` the number of iterations.
This will write out the alignment constants in the default directory `default-yml-dir`.

The output directory can be specified with `--aligncond-outdir`, e.g.
```
./Alignment/run gaudiiter.py Alignment/Alignment/Humboldt/options/AlignVPHalvesModules.py -e 100 --aligncond-outdir veloconstants
```
Input constants can be loaded with `--aligncond-indir`
```
./Alignment/run gaudiiter.py Alignment/Alignment/Humboldt/options/AlignVPHalvesModules.py -e 100 --aligncond-indir veloconstants --aligncond-outdir veloconstants2
```



### Differences from DetDesc
* save constants as yml instead of xml
* new handling of conditions https://lhcb-conddb-run3.web.cern.ch/
* use `--aligncond-indir` and `--aligncond-outdir` to load and write conditions with `gaudiiter`
* some arguments related to database are no longer needed in `gaudisplititer` and `gaudiiter`
* XmlWriters not used anymore

### Known issues
* DD4hep doesn't work with UT yet -> reconstruction trying to access UT hits will fail

### Using gaudisplititer













