# Input files
## Simulation
Samples are readily available at the [test file database](https://gitlab.cern.ch/lhcb-datapkg/PRConfig/-/blob/master/python/PRConfig/TestFileDB.py)

## 2022 data
### HLT2 processed
If you want to use a specific run, look it up in the [RunDB](https://lbrundb.cern.ch/rundb/export/). The `State` field indicates where the data can be found. If it is `MIGRATED EOS`, the data can be found at `/eos/lhcb/hlt2/LHCb/`. If it is `MIGRATED OFFLINE`, it can be found in the bookkeeping.
### AlignmentWriter
Data selected by HLT1 for alignment can be found at `/calib/align/LHCb/` on the online machines.