# General information

## Meetings

## Asking questions
A good place to ask general questions about alignment is the [alignment mattermost channel](https://mattermost.web.cern.ch/lhcb/channels/alignment).
For more technical questions about alignment software and devlopment, there is a [dedicated channel](https://mattermost.web.cern.ch/lhcb/channels/alignment-software-testing).


## Logbook
For operational matters, see the [log book](https://lblogbook.cern.ch/Alignment+and+calibration/).