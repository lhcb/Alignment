# Alignment

Information on contributing to Alignment is given in the [CONTRIBUTING
file](CONTRIBUTING.md).

Documentation can be found [here](https://lhcb-alignment.docs.cern.ch/).