## Branches
The `master` branch is used for Run 3 developments. 
Dedicated branches exist for the data-taking in a specific year, eg `2024-patches`. How to know which branch your MR should target:
- Fixes and additions for the data-taking in the current year should target the specific `{year}-patches` branch.
By default, it is understood that a MR targeting the current year branch should also be included in `master`,
so the `master` branch is regularly synchronised with the current year branch. 
If you are putting in a temporary fix that is not meant as a long-term solution and should not be included in `master`, please state it clearly in the MR description.
- If you are working on a long-term development that is not meant for immediate inclusion in the data taking (e.g., for end-of-year Sprucing), your MR should target the `master` branch.

In case of doubt, you are encouraged to contact the coordinators of the WP you work within.
