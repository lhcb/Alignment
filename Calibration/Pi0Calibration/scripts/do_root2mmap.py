#!/bin/python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

#--> convert the ROOT (TTree) to MMap file
#--> any further questions, please contact Zhirui at zhirui.xu@cern.ch

from GaudiPython import gbl
pi0Calib = gbl.Calibration.Pi0Calibration

import os
dataName = "Second"
tuplefile = "/gpfs/LAPP-DATA/lhcb/xu/%s_data_2016_pi0.root" % dataName
tuplename = "KaliPi0/Pi0-Tuple"
outputfile = "/gpfs/LAPP-DATA/lhcb/xu/%s_data_2016_pi0.MMap" % dataName

if '__main__' == __name__:
    pi0Calib.Pi0CalibrationFile(tuplefile, tuplename, outputfile)
