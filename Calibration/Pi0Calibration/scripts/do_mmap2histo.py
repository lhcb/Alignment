###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#--> this script calls class Pi0MMap2Histo to read from a list of MMap files and fill in histograms.
#--> if nworker is larger than 1, the CALO cells indices are splitted into nworker groups and only histograms of cells from the same group are saved into the same file.
#--> any further questions, please contact Zhirui at zhirui.xu@cern.ch

year = '2016'

from Gaudi.Configuration import *
from Configurables import LHCbApp
LHCbApp().DataType = year
LHCbApp().EvtMax = -1

from Configurables import CondDBAccessSvc, CondDB, UpdateManagerSvc
CondDB().UseOracle = False
CondDB().UseLatestTags = [year]

from GaudiPython import *
ApplicationMgr(OutputLevel=INFO, AppName="Pi0MMap2Histo")

from Configurables import Pi0MMap2Histo
pi02Histo = Pi0MMap2Histo("Pi0MMap2Histo")
pi02Histo.nworker = 10
pi02Histo.filenames = [
    "/gpfs/LAPP-DATA/lhcb/xu/Second_data_2016_pi0.MMap",
    "/gpfs/LAPP-DATA/lhcb/xu/First_data_2016_pi0.MMap"
]
pi02Histo.outputDir = '/gpfs/LAPP-DATA/lhcb/xu/'
pi02Histo.outputName = 'Second'
pi02Histo.lambdaFileName = ""

from Configurables import GaudiSequencer
mainSeq = GaudiSequencer("MainSeq")
mainSeq.Members = [pi02Histo]
ApplicationMgr().TopAlg.append(mainSeq)
AppMgr().run(1)
