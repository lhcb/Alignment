/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * =====================================================================================
 *
 *       Filename:  Pi0MassFiller.h
 *
 *    Description:  (1) create the pi0 mass histogram;
 *                  (2) fit the histogram;
 *                  (3) save and return the results
 *
 *        Version:  1.0
 *        Created:  11/21/2016 04:32:16 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 *   Organization:
 *
 * =====================================================================================
 */

#ifndef CALIBRATION_PI0CALIBRATION_PI0MASSFILLER_H
#define CALIBRATION_PI0CALIBRATION_PI0MASSFILLER_H 1

#include <map>
#include <string>
#include <vector>

#include "TH1.h"
#include "TH2.h"

#include "Pi0Calibration/MMapVector.h"
#include "Pi0Calibration/Pi0CalibrationFile.h"
#include "Pi0Calibration/Pi0LambdaMap.h"

#include "Detector/Calo/CaloCellID.h"

const unsigned int BitsCol   = 6;
const unsigned int BitsRow   = 6;
const unsigned int BitsArea  = 2;
const unsigned int ShiftCol  = 0;
const unsigned int ShiftRow  = ShiftCol + BitsCol;
const unsigned int ShiftArea = ShiftRow + BitsRow;
const unsigned int MaskArea  = ( ( ( (unsigned int)1 ) << BitsArea ) - 1 ) << ShiftArea;

namespace Calibration {
  namespace Pi0Calibration {
    class Pi0MassFiller {
    public:
      Pi0MassFiller();
      Pi0MassFiller( const std::string& filename );

      virtual ~Pi0MassFiller() { m_hists->Clear(); } //---m_hists_bg->Clear(); }

      TH2* hists() { return m_hists; }

      static double scale_factor( Candidate c, const Pi0LambdaMap& lambdamap ) {
        auto ind1 = c.ind1;
        auto ind2 = c.ind2;

        double                    scale   = 1.;
        std::pair<double, double> lambda1 = lambdamap.get_lambda( ind1 );
        std::pair<double, double> lambda2 = lambdamap.get_lambda( ind2 );
        if ( lambda1.first < 0.0 ) lambda1 = std::make_pair( 1.0, 0.0 );
        if ( lambda2.first < 0.0 ) lambda2 = std::make_pair( 1.0, 0.0 );

        scale = std::sqrt( lambda1.first ) * std::sqrt( lambda2.first );

        return scale;
      }

    private:
      TH2* m_hists;
    };
  } // namespace Pi0Calibration
} // namespace Calibration

#endif
