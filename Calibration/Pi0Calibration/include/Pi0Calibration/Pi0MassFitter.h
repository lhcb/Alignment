/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * =====================================================================================
 *
 *       Filename:  Pi0MassFitter.h
 *
 *    Description:  (1) create the pi0 mass histogram;
 *                  (2) fit the histogram;
 *                  (3) save and return the results
 *
 *        Version:  1.0
 *        Created:  11/21/2016 04:32:16 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 *   Organization:
 *
 * =====================================================================================
 */

#ifndef CALIBRATION_PI0CALIBRATION_PI0MASSFITTER_H
#define CALIBRATION_PI0CALIBRATION_PI0MASSFITTER_H 1

#include <iostream>
#include <string>
#include <utility>

#include "TH1.h"
// from ROOT
#include "TCanvas.h"
#include "TError.h"
#include "TF1.h"
#include "TFile.h"
#include "TFitResult.h"
#include "TFitResultPtr.h"
#include "TH1.h"
#include "TMath.h"
#include "TSpectrum.h"
#include "TStyle.h"
#include "TTree.h"
// from RooFit
#include "RooAddPdf.h"
#include "RooArgList.h"
#include "RooArgusBG.h"
#include "RooChebychev.h"
#include "RooDataHist.h"
#include "RooFitResult.h"
#include "RooFormulaVar.h"
#include "RooGaussian.h"
#include "RooGenericPdf.h"
#include "RooMinimizer.h"
#include "RooPlot.h"
#include "RooPolynomial.h"
#include "RooProdPdf.h"
#include "RooRealIntegral.h"
#include "RooRealVar.h"
namespace Calibration {
  namespace Pi0Calibration {
    class Pi0MassFitter {
    public:
      Pi0MassFitter( TH1* hist, double mpi0, double sigma );
      virtual ~Pi0MassFitter() {}

      void                      chi2fit( const std::string& outputdir, const std::string& outfilename, bool verbose );
      std::pair<double, double> getMean() { return m_mean; }
      std::pair<double, double> getSigma() { return m_sigma; }
      std::pair<double, double> getNSignal() { return m_nsignal; }
      int                       getStatus() { return m_status; }
      int                       getCovQual() { return covQual; }

    private:
      TH1*   m_hist;
      double m_mpi0;
      double m_sigma0;

      std::pair<double, double> m_mean;
      std::pair<double, double> m_sigma;
      std::pair<double, double> m_nsignal;
      int                       m_status;
      int                       covQual;
    };
  } // namespace Pi0Calibration
} // namespace Calibration

#endif
