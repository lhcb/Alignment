/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @file MMapVector.h
 *
 * @date 2017-02-11
 * @author Manuel Schiller      <Manuel.Schiller@glasgow.ac.uk>
 *
 * @brief implementation of the MMapVector class
 *
 * MMapVector is a class that exposes a std::vector-like interface, but
 * allocates its storage by mmapping a file, either read-write, or read-only.
 * This allows vectors to grow substantially beyond the amount of RAM
 * installed in the machine by leveraging the VM subsystem of the operating system.
 */

#ifndef MMAPVECTOR_H
#  define MMAPVECTOR_H 1

#  include <algorithm>
#  include <cassert>
#  include <cstdint>
#  include <cstdlib>
#  include <cstring>
#  include <iterator>
#  include <limits>
#  include <stdexcept>
#  include <system_error>
#  include <typeinfo>

#  include <fcntl.h>
#  include <sys/mman.h>
#  include <unistd.h>

class MMapVectorBase {
public:
  /// flags for how to open a file
  enum Flags {
    SizeOnly,        ///< open existing file to obtain size
    ReadOnly,        ///< open existing file read-only
    ReadWrite,       ///< open existing file read-write
    ReadWriteCreate, ///< open r/w, create if needed
    ReadWriteTemp    ///< open a temp file r/w
  };
};

/// imlementation details of MMapVector class
namespace MMapVector_impl {
  /// implementation of 64 bit FNV1a hash for checksums etc
  uint64_t fnv1a( const void* data, uint64_t len ) noexcept;
  /// return cached system page size
  uint64_t pagesize();
  /// class for a memory mapping (RAII-style)
  class Mapping {
  private:
    void*    m_data; ///< mmapped area
    uint64_t m_len;  ///< length of mmapped area
  public:
    /// read-only or read-write mapping?
    enum Protection { ReadOnly = PROT_READ, ReadWrite = PROT_READ | PROT_WRITE };
    /// default constructor
    Mapping() : m_data( MAP_FAILED ), m_len( 0 ) {}
    /// constructor from file descriptor, offset, length
    Mapping( int fd, uint64_t offset, uint64_t len, Protection prot );
    /// destructor
    ~Mapping();
    /// it's illegal to copy a mapping
    Mapping( const Mapping& ) = delete;
    /// mappings cannot be assigned through copy
    Mapping& operator=( const Mapping& ) = delete;
    /// move constructor
    Mapping( Mapping&& other ) : m_data( other.m_data ), m_len( other.m_len ) {
      other.m_data = MAP_FAILED, other.m_len = 0;
    }
    /// move assignment
    Mapping& operator=( Mapping&& other );
    /// change the size of the memory mapping
    void resize( uint64_t newsz );
    /// size of the mmapped area
    uint64_t size() const noexcept { return m_len; }
    /// the mmapped area itself
    void* data() const noexcept { return ( MAP_FAILED != m_data ) ? m_data : nullptr; }
    /// make sure dirty pages are written to disk
    void msync();
    /// swap two mappings
    void swap( Mapping& other ) noexcept {
      std::swap( m_data, other.m_data );
      std::swap( m_len, other.m_len );
    }
  };
  /// class representing an open file (RAII-style)
  class File {
  public:
    using Flags = MMapVectorBase::Flags;

  private:
    int   m_fd;    ///< file descriptor
    Flags m_flags; ///< flags

    /// default constructor
    File() : m_fd( -1 ) {}

  public:
    /// open a file
    File( const char* fname, Flags flags );
    /// duplicate a file descriptor
    File( const File& other );
    /// move-construct a file
    File( File&& other ) : m_fd( std::move( other.m_fd ) ) { other.m_fd = -1; }
    /// destructor
    ~File() {
      if ( -1 != m_fd ) close( m_fd );
    }
    /// move assignment
    File& operator=( File&& other );
    /// duplicate file descriptor
    File& operator=( const File& other );
    /// return flags
    Flags flags() const noexcept { return m_flags; }
    /// read from file
    void read( void* buf, std::size_t count );
    /// write to file
    void write( const void* buf, std::size_t count );
    enum Whence { SeekBegin = SEEK_SET, SeekEnd = SEEK_END, SeekCur = SEEK_CUR };
    /// seek in file (move position to read/write)
    uint64_t seek( int64_t offset, Whence w );
    /// truncate (set file length)
    void truncate( int64_t offset );
    /// obtain a mmapped area
    Mapping map( uint64_t offset, uint64_t len ) {
      return Mapping( m_fd, offset, len, ( Flags::ReadOnly >= m_flags ) ? Mapping::ReadOnly : Mapping::ReadWrite );
    }
    /// swap two files
    void swap( File& other ) noexcept {
      std::swap( m_fd, other.m_fd );
      std::swap( m_flags, other.m_flags );
    }
  };
  /// file with a simple header that has a mmapped data section
  template <class UserHeader>
  class FileWithHeader {
  public:
    using Flags = MMapVectorBase::Flags;

  private:
    File m_file; ///< open file
    struct Header {
      std::uint64_t m_datasz;
      std::uint64_t m_offset;
      std::uint64_t m_checksum_data;
      UserHeader    m_userheader;
      std::uint64_t m_checksum_header;
    } m_header;        ///< in-memory version of file header
    Mapping m_mapping; ///< mmapped data section

    static_assert( sizeof( Header ) < 512, "Header must fit into smallest page size." );

    void flush();

  public:
    /// constructor
    FileWithHeader( const char* fname, File::Flags flags );
    /// destructor
    ~FileWithHeader() {
      if ( m_mapping.data() ) flush();
    }
    FileWithHeader( const FileWithHeader<UserHeader>& ) = delete;
    FileWithHeader( FileWithHeader<UserHeader>&& other )
        : m_file( std::move( other.m_file ) )
        , m_header( std::move( other.m_header ) )
        , m_mapping( std::move( other.m_mapping ) ) {}
    FileWithHeader<UserHeader>& operator=( const FileWithHeader<UserHeader>& ) = delete;
    FileWithHeader<UserHeader>& operator                                       =( FileWithHeader<UserHeader>&& other );
    /// return user header
    const UserHeader& userHeader() const noexcept { return m_header.m_userheader; }
    /// return user header
    UserHeader& userHeader() noexcept { return m_header.m_userheader; }
    /// return a pointer to the mmapped data section
    void* data() const noexcept { return m_mapping.data(); }
    /// return size of the data section
    uint64_t size() const noexcept { return m_mapping.size(); }
    /// resize the data section
    void resize( uint64_t newsz );
    void
    swap( FileWithHeader& other ) noexcept( noexcept( std::swap( std::declval<Header>(), std::declval<Header>() ) ) ) {
      std::swap( m_header, other.m_header );
      m_file.swap( other.m_file );
      m_mapping.swap( other.m_mapping );
    }
  };
  template <class UserHeader>
  void FileWithHeader<UserHeader>::flush() {
    // update header fields - this makes sense only if we keep the
    // file; if it's a temporary one, skip this step, as it's
    // expensive
    if ( Flags::ReadWriteTemp != m_file.flags() && Flags::ReadOnly < m_file.flags() ) {
      m_header.m_checksum_data   = fnv1a( m_mapping.data(), m_mapping.size() );
      m_header.m_checksum_header = fnv1a( &m_header, sizeof( Header ) - sizeof( uint64_t ) );
      m_mapping.msync();
      m_file.seek( 0, File::SeekBegin );
      m_file.write( &m_header, sizeof( Header ) );
    }
  }
  template <class UserHeader>
  FileWithHeader<UserHeader>::FileWithHeader( const char* fname, File::Flags flags )
      : m_file( fname, flags ), m_header{0, 0, 0, UserHeader(), 0} {
    auto pgsz   = pagesize();
    auto filesz = m_file.seek( 0, File::SeekEnd );
    m_file.seek( 0, File::SeekBegin );
    if ( filesz > 0 ) {
      // reading an existing file
      m_file.read( &m_header, sizeof( m_header ) );
      if ( m_header.m_checksum_header != fnv1a( &m_header, sizeof( Header ) - sizeof( uint64_t ) ) )
        throw std::runtime_error( "Header checksum invalid" );
      if ( ( filesz != ( m_header.m_datasz + pgsz ) ) || ( pgsz != m_header.m_offset ) )
        throw std::runtime_error( "Header contains invalid data" );
      m_mapping = m_file.map( m_header.m_offset, m_header.m_datasz );
      // check user header
      m_header.m_userheader.validate( *this );
      if ( Flags::SizeOnly != flags ) {
        // check data checksum
        if ( m_header.m_checksum_data != fnv1a( m_mapping.data(), m_mapping.size() ) )
          throw std::runtime_error( "Data section of file corrupt" );
      }
    } else {
      // new file, populate header, and write a skeleton
      // file...
      m_header = {pgsz, pgsz, fnv1a( nullptr, 0 ), UserHeader(), fnv1a( nullptr, 0 )};
      m_file.write( &m_header, sizeof( Header ) );
      m_file.truncate( 2 * pgsz );
      m_mapping = m_file.map( m_header.m_offset, m_header.m_datasz );
    }
  }
  template <typename UserHeader>
  FileWithHeader<UserHeader>& FileWithHeader<UserHeader>::operator=( FileWithHeader<UserHeader>&& other ) {
    if ( &other != this ) {
      if ( m_mapping.data() ) flush();
      m_mapping = std::move( other.m_mapping );
      m_header  = std::move( other.m_header );
      m_file    = std::move( other.m_file );
    }
    return *this;
  }
  template <typename UserHeader>
  void FileWithHeader<UserHeader>::resize( uint64_t newsz ) {
    auto oldsz = size();
    auto pgsz  = pagesize();
    if ( newsz < oldsz ) {
      m_mapping.resize( newsz );
      m_file.truncate( pgsz + newsz );
    } else if ( oldsz < newsz ) {
      m_file.truncate( pgsz + newsz );
      m_mapping.resize( newsz );
    }
    m_header.m_datasz = newsz;
  }
  /// swap to mappings
  inline void swap( Mapping& a, Mapping& b ) noexcept { a.swap( b ); }
  /// swap two file handles
  inline void swap( File& a, File& b ) noexcept { a.swap( b ); }
  /// swap two files
  template <typename UserHeader>
  inline void swap( FileWithHeader<UserHeader>& a, FileWithHeader<UserHeader>& b ) noexcept( noexcept( a.swap( b ) ) ) {
    a.swap( b );
  }
} // namespace MMapVector_impl

/** @brief file-backed std::vector workalike
 *
 * @date 2017-02-11
 * @author Manuel Schiller      <Manuel.Schiller@glasgow.ac.uk>
 *
 * The MMapVector class exposes a std::vector-like interface for all
 * operations except construction. Construction takes a file name and a mode,
 * much like fopen in the C standard library. The underlying file provides the
 * storage backing the MMapVector; it can be opened read-only and read-write.
 * By using a file to back the storage, the vector can grow larger than the
 * physical amount of RAM on the machine, leveraging the OS's VM subsystem.
 *
 * @tparam T    contained type
 *
 * @note PLEASE MAKE SURE THAT THE TYPE T CONTAINED IN THE MMAPVECTOR DOES NOT
 * CONTAIN POINTERS, REFERENCES OR ITERATORS! OPERATIONS THAT INVALIDATE
 * ITERATORS (push_back, insert, resize...) MAY MOVE THE UNDERLYING MEMORY
 * AREA AT WILL.
 */
template <typename T>
class MMapVector : public MMapVectorBase {
public:
  // start with the usual typedefs...
  typedef std::size_t    size_type;
  typedef std::ptrdiff_t difference_type;
  typedef T              value_type;
  typedef T*             pointer;
  typedef const T*       const_pointer;
  typedef T&             reference;
  typedef const T&       const_reference;

  typedef pointer                               iterator;
  typedef const_pointer                         const_iterator;
  typedef std::reverse_iterator<iterator>       reverse_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

  using MMapVectorBase::Flags;

private:
  // classic vector-like fields
  pointer   m_begin;    ///< pointer to underlying array
  size_type m_size;     ///< number of elements in array
  size_type m_capacity; ///< capacity of array

  /// helper class to encapsulate our on-disk information
  struct Header {
    size_type m_size;     ///< size of data on disk
    uint64_t  m_typehash; ///< hash of contained type name

    /// constructor
    Header() : m_size( 0 ) {
      auto type_name = typeid( T ).name();
      m_typehash     = MMapVector_impl::fnv1a( type_name, std::strlen( type_name ) );
    }
    // default operations for the rest
    Header( const Header& ) = default;
    Header( Header&& )      = default;
    Header& operator=( const Header& ) = default;
    Header& operator=( Header&& ) = default;
    ~Header()                     = default;
    /// validate our information against parent's
    template <typename P>
    void validate( const P& parent ) const {
      auto type_name = typeid( T ).name();
      if ( m_typehash != MMapVector_impl::fnv1a( type_name, std::strlen( type_name ) ) )
        throw std::runtime_error( "MMapVector<T> has "
                                  "incompatible type T in header of file" );
      if ( m_size > parent.size() )
        throw std::runtime_error( "Header size bigger than"
                                  "header capacity" );
    }
  };
  /// member taking care of the file backing this MMapVector
  MMapVector_impl::FileWithHeader<Header> m_file;
  /// reference to the header information
  Header& m_header;

  /// the dance needed to change the size of the mmapped area
  void change_capacity( size_type newcap ) {
    std::size_t newsz = newcap * sizeof( T );
    auto        pgsz  = MMapVector_impl::pagesize();
    if ( newsz & ( pgsz - 1 ) ) {
      // round up to next multiple of page size
      newsz += pgsz;
      newsz &= ~( pgsz - 1 );
    }
    // keep at least one page while we're running
    if ( newsz < pgsz ) newsz = pgsz;
    assert( newsz >= ( newcap * sizeof( T ) ) );
    m_file.resize( newsz );
    m_begin    = static_cast<pointer>( m_file.data() );
    m_capacity = newsz / sizeof( T );
    assert( newcap <= m_capacity );
  }

  /// destroy an element
  static void destroy( iterator it ) noexcept( noexcept( std::declval<T>().~T() ) ) { it->~T(); }
  /// destroy a range of elements
  static void destroy_range( iterator begin, iterator end ) noexcept( noexcept( std::declval<T>().~T() ) ) {
    for ( ; end != begin; ++begin ) begin->~T();
  }
  /// construct an element
  static iterator construct( iterator it, const_reference proto ) noexcept( noexcept( T( proto ) ) ) {
    return new ( it ) T( proto );
  }
  /// move-construct an element
  static iterator construct( iterator it, T&& proto ) noexcept( noexcept( T( std::move( proto ) ) ) ) {
    return new ( it ) T( std::move( proto ) );
  }
  /// construct an element in-place
  template <typename... ARGS>
  static iterator construct( iterator it, ARGS&&... args ) noexcept( noexcept( T( std::forward<ARGS>( args )... ) ) ) {
    return new ( it ) T( std::forward<ARGS>( args )... );
  }
  /// default-construct a range of elements
  static void construct_range( iterator begin, iterator end ) noexcept( noexcept( T() ) ) {
    for ( ; end != begin; ++begin ) new ( begin ) T();
  }
  /// construct a range of elements
  static void construct_range( iterator begin, iterator end,
                               const_reference proto ) noexcept( noexcept( T( proto ) ) ) {
    for ( ; end != begin; ++begin ) new ( begin ) T( proto );
  }
  /// construct a range of elements by copying from another range
  template <typename IT>
  static void construct_range( iterator begin, iterator end, IT protosrc ) noexcept( noexcept( T( *protosrc ) ) ) {
    for ( ; end != begin; ++begin, ++protosrc ) new ( begin ) T( *protosrc );
  }

public:
  /// constructor: give file name and options
  explicit MMapVector( const char* fname, Flags flags )
      : m_begin( nullptr ), m_size( 0 ), m_capacity( 0 ), m_file( fname, flags ), m_header( m_file.userHeader() ) {
    m_size     = m_header.m_size;
    m_begin    = static_cast<pointer>( m_file.data() );
    m_capacity = m_file.size() / sizeof( T );
  }
  /// get the size of the data saved in fname
  static size_type size( const char* fname ) {
    MMapVector_impl::FileWithHeader<Header> file( fname, Flags::SizeOnly );
    return file.size() / sizeof( T );
  }
  /// destructor
  ~MMapVector() {
    if ( m_file.data() ) {
      m_header.m_size = m_size;
      m_file.resize( sizeof( T ) * m_size );
    }
  }
  /// default constuctor
  MMapVector() : MMapVector( nullptr, ReadWriteTemp ) {}
  /// copy constructor (expensive!)
  MMapVector( const MMapVector<T>& other ) : MMapVector() { assign( other.begin(), other.end() ); }
  /// move constructor
  MMapVector( MMapVector<T>&& other )
      : m_begin( other.m_begin )
      , m_size( other.m_size )
      , m_capacity( other.m_capacity )
      , m_file( std::move( other.m_file ) )
      , m_header( m_file.userHeader() ) {
    other.m_begin = nullptr;
    other.m_size = other.m_capacity = 0;
  }
  /// constructor - default-construct count instances of T
  explicit MMapVector( size_type count ) : MMapVector() {
    reserve( count );
    construct_range( begin(), begin() + count );
    m_size = count;
  }
  /// constructor - construct count copies of val
  explicit MMapVector( size_type count, const_reference val ) : MMapVector() { assign( count, val ); }
  /// constructor from a range (first, last(
  template <typename IT>
  MMapVector( IT first, IT last ) : MMapVector() {
    assign( first, last );
  }
  /// constructor from an initializer_list
  MMapVector( std::initializer_list<T> ilist ) : MMapVector() { assign( ilist.begin(), ilist.end() ); }

  // assignment from initializer_list
  MMapVector<T>& operator=( std::initializer_list<T> ilist ) {
    assign( ilist.begin(), ilist.end() );
    return *this;
  }
  /// (copy) assignment (expensive!)
  MMapVector<T>& operator=( const MMapVector<T>& other ) {
    if ( &other != this ) assign( other.begin(), other.end() );
    return *this;
  }
  /// (move) assignment
  MMapVector<T>& operator=( MMapVector<T>&& other ) {
    if ( &other != this ) {
      if ( m_file.data() ) {
        m_header.m_size = m_size;
        m_file.resize( sizeof( T ) * m_size );
      }
      m_begin    = other.m_begin;
      m_size     = other.m_size;
      m_capacity = other.m_capacity;
      m_file     = std::move( other.m_file );
    }
    return *this;
  }

  /// assign count copies of val
  void assign( size_type count, const_reference val ) {
    clear();
    reserve( count );
    construct_range( begin(), begin() + count, val );
    m_size = count;
  }
  /// assign copies of elements in range (first, last(
  template <typename IT>
  void assign( IT first, IT last ) {
    clear();
    const auto count = std::distance( first, last );
    reserve( count );
    construct_range( begin(), begin() + count, first );
    m_size = count;
  }
  /// assign from initializer_list
  void assign( std::initializer_list<T> ilist ) { assign( ilist.begin(), ilist.end() ); }

  /// is vector empty?
  bool empty() const noexcept { return !m_size; }
  /// return size of vector
  size_type size() const noexcept { return m_size; }
  /// return capacity of vector
  size_type capacity() const noexcept { return m_capacity; }
  /// return maximum capacity of vector
  constexpr size_type max_size() const noexcept { return std::numeric_limits<size_type>::max() / sizeof( T ); }
  /// shrink on-disk capacity to size
  void shrink_to_fit() {
    if ( m_size < m_capacity ) change_capacity( m_size );
  }
  /// reserve capacity for at least newcap elements
  void reserve( size_type newcap ) {
    if ( newcap > m_capacity ) change_capacity( newcap );
  }

  // iterators: begin/end, cbegin/cend
  iterator       begin() noexcept { return m_begin; }
  const_iterator begin() const noexcept { return m_begin; }
  const_iterator cbegin() const noexcept { return m_begin; }
  iterator       end() noexcept { return m_begin + m_size; }
  const_iterator end() const noexcept { return m_begin + m_size; }
  const_iterator cend() const noexcept { return m_begin + m_size; }

  // reverse iterators: rbegin/rend, crbegin/crend
  reverse_iterator       rbegin() noexcept { return end(); }
  const_reverse_iterator rbegin() const noexcept { return end(); }
  const_reverse_iterator crbegin() const noexcept { return end(); }
  reverse_iterator       rend() noexcept { return begin(); }
  const_reverse_iterator rend() const noexcept { return begin(); }
  const_reverse_iterator crend() const noexcept { return begin(); }

  // access elements (no bounds checking)
  reference       operator[]( size_type idx ) noexcept { return *( m_begin + idx ); }
  const_reference operator[]( size_type idx ) const noexcept { return *( m_begin + idx ); }

  // access elements (throws std::out_of_range if out of bounds)
  reference at( size_type idx ) {
    if ( idx >= m_size ) throw std::out_of_range( __func__ );
    return *( m_begin + idx );
  }
  const_reference at( size_type idx ) const {
    if ( idx >= m_size ) throw std::out_of_range( __func__ );
    return *( m_begin + idx );
  }
  // access to first and last element
  reference       front() noexcept { return *begin(); }
  const_reference front() const noexcept { return *begin(); }
  reference       back() noexcept { return *( end() - 1 ); }
  const_reference back() const noexcept { return *( end() - 1 ); }

  // access to underlying array
  const T* data() const noexcept { return m_begin; }

  /// clear the vector
  void clear() noexcept( noexcept( destroy_range( begin(), end() ) ) ) {
    destroy_range( begin(), end() );
    m_size = 0;
  }

  /// resize the vector (default-construct new elements if needed)
  void resize( size_type newsz ) {
    reserve( newsz );
    if ( newsz > m_size ) {
      construct_range( end(), begin() + newsz );
    } else {
      destroy_range( begin() + newsz, end() );
    }
    m_size = newsz;
  }
  /// resize the vector (copy-construct new elements from proto)
  void resize( size_type newsz, const_reference proto ) {
    reserve( newsz );
    if ( newsz > m_size ) {
      construct_range( end(), begin() + newsz, proto );
    } else {
      destroy_range( begin() + newsz, end() );
    }
    m_size = newsz;
  }
  /// insert a new element at the back
  void push_back( const_reference value ) {
    reserve( m_size + 1 );
    construct( end(), value );
    ++m_size;
  }
  /// move a new element to the back
  void push_back( T&& value ) {
    reserve( m_size + 1 );
    construct( end(), std::move( value ) );
    ++m_size;
  }
  /// construct a new element at the back in-place
  template <typename... ARGS>
  reference emplace_back( ARGS&&... args ) {
    reserve( m_size + 1 );
    auto retVal = construct( end(), std::forward<ARGS>( args )... );
    ++m_size;
    return *retVal;
  }
  /// insert an element at position pos
  iterator insert( iterator pos, const_reference val ) {
    const auto idx = std::distance( begin(), pos );
    reserve( m_size + 1 );
    auto retVal = begin() + idx;
    if ( end() != retVal ) {
      construct( end() );
      std::move_backward( retVal, end(), end() + 1 );
      destroy( retVal );
    }
    construct( retVal, val );
    ++m_size;
    return retVal;
  }
  /// move an element into the vector at position pos
  iterator insert( iterator pos, T&& val ) {
    const auto idx = std::distance( begin(), pos );
    reserve( m_size + 1 );
    auto retVal = begin() + idx;
    if ( end() != retVal ) {
      construct( end() );
      std::move_backward( retVal, end(), end() + 1 );
      destroy( retVal );
    }
    construct( retVal, std::move( val ) );
    ++m_size;
    return retVal;
  }
  /// construct an element in-place at position pos
  template <typename... ARGS>
  iterator emplace( iterator pos, ARGS&&... args ) {
    const auto idx = std::distance( begin(), pos );
    reserve( m_size + 1 );
    auto retVal = begin() + idx;
    if ( end() != retVal ) {
      construct( end() );
      std::move_backward( retVal, end(), end() + 1 );
      destroy( retVal );
    }
    construct( retVal, std::forward<ARGS>( args )... );
    ++m_size;
    return retVal;
  }
  /// insert count copies of val at position pos
  iterator insert( iterator pos, size_type count, const_reference val ) {
    const auto idx = std::distance( begin(), pos );
    reserve( m_size + count );
    auto retVal = begin() + idx;
    if ( end() != retVal ) {
      construct_range( end(), end() + count );
      std::move_backward( retVal, end(), end() + count );
      destroy_range( retVal, retVal + count );
    }
    construct_range( retVal, retVal + count, val );
    m_size += count;
    return retVal;
  }
  /// insert at position pos copies of elements in range (first, last(
  template <typename IT>
  iterator insert( iterator pos, IT first, IT last ) {
    const auto idx   = std::distance( begin(), pos );
    auto       count = std::distance( first, last );
    reserve( m_size + count );
    auto retVal = begin() + idx;
    if ( end() != retVal ) {
      construct_range( end(), end() + count );
      std::move_backward( retVal, end(), end() + count );
      destroy_range( retVal, retVal + count );
    }
    construct_range( retVal, retVal + count, first );
    m_size += count;
    return retVal;
  }
  /// insert elements from initializer_list
  iterator insert( iterator pos, std::initializer_list<T> ilist ) { return insert( pos, ilist.begin(), ilist.end() ); }
  /// pop off the last element
  void pop_back() noexcept( noexcept( destroy( end() - 1 ) ) ) {
    destroy( end() - 1 );
    --m_size;
  }
  /// erase the element at position pos
  iterator erase( const_iterator pos ) noexcept( noexcept( destroy( std::move( pos + 1, end(),
                                                                               const_cast<iterator>( pos ) ) ) ) ) {
    destroy( std::move( pos + 1, end(), const_cast<iterator>( pos ) ) );
    --m_size;
    return const_cast<iterator>( pos );
  }
  /// erase the elements in range (first, last(
  iterator erase( const_iterator first, const_iterator last ) noexcept(
      noexcept( destroy_range( std::move( last, end(), const_cast<iterator>( first ) ), end() ) ) ) {
    destroy_range( std::move( last, end(), const_cast<iterator>( first ) ), end() );
    m_size -= std::distance( first, last );
    return const_cast<iterator>( first );
  }

  /// swap contents with another MMapVector<T>
  void swap( MMapVector<T>& other ) noexcept( noexcept( m_file.swap( other.m_file ) ) ) {
    using std::swap;
    m_file.swap( other.m_file );
    swap( m_begin, other.m_begin );
    swap( m_size, other.m_size );
    swap( m_capacity, other.m_capacity );
  }
};

/// swap contents of two MMapVector<T>
template <typename T>
void swap( MMapVector<T>& a, MMapVector<T>& b ) noexcept( noexcept( a.swap( b ) ) ) {
  a.swap( b );
}

/// equality of two MMapVector<T>
template <typename T>
bool operator==( const MMapVector<T>& a,
                 const MMapVector<T>& b ) noexcept( noexcept( std::declval<T>() == std::declval<T>() ) ) {
  if ( a.size() != b.size() ) return false;
  for ( auto it = a.begin(), end = a.end(), jt = b.begin(); end != it; ++it, ++jt ) {
    if ( !( *it == *jt ) ) return false;
  }
  return true;
}
/// inequality of two MMapVector<T>
template <typename T>
bool operator!=( const MMapVector<T>& a,
                 const MMapVector<T>& b ) noexcept( noexcept( std::declval<T>() == std::declval<T>() ) ) {
  return !operator==( a, b );
}
/// lexicographical < of two MMapVector<T>
template <typename T>
bool operator<( const MMapVector<T>& a,
                const MMapVector<T>& b ) noexcept( noexcept( std::lexicographical_compare( a.begin(), a.end(),
                                                                                           b.begin(), b.end() ) ) ) {
  return std::lexicographical_compare( a.begin(), a.end(), b.begin(), b.end() );
}
/// lexicographical > of two MMapVector<T>
template <typename T>
bool operator>( const MMapVector<T>& a,
                const MMapVector<T>& b ) noexcept( noexcept( std::lexicographical_compare( a.begin(), a.end(),
                                                                                           b.begin(), b.end() ) ) ) {
  return std::lexicographical_compare( b.begin(), b.end(), a.begin(), a.end() );
}
/// lexicographical <= of two MMapVector<T>
template <typename T>
bool operator<=( const MMapVector<T>& a,
                 const MMapVector<T>& b ) noexcept( noexcept( std::lexicographical_compare( a.begin(), a.end(),
                                                                                            b.begin(), b.end() ) ) ) {
  return !std::lexicographical_compare( b.begin(), b.end(), a.begin(), a.end() );
}
/// lexicographical >= of two MMapVector<T>
template <typename T>
bool operator>=( const MMapVector<T>& a,
                 const MMapVector<T>& b ) noexcept( noexcept( std::lexicographical_compare( a.begin(), a.end(),
                                                                                            b.begin(), b.end() ) ) ) {
  return !std::lexicographical_compare( a.begin(), a.end(), b.begin(), b.end() );
}

#endif // MMAPVECTOR_H

// vim: sw=4:tw=78:ft=cpp:et
