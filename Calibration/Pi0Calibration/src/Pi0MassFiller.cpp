/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * =====================================================================================
 *
 *       Filename:  Pi0MassFiller.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  11/21/2016 04:40:34 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 *   Organization:
 *
 * =====================================================================================
 */

#include <iostream>

// from ROOT
#include "TCanvas.h"
#include "TChain.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
// from local
#include "Pi0Calibration/Pi0MassFiller.h"
// from boost
#include <boost/filesystem.hpp>

using namespace boost;
using namespace boost::filesystem;
using namespace Calibration::Pi0Calibration;

Pi0MassFiller::Pi0MassFiller() { m_hists = new TH2D(); }

Pi0MassFiller::Pi0MassFiller( const std::string& filename ) {

  TFile* file = new TFile( filename.c_str() );
  m_hists     = (TH2D*)file->Get( "hists" );
  m_hists->SetDirectory( 0 );
  file->Close();
}