/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 *        Version:  1.0
 *        Created:  02/27/2017 05:05:55 PM
 *         Author:  Zhirui Xu (), Zhirui.Xu@cern.ch
 */
#include "Pi0Calibration/MMapVector.h"
#include "Pi0Calibration/Pi0CalibrationFile.h"
#include "Pi0Calibration/Pi0LambdaMap.h"
#include "Pi0Calibration/Pi0MassFiller.h"

#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include "CaloDet/DeCalorimeter.h"
#include "CaloKernel/CaloVector.h"
#include "Detector/Calo/CaloCellCode.h"
#include "Detector/Calo/CaloCellID.h"
#include "LHCbAlgs/Consumer.h"

#include "GaudiKernel/IDataProviderSvc.h"

#include "TFile.h"
#include "TH2.h"

#include <boost/filesystem.hpp>

#include <yaml-cpp/yaml.h>

#include <cstdint>
#include <iostream>
#include <map>
#include <string>
#include <utility>
#include <vector>

namespace {
  struct Conditions {
    std::vector<double>                       beta;
    std::vector<double>                       gamma;
    std::vector<unsigned int>                 indices;
    std::vector<LHCb::Detector::Calo::CellID> cells;
  };
} // namespace

class Pi0MMap2Histo
    : public LHCb::Algorithm::Consumer<void( Conditions const& ), LHCb::DetDesc::usesConditions<Conditions>> {
public:
  Pi0MMap2Histo( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  void       operator()( Conditions const& ) const override;

private:
  Gaudi::Property<std::vector<std::string>> m_filenames{this, "filenames", {}};
  Gaudi::Property<std::string>              m_outputDir{this, "outputDir", ""};
  Gaudi::Property<std::string>              m_outputName{this, "outputName", ""};
  Gaudi::Property<unsigned int>             m_nworker{this, "nworker", 0};
  Gaudi::Property<std::string>              m_lambdaFileName{this, "lambdaFileName", ""};
};

Pi0MMap2Histo::Pi0MMap2Histo( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, {"Conditions", name + "_Conditions"} ) {}

StatusCode Pi0MMap2Histo::initialize() {
  info() << "Pi0MMap2Histo : INITIALIZE  " << endmsg;

  return Consumer::initialize();
}

using namespace Calibration::Pi0Calibration;

DECLARE_COMPONENT( Pi0MMap2Histo )

void Pi0MMap2Histo::operator()( Conditions const& conds ) const {
  info() << "Pi0MMap2Histo : START  " << endmsg;
  std::vector<std::vector<unsigned int>> subtasks( m_nworker );
  for ( auto& c : subtasks ) c.reserve( conds.indices.size() / m_nworker );
  info() << "total cells to be processed by each worker " << conds.indices.size() / m_nworker << endmsg;

  int i = 0;
  for ( auto index : conds.indices ) subtasks[i++ % m_nworker].push_back( index );

  auto min_max = std::minmax_element( conds.indices.begin(), conds.indices.end() );
  int  xmax    = min_max.second - conds.indices.begin();

  Pi0LambdaMap lambdamap( conds.indices );
  if ( boost::filesystem::exists( m_lambdaFileName.value() ) ) {
    info() << "the lambda files used comes from : " << m_lambdaFileName.value() << endmsg;
    lambdamap.loadMapFromFile( m_lambdaFileName );
  } else {
    info() << "Lambda files not found in  : " << m_lambdaFileName.value() << endmsg;
  }

  int iwoker = 1;
  for ( auto& subtask : subtasks ) {

    info() << " start worker " << iwoker << " for the following indices: " << endmsg;

    auto sel = Pi0CalibrationFile::make_indices_selector( subtask );

    info() << "make_indices_selector called successfully " << endmsg;

    info() << " a list of file names " << endmsg;

    for ( auto& filename : m_filenames ) info() << "  file : " << filename << endmsg;

    boost::filesystem::path outfilename( m_outputDir + "/" + m_outputName );

    TFile* fout  = new TFile( outfilename.c_str(), "RECREATE" );
    TH2D*  hists = new TH2D( "hists", "hists", conds.indices.at( xmax ), 0, conds.indices.at( xmax ), 100, 0.0, 250.0 );
    info() << "test of limits : " << conds.indices.at( xmax ) << endmsg;

    for ( auto& filename : m_filenames ) {
      if ( !boost::filesystem::exists( filename ) ) {
        err() << "file not exist! " << filename << endmsg;
        continue;
      }

      info() << "start to read file from " << filename << endmsg;
      MMapVector<Candidate> vorigin( filename.c_str(), MMapVector<Candidate>::ReadOnly );

      info() << "open the file successfully with " << vorigin.size() << " entries " << endmsg;
      for ( auto& c : vorigin ) {
        auto ind1 = c.ind1;
        auto ind2 = c.ind2;
        auto m12  = c.m12;

        // auto scale = Pi0MassFiller::scale_factor( c, lambdamap, &conds.beta, &conds.gamma );
        auto scale = Pi0MassFiller::scale_factor( c, lambdamap );
        // ind1 = ind1-32768;
        // ind2 = ind2-32768;

        // select the cells to fill in the histograms
        if ( sel( ind1 ) ) { hists->Fill( ind1, m12 * scale ); }
        if ( sel( ind2 ) ) { hists->Fill( ind2, m12 * scale ); }
      }

      info() << "finished reading file " << filename << endmsg;
    } // all input files
    hists->Write();
    fout->Close();

    info() << "saved histograms for worker " << iwoker << endmsg;

    iwoker++;
  } // all the workers
}
