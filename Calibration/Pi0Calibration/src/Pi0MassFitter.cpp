/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/*
 * =====================================================================================
 *
 *       Filename:  Pi0MassFitter.cpp
 *
 *    Description:
 *
 *        Version:  1.0
 *        Created:  11/21/2016 04:40:34 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 *   Organization:
 *
 * =====================================================================================
 */

// from local
#include "Pi0Calibration/Pi0MassFitter.h"
#include "RooBernstein.h"

// from boost
#include <boost/filesystem.hpp>

using namespace boost;
using namespace boost::filesystem;
using namespace RooFit;
using namespace Calibration::Pi0Calibration;

// const unsigned int BitsCol   = 6;
// const unsigned int BitsRow   = 6;
// const unsigned int BitsArea  = 2;
// const unsigned int ShiftCol  = 0;
// const unsigned int ShiftRow  = ShiftCol + BitsCol;
// const unsigned int ShiftArea = ShiftRow + BitsRow;
// const unsigned int MaskArea  = ( ( ( (unsigned int)1 ) << BitsArea ) - 1 ) << ShiftArea;

Pi0MassFitter::Pi0MassFitter( TH1* hist, double mpi0, double sigma ) {
  m_hist   = hist;
  m_mpi0   = mpi0;
  m_sigma0 = sigma;

  m_mean    = std::make_pair( 0., 0. );
  m_sigma   = std::make_pair( 0., 0. );
  m_nsignal = std::make_pair( 0., 0. );
  m_status  = -1;
  covQual   = -1;
}

void Pi0MassFitter::chi2fit( const std::string& outputdir, const std::string& outfilename, bool verbose ) {
  std::string subdir = "good";

  gErrorIgnoreLevel = kError;
  gErrorIgnoreLevel = kInfo;
  RooFit::PrintLevel( 0 );
  RooFit::Warnings( 0 );
  RooFit::Verbose( 0 );
  RooMsgService::instance().setGlobalKillBelow( RooFit::WARNING );
  RooMsgService::instance().setGlobalKillBelow( RooFit::DEBUG );
  RooMsgService::instance().setGlobalKillBelow( RooFit::INFO );
  RooMsgService::instance().setGlobalKillBelow( RooFit::PROGRESS );
  RooMsgService::instance().setGlobalKillBelow( RooFit::ERROR );
  RooMsgService::instance().setGlobalKillBelow( RooFit::FATAL );

  std::string  hist_name = m_hist->GetName();
  unsigned int cellID    = (unsigned int)atoi(
      ( hist_name.substr( hist_name.find( "_ID" ) + 3, hist_name.length() )
            .substr( 0, hist_name.substr( hist_name.find( "_ID" ) + 3, hist_name.length() ).find( "_" ) ) )
          .c_str() );

  RooRealVar mass( "pi0Mass", "", 50., 250. );
  RooRealVar Nsig( "Nsig", "", m_hist->GetEntries() * 0.5, 0., m_hist->GetEntries() );
  RooRealVar Nbkg( "Nbkg", "", m_hist->GetEntries() * 0.5, 0., m_hist->GetEntries() );

  RooRealVar  mean( "mean", "pi0Mass", 135, 90., 170. );
  RooRealVar  sigma( "sigma", "", 9., 1., 30. );
  RooGaussian sig( "Signal", "Signal", mass, mean, sigma );

  RooRealVar    c0( "c0", "", 1., -10, 10 );
  RooRealVar    c1( "c1", "", 1., -10, 10 );
  RooRealVar    c2( "c2", "", 1., -10, 10 );
  RooPolynomial bkg( "Background", "Background", mass, RooArgList( c0, c1, c2 ) );
  //  RooBernstein bkg( "Background", "Background", mass, RooArgList( c0, c1, c2 ) );

  RooAddPdf total( "totalPDF", "", RooArgList( sig, bkg ), RooArgList( Nsig, Nbkg ) );

  if ( m_hist->GetEntries() < 100 ) {
    subdir = "nofit";
    return;
  }

  bool        isGoodFit = false;
  RooDataHist hMass( "hMass", "hMass", mass, Import( *m_hist ) );

  mass.setRange( "low1", 50, 100 );
  mass.setRange( "low2", 0, 100 );
  mass.setRange( "high", 190, 220 );
  RooMinimizer bkgOnly1( *bkg.createNLL( hMass, Extended( true ), Range( "low1", "high" ) ) );
  RooMinimizer bkgOnly2( *bkg.createNLL( hMass, Extended( true ), Range( "low2", "high" ) ) );

  RooMinimizer minimizer( *total.createNLL( hMass, Extended( true ), Range( 70., 190. ) ) );

  minimizer.setVerbose( false );
  minimizer.setPrintLevel( -1 );
  minimizer.setStrategy( 1 );
  minimizer.setMaxFunctionCalls( 10000 );
  minimizer.setEvalErrorWall( true );
  minimizer.minimize( "Minuit2", "Migrad" );

  RooFitResult* backgroundFitResult = bkgOnly1.save();

  const RooArgList& backgroundParams = backgroundFitResult->floatParsFinal();

  c0.setVal( dynamic_cast<RooRealVar*>( backgroundParams.find( "c0" ) )->getVal() );
  c1.setVal( dynamic_cast<RooRealVar*>( backgroundParams.find( "c1" ) )->getVal() );
  c2.setVal( dynamic_cast<RooRealVar*>( backgroundParams.find( "c2" ) )->getVal() );
  // c0.setConstant();
  // c1.setConstant();
  // c2.setConstant();

  RooFitResult* fitResult = minimizer.save();
  int           i         = 0;
  if ( fitResult->covQual() != 3 ) {

    while ( fitResult->covQual() != 3 ) {
      if ( i == 100 ) break;

      fitResult->randomizePars();
      minimizer.setStrategy( 1 );
      minimizer.minimize( "Minuit2", "Migrad" );
      i++;
    }
  }

  if ( ( fitResult->status() == 0 || fitResult->status() == 1 ) && ( fitResult->covQual() == 3 ) &&
       ( mean.getVal() > 105. && mean.getVal() < 155. && ( sigma.getVal() > 4. && sigma.getVal() < 25. ) ) ) {
    isGoodFit = true;
  } else if ( ( sigma.getVal() > 4. && sigma.getVal() < 20. ) ) {
    isGoodFit = true;
  }

  if ( isGoodFit == false ) {
    RooFitResult*     backgroundFitResult = bkgOnly2.save();
    const RooArgList& backgroundParams    = backgroundFitResult->floatParsFinal();

    c0.setVal( dynamic_cast<RooRealVar*>( backgroundParams.find( "c0" ) )->getVal() );
    c1.setVal( dynamic_cast<RooRealVar*>( backgroundParams.find( "c1" ) )->getVal() );
    c2.setVal( dynamic_cast<RooRealVar*>( backgroundParams.find( "c2" ) )->getVal() );

    RooFitResult* fitResult = minimizer.save();

    if ( fitResult->covQual() != 3 ) {
      i = 0;
      while ( fitResult->covQual() != 3 ) {
        if ( i == 100 ) break;

        fitResult->randomizePars();
        minimizer.setStrategy( 1 );
        minimizer.minimize( "Minuit2", "Migrad" );
        i++;
      }
    }
    if ( ( fitResult->status() == 0 || fitResult->status() == 1 ) && ( fitResult->covQual() == 3 ) &&
         ( mean.getVal() > 105. && mean.getVal() < 155. && ( sigma.getVal() > 4. && sigma.getVal() < 25. ) ) ) {
      isGoodFit = true;
    } else if ( ( sigma.getVal() > 4. && sigma.getVal() < 20. ) ) {
      isGoodFit = true;
    }
  }

  m_mean  = std::make_pair( mean.getVal(), mean.getError() );
  m_sigma = std::make_pair( sigma.getVal(), sigma.getError() );
  mass.setRange( "twoSigma", 50., 250. );

  double nS  = Nsig.getVal();
  double nSE = Nsig.getError();
  m_nsignal  = std::make_pair( nS, nSE );

  if ( isGoodFit )
    m_status = 0;
  else {
    verbose  = true;
    subdir   = "bad";
    m_status = -1;
  }

  if ( verbose ) {
    gStyle->SetOptFit( 1112 );
    gStyle->SetStatFormat( "6.2g" );
    gStyle->SetStatStyle( 4001 );
    TCanvas* cc = new TCanvas();

    auto mass_frame = mass.frame( 50., 250., 100 );
    hMass.plotOn( mass_frame, Name( "dataset" ) );
    total.plotOn( mass_frame, Name( "Signal" ), LineColor( 2 ) );
    total.plotOn( mass_frame, Components( "Background" ), Name( "Background" ), LineColor( kGreen + 3 ),
                  LineStyle( kDashed ) );
    total.paramOn( mass_frame );
    mass_frame->SetTitle( Form( " %s fit , cellID = %i", subdir.c_str(), cellID ) );
    gPad->SetLogz( true );
    mass_frame->Draw();
    if ( !exists( outputdir ) ) create_directories( outputdir );
    if ( !exists( outputdir + "/" + subdir ) ) create_directories( outputdir + "/" + subdir );
    boost::filesystem::path outfile( outputdir + "/" + subdir + "/" + outfilename );

    cc->SaveAs( outfile.c_str() );
    cc->Close();
  }
}
