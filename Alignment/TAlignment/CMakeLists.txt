###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Alignment/TAlignment
--------------------
#]=======================================================================]

gaudi_add_library(TAlignmentLib
    SOURCES
        src/AlignAlgorithmBase.cpp
        src/AlignAlgorithmHelper.cpp
        src/AlignmentElement.cpp
        src/AlParameters.cpp
        src/GetElementsToBeAligned.cpp
        src/XmlWriter.cpp
	src/TrackResidualHelper.cpp
    LINK
        PUBLIC
            Alignment::AlignKernel
            Alignment::AlignmentInterfacesLib
            Boost::headers
            Boost::regex
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::DAQEventLib
            LHCb::DetDescLib
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::PhysEvent
            LHCb::RecEvent
            LHCb::TrackEvent
            Rec::TrackInterfacesLib
            ROOT::GenVector
            ROOT::MathCore
        PRIVATE
            fmt::fmt
            Gaudi::GaudiUtilsLib
            LHCb::FTDetLib
            LHCb::MuonDetLib
            LHCb::UTDetLib
            LHCb::VPDetLib
            Rec::PrKalmanFilterLib
            Rec::TrackFitEvent
            Rec::TrackKernel
            ROOT::Hist
)

if(USE_DD4HEP)
    target_link_libraries(TAlignmentLib PUBLIC DD4hep::DDCore LHCb::LbDD4hepLib)
endif()

gaudi_add_module(TAlignment
    SOURCES
        src/AlignAlgorithm.cpp
        src/AlignOnlineIterator.cpp
        src/AlignOnlineYMLCopier.cpp
        src/AlignConstraints.cpp
	src/AlignJointConstraints.cpp
	src/AlignSurveyConstraints.cpp
        src/AlignUpdateTool.cpp
        src/CountingPrescaler.cpp
        src/VertexResidualTool.cpp
    LINK
        Alignment::AlignKernel
        Alignment::AlignmentInterfacesLib
        Boost::headers
        Boost::regex
        fmt::fmt
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        Gaudi::GaudiUtilsLib
        LHCb::DetDescLib
        LHCb::LHCbAlgsLib
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::RecEvent
        Rec::PrKalmanFilterLib
        Rec::TrackFitEvent
        Rec::TrackInterfacesLib
        Rec::TrackKernel
        ROOT::Hist
        ROOT::MathCore
        TAlignmentLib
        AlignmentMonitoringLib
)

gaudi_install(PYTHON)
