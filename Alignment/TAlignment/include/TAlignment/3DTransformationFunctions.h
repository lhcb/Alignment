/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Math/RotationZYX.h"
#include "Math/SMatrix.h"
#include "Math/Transform3D.h"
#include "Math/Translation3D.h"
#include <array>

namespace LHCb::Alignment {

  template <typename Iterator, typename PivotParams>
  void getZYXTransformParameters( const ROOT::Math::Transform3D& CDM, Iterator& translationParams,
                                  Iterator& rotationParams, const PivotParams& pivotParams ) {

    // This is the definition of the transform: (See the routine above).
    //
    //   A_tot    =   A_trans  * A_pivot  * A_rot * A_pivot^-1
    //
    // Note that 'A_trans' and 'A_pivot' are transforms that contain
    // only a translation, while A_rot applies exclusively a
    // rotation. We now need to compute A_trans and A_rot for given
    // A_tot and A_pivot.

    // Extracting the rotation is simple: Since there is only a single
    // rotation and translations do not change rotations, it must be
    // equal to the rotation part of the total transform:
    const ROOT::Math::RotationZYX newRot = CDM.Rotation<ROOT::Math::RotationZYX>();

    // To compute A_trans we now first create a pivot point transform
    const ROOT::Math::Translation3D pivotTrans = ROOT::Math::Translation3D( pivotParams.begin(), pivotParams.end() );

    // we then create ' A_pivot * A_rot * A_pivot^-1 ', so the term on
    // the right hand side in the equation above. (If you are worried
    // about precision, it would be better to compute directly its
    // inverse, because that is what we need below.)
    const ROOT::Math::Transform3D pivotRot = pivotTrans * newRot * pivotTrans.Inverse();

    // we then construct A_trans by multiplying A_tot on the right side
    // with the inverse of this thing. note that the result should not
    // have a rotation part anymore: it should be exclusively translation.
    const ROOT::Math::Translation3D newTrans = ( CDM * pivotRot.Inverse() ).Translation();

    // finally we extract the 6 parameters
    newRot.GetComponents( rotationParams[2], rotationParams[1], rotationParams[0] );
    newTrans.GetComponents( translationParams[0], translationParams[1], translationParams[2] );
  }

  template <typename TranslationParams, typename RotationParams, typename PivotParams>
  void getZYXTransformParameters( const ROOT::Math::Transform3D& CDM, TranslationParams& translationParams,
                                  RotationParams& rotationParams, const PivotParams& pivotParams ) {
    getZYXTransformParameters( CDM, std::begin( translationParams ), std::begin( rotationParams ), pivotParams );
  }

  template <typename TranslationParams, typename RotationParams>
  void getZYXTransformParameters( const ROOT::Math::Transform3D& CDM, TranslationParams& translationParams,
                                  RotationParams& rotationParams ) {
    getZYXTransformParameters( CDM, translationParams, rotationParams, std::array<double, 3>{0, 0, 0} );
  }

  template <typename TransformParams>
  void getZYXTransformParameters( const ROOT::Math::Transform3D& CDM, TransformParams& transformparams ) {
    auto trans = std::begin( transformparams );
    auto rot   = trans;
    std::advance( rot, 3 );
    getZYXTransformParameters( CDM, trans, rot );
  }

} // namespace LHCb::Alignment
