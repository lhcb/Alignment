/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/StatusCode.h"

#include "boost/algorithm/string.hpp"

#include <charconv>
#include <stdexcept>

namespace LHCb::Alignment {

  /// small struct holding the description of an XmlWriter
  struct XmlWriter {
    std::string               outputFileName;
    std::string               topElement;
    unsigned int              precision;
    std::vector<unsigned int> depths;
    bool                      online;
    bool                      removePivot;
  };

  /// stream an XmlWriter
  std::ostream& operator<<( std::ostream& s, const XmlWriter& w );

  /// parse string to vector of XmlWriters
  StatusCode parse( std::vector<XmlWriter>& w, const std::string& str );

  /// stream a vector of XmlWriter
  std::ostream& toStream( const std::vector<XmlWriter>& v, std::ostream& s );

  /// Exception raised when parsing an XmlWriter property
  struct InvalidXmlWriterString : std::logic_error {
    using logic_error::logic_error;
  };
} // namespace LHCb::Alignment
