/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "AlResiduals.h"

#include "DetDesc/IGeometryInfo.h"

#include "Event/Track.h"

#include "GaudiKernel/IAlgTool.h"

namespace LHCb {
  class RecVertex;
  class Particle;
} // namespace LHCb

namespace LHCb::Alignment {
  class TrackResidualHelper;
  class GetElementsToBeAligned;

  struct IVertexResidualTool : public extend_interfaces<IAlgTool> {
    DeclareInterfaceID( IVertexResidualTool, 2, 0 );

    using ReturnType = std::unique_ptr<const MultiTrackResiduals>;

    // process a vertex (primary vertex of twoprongvertex)
    virtual ReturnType get( const RecVertex& vertex, TrackResidualHelper& trackResidualHelper,
                            const GetElementsToBeAligned& elementTool, IGeometryInfo const& geometry ) const = 0;
    // process a particle. this applies a mass constraint.
    virtual ReturnType get( const Particle& p, TrackResidualHelper& trackResidualHelper,
                            const GetElementsToBeAligned& elementTool, IGeometryInfo const& geometry ) const = 0;
  };
} // namespace LHCb::Alignment
