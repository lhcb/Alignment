/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/Track.h"

#include "TAlignment/AlResiduals.h"
//#include "TAlignment/GetElementsToBeAligned.h"

#include <map>
#include <vector>
class ITrackProjectorSelector;

namespace LHCb::Alignment {
  class TrackResiduals;
  class GetElementsToBeAligned;

  class TrackResidualHelper {
  public:
    // get the residuals for this track
    TrackResidualHelper( const ITrackProjectorSelector& p ) : m_projectors{&p} {}
    const TrackResiduals* get( const LHCb::Track& track, const GetElementsToBeAligned& elementTool,
                               MsgStream& warnLog );

  private:
    const ITrackProjectorSelector*                                m_projectors;
    std::map<const LHCb::Track*, std::unique_ptr<TrackResiduals>> m_residuals;
  };
} // namespace LHCb::Alignment
