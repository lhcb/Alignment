/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <cstddef>
#include <string>

class IAlgorithm;
class IAlgTool;

namespace LHCb::Alignment::Helper {

  /**
   * helper method for python interface
   * Basically dynamic_casting the given algorithm into AlignAlgorithm and
   * if successful calling updateGeometry on it.
   */
  void updateGeometry( IAlgorithm* alg, size_t iter, size_t maxiter );

  /**
   * helper method for python interface
   * Basically dynamic_casting the given algorithm into AlignAlgorithm and
   * if successful calling clearEquationsAndReloadGeometry on it.
   * Also reset the MeasurementProvider tool as it internally caches some
   * geometry dependent data
   */
  void clearEquationsAndReloadGeometry( IAlgorithm* alg );

/**
 * helper method for python interface
 * Basically dynamic_casting the given algorithm into AlignAlgorithm and
 * if successful calling writeAlignmentConditions on it.
 */
#ifdef USE_DD4HEP
  void writeAlignmentConditions( IAlgorithm* alg, std::string out_dir );
  void loadAlignmentConditions( IAlgorithm* alg, std::string in_dir );
#else

  void writeAlignmentConditions( IAlgorithm* alg );

#endif

} // namespace LHCb::Alignment::Helper
