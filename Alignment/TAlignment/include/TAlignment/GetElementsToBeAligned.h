/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "AlignKernel/AlEquations.h"
#include "TAlignment/AlignmentElement.h"
#include "TAlignment/XmlWriter.h"

#include "Kernel/LHCbID.h"
#include "Kernel/STLExtensions.h"

#ifdef USE_DD4HEP
#  include <DD4hep/GrammarUnparsed.h>
#endif

#include <map>
#include <string>
#include <vector>

/** @class GetElementsToBeAligned GetElementsToBeAligned.h
 *
 *
 *  @author Jan Amoraal
 *  @date   2007-10-08
 */

namespace LHCb {
  class FitNode;
  namespace Pr::Tracks::Fit {
    struct Node;
  }
} // namespace LHCb

namespace LHCb::Alignment {

  class GetElementsToBeAligned {
  public:
    GetElementsToBeAligned( LHCb::span<const std::string> elemsToBeAligned, const DetectorElement& topOfGeometry,
                            const Gaudi::Time inittime, MsgStream& warningStream );
    GetElementsToBeAligned( const GetElementsToBeAligned& ) = delete;
    GetElementsToBeAligned( GetElementsToBeAligned&& )      = default;
    GetElementsToBeAligned& operator=( const GetElementsToBeAligned& ) = delete;
    GetElementsToBeAligned& operator=( GetElementsToBeAligned&& ) = default;
    ~GetElementsToBeAligned();

    // Return pair of begin iter and end iter
    const Elements& elements() const { return m_elements; }

    // Return method that finds an alignment element for a given Measuerment
    const Element* findElement( const LHCb::FitNode& node ) const;
    const Element* findElement( const LHCb::Pr::Tracks::Fit::Node& node ) const;
    const Element* findElement( const LHCb::LHCbID id ) const;

    // Find the list of elements corresponding to a path (which can be a regular expression)
    Elements findElements( const std::string& path ) const;

    // initialize the alignment frames now. if time=0, it will use the
    // current time.
    void initAlignmentFrame();

    // initialize an Equations object
    void initEquations( Equations& ) const;

    /// Write alignment conditions to file
    void writeAlignmentConditions( const std::vector<XmlWriter>& xmlWriters, MsgStream& warningStream ) const;

    std::map<std::string, double> getAlignmentConstants() const;

    /// Return the IOV for the cached geometry
    auto iov() const { return m_iov; }
    //
    bool isInitialized() const { return m_isInitialized; }
    void resetInitialized() { m_isInitialized = false; }
    /// Update the cached geometry
    void update_iov( const DetectorElement& new_iov, MsgStream& msg );

  private:
    enum e_DoFs { Tx, Ty, Tz, Rx, Ry, Rz };

    void            writeAlignmentConditions( const XmlWriter& writer, MsgStream& warningStream ) const;
    DetectorElement getDet( const DetectorElement& top, std::string const& path ) const;

  private:
    DetectorElement                 m_topOfGeometry; /// Current top of geometry
    Elements                        m_elements;      /// Flat vector of alignment elements
    std::array<const Element*, 208> m_vpidmap{};     // map from vp sensor id to alignable
    std::map<int, const Element*> m_utidmap; // map from ut sensor id to alignable FIXME: invent new unique UT ID, then
                                             // replace by array
    std::array<const Element*, 258 * 4>                             m_ftidmap{};   // map from ft module id to alignable
    std::map<std::pair<unsigned int, unsigned int>, const Element*> m_muonregions; // Map of Muon detector elements to
                                                                                   // align 4Stations x 2sides

    bool        m_isInitialized{false};
    size_t      m_iov{0};
    Gaudi::Time m_inittime;
  };

} // namespace LHCb::Alignment
