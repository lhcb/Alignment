/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "AlignConstraints.h"

#include "TAlignment/AlignmentElement.h"
#include "TAlignment/GetElementsToBeAligned.h"

#include "LHCbMath/LHCbMath.h"

#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Transform3DTypes.h"

#include "boost/assign/list_of.hpp"
#include "boost/tokenizer.hpp"

#include "fmt/format.h"

namespace {

  std::string removechars( const std::string& s, const std::string& chars ) {
    std::string rc;
    for ( std::string::const_iterator it = s.begin(); it != s.end(); ++it )
      if ( std::find( chars.begin(), chars.end(), *it ) == chars.end() ) rc.push_back( *it );
    return rc;
  }

  std::vector<std::string> tokenize( const std::string& s, const char* separator ) {
    typedef boost::char_separator<char> Separator;
    typedef boost::tokenizer<Separator> Tokenizer;
    Tokenizer                           split( s, Separator( separator ) );
    std::vector<std::string>            rc;
    rc.insert( rc.end(), split.begin(), split.end() );
    return rc;
  }

  LHCb::Alignment::EConstraintType index( std::string_view s ) {
    LHCb::Alignment::EConstraintType v;
    parse( v, s ).ignore();
    return v;
  }

  bool check( const std::vector<std::string>& names ) {
    for ( const auto& name : names ) {
      auto constraint = index( name );
      if ( constraint == LHCb::Alignment::EConstraintType::Unknown ) {
        std::cout << "Warning: unknown constraint " << name << std::endl;
        return false;
      }
    }
    return true;
  }

  constexpr size_t NumConstraintTypes = LHCb::Alignment::EConstraintType_internal_size() - 1; // do not count 'Unknown'

} // namespace

namespace LHCb::Alignment {

  ConstraintDerivatives::ConstraintDerivatives( size_t dim, const std::vector<std::string>& activeconstraints,
                                                const std::string& nameprefix, int offset )
      : m_dofmask{NumConstraintTypes}
      , m_derivatives{NumConstraintTypes, dim}
      , m_residuals{NumConstraintTypes}
      , m_nameprefix( nameprefix )
      , m_activeParOffset( offset ) {
    std::vector<bool> active( NumConstraintTypes, false );
    if ( check( activeconstraints ) ) {
      for ( const auto& name : activeconstraints ) { active[static_cast<int>( index( name ) )] = true; }
    }
    m_dofmask = AlDofMask( active );
  }

  AlignConstraints::AlignConstraints( const GetElementsToBeAligned& elementProvider,
                                      LHCb::span<const std::string> constraintNames, bool useWeightedAverage,
                                      MsgStream& log )
      : m_useWeightedAverage( useWeightedAverage ) {
    // now we need to decode the string with constraints
    ConstraintDefinition common( "" );
    common.addElements( elementProvider.elements() );
    m_definitions.push_back( common );
    for ( const auto& constraintName : constraintNames ) {
      // tokenize
      std::vector<std::string> tokens = tokenize( constraintName, ":" );
      if ( tokens.size() == 1 ) {
        // this is a single or a set of common constraints
        std::vector<std::string> dofs = tokenize( constraintName, " ," );
        for ( const auto& dof : dofs ) m_definitions.front().addDof( removechars( dof, " ," ) );
      } else if ( tokens.size() == 3 || tokens.size() == 4 ) {
        ConstraintDefinition newconstraint( tokens.at( 0 ) );
        auto                 elements = elementProvider.findElements( removechars( tokens.at( 1 ), " " ) );
        newconstraint.addElements( elements );
        std::vector<std::string> dofs = tokenize( tokens.at( 2 ), " ," );
        for ( const auto& dof : dofs ) newconstraint.addDof( removechars( dof, " ," ) );
        if ( tokens.size() == 4 ) {
          std::string modestr = removechars( tokens.at( 3 ), " " );
          std::transform( modestr.begin(), modestr.end(), modestr.begin(), tolower );
          if ( modestr == "total" )
            newconstraint.mode = ConstraintMode::Total;
          else if ( modestr == "delta" )
            newconstraint.mode = ConstraintMode::Delta;
          else if ( modestr == "survey" )
            newconstraint.mode = ConstraintMode::Survey;
          else {
            throw GaudiException(
                "AlignConstraints",
                format( "Unknown mode in constraint definition: %s | '%s'", constraintName.c_str(), modestr.c_str() ),
                StatusCode::FAILURE );
          }
        }
        m_definitions.push_back( newconstraint );
      }
    }

    int nactive( 0 );
    for ( const auto& constraint : m_definitions ) {
      if ( !check( constraint.dofs ) ) {
        throw GaudiException( "AlignConstraints", "Constraint list contains unknown constraints!",
                              StatusCode::FAILURE );
      } else {
        nactive += constraint.dofs.size();
        log << "Constraint definition: " << constraint.name << " "
            << "dofs = ";
        for ( const auto& dof : constraint.dofs ) log << dof << " , ";
        log << "num elements = " << constraint.elements.size() << endmsg;
      }
    }
    log << "Number of constraint definitions= " << m_definitions.size()
        << ",  number of active constraints = " << nactive << endmsg;
  }

  ConstraintDerivatives AlignConstraints::createConstraintDerivatives( const ConstraintDefinition& c,
                                                                       const Equations&            equations,
                                                                       const SurveyData&           surveydata,
                                                                       size_t numAlignPars, MsgStream& log ) const {
    // This adds lagrange multipliers to constrain the average rotation
    // and translation. Ideally, we could calculate this in any
    // frame. In practise, the average depends on the reference frame in
    // which is is calculated. We will calculate a single 'pivot' point
    // to define the transform to the frame in which we apply the
    // constraint.

    // To make the bookkeeping easy, we add all possible constraints and
    // then 'disable' those we don't need with the 'dofmask'.

    // If there is only a single element, then we use its alignment
    // frame. In that case we can also not compute shearings, so we
    // leave zmin.max and xmin.max invalid. If there is more than one
    // element, we use the average center as the frame.
    double zmin( 9999999 ), zmax( -999999 ), xmin( 9999999 ), xmax( -999999 ), weight( 1 );
    bool   useWeightedAverage = false;
    if ( c.elements.size() != 1 ) {
      // FIXME: it should be possible to do this better. we could for example
      // take the frame from the parent. see Element.
      double                weight( 0 );
      ROOT::Math::XYZVector pivot;
      useWeightedAverage = m_useWeightedAverage;
      for ( const auto& element : c.elements ) {
        if ( element->activeParOffset() >= 0 ) {
          size_t elemindex  = element->index();
          double thisweight = useWeightedAverage ? equations.element( elemindex ).weightV() : 1;
          weight += thisweight;
          ROOT::Math::XYZPoint cog = element->centerOfGravity();
          pivot += thisweight * ROOT::Math::XYZVector( cog );
          zmin = std::min( cog.z(), zmin );
          zmax = std::max( cog.z(), zmax );
          xmin = std::min( cog.x(), xmin );
          xmax = std::max( cog.x(), xmax );
        }
      }
      if ( weight > 0 ) pivot *= 1 / weight;
      log << MSG::DEBUG << "Pivot, z/x-range for canonical constraints: " << c.name << " " << std::fixed
          << std::setprecision( 3 ) << pivot << ", [" << zmin << "," << zmax << "]"
          << ", [" << xmin << "," << xmax << "]" << endmsg << log.level();
    }

    // create the object that we will return
    ConstraintDerivatives constraints{numAlignPars, c.dofs, c.name};

    for ( const auto& element : c.elements ) {
      if ( element->activeParOffset() >= 0 ) {
        // calculate the Jacobian for going from the 'alignment' frame to
        // the 'canonical' frame. This is the first place where we could
        // turn things around. It certainly works if the transforms are
        // just translations.
        size_t     elemindex      = element->index();
        const auto canonicalframe = ROOT::Math::Transform3D( ROOT::Math::XYZVector( element->centerOfGravity() ) );
        const auto trans          = canonicalframe.Inverse() * element->alignmentFrame();
        auto       jacobian       = AlParameters::jacobian( trans );
        {
          // We had trouble with the existing implementation because it leads to odd effects if objects have small
          // rotations but constraints are applied on parameters that we do not align for. To do this properly, we need
          // to rewrite things a bit. For now, we take a shortcut: The easiest solution that does not require changes
          // below is to 'round off' the jacobian:
          auto roundit = []( double x ) { return x < -0.5 ? -1 : ( x > 0.5 ? 1 : 0 ); };
          for ( int i = 0; i < 6; ++i )
            for ( int j = 0; j < 6; ++j ) jacobian( i, j ) = roundit( jacobian( i, j ) );
        }

        double thisweight = ( useWeightedAverage ? equations.element( elemindex ).weightV() : 1.0 ) / weight;
        double deltaZ     = element->centerOfGravity().z() - 0.5 * ( zmax + zmin );
        double deltaX     = element->centerOfGravity().x() - 0.5 * ( xmax + xmin );
        double zweight    = zmax > zmin ? deltaZ / ( zmax - zmin ) : 0;
        double xweight    = xmax > xmin ? deltaX / ( xmax - xmin ) : 0;

        // loop over all parameters in this element. skip inactive parameters.
        for ( size_t j = 0; j < 6; ++j ) {
          int jpar = element->activeParIndex( j );
          if ( jpar >= 0 ) {
            // Derivatives for global rotations and translations
            for ( size_t i = 0u; i < 6; ++i )
              // and here comes the 2nd place we could do things entirely
              // wrong, but I think that this is right. if( element->activeParOffset() >= 0 ) { // only take selected
              // elements
              constraints.derivatives()( i, jpar ) = thisweight * jacobian( i, j );

            // Derivatives for z shearings and twists
            for ( size_t i = 0u; i < 6u; ++i )
              constraints.derivatives()( int( EConstraintType::SzTx ) + i, jpar ) =
                  thisweight * zweight * jacobian( i, j );

            // Derivatives for bowing and bowing twist
            for ( size_t i = 0u; i < 6u; ++i )
              constraints.derivatives()( static_cast<Eigen::Index>( EConstraintType::Sz2Tx ) + i, jpar ) =
                  thisweight * zweight * zweight * jacobian( i, j );

            // Derivative for scaling in x
            for ( size_t i = 0u; i < 6u; ++i )
              constraints.derivatives()( int( EConstraintType::SxTx ) + i, jpar ) =
                  thisweight * xweight * jacobian( i, j );

            // Derivative for scalings quadratic in x
            for ( size_t i = 0u; i < 6u; ++i )
              constraints.derivatives()( int( EConstraintType::Sx2Tx ) + i, jpar ) =
                  thisweight * xweight * xweight * jacobian( i, j );

            // Average track parameter constraint
            for ( size_t trkpar = 0; trkpar < 5; ++trkpar )
              constraints.derivatives()( static_cast<Eigen::Index>( EConstraintType::Trx ) + trkpar, jpar ) =
                  equations.element( elemindex ).dStateDAlpha()( j, trkpar ) / equations.numTracks();

            // Average PV position constraint
            if ( equations.numVertices() > 0 )
              for ( size_t vtxpar = 0; vtxpar < 3; ++vtxpar )
                constraints.derivatives()( static_cast<Eigen::Index>( EConstraintType::PVx ) + vtxpar, jpar ) =
                    equations.element( elemindex ).dVertexDAlpha()( j, vtxpar ) / equations.numVertices();
          }
        }

        // now we still need to do the residuals, as far as those are
        // meaningful. note the minus signs.
        if ( c.mode == ConstraintMode::Total || c.mode == ConstraintMode::Survey ) {
          // Obtain the current parameters
          AlParameters::TransformParameters deltaparameters = element->currentDelta().transformParameters();
          // Subtract the survey, if appropriate
          if ( c.mode == ConstraintMode::Survey ) {
            const auto it = surveydata.find( element );
            // FIXME: It is difficult to make this consistent with how we apply the Chi2Constraints:
            // those are applied in the survey frame.
            const AlParameters surveypars = it->second.asParametersInAlignmentFrame( *element );
            if ( it != surveydata.end() ) deltaparameters -= surveypars.transformParameters();
          }
          // Transform to the right frame
          const auto residual = jacobian * deltaparameters;
          // Residuals for global rotations and translations
          for ( size_t i = 0u; i < 6; ++i ) constraints.residuals()( i ) -= thisweight * residual( i );
          // Residuals for shearings and twists
          for ( size_t i = 0u; i < 6u; ++i )
            constraints.residuals()( int( EConstraintType::SzTx ) + i ) -= thisweight * zweight * residual( i );
          // Residuals for bowing and bowing twist
          for ( size_t i = 0u; i < 6u; ++i )
            constraints.residuals()( int( EConstraintType::Sz2Tx ) + i ) -=
                thisweight * zweight * zweight * residual( i );
          // Residuals for scaling in x
          for ( size_t i = 0u; i < 6u; ++i )
            constraints.residuals()( int( EConstraintType::SxTx ) + i ) -= thisweight * xweight * residual( i );
          // Residuals for scaling quadratic in x
          for ( size_t i = 0u; i < 6u; ++i )
            constraints.residuals()( int( EConstraintType::Sx2Tx ) + i ) -=
                thisweight * xweight * xweight * residual( i );
        }
      }
    }
    // turn off constraints with only zero derivatives
    for ( size_t ipar = 0; ipar < NumConstraintTypes; ++ipar )
      if ( constraints.isActive( ipar ) ) {
        bool hasNonZero( false );
        for ( size_t jpar = 0; jpar < numAlignPars && !hasNonZero; ++jpar )
          hasNonZero = std::abs( constraints.derivatives()( ipar, jpar ) ) > LHCb::Math::lowTolerance;
        if ( !hasNonZero ) {
          log << "removing constraint \'" << constraints.name( static_cast<EConstraintType>( ipar ) )
              << "\' because it has no non-zero derivatives." << endmsg;
          constraints.setActive( ipar, false );
        }
      }
    return constraints;
  }

  size_t AlignConstraints::add( const Elements& /*elements*/, const Equations& equations, const SurveyData& surveydata,
                                AlVec& halfDChi2DAlpha, AlSymMat& halfD2Chi2DAlpha2, MsgStream& log ) {
    // get the total number of alignment parameters
    size_t numpars = halfDChi2DAlpha.size();

    // fill the constraint derivatives
    m_derivatives.clear();
    size_t numactive( 0 );
    for ( const auto& constraint : m_definitions ) {
      ConstraintDerivatives derivative = createConstraintDerivatives( constraint, equations, surveydata, numpars, log );
      derivative.setActiveParOffset( numpars + numactive );
      numactive += derivative.nActive();
      m_derivatives.emplace_back( std::move( derivative ) );
    }
    log << "Total number of active constraints: " << numactive << endmsg;

    // now add them, if any constraints are active
    if ( numactive > 0 ) {

      // create new matrices
      size_t   dim = numpars + numactive;
      AlVec    halfDChi2DAlphaNew( dim );
      AlSymMat halfD2Chi2DAlpha2New( dim );

      // copy the old matrices in there
      for ( size_t irow = 0; irow < numpars; ++irow ) {
        halfDChi2DAlphaNew( irow ) = halfDChi2DAlpha( irow );
        for ( size_t icol = 0; icol <= irow; ++icol )
          halfD2Chi2DAlpha2New.fast( irow, icol ) = halfD2Chi2DAlpha2.fast( irow, icol );
      }

      // now add the constraints, only to the 2nd derivative (residuals are zero for now)
      for ( const auto& derivative : m_derivatives ) {
        if ( derivative.nActive() > 0 ) {
          for ( size_t irow = 0; irow < NumConstraintTypes; ++irow ) {
            int iactive = derivative.activeParIndex( irow );
            if ( 0 <= iactive ) {
              for ( size_t jrow = 0; jrow < numpars; ++jrow )
                halfD2Chi2DAlpha2New.fast( derivative.activeParOffset() + iactive, jrow ) =
                    derivative.derivatives()( irow, jrow );
            }
          }

          for ( size_t irow = 0; irow < NumConstraintTypes; ++irow ) {
            int iactive = derivative.activeParIndex( irow );
            if ( 0 <= iactive ) {
              halfDChi2DAlphaNew( derivative.activeParOffset() + iactive ) = derivative.residuals()( irow );
            }
          }
        }
      }
      // copy the result back
      halfDChi2DAlpha   = halfDChi2DAlphaNew;
      halfD2Chi2DAlpha2 = halfD2Chi2DAlpha2New;
    }
    return numactive;
  }

  void AlignConstraints::print( const AlVec& parameters, const AlSymMat& covariance, std::ostream& logmessage ) const {
    if ( !m_derivatives.empty() ) {
      // the total number of alignment parameters
      const size_t numPars = m_derivatives.front().activeParOffset();
      // the number of lagrange parameters (which is the number of active constraints)
      const size_t numConstraintDerivatives = parameters.size() - numPars;

      // ugly: create a map from an index to an alignment parameter in a constraints object
      AlDofMask totalmask;
      for ( const auto& derivative : m_derivatives ) { totalmask.push_back( derivative.dofMask() ); }

      // first extract the part concerning the lagrange multipliers
      AlVec lambda{numConstraintDerivatives};
      AlMat covlambda{numConstraintDerivatives, numConstraintDerivatives};
      for ( size_t i = 0u; i < numConstraintDerivatives; ++i ) {
        lambda( i ) = parameters[numPars + i];
        for ( size_t j = 0u; j < numConstraintDerivatives; ++j )
          covlambda( i, j ) = covariance( numPars + i, numPars + j );
      }

      // For a lagrange constraint defined as delta-chisquare = 2 * lambda * constraint, the solution for lambda is
      //  lambda     =   W * constraint
      //  cov-lambda =   W
      // where W is minus the inverse of the covariance of the constraint. From this we extract
      //   constraint       = W^{-1} * lambda
      //   constraint error = sqrt( W^{-1} )
      //   chisquare = - lambda * constraint
      // Note that this all needs to work for vectors.

      const auto   minusV    = covlambda.inverse();
      const auto   V         = -minusV;
      const auto   x         = V * lambda; // This might need a minus sign ...
      const double chisquare = ( lambda.transpose() * V * lambda );
      logmessage << "Canonical constraint values: " << std::endl;
      for ( size_t iactive = 0u; iactive < numConstraintDerivatives; ++iactive ) {
        size_t     parindex = totalmask.parIndex( iactive );
        const auto thischi2 = x( iactive ) * x( iactive ) / V( iactive, iactive );
        logmessage << std::setw( 15 ) << std::setiosflags( std::ios_base::left )
                   << m_derivatives[parindex / NumConstraintTypes].name(
                          static_cast<EConstraintType>( parindex % NumConstraintTypes ) )
                   << " chi2: " << std::setw( 12 ) << thischi2 << " residual: " << std::setw( 12 ) << x( iactive )
                   << " +/- " << std::setw( 12 ) << AlParameters::signedSqrt( V( iactive, iactive ) )
                   << " gcc^2: " << std::setw( 12 ) << 1 + 1 / ( V( iactive, iactive ) * covlambda( iactive, iactive ) )
                   << std::endl;
      }
      logmessage << "Canonical constraint chisquare: " << chisquare << std::endl;

      // Now the inactive constraints. Shrink the parameter and covariance matrix to the parameters only part
      AlVec subparameters{numPars};
      AlMat subcovariance{numPars, numPars};
      for ( size_t irow = 0; irow < numPars; ++irow ) {
        subparameters[irow] = parameters[irow];
        for ( size_t icol = 0; icol < numPars; ++icol ) subcovariance( irow, icol ) = covariance( irow, icol );
      }
      // Calculate the delta
      const auto derivatives     = m_derivatives.front().derivatives();
      const auto constraintdelta = derivatives * subparameters;
      const auto constraintcov   = derivatives * subcovariance * derivatives.transpose();

      logmessage << "Values of constraint equations after solution (X=active,O=inactive): " << std::endl;
      for ( size_t i = 0; i < NumConstraintTypes; ++i ) {
        logmessage << std::setw( 4 ) << m_derivatives.front().name( static_cast<EConstraintType>( i ) ) << " "
                   << std::setw( 2 ) << ( m_derivatives.front().isActive( i ) ? 'X' : 'O' ) << " " << std::setw( 12 )
                   << constraintdelta( i ) << " +/- " << AlParameters::signedSqrt( constraintcov( i, i ) ) << std::endl;
      }
    }
  }

} // namespace LHCb::Alignment
