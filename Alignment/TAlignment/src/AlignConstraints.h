/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "AlignKernel/AlMat.h"
#include "AlignKernel/AlSymMat.h"
#include "AlignKernel/AlVec.h"
#include "TAlignment/AlDofMask.h"
#include "TAlignment/AlignmentElement.h"

#include "Kernel/STLExtensions.h"
#include "Kernel/meta_enum.h"

#include "AlignSurveyConstraints.h"

#include <string>

namespace LHCb::Alignment {

  meta_enum_class( EConstraintType, size_t, Tx = 0, Ty, Tz, Rx, Ry, Rz, SzTx, SzTy, SzTz, SzRx, SzRy, SzRz, SxTx, SxTy,
                   SxTz, SxRx, RxRy, SxRz, Sz2Tx, Sz2Ty, Sz2Tz, Sz2Rx, Sz2Ry, Sz2Rz, Sx2Tx, Sx2Ty, Sx2Tz, Sx2Rx, Sx2Ry,
                   Sx2Rz, Trx, Try, Trtx, Trty, Trcur, PVx, PVy, PVz, Unknown )

      class GetElementsToBeAligned;
  class Equations;

  class ConstraintDerivatives {
  public:
    ConstraintDerivatives( size_t dim, const std::vector<std::string>& activeconstraints,
                           const std::string& nameprefix = "", int offset = 0 );
    AlMat&       derivatives() { return m_derivatives; }
    const AlMat& derivatives() const { return m_derivatives; }
    AlVec&       residuals() { return m_residuals; }
    const AlVec& residuals() const { return m_residuals; }

    std::string name( EConstraintType i ) const { return m_nameprefix + toString( i ); }

    bool             isActive( size_t ipar ) const { return m_dofmask.isActive( ipar ); }
    void             setActive( size_t ipar, bool b = true ) { return m_dofmask.setActive( ipar, b ); }
    size_t           nActive() const { return m_dofmask.nActive(); }
    int              activeParIndex( size_t ipar ) const { return m_dofmask.activeParIndex( ipar ); }
    int              activeParOffset() const { return m_activeParOffset; }
    void             setActiveParOffset( int offset ) { m_activeParOffset = offset; }
    const AlDofMask& dofMask() const { return m_dofmask; }

  private:
    AlDofMask   m_dofmask;
    AlMat       m_derivatives;
    AlVec       m_residuals;
    std::string m_nameprefix;
    int         m_activeParOffset;
  };

  enum class ConstraintMode { Total, Delta, Survey };

  struct ConstraintDefinition {
    std::string              name;
    std::vector<std::string> dofs;
    Elements                 elements;
    ConstraintMode           mode{ConstraintMode::Delta};

    ConstraintDefinition( const std::string& name ) : name( name ) {}
    void addDof( const std::string& dof ) { dofs.push_back( dof ); }
    void addElements( const Elements& newElements ) {
      elements.insert( elements.end(), newElements.begin(), newElements.end() );
    }
  };

  class AlignConstraints {
  public:
    AlignConstraints( const GetElementsToBeAligned& elementProvider, LHCb::span<const std::string> constraintNames,
                      bool useWeightedAverage, MsgStream& log );

    size_t add( const Elements& elements, const Equations& equations, const SurveyData& surveydata,
                AlVec& halfDChi2DAlpha, AlSymMat& halfD2Chi2DAlpha2, MsgStream& log );

    void print( const AlVec& parameters, const AlSymMat& covariance, std::ostream& logmessage ) const;

  private:
    ConstraintDerivatives createConstraintDerivatives( const ConstraintDefinition& constraint,
                                                       const Equations& equations, const SurveyData& surveypars,
                                                       size_t numAlignPars, MsgStream& log ) const;

    void addConstraintToNominal( const Elements& elements, AlVec& halfDChi2DAlpha, AlSymMat& halfD2Chi2DAlpha2 ) const;

    bool                               m_useWeightedAverage{false};
    std::vector<ConstraintDefinition>  m_definitions;
    std::vector<ConstraintDerivatives> m_derivatives;
  };

} // namespace LHCb::Alignment
