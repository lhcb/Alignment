/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "TAlignment/XmlWriter.h"

namespace {

  void getnum( std::string_view str, unsigned int& num ) {
    const auto end = str.data() + str.size();
    if ( std::from_chars( str.data(), end, num ).ptr != end ) {
      throw LHCb::Alignment::InvalidXmlWriterString( std::string( "Unable to parse number : " ) + std::string( str ) );
    }
  }
  void getbool( std::string_view str, bool& b ) {
    unsigned int num;
    getnum( str, num );
    b = ( num != 0 );
  }
  void getdvec( std::string_view str, std::vector<unsigned int>& v ) {
    v.clear();
    std::vector<std::string> fragments;
    boost::split( fragments, str, []( char c ) { return c == '/'; } );
    v.resize( fragments.size() );
    for ( unsigned int n = 0; n < fragments.size(); n++ ) { getnum( fragments[n], v[n] ); }
  }
  void getwriter( std::string_view str, LHCb::Alignment::XmlWriter& w ) {
    std::vector<std::string> fragments;
    boost::split( fragments, str, []( char c ) { return c == ':'; } );
    if ( fragments.size() != 6 ) {
      throw LHCb::Alignment::InvalidXmlWriterString( std::string( "Wrong number of fragments in XmlWriter string : " ) +
                                                     std::string( str ) );
    }
    w.outputFileName = std::move( fragments[0] );
    w.topElement     = std::move( fragments[1] );
    getnum( fragments[2], w.precision );
    getdvec( fragments[3], w.depths );
    getbool( fragments[4], w.online );
    getbool( fragments[5], w.removePivot );
  }
} // namespace

namespace LHCb::Alignment {

  // stream an XmlWriter
  std::ostream& operator<<( std::ostream& s, const XmlWriter& w ) {
    s << w.outputFileName << ":" << w.topElement << ":" << w.precision << ":";
    bool first = true;
    for ( auto e : w.depths ) {
      if ( !first ) { s << "/"; }
      first = false;
      s << e;
    }
    return s << ":" << w.online << ":" << w.removePivot;
  }

  // parse string to vector of XmlWriters
  StatusCode parse( std::vector<XmlWriter>& w, const std::string& str ) {
    if ( str.size() < 2 ) {
      throw InvalidXmlWriterString( "Too small string for vector<XmlWriter> property : " + str );
    }
    if ( str[0] != '[' ) {
      throw InvalidXmlWriterString( "Missing '[' at start of vector<XmlWriter> property : " + str );
    }
    if ( str[str.size() - 1] != ']' ) {
      throw InvalidXmlWriterString( "Missing ']' at end of vector<XmlWriter> property : " + str );
    }
    std::vector<std::string> fragments;
    boost::split( fragments, std::string_view{str.c_str() + 1, str.size() - 2}, []( char c ) { return c == ','; } );
    w.resize( fragments.size() );
    for ( unsigned int n = 0; n < fragments.size(); n++ ) {
      auto& fragment = fragments[n];
      boost::algorithm::trim( fragment );
      if ( fragment[0] != '\'' || fragment[fragment.size() - 1] != '\'' ) {
        throw InvalidXmlWriterString( "Missing \"'\" around XmlWriter element spec : " + fragment );
      }
      getwriter( std::string_view{fragment.c_str() + 1, fragment.size() - 2}, w[n] );
    }
    return StatusCode::SUCCESS;
  }

  // stream a vector of XmlWriter
  std::ostream& toStream( const std::vector<XmlWriter>& v, std::ostream& s ) {
    s << "[";
    bool first = true;
    for ( auto& e : v ) {
      if ( !first ) { s << ","; }
      first = false;
      s << "'" << e << "'";
    }
    return s << "]";
  }

} // namespace LHCb::Alignment
