/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "AlignConditionHelpers.h"
#include "AlignKernel/AlEquations.h"
#include "AlignOnlineYMLCopier.h"
#include "AlignmentInterfaces/IAlignUpdateTool.h"
#include "AlignmentMonitoring/CompareConstants.h"
#include "TAlignment/AlParameters.h"
#include "TAlignment/AlignmentElement.h"
#include "TAlignment/GetElementsToBeAligned.h"
#include "TAlignment/XmlWriter.h"

#include "Kernel/LHCbID.h"
#include "TrackInterfaces/ITrackProjector.h"
#include "TrackKernel/TrackFunctors.h"

#include "Event/FitNode.h"
#include "Event/Particle.h"
#include "Event/TwoProngVertex.h"
#include "VPDet/DeVP.h"

#include "Gaudi/Algorithm.h"
#include "GaudiKernel/IDetDataSvc.h"
#include "GaudiKernel/IEventProcessor.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IMonitorSvc.h"
#include "GaudiKernel/IRunable.h"
#include "GaudiKernel/IUpdateable.h"
#include "GaudiKernel/Service.h"
#include "GaudiKernel/SmartIF.h"
#include <GaudiKernel/IPublishSvc.h>

#include "TFileMerger.h"
#include "TH1D.h"

#include <algorithm>
#include <filesystem>
#include <string>
#include <thread>
#include <vector>

namespace fs = std::filesystem;

namespace LHCb::Alignment {

  // helper using to design the base detectorElementClass, as it's different
  // for DDDB and DD4hep
#ifdef USE_DD4HEP
  using GenericDetElem = LHCb::Detector::DeIOV;
#else
  using GenericDetElem = ::DetectorElementPlus;
#endif

  class AlignOnlineIterator : public extends3<Service, IEventProcessor, IRunable, IIncidentListener> {
  public:
    AlignOnlineIterator( const std::string& name, ISvcLocator* sl );
    virtual ~AlignOnlineIterator();

  public:
    /// the execution of the algorithm
    SmartIF<IIncidentSvc>     m_incidentSvc;
    SmartIF<IDataProviderSvc> m_detectorSvc;
    SmartIF<IPublishSvc>      m_publishSvc;
    /// Its initialization
    StatusCode         initialize() override;
    StatusCode         stop() override;
    virtual StatusCode run() override;
    StatusCode         pause();
    void               doContinue();
    void               handle( const Incident& inc ) override;
    void               doIteration();
    virtual StatusCode finalize() override;

    EventContext createEventContext() override {
      throw GaudiException( "Not implemented", __PRETTY_FUNCTION__, StatusCode::FAILURE );
    }
    StatusCode executeEvent( EventContext&& ) override {
      throw GaudiException( "Not implemented", __PRETTY_FUNCTION__, StatusCode::FAILURE );
    }
    StatusCode executeRun( int ) override { return pause(); }
    StatusCode nextEvent( int ) override {
      throw GaudiException( "Not implemented", __PRETTY_FUNCTION__, StatusCode::FAILURE );
    }
    StatusCode stopRun() override {
      throw GaudiException( "Not implemented", __PRETTY_FUNCTION__, StatusCode::FAILURE );
    }

    void                                      doStop();
    ToolHandle<IAlignUpdateTool>              m_updatetool{this, "UpdateTool", "AlignUpdateTool"};
    Gaudi::Property<unsigned int>             m_runNumber{this, "RunNumber", 234567, "Run number"};
    Gaudi::Property<std::string>              m_overlayRoot{this, "OverlayRoot", "./OverlayRoot",
                                               "directory where to write the overlay content "};
    Gaudi::Property<std::vector<std::string>> m_elemsToBeAligned{this, "Elements", {}, "Path to elements"};
    Gaudi::Property<std::vector<std::string>> m_derivativeFiles{this, "DerivativeFiles", {}, "List of paths"};
    Gaudi::Property<std::vector<std::string>> m_analyzerHistoFiles{
        this, "AnalyzerHistograms", {}, "List of .root paths"};
    Gaudi::Property<std::string>              m_runType{this, "RunType", "NotDefined", "run type (VP, SciFi, ...)"};
    Gaudi::Property<std::string>              m_ServInfix{this, "ServiceInfix", "", "prefix to Conditions PublishSvc"};
    Gaudi::Property<std::vector<std::string>> m_subDets{this, "SubDetectors", {"NotDefined"}, "sub-detectors used"};
    Gaudi::Property<std::vector<std::string>> m_align_elements{
        this, "AlignElements", {"NotDefined"}, "elements to align (Global, Modules, etc.)"};
    Gaudi::Property<std::string> m_alignymldir{this, "AlignYMLDir", "", "directory where all jobs run"};
    Gaudi::Property<std::string> m_onlineymldir{this, "OnlineYMLDir", "", "online conditions"};
    Gaudi::Property<bool>        m_online_mode{this, "OnlineMode", false, "running in Online"};

  private:
    std::thread*                  m_thread    = nullptr;
    int                           m_iteration = 0;
    StatusCode                    m_status    = StatusCode::SUCCESS;
    std::map<std::string, double> m_initAlConsts; // inital alignment constants. Comapred with constants after
                                                  // convergence to see if update is needed
    std::string m_align_message;                  // < string with error messages or info to publish with DIM service
    std::map<std::string, std::string*>         m_condmap;
    std::map<std::string, AlignOnlineYMLCopier> m_ymlcopiers;

#ifdef USE_DD4HEP
    ServiceHandle<LHCb::Det::LbDD4hep::IDD4hepSvc> m_dd4hepSvc{this, "DD4hepSvc", "LHCb::Det::LbDD4hep::DD4hepSvc",
                                                               "underlying DD4Hep service handling conditions"};
#else
    Gaudi::Property<std::vector<XmlWriter>> m_xmlWriters{
        this, "XmlWriters", {}, "Definitions of the XmlWriters handling the output"};
#endif
  };

  static AlignOnlineIterator* IteratorInstance;

  extern "C" {
  int AlignOnlineIteratorThreadFunction( void* t ) {
    AlignOnlineIterator* f = (AlignOnlineIterator*)t;
    IteratorInstance       = f;
    f->doIteration();
    return 1;
  }
  }

  void AlignOnlineIterator::doIteration() {
    // first read the data, because that defines the IOV for the geometry
    Equations equations( 0 );
    for ( const auto& filename : m_derivativeFiles ) {
      Equations tmp( 0 );
      info() << "Reading derivative file " << filename << endmsg;
      const auto [readerSc, readerMsg] = tmp.readFromFile( filename.c_str() );
      if ( readerSc.isFailure() ) {
        error() << "Failed to read " << filename << " : " << readerMsg;
        continue;
      }
      if ( tmp.numEvents() == 0 ) { continue; }
      equations.add( tmp );
    }

    if ( equations.iov() != m_runNumber ) {
      warning() << "The specified runnumber does not correspond to the input data." << endmsg;
    }

#ifdef USE_DD4HEP
    // LHCb::standard_geometry_top
    const auto& lhcb = ConditionHelpers::getTopOfGeometry( *m_dd4hepSvc, equations.iov() );
#else
    const std::string lhcbDetectorPath = "/dd/Structure/LHCb";
    DataObject*       lhcbObj          = 0;
    m_detectorSvc->retrieveObject( lhcbDetectorPath, lhcbObj ).ignore();
    const GenericDetElem* lhcb = dynamic_cast<const GenericDetElem*>( lhcbObj );
    if ( lhcbObj == nullptr ) error() << "lhcbObj is null!" << endmsg;
#endif

    // GetElementsToBeAligned* elementProvider = nullptr;
    auto elementProvider =
        std::make_unique<GetElementsToBeAligned>( m_elemsToBeAligned.value(), lhcb, equations.initTime(), warning() );

    if ( elementProvider == nullptr ) error() << "elementProvider is null!" << endmsg;
    if ( m_iteration == 0 ) { m_initAlConsts = elementProvider->getAlignmentConstants(); }
    int               maxIteration      = 5;
    ConvergenceStatus convergencestatus = ConvergenceStatus::Unknown;

    TFileMerger histomerger;
    for ( const auto& filename : m_analyzerHistoFiles ) {
      info() << "Reading histogram file " << filename << endmsg;
      histomerger.AddFile( filename.c_str() );
    }
    auto histoOutput = fs::path( m_overlayRoot.value() ).replace_filename( "iter" + std::to_string( m_iteration ) );
    fs::create_directory( histoOutput );
    auto merged_filename = histoOutput.string() + "/MergedHistos_Iter" + std::to_string( m_iteration ) + ".root";
    info() << "Merging histograms to " << merged_filename << endmsg;
    histomerger.OutputFile( merged_filename.c_str() );
    histomerger.Merge();

    for ( const auto& filename : m_analyzerHistoFiles ) {
      auto target_filename = fs::path( filename ).filename();
      auto target_path     = histoOutput / target_filename;
      if ( fs::exists( filename ) ) {
        info() << "moving histogram file " << filename << " to " << target_path << endmsg;
        fs::rename( filename, target_path );
      } else {
        info() << "expected histogram file " << filename << " does not exist" << endmsg;
      }
    }

    auto iteratorOutput =
        fs::path( m_overlayRoot.value() ).replace_filename( "new-constants-" + std::to_string( m_iteration ) );
    fs::create_directory( iteratorOutput );

#ifdef USE_DD4HEP
    StatusCode sc =
        m_updatetool->process( equations, *elementProvider, m_iteration, maxIteration, convergencestatus, *m_dd4hepSvc )
            .andThen( [&] {
              m_dd4hepSvc->write_alignments( iteratorOutput.string() );
              return StatusCode::SUCCESS;
            } );
#else
    StatusCode sc =
        m_updatetool->process( equations, *elementProvider, m_iteration, maxIteration, convergencestatus )
            .andThen( [&] { elementProvider->writeAlignmentConditions( m_xmlWriters.value(), warning() ); } );
#endif

    fs::copy( iteratorOutput, m_overlayRoot.value(),
              fs::copy_options::overwrite_existing | fs::copy_options::recursive );

    if ( sc.isFailure() ) {
      error() << "Iteration failed. Stopping" << endmsg;
      // TODO are we supposed to use DAQ_ERROR?
      // this->m_incidentSvc->fireIncident( Incident( this->name(), "DAQ_ERROR" ) );
      m_status = sc;
      doStop();
      return;
    }
    if ( convergencestatus == ConvergenceStatus::Converged ) {
      info() << "Iteration converged. Comparing constants" << endmsg;
      m_align_message                                                  = "INFO: " + m_runType + " Iterator converged";
      std::map<std::string, double>                      finalAlConsts = elementProvider->getAlignmentConstants();
      ::Alignment::AlignmentMonitoring::CompareConstants cmp( m_initAlConsts, finalAlConsts );
      cmp.SetVerbosity( true );
      cmp.Compare();
      info() << "compare wwarnings " << cmp.GetNumWarnings( 3 ) << " " << cmp.GetNumWarnings( 2 ) << endmsg;
      // TODO: implement failure cases
      if ( cmp.GetNumWarnings( 3 ) ) { // changes to big, make it fail
        warning() << " Alignment converged but constants changes is unreasonably large | " + cmp.GetWarningMessages( 3 )
                  << endmsg;
      }
      // TODO: when should this be triggered?
      /*
    else if ( cmp.GetNumWarnings( 2 ) ) { // no significant change
      warning() << "Alignment converged in more than one iteration but constants didn't change significantly. " +
            cmp.GetWarningMessages( 2 )
                << endmsg;
    }
  */
      else {
        info() << "Copying updated constants to database" << endmsg;
        m_status = StatusCode::SUCCESS;
        for ( auto& j : m_condmap ) {
          auto condname      = j.first;
          auto cur_ymlcopier = m_ymlcopiers.find( condname );
          auto s             = j.second;
          cur_ymlcopier->second.copyToOnlineArea();
          *s = std::to_string( m_runNumber ) + " v" + std::to_string( cur_ymlcopier->second.version() );
        }
        // TODO: think a bit more about where to call updateAll
        if ( m_online_mode ) m_publishSvc->updateAll();
      }
      doStop();
    } else {
      info() << "Iteration not converged. Continue" << endmsg;
      m_align_message = "INFO :" + m_runType + " Iterator not converged";
#ifdef USE_DD4HEP
      // refresh the overlay from files
      m_dd4hepSvc->load_alignments( m_overlayRoot );
      // force a reload of the conditions at the next possible occasion
      m_dd4hepSvc->clear_slice_cache();
#endif
      m_status = StatusCode::SUCCESS;
      doContinue();
    }

    m_iteration++;
  }

  DECLARE_COMPONENT_WITH_ID( AlignOnlineIterator, "AlignOnlineIterator" )

  StatusCode AlignOnlineIterator::initialize() {
    StatusCode sc = this->Service::initialize().ignore();
    if ( !sc ) return sc;
    this->m_incidentSvc = this->service( "IncidentSvc", true );
    if ( !this->m_incidentSvc.get() ) {
      error() << "Cannot access incident service of type IncidentSvc." << endmsg;
      return StatusCode::FAILURE;
    }
    this->m_detectorSvc = this->service( "DetectorDataSvc" );
    if ( !this->m_detectorSvc.get() ) {
      error() << "Cannot access incident service of type DetectorDataSvc." << endmsg;
      return StatusCode::FAILURE;
    }
    if ( m_online_mode ) {
      m_publishSvc = this->service( "LHCb::PublishSvc" );
      if ( !this->m_publishSvc.get() ) {
        error() << "Cannot access incident service of type PublishSvc." << endmsg;
        return StatusCode::FAILURE;
      }
    }
    this->m_incidentSvc->addListener( this, "DAQ_PAUSED" );
    this->m_incidentSvc->addListener( this, "DAQ_CONTINUE" );

    // Define error DIM service
    m_align_message = "INFO: " + m_runType + " Initializing align messenger"; // This message will be overwrittenyy
    if ( m_online_mode ) m_publishSvc->declarePubItem( "AlignmentMessenger", m_align_message );
    // m_publishSvc->updateItem("AlignmentMessenger");

    std::vector<std::string> condnames;
    for ( const auto& sd : m_subDets ) {
      for ( const auto& ele : m_align_elements ) {
        auto condname = sd + "/Alignment/" + ele;
        condnames.push_back( condname );
        if ( m_online_mode )
          m_ymlcopiers.insert( std::make_pair(
              condname, AlignOnlineYMLCopier( m_onlineymldir, m_overlayRoot, sd + "/Alignment/" + ele, &info() ) ) );
      }
    }
    if ( m_online_mode ) {
      for ( const auto& condname : condnames ) {
        std::string* s = new std::string( "" );
        m_condmap.insert( std::make_pair( condname, s ) );
        m_publishSvc->declarePubItem( m_ServInfix + condname, *s );
      }
    }

    return sc;
  }

  StatusCode AlignOnlineIterator::pause() // the execution of the algorithm
  {
    info() << "iterator pausing " << m_iteration << endmsg;
    if ( m_thread != nullptr ) {
      m_thread->join();
      delete m_thread;
    }
    m_thread = new std::thread( AlignOnlineIteratorThreadFunction, this );
    info() << "iterator paused " << endmsg;
    return StatusCode::SUCCESS;
  }

  void AlignOnlineIterator::doContinue() {
    info() << "doContinue" << endmsg;
    this->m_incidentSvc->fireIncident( Incident( this->name(), "DAQ_CONTINUE" ) );
  }

  void AlignOnlineIterator::doStop() { this->m_incidentSvc->fireIncident( Incident( this->name(), "DAQ_STOP" ) ); }

  StatusCode AlignOnlineIterator::stop() {
    info() << "iterator stopping " << endmsg;
    m_iteration = 0;
    if ( m_thread != nullptr ) {
      m_thread->join();
      delete m_thread;
    }
    auto sc = Service::stop();
    sc &= m_status;
    return sc;
  }

  StatusCode AlignOnlineIterator::finalize() {
    info() << "iterator finalizing " << endmsg;
    if ( m_online_mode ) m_publishSvc->undeclarePubAll();
    this->m_incidentSvc.reset();
    return this->Service::finalize();
  }

  void AlignOnlineIterator::handle( const Incident& inc ) {
    info() << "iterator received DAQ_HANDLE " << endmsg;
    if ( inc.type() == "DAQ_PAUSED" ) {
      info() << "iterator received DAQ_PAUSED " << endmsg;
      this->pause().ignore();
    }
    if ( inc.type() == "DAQ_CONTINUE" ) { info() << "iterator received DAQ_CONTINUE " << endmsg; }
  }

  AlignOnlineIterator::~AlignOnlineIterator() {}

  StatusCode AlignOnlineIterator::run() {
    info() << "iterator running " << endmsg;
    return StatusCode::SUCCESS;
  }

  AlignOnlineIterator::AlignOnlineIterator( const std::string& name, ISvcLocator* sl ) : base_class( name, sl ) {}

} // namespace LHCb::Alignment
