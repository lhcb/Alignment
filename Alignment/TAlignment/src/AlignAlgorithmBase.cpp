/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TAlignment/AlignAlgorithmBase.h"

#include "AlignConditionHelpers.h"
#include "AlignKernel/AlEquations.h"
#include "TAlignment/AlParameters.h"
#include "TAlignment/AlignmentElement.h"

#include "Kernel/LHCbID.h"
#include "TrackKernel/TrackFunctors.h"

#include "DetDesc/IDetectorElement.h"
#include "Event/TwoProngVertex.h"
#include "VPDet/DeVP.h"

#include "GaudiKernel/IDetDataSvc.h"

#include <algorithm>

namespace LHCb::Alignment {
  AlignAlgorithmBase::AlignAlgorithmBase( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, {KeyValue{"DELHCbPath", LHCb::standard_geometry_top}} ) {}

  StatusCode AlignAlgorithmBase::initialize() {
    return Consumer::initialize().andThen( [&]() {
      if ( m_histoRefFileName.empty() ) {
        m_histoRefFileName = "/group/online/dataflow/options/" + m_histoPartitionName + "/Alignment_Reference_File.txt";
      }

      // set initial time. From this moment on geometry and conditions are
      // bound to this time
#ifndef USE_DD4HEP
      auto detDataSvc = service<IDetDataSvc>( "DetectorDataSvc" );
      if ( !detDataSvc ) {
        error() << "Could not retrieve DetectorDataSvc" << endmsg;
        return;
      }
      info() << "Updating detector data svc and update manager svc with time: " << m_initialTime.value() << endmsg;
      detDataSvc->setEventTime( m_initialTime.value() );
#endif

      info() << "Use correlations = " << m_correlation << endmsg;
    } );
  }

  void AlignAlgorithmBase::initializeGetElementsToBeAligned( const GenericDetElem& lhcb,
                                                             const Gaudi::Time     inittime ) const {
    m_elementProvider.with_lock( [&]( ElementProvider& elementProvider ) {
      if ( !elementProvider ) {
        info() << "Use local frame = " << m_useLocalFrame.value() << endmsg;
        info() << "===================== GetElementsToAlign =====================" << endmsg;
        elementProvider =
            std::make_unique<GetElementsToBeAligned>( m_elemsToBeAligned.value(), lhcb, inittime, warning() );
        const Elements& elements = elementProvider->elements();
        info() << "   Going to align " << elements.size() << " detector elements:" << endmsg;
        info() << "==============================================================" << endmsg;
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "==> Got " << elements.size() << " elements to align!" << endmsg;
          for ( auto& element : elements ) {
            const auto& ownDoFMask = element->dofMask();
            debug() << "        " << ( *element ) << endmsg;
            const std::vector<std::string> dofs = {"Tx", "Ty", "Tz", "Rx", "Ry", "Rz"};
            debug() << "DOFs: ";
            for ( auto j = ownDoFMask.begin(), jEnd = ownDoFMask.end(); j != jEnd; ++j ) {
              if ( ( *j ) ) info() << dofs.at( std::distance( ownDoFMask.begin(), j ) );
            }
            debug() << endmsg;
          }
        }
        if ( m_fillHistos ) {
          info() << "booking histograms assuming " << m_nIterations.value() << " iterations " << endmsg;
          for ( auto& element : elementProvider->elements() )
            m_elemHistos.emplace_back( std::make_unique<AlElementHistos>( this, *element, m_nIterations ) );
        }
        if ( m_Online ) { m_HistoUpdater.setMonitorService( monitorSvc() ); }
      } else {
        // data.elementProvider->update_iov( lhcb, warning() );
      }
      if ( !elementProvider->isInitialized() ) {
        elementProvider->initAlignmentFrame();
        // data.equations = data.elementProvider->initEquations(true, data.equations.initTime());
        info() << "Initializing alignment frames with time: " << inittime.ns() << " --> "
               << inittime.format( true, "%F %r" ) << endmsg;
      }
    } );
  }

  StatusCode AlignAlgorithmBase::finalize() {
    debug() << "AlignAlgo finalizing" << endmsg;
    if ( m_updateInFinalize && !m_Online ) update();

    if ( m_Online ) { m_HistoUpdater.m_MonSvc = 0; }
    return Consumer::finalize();
  }

  StatusCode AlignAlgorithmBase::stop() {
    debug() << "AlignAlgo stopping" << endmsg;
    if ( m_Online ) {
      if ( m_RunList.size() > 0 ) {
        if ( m_RunList[0] != "*" ) { m_HistoUpdater.Update( ::atoi( m_RunList[0].c_str() ), m_histoRefFileName ); }
      }
    }
    if ( !m_outputDataFileName.empty() ) {
      // FIXME some debugging output below to be removed?
      info() << "Total number of tracks: " << m_nTracks << endmsg;
      info() << "Number of covariance calculation failures: " << m_covFailure << endmsg;
      m_equations.with_lock( [&]( const Equations& equations ) {
        info() << "data.equations  " << equations.numTracks() << endmsg;
        const auto [tmp_numevents, tmp_buffersize] = equations.writeToFile( m_outputDataFileName.value().c_str() );
        info() << "Equations::writeToFile: wrote " << tmp_numevents << " events in " << tmp_buffersize
               << " bytes to file " << m_outputDataFileName.value().c_str() << endmsg;
      } );
    }
    return StatusCode::SUCCESS;
  }

  StatusCode AlignAlgorithmBase::start() {
    info() << "AlignAlgo starting iteration " << m_iteration << endmsg;
    StatusCode sc;
    if ( m_Online || m_iteration.value() > 1 ) {

#ifdef USE_DD4HEP
      if ( m_load_conditions && !m_online_overlay_path.empty() ) {
        loadAlignmentConditions( m_online_overlay_path.value().c_str() );
      }
#endif

      // reset contents of ASD
      reset();
      info() << "Alignalgorithm resetted" << m_iteration << endmsg;
    }
    return StatusCode::SUCCESS;
  }

  //=============================================================================
  //  Update
  //=============================================================================
  void AlignAlgorithmBase::update() {
    info() << "Total number of tracks: " << m_nTracks << endmsg;
    info() << "Number of covariance calculation failures: " << m_covFailure << endmsg;
    if ( !m_inputDataFileNames.empty() ) {
      m_equations.with_lock( [&]( Equations& equations ) {
        for ( const auto& ifile : m_inputDataFileNames ) {
          Equations tmp( 0 );
          const auto [readerSc, readerMsg] = tmp.readFromFile( ifile.c_str() );
          if ( readerSc == StatusCode::SUCCESS ) {
            info() << readerMsg << endmsg;
          } else {
            error() << "AlignAlgorithmBase::update: could not read file " << ifile << endmsg;
            error() << readerMsg << endmsg;
          }
          if ( equations.numEvents() == 0 )
            equations = tmp;
          else {
            equations.add( tmp, true, &( info() ) );
          }
          warning() << "Adding derivatives from input file: " << ifile << " " << tmp.numHits() << " "
                    << tmp.totalChiSquare() << " " << equations.totalChiSquare() << endmsg;
        }
      } );
    }
    if ( !m_outputDataFileName.empty() )
      m_equations.with_lock( [&]( const Equations& equations ) {
        const auto [tmp_numevents, tmp_buffersize] = equations.writeToFile( m_outputDataFileName.value().c_str() );
        info() << "Equations::writeToFile: wrote " << tmp_numevents << " events in " << tmp_buffersize
               << " bytes to file " << m_outputDataFileName.value().c_str() << endmsg;
      } );
    // Only call update if something was processed or we will fail
    // trying to read the IOV from an empty TES
    if ( m_nTracks.nEntries() > 0 ) { update( m_iteration.value(), m_nIterations ); }
  }

  void AlignAlgorithmBase::updateGeometry( size_t iter, size_t maxiter ) {
    if ( m_elementProvider.with_lock(
             []( const ElementProvider& elementProvider ) { return elementProvider == nullptr; } ) ) {
      throw GaudiException( "ElementProvider not initialized when updateGeometry was called", "AlignAlgorithm",
                            StatusCode::FAILURE );
    }
    update( iter, maxiter );
  }

  void AlignAlgorithmBase::clearEquationsAndReloadGeometry() {
    if ( m_elementProvider.with_lock(
             []( const ElementProvider& elementProvider ) { return elementProvider == nullptr; } ) ) {
      throw GaudiException( "ElementProvider not initialized when clearEquationsAndReloadGeometry was called",
                            "AlignAlgorithm", StatusCode::FAILURE );
    }
    reset();

#ifdef USE_DD4HEP_NOT_USED
    // No longer used but I'd like to keep this example because we may want to move the iov check into the event loop
    //
    // drop current slice from the cache. It will be reloaded when recreating m_elementProvider
    getDataSvc().drop_slice( data.elementProvider->iov() );
    // recreate m_elementProvider based on new geometry
    const auto& ctx  = getDataSvc().get_slice( data.elementProvider->iov() );
    const auto& lhcb = std::get<0>( m_inputs ).get( ctx );
    initializeGetElementsToBeAligned( lhcb );
#endif
  }

#ifdef USE_DD4HEP
  void AlignAlgorithmBase::loadAlignmentConditions( std::string in_dir ) {
    info() << "Loading new alignment conditions from directory " << in_dir << endmsg;
    getDataSvc().load_alignments( in_dir );
    getDataSvc().clear_slice_cache();
    m_elementProvider.with_lock( []( ElementProvider& elementProvider ) {
      if ( elementProvider ) elementProvider.reset();
    } );
  }

  void AlignAlgorithmBase::writeAlignmentConditions( std::string out_dir ) {
    if ( m_elementProvider.with_lock(
             []( const ElementProvider& elementProvider ) { return elementProvider == nullptr; } ) ) {
      throw GaudiException( "ElementProvider not initialized when writeAlignmentConditions was called",
                            "AlignAlgorithm", StatusCode::FAILURE );
    }
    getDataSvc().write_alignments( out_dir );
  }

#else

  void AlignAlgorithmBase::writeAlignmentConditions() const {
    m_elementProvider.with_lock( [&]( const ElementProvider& elementProvider ) {
      if ( !elementProvider )
        throw GaudiException( "ElementProvider not initialized when writeAlignmentConditions was called",
                              "AlignAlgorithm", StatusCode::FAILURE );
      elementProvider->writeAlignmentConditions( m_xmlWriters.value(), warning() );
    } );
  }
#endif

  void AlignAlgorithmBase::update( size_t iter, size_t maxiter ) {
    auto convergencestatus = ConvergenceStatus();
    m_equations.with_lock( [&]( const Equations& equations ) {
      m_elementProvider.with_lock( [&]( ElementProvider& elementProvider ) {
        debug() << "Calling alignment update: " << iter << " " << maxiter << " " << equations.iov() << " "
                << elementProvider->iov() << endmsg;
#ifdef USE_DD4HEP
        // Make sure to load the correct geometry slice because we need access to the detector elements. It is not
        // sufficient to check that the alignent geometry was initialized with the right iov, because its underlying
        // detector geometry may have run out of the geometry cache.
        getDataSvc().drop_slice( equations.iov() );
        DetectorElement lhcb = LHCb::Alignment::ConditionHelpers::getTopOfGeometry( getDataSvc(), equations.iov() );
        info() << "Updating geometry for iov. New top of geometry: " << lhcb.iov() << " " << lhcb.name() << " "
               << lhcb.detector().path() << endmsg;
        elementProvider->update_iov( lhcb, warning() );
#endif
        m_updatetool
            ->process( equations, *elementProvider, iter, maxiter, convergencestatus
#ifdef USE_DD4HEP
                       ,
                       getDataSvc()
#endif
                           )
            .ignore();
      } );
    } );
  }

  void AlignAlgorithmBase::reset() {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "increasing iteration counter and resetting accumulated data..." << endmsg;
    /// increment iteration counter
    ++m_iteration;
    // set counters to zero
    m_nTracks.reset();
    m_covFailure.reset();
    // clear derivatives. update the parameter vectors used for bookkeeping.
    m_elementProvider.with_lock( []( ElementProvider& elementProvider ) {
      if ( elementProvider ) elementProvider->resetInitialized();
    } );
    m_equations.with_lock( []( Equations& equations ) { equations.clear(); } );
    // load conditions from file for subsequent iterations
    m_load_conditions = true;
  }

  namespace {
    void extractTracks( const LHCb::Particle& p, std::vector<const LHCb::Track*>& tracks ) {
      if ( p.proto() && p.proto()->track() ) tracks.push_back( p.proto()->track() );
      for ( const LHCb::Particle* dau : p.daughters() ) extractTracks( *dau, tracks );
    }

    struct CompareLHCbIds {
      bool operator()( const LHCb::LHCbID& lhs, const LHCb::LHCbID& rhs ) const { return lhs.lhcbID() < rhs.lhcbID(); }
    };

    struct TrackClonePredicate {
      const LHCb::Track* lhs;
      TrackClonePredicate( const LHCb::Track* tr ) : lhs( tr ) {}
      bool operator()( const LHCb::Track* rhs ) const {
        // either it is the same tracks, or all LHCbIDs of rhs appear in lhs or vice versa
        return rhs == lhs || lhs->nCommonLhcbIDs( *rhs ) == std::min( lhs->lhcbIDs().size(), rhs->lhcbIDs().size() );
      }
    };

    struct CompareTypeAndSide {
      int direction( const LHCb::Track& track ) const { return track.isVeloBackward() ? -1 : 1; }
      int side( const LHCb::Track& track ) const {
        // find the first VP lhcbid
        auto it = std::find_if( track.lhcbIDs().begin(), track.lhcbIDs().end(),
                                []( const auto& lhcbid ) { return lhcbid.isVP(); } );
        if ( it != track.lhcbIDs().end() ) return it->vpID().sidepos() == LHCb::Detector::VPChannelID::Side::A ? 1 : -1;
        int txside = track.firstState().tx() > 0 ? 1 : -1;
        return txside * direction( track );
      }
      bool operator()( const LHCb::Track* lhs, const LHCb::Track* rhs ) const {
        return lhs->type() < rhs->type() ||
               ( lhs->type() == rhs->type() &&
                 ( side( *lhs ) < side( *rhs ) ||
                   ( side( *lhs ) == side( *rhs ) && direction( *lhs ) < direction( *rhs ) ) ) );
      }
    };

    template <class TYPE>
    struct IsEqual {
      const TYPE* m_p;
      IsEqual( const TYPE& ref ) : m_p( &ref ) {}
      bool operator()( const SmartRef<TYPE>& lhs ) { return &( *lhs ) == m_p; }
    };
  } // namespace

  void AlignAlgorithmBase::selectVertexTracks( const LHCb::RecVertex& vertex, const TrackContainer& tracks,
                                               TrackContainer& tracksinvertex ) const {
    // loop over the list of vertices, collect tracks in the vertex
    tracksinvertex.reserve( tracks.size() );
    for ( const auto& track : vertex.tracks() )
      if ( track ) {
        // we'll use the track in the provided list, not the track in the vertex
        TrackContainer::const_iterator jtrack =
            std::find_if( tracks.begin(), tracks.end(), TrackClonePredicate( track ) );
        if ( jtrack != tracks.end() ) tracksinvertex.push_back( *jtrack );
      }
  }

  void AlignAlgorithmBase::removeVertexTracks( const LHCb::RecVertex& vertex, TrackContainer& tracks ) const {
    TrackContainer unusedtracks;
    for ( auto& track : tracks )
      if ( std::find_if( vertex.tracks().begin(), vertex.tracks().end(), IsEqual<LHCb::Track>( *track ) ) ==
           vertex.tracks().end() )
        unusedtracks.push_back( track );
    tracks = unusedtracks;
  }

  LHCb::RecVertex* AlignAlgorithmBase::cloneVertex( const LHCb::RecVertex& vertex,
                                                    const TrackContainer&  selectedtracks ) const {
    LHCb::RecVertex* rc( 0 );
    TrackContainer   tracksinvertex;
    selectVertexTracks( vertex, selectedtracks, tracksinvertex );
    if ( tracksinvertex.size() >= 2 ) {
      rc = vertex.clone();
      rc->clearTracks();
      for ( auto& track : tracksinvertex ) rc->addToTracks( track );
    }
    return rc;
  }

  void AlignAlgorithmBase::splitVertex( const LHCb::RecVertex& vertex, const TrackContainer& tracks,
                                        VertexContainer& splitvertices ) const {
    //
    TrackContainer tracksinvertex;
    selectVertexTracks( vertex, tracks, tracksinvertex );

    if ( tracksinvertex.size() >= m_minTracksPerVertex ) {
      // sort the tracks by track type, then by side in the velo
      std::sort( tracksinvertex.begin(), tracksinvertex.end(), CompareTypeAndSide() );
      // the number of vertices after splitting. minimum 2 tracks per vertex.
      size_t numsplit = tracksinvertex.size() / m_maxTracksPerVertex +
                        ( ( tracksinvertex.size() % m_maxTracksPerVertex ) >= m_minTracksPerVertex ? 1 : 0 );
      splitvertices.clear();
      splitvertices.resize( numsplit, LHCb::RecVertex( vertex.position() ) );
      for ( size_t itrack = 0; itrack < tracksinvertex.size(); ++itrack )
        splitvertices[itrack % numsplit].addToTracks( tracksinvertex[itrack] );
    }
  }

  void AlignAlgorithmBase::removeParticleTracks( const LHCb::Particle& particle, TrackContainer& tracks ) const {
    std::vector<const LHCb::Track*> particletracks;
    extractTracks( particle, particletracks );

    TrackContainer unusedtracks;
    for ( auto& track : tracks ) {
      if ( std::find_if( particletracks.begin(), particletracks.end(), TrackClonePredicate( track ) ) ==
           particletracks.end() ) {
        unusedtracks.push_back( track );
      }
    }
    tracks = unusedtracks;
  }
} // namespace LHCb::Alignment
