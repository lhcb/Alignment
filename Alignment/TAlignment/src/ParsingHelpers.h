/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include <boost/assign/list_of.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>
#include <boost/tokenizer.hpp>

namespace LHCb::Alignment {
  namespace ParsingHelpers {
    inline std::string assembleConditionLine( std::ifstream& file ) {
      // creates a single line for a condition that may span several lines
      bool        conditionopen = false;
      std::string rc;
      while ( file && ( conditionopen || rc.empty() ) ) {
        char line[2048];
        file.getline( line, 2048 );
        std::string linestr( line );
        size_t      ipos( 0 ), jpos( 0 );
        try {
          if ( ( ipos = linestr.find( "<condition", 0 ) ) != std::string::npos ) {
            conditionopen = true;
          } else {
            ipos = 0;
          }
          if ( ( jpos = linestr.find( "/condition>", 0 ) ) != std::string::npos ) {
            conditionopen = false;
            rc += linestr.substr( ipos, jpos - ipos + 11 );
          } else if ( conditionopen )
            rc += linestr.substr( ipos, linestr.size() - ipos );
        } catch ( const std::exception& e ) {
          std::cout << "problem parsing line: " << linestr << std::endl;
          throw e;
        }
      }
      return rc;
    }

    inline std::string removechars( const std::string& s, const std::string& chars ) {
      std::string rc;
      for ( std::string::const_iterator it = s.begin(); it != s.end(); ++it )
        if ( std::find( chars.begin(), chars.end(), *it ) == chars.end() ) rc.push_back( *it );
      return rc;
    }

    inline std::vector<std::string> tokenize( const std::string& s, const char* separator ) {
      typedef boost::char_separator<char> Separator;
      typedef boost::tokenizer<Separator> Tokenizer;
      Tokenizer                           split( s, Separator( separator ) );
      std::vector<std::string>            rc;
      rc.insert( rc.end(), split.begin(), split.end() );
      return rc;
    }

    inline double extractdouble( const std::string& str ) {
      std::string input = removechars( str, " \t" );
      double      rc    = 1;
      size_t      ipos;
      if ( ( ipos = input.find( "*mrad" ) ) != std::string::npos ) {
        rc = 0.001;
        input.erase( ipos, 5 );
      } else if ( ( ipos = input.find( "*mm" ) ) != std::string::npos ) {
        input.erase( ipos, 3 );
      }
      try {
        rc *= boost::lexical_cast<double>( input );
      } catch ( const std::exception& e ) {
        std::cout << "error parsing input string: \'" << str << "\'" << std::endl;
        throw e;
      }
      return rc;
    }

    inline bool match( const std::string& name, const std::string& pattern ) {
      boost::regex ex;
      bool         rc = false;
      try {
        ex.assign( pattern, boost::regex_constants::icase );
        rc = boost::regex_match( name, ex );
      } catch ( ... ) { std::cout << "Problem with pattern: " << pattern << std::endl; }
      return rc;
    }
  } // namespace ParsingHelpers
} // namespace LHCb::Alignment
