/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/ILHCbMagnetSvc.h"

class CountingPrescaler : public GaudiAlgorithm {

public:
  using GaudiAlgorithm::GaudiAlgorithm;
  StatusCode initialize() override;
  StatusCode execute() override;

private:
  Gaudi::Property<size_t> m_interval{this, "Interval", 1};
  Gaudi::Property<size_t> m_offset{this, "Offset", 0};
  Gaudi::Property<size_t> m_printfreq{this, "PrintFreq", 0};
  size_t                  m_counter{0};
};

DECLARE_COMPONENT( CountingPrescaler )

StatusCode CountingPrescaler::initialize() {
  info() << "Interval, offset = " << m_interval << ", " << m_offset << endmsg;
  m_counter = 0;
  return GaudiAlgorithm::initialize();
}

StatusCode CountingPrescaler::execute() {
  bool passed = ( m_counter >= m_offset ) && ( ( m_counter - m_offset ) % m_interval == 0 );
  setFilterPassed( passed );

  if ( passed && m_printfreq > 0 && ( ( ( m_counter - m_offset ) / m_interval ) % m_printfreq == 0 ) )
    info() << "Interval=" << m_interval << " Offset=" << m_offset << " Accepting event : " << m_counter << endmsg;

  ++m_counter;
  return StatusCode::SUCCESS;
}
