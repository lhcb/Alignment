/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifdef USE_DD4HEP

#  include "Detector/LHCb/DeLHCb.h"
#  include "LbDD4hep/IDD4hepSvc.h"

namespace LHCb::Alignment::ConditionHelpers {
  using namespace LHCb::Det::LbDD4hep;
  using ConditionContext = IDD4hepSvc::DD4HepSlicePtr;

  template <typename T>
  decltype( auto ) getCondition( const ConditionContext& ctx, const dd4hep::Condition::key_type& key ) {
    try {
      const dd4hep::Condition& cond = ctx->pool->get( key );
      if constexpr ( detail::IsHandle_v<T> ) {
        return T( cond );
      } else {
        using DT = std::decay_t<T>;
        if constexpr ( std::is_same_v<DT, YAML::Node> ) {
          return LHCb::Detector::utils::json2yaml( cond.get<nlohmann::json>() );
        } else if constexpr ( !detail::PassAsAny<DT>::value ) {
          return cond.get<DT>();
        } else {
          const DT* p = nullptr;
          // First see if type is std::any. If it is we can then apply our own type checks
          // as we cannot rely on those in dd4hep itself giving anything like good diagnostics.
          // Unfortunately due to how dd4hep handles this no other way than just trying a
          // cast to std::any and catching the bad_cast exception if that fails.
          try {
            if constexpr ( detail::IsBoxed<DT>::value ) {
              const auto* cp = std::any_cast<detail::BoxedType<DT>>( &cond.get<std::any>() );
              if ( cp ) { p = &static_cast<const DT&>( *cp ); }
            } else {
              p = std::any_cast<DT>( &cond.get<std::any>() );
            }
            if ( !p ) { detail::throwTypeMismatch( cond.get<std::any>().type(), typeid( DT ) ); }
          } catch ( const std::bad_cast& ) {
            // dd4hep 'cast' to std::any failed. Fallback to directly accessing type
            // i.e. we just assume that is what dd4hep has stored.
            // If not, and we get another bad_cast exception, throw as meaningful exception as we can.
            try {
              p = &cond.get<DT>();
            } catch ( const std::bad_cast& ) { detail::throwTypeMismatch( typeid( nullptr ), typeid( DT ) ); }
          }
          assert( p );
          return *p;
        }
      }
    } catch ( const std::exception& e ) {
      std::stringstream sstr;
      sstr << "Error getting condition '"
           << "<string key>"
           << "' (" << std::hex << std::uppercase << key << "): " << e.what();
      throw std::runtime_error( sstr.str() );
    }
  }

  inline auto getTopOfGeometry( LHCb::Det::LbDD4hep::IDD4hepSvc& dataSvc, size_t iov ) {
    const dd4hep::ConditionKey::KeyMaker m( dd4hep::detail::hash32( "/world" ),
                                            dd4hep::ConditionKey::itemCode( "DetElement-Info-IOV" ) );
    return getCondition<LHCb::Detector::DeIOV>( dataSvc.get_slice( iov ), m.hash );
  }
} // namespace LHCb::Alignment::ConditionHelpers
#endif
