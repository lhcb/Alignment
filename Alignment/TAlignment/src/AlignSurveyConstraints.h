/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "AlignKernel/AlEquations.h"
#include "Event/ChiSquare.h"
#include "TAlignment/AlignmentElement.h"

namespace LHCb::Alignment {

  struct ElementSurveyData {
    Gaudi::Vector6   par;
    Gaudi::Vector6   err;
    Gaudi::XYZVector pivot{0, 0, 0};
    // Gaudi::Transform3D elementframe ;

    Gaudi::Transform3D surveyFrame( const Element& element ) const {
      return element.elementFrame() * ROOT::Math::Translation3D( pivot );
    }

    AlParameters asParametersInSurveyFrame() const {
      AlParameters::TransformCovariance covmatrix;
      for ( int i = 0; i < 6; ++i ) covmatrix( i, i ) = err[i] * err[i];
      return AlParameters{par, covmatrix};
    }

    AlParameters asParametersInAlignmentFrame( const Element& element ) const {
      auto       surveyAsParameters    = asParametersInSurveyFrame();
      const auto surveyframe           = surveyFrame( element );
      const auto fromSurveyToAlignment = element.alignmentFrame().Inverse() * surveyframe;
      surveyAsParameters               = surveyAsParameters.transformTo( fromSurveyToAlignment );
      return surveyAsParameters;
    }

    Gaudi::XYZPoint pivotInGlobal( const Element& element ) const {
      return surveyFrame( element ) * Gaudi::XYZPoint{0, 0, 0};
    }
  };

  using SurveyData = std::map<const Element*, ElementSurveyData>;

  class AlignSurveyConstraints {
  public:
    // Constructor called from AlignChisqConstraintTool
    AlignSurveyConstraints( const Elements& elements, const std::vector<std::string>& constraintnames,
                            const std::vector<std::string>& xmlfilenames,
                            const std::vector<std::string>& xmluncertainties, MsgStream& log );
    // Access to survey data
    const auto&              surveyData() const { return m_surveyParameters; }
    const ElementSurveyData* surveyData( const Element& element ) const {
      const auto it = m_surveyParameters.find( &element );
      return it != m_surveyParameters.end() ? &( it->second ) : nullptr;
    }
    std::optional<AlParameters> surveyParameters( const Element& element ) const {
      const auto it = m_surveyParameters.find( &element );
      return it != m_surveyParameters.end()
                 ? std::optional<AlParameters>{it->second.asParametersInAlignmentFrame( element )}
                 : std::nullopt;
    }
    /// Add survey and joint constraints to equations
    LHCb::ChiSquare addConstraints( Equations& equations, std::ostream& logmessage ) const;
    /// Retrieve the survey chi2 from a single element
    LHCb::ChiSquare chiSquare( const Element& element, bool activeonly = true ) const;

  private:
    SurveyData m_surveyParameters;
  };

} // namespace LHCb::Alignment
