/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Gaudi/Algorithm.h"
#include "GaudiKernel/IDetDataSvc.h"
#include "GaudiKernel/IEventProcessor.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IMonitorSvc.h"
#include "GaudiKernel/IRunable.h"
#include "GaudiKernel/IUpdateable.h"
#include "GaudiKernel/Service.h"
#include "GaudiKernel/SmartIF.h"
#include <GaudiKernel/IPublishSvc.h>
#include <filesystem>
#include <regex>
#include <string>

namespace fs = std::filesystem;

class AlignOnlineYMLCopier {
public:
  AlignOnlineYMLCopier( const std::string& onlinedir, const std::string& aligndir, const std::string& condname,
                        MsgStream* msg_stream );
  void        copyToOnlineArea();
  std::string alignfilename() const;
  std::string aligndirname() const;
  int         version() const { return m_version; }
  std::string onlinedirname() const;
  std::string onlinefilename() const { return onlinefilename( m_version ); }
  std::string copybackfilename() const { return m_newfilename; }
  std::string condition() const { return m_condname; }

private:
  std::string onlinefilename( int v ) const;

private:
  std::string m_condname; // e.g. "VP/VPGlobal"
  std::string m_onlinedir;
  std::string m_aligndir;
  std::time_t m_time;
  std::string m_newfilename;
  std::string m_filename;
  MsgStream*  m_msg_stream;
  int         m_version;
};
