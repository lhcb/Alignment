/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "AlignOnlineYMLCopier.h"

AlignOnlineYMLCopier::AlignOnlineYMLCopier( const std::string& onlinedir, const std::string& aligndir,
                                            const std::string& condname, MsgStream* msg_stream )
    : m_condname( condname )
    , m_onlinedir( onlinedir )
    , m_aligndir( aligndir )
    , m_msg_stream( msg_stream )
    , m_version( 0 ) {
  *msg_stream << "YMLCOPIER" << endmsg;

  m_onlinedir = "/group/online/hlt/conditions.run3/lhcb-conditions-database/Conditions/";
  m_onlinedir = m_onlinedir + m_condname + ".yml/.pool";

  // std::string path = "/group/online/alignment/onlinetestdb/Conditions/VP/Alignment/Global.yml/.pool";
  std::regex regex_version_file( "^v([0-9]+)$" );
  int        max_version_nr = -1;
  for ( const auto& entry : fs::directory_iterator( m_onlinedir ) ) {
    auto version_file_name = entry.path().stem().string();
    bool isMatched         = std::regex_match( version_file_name, regex_version_file );
    if ( !isMatched ) continue;

    *msg_stream << "YMLCOPIER" << version_file_name << " " << entry.path() << " " << isMatched << endmsg;

    version_file_name.erase( 0, 1 );
    int version_nr = std::stoi( version_file_name );
    *msg_stream << "YMLCOPIER version nr " << version_nr << endmsg;
    if ( version_nr > max_version_nr ) max_version_nr = version_nr;
  }
  *msg_stream << "MAXVERSION " << max_version_nr << endmsg;
  m_version = max_version_nr;
  *msg_stream << "aligndir " << m_aligndir << endmsg;
  *msg_stream << "onlinedir " << m_onlinedir << endmsg;
  *msg_stream << "condname " << m_condname << endmsg;
}

void AlignOnlineYMLCopier::copyToOnlineArea() {

  std::string file_to_copy = m_aligndir + "/Conditions/" + m_condname + ".yml";
  m_version++;
  std::string target_file = m_onlinedir + "/v" + std::to_string( m_version );
  *m_msg_stream << "file_to_copy " << file_to_copy << endmsg;
  *m_msg_stream << "target_file " << target_file << endmsg;
  fs::path origin( file_to_copy );
  fs::path target( target_file );
  *m_msg_stream << "AlignOnlineYMLCopier: copying " << origin << " to " << target << "." << endmsg;
  fs::copy( origin, target );
}