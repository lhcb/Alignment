/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "TAlignment/AlignmentElement.h"
#include "TAlignment/3DTransformationFunctions.h"

#include "DetDesc/ParamException.h"

#include "GaudiKernel/Vector3DTypes.h"

#include "boost/algorithm/string_regex.hpp"
#include "boost/regex.hpp"

#include <algorithm>
#include <functional>
#include <iomanip>

namespace {
#ifdef DEBUG_USE_DD4HEP
  dd4hep::Delta amendDelta( const dd4hep::Delta& d, const Gaudi::Transform3D& m ) {
    // We should not have to do that manually ourselves, but DD4hep does not
    // provide anything helpful...
    Gaudi::Transform3D dm{};
    switch ( d.flags ) {
    case dd4hep::Delta::HAVE_TRANSLATION + dd4hep::Delta::HAVE_ROTATION + dd4hep::Delta::HAVE_PIVOT:
      dm = ROOT::Math::Translation3D( d.translation ) * ( d.pivot * ( d.rotation * ( d.pivot.Inverse() ) ) );
      break;
    case dd4hep::Delta::HAVE_TRANSLATION + dd4hep::Delta::HAVE_ROTATION:
      dm = Gaudi::Transform3D( d.rotation, d.translation );
      break;
    case dd4hep::Delta::HAVE_ROTATION + dd4hep::Delta::HAVE_PIVOT:
      dm = Gaudi::Transform3D( d.pivot * ( d.rotation * ( d.pivot.Inverse() ) ) );
      break;
    case dd4hep::Delta::HAVE_ROTATION:
      dm = Gaudi::Transform3D( d.rotation );
      break;
    case dd4hep::Delta::HAVE_TRANSLATION:
      dm = Gaudi::Transform3D( d.translation );
      break;
    default:
      break;
    }
    dm = m * dm;
    return {dd4hep::Position( dm.Translation().Vect() ), dm.Rotation<dd4hep::RotationZYX>()};
  }
#endif
} // namespace

Gaudi::Transform3D LHCb::Alignment::Element::localDelta( const LHCb::Alignment::DetectorElement& element ) {
  return element.localDelta();
}

Gaudi::Transform3D
LHCb::Alignment::Element::toGlobalMatrixMinusDelta( const LHCb::Alignment::DetectorElement& element ) {
  return element.toGlobalMatrix() * element.ownToOffNominalMatrix().Inverse();
}

std::string LHCb::Alignment::Element::stripElementName( const std::string& name ) {
#ifdef USE_DD4HEP
  return boost::algorithm::erase_all_regex_copy( name, boost::regex( "/world/.*Region(/T/|/)" ) );
#else
  return boost::algorithm::erase_all_regex_copy( name, boost::regex( "/dd/Structure/LHCb/.*Region(/T/|/)" ) );
#endif
}

namespace {
  struct SortByName {
    bool operator()( const LHCb::Alignment::DetectorElement& lhs, const LHCb::Alignment::DetectorElement& rhs ) {
      return lhs.name() < rhs.name();
    }
  };
} // namespace

LHCb::Alignment::Element::Element( LHCb::Alignment::DetectorElement element, const unsigned int index,
                                   const std::string& dofs, const Gaudi::XYZVector& localpivot )
    : m_fullname{element.name()}
    , m_name{stripElementName( m_fullname )}
    , m_basename{element.name()}
    , m_index{index}
    , m_localpivot{localpivot} {
  std::vector<DetectorElement> elements( 1u, element );
  addElements( elements );
  addDofs( dofs );
}

LHCb::Alignment::Element::Element( const std::string&                                   aname,
                                   const std::vector<LHCb::Alignment::DetectorElement>& elements,
                                   const unsigned int index, const std::string& dofs,
                                   const Gaudi::XYZVector& localpivot )
    : m_fullname{aname}, m_name{stripElementName( aname )}, m_index{index}, m_localpivot{localpivot} {
  size_t pos = aname.find_first_of( "(.*" );
  m_basename = pos == std::string::npos ? aname : aname.substr( 0, pos );
  addElements( elements );
  addDofs( dofs );
}

void LHCb::Alignment::Element::addDofs( const std::string& dofs ) {
  for ( size_t ipar = 0; ipar < AlParameters::NumPars; ++ipar )
    if ( dofs.find( AlParameters::parName( ipar ) ) != std::string::npos ) m_dofMask.setActive( ipar, true );
  m_lastDeltaDelta = AlParameters( m_dofMask );
}

void LHCb::Alignment::Element::setDofs( const std::string& dofs ) {
  for ( size_t ipar = 0; ipar < AlParameters::NumPars; ++ipar )
    m_dofMask.setActive( ipar, dofs.find( AlParameters::parName( ipar ) ) != std::string::npos );
  m_lastDeltaDelta = AlParameters( m_dofMask );
}

void LHCb::Alignment::Element::addElements( const std::vector<LHCb::Alignment::DetectorElement>& newelements ) {
  // first check that the element hasn't been used yet
  std::vector<DetectorElement> elements;
  elements.reserve( newelements.size() );
  for ( const auto& elem : newelements )
    if ( std::find( m_elements.begin(), m_elements.end(), elem ) == m_elements.end() ) elements.push_back( elem );

  m_elements.insert( m_elements.end(), elements.begin(), elements.end() );
  std::sort( m_elements.begin(), m_elements.end(), SortByName() );

  // loop over all elements in tree and find the largest name they have in common
  m_basename = m_elements.front().name();
  for ( const auto& elem : m_elements ) {
    size_t      ipos( 0 );
    std::string thisname = elem.name();
    while ( ipos < m_basename.size() && ipos < thisname.size() && m_basename[ipos] == thisname[ipos] ) ++ipos;
    if ( ipos < m_basename.size() ) m_basename.resize( ipos );
  }
  // set the element paths
  m_elementpaths.resize( m_elements.size() );
  std::transform( m_elements.begin(), m_elements.end(), m_elementpaths.begin(),
                  []( const auto& elem ) { return elem.name(); } );

  // initialize the frame and jacobian
  initAlignmentFrame();
}

void LHCb::Alignment::Element::initAlignmentFrame() {
  // Calculate the center of gravity (the 'local' center)
  double xmin( 1e10 ), xmax( -1e10 ), ymin( 1e10 ), ymax( -1e10 ), zmin( 1e10 ), zmax( -1e10 );
  for ( const auto& i : m_elements ) {
    Gaudi::XYZPoint R = toGlobalMatrixMinusDelta( i ) * Gaudi::XYZPoint( 0, 0, 0 );
    xmin              = std::min( xmin, R.x() );
    xmax              = std::max( xmax, R.x() );
    ymin              = std::min( ymin, R.y() );
    ymax              = std::max( ymax, R.y() );
    zmin              = std::min( zmin, R.z() );
    zmax              = std::max( zmax, R.z() );
  }
  Gaudi::XYZVector averageR( 0.5 * ( xmin + xmax ), 0.5 * ( ymin + ymax ), 0.5 * ( zmin + zmax ) );
  m_centerOfGravity = Gaudi::XYZPoint( 0.0, 0.0, 0.0 ) + averageR;
  // Initialize the element transform. For combinations of elements we make up something using the center of gravity.
  if ( m_elements.size() == 1 )
    // if there is only only element, takes its frame
    m_elementFrame = toGlobalMatrixMinusDelta( m_elements.front() );
  else {
    // if there is more than one element, we'll use the center of
    // gravity for the translation, and the frame of the first
    // element for the rotation.
    ROOT::Math::Rotation3D rotation = toGlobalMatrixMinusDelta( m_elements.front() ).Rotation();
    m_elementFrame                  = Gaudi::Transform3D( averageR ) * rotation;
  }
  // Initialize the alignment transform
  const auto localtranslation = ROOT::Math::Translation3D( m_localpivot );
  m_alignmentFrame            = m_elementFrame * localtranslation;

  // We do something special for the 'Tracker': choose its pivot point
  // in the center of the magnet, such that we can align for magnetic
  // field rotations
  if ( m_name == std::string( "Tracker" ) ) {
    m_centerOfGravity = Gaudi::XYZPoint( 0.0, 0.0, 5020. );
    m_elementFrame    = Gaudi::Transform3D( Gaudi::XYZVector( 0, 0, 5020. ) );
    m_alignmentFrame  = m_elementFrame;
  }

  // Set the Jacobian
  m_jacobian = AlParameters::jacobian( m_alignmentFrame );

  // Set the Jacobian relative to a frame that is the 'difference' between the COG and the Alignment frame
  m_jacobianCOG =
      AlParameters::jacobian( Gaudi::Transform3D( Gaudi::XYZVector{m_centerOfGravity} ).Inverse() * m_alignmentFrame );
}

std::string LHCb::Alignment::Element::description() const {
  std::string begin = ( m_elements.size() > 1u ? "Group = { " : "" );

  std::string middle;
  bool        first = true;
  for ( const auto& i : m_elements ) {
    if ( first )
      first = false;
    else
      middle += ", ";
    middle += stripElementName( i.name() );
  }

  std::string end = ( m_elements.size() > 1u ? "}" : "" );

  return begin + middle + end;
}

AlParameters LHCb::Alignment::Element::currentTotalDelta() const {
  AlParameters::TransformParameters par               = {0, 0, 0, 0, 0, 0};
  Gaudi::Transform3D                alignmentFrameInv = alignmentFrame().Inverse();
  for ( const auto& elem : m_elements ) {
    Gaudi::Transform3D                globalNominal    = elem.toGlobalMatrixNominal();
    Gaudi::Transform3D                globalDelta      = elem.toGlobal( globalNominal.Inverse() );
    Gaudi::Transform3D                alignDeltaMatrix = alignmentFrameInv * globalDelta * alignmentFrame();
    AlParameters::TransformParameters thispar;
    LHCb::Alignment::getZYXTransformParameters( alignDeltaMatrix, thispar ); //, it->pivot());
    par += thispar;
  }
  par /= m_elements.size();
  return AlParameters( par );
}

AlParameters LHCb::Alignment::Element::currentActiveTotalDelta() const {
  return AlParameters( currentTotalDelta().transformParameters(), m_dofMask );
}

AlParameters LHCb::Alignment::Element::currentDelta() const {
  AlParameters d = currentDelta( alignmentFrame() );
  return AlParameters( d.transformParameters(), m_lastDeltaDelta.transformCovariance() );
}

AlParameters LHCb::Alignment::Element::currentLocalDelta() const {
  AlParameters d = currentLocalDelta( alignmentFrame() );
  return AlParameters( d.transformParameters(), m_lastDeltaDelta.transformCovariance() );
}

AlParameters LHCb::Alignment::Element::currentDelta( const Gaudi::Transform3D& frame ) const {
  // cache the inverse of the frame
  Gaudi::Transform3D                frameInv = frame.Inverse();
  AlParameters::TransformParameters par{0, 0, 0, 0, 0, 0};
  for ( const auto& ielem : m_elements ) {
    // here I want just the _local_ delta.
    Gaudi::Transform3D localdelta = localDelta( ielem );
    // this one includes the localDelta, I think.
    const Gaudi::Transform3D& global = ielem.toGlobalMatrix();
    // so we subtract the delta to get the transformation matrix to global
    // Gaudi::Transform3D globalMinusDelta = global * localDelta.Inverse() ;
    // transform the local delta into the global frame
    // Gaudi::Transform3D localDeltaInGlobal = globalMinusDelta * localDelta * globalMinusDelta.Inverse() ;
    // which could be shorter written like this (really!)
    Gaudi::Transform3D localDeltaInGlobal = global * localdelta * global.Inverse();
    // now transform it into the alignment frame
    Gaudi::Transform3D                alignDeltaMatrix = frameInv * localDeltaInGlobal * frame;
    std::vector<double>               translations( 3, 0.0 ), rotations( 3, 0.0 );
    AlParameters::TransformParameters thispar;
    LHCb::Alignment::getZYXTransformParameters( alignDeltaMatrix, thispar ); //, it->pivot());
    par += thispar;
  }
  par /= m_elements.size();
  return AlParameters( par );
}

AlParameters LHCb::Alignment::Element::currentLocalDelta( const Gaudi::Transform3D& frame ) const {
  // if our mother uses the same conditions as we, then we want to
  // subtract the delta of the mother. this is important for the
  // survey constraints.
  AlParameters rc = currentDelta( frame );
  if ( m_mother && std::includes( m_mother->m_elements.begin(), m_mother->m_elements.end(), m_elements.begin(),
                                  m_elements.end(), SortByName() ) ) {
    AlParameters       motherDelta = m_mother->currentActiveDelta();
    Gaudi::Transform3D frameInv    = frame.Inverse();
    Gaudi::Transform3D localMotherDelta =
        frameInv * m_mother->alignmentFrame() * motherDelta.transform() * m_mother->alignmentFrame().Inverse() * frame;
    // does the order matter here? I hope not ...
    Gaudi::Transform3D localCorrectedDelta = localMotherDelta.Inverse() * rc.transform();
    rc                                     = AlParameters( localCorrectedDelta );
  }
  return rc;
}

AlParameters LHCb::Alignment::Element::currentActiveDelta() const {
  // this one has correct covariance _and_ weight matrix.
  AlParameters rc = m_lastDeltaDelta;
  rc.setParameters( currentDelta().transformParameters() );
  return rc;
}

void LHCb::Alignment::Element::addToDeltaList(
    const AlParameters&                                           deltaInAlignmentFrame,
    std::map<DetectorElement, AlParameters::TransformParameters>& deltabyelement ) {
  for ( const auto& i : m_elements ) {
    // create the transform that goes from the alignment frame to the element frame
    const auto fromAlignmentToLocal = i.toGlobalMatrix().Inverse() * m_alignmentFrame;
    // compute the delta alignment parameters in the element frame
    // const auto localDeltaDeltaMatrix = fromAlignmentToLocal * deltaInAlignmentFrame.transform() *
    // fromAlignmentToLocal.Inverse();
    // const AlParameters pars( localDeltaDeltaMatrix ) ;
    const AlParameters pars = deltaInAlignmentFrame.transformTo( fromAlignmentToLocal );
    auto               it   = deltabyelement.find( i );
    if ( it == deltabyelement.end() ) {
      deltabyelement.insert( std::make_pair( i, pars.transformParameters() ) );
    } else {
      it->second += pars.transformParameters();
    }
  }
  m_lastDeltaDelta = deltaInAlignmentFrame;
}

std::ostream& LHCb::Alignment::Element::fillStream( std::ostream& lhs ) const {
  const auto                            delta  = AlParameters{currentLocalDelta()}.transformParameters();
  static const std::vector<std::string> dofs   = {"Tx", "Ty", "Tz", "Rx", "Ry", "Rz"};
  const auto                            afpars = AlParameters( alignmentFrame() ).transformParameters();

  lhs << std::left << std::setw( 80u ) << std::setfill( '*' ) << "" << std::endl;
  lhs << "* Alignable: " << name() << "\n"
      << "* FullName : " << fullname() << std::endl
      << "* Element  : " << description() << std::endl
      << "* Num elements : " << m_elements.size() << std::endl
      << "* Basename : " << basename() << std::endl
      << "* Index    : " << index() << "\n"
      << "* dPosXYZ  : " << Gaudi::XYZPoint( delta[0], delta[1], delta[2] ) << "\n"
      << "* dRotXYZ  : " << Gaudi::XYZPoint( delta[3], delta[4], delta[5] ) << "\n"
      << "* CogXYZ   : " << centerOfGravity() << "\n"
      << "* Alignment frame: " << afpars << "\n"
      << "* First elem nominal: " << m_elements.front().toGlobalMatrixNominal() * Gaudi::XYZPoint( 0, 0, 0 )
      << std::endl
      << "* First elem aligned: " << m_elements.front().toGlobalMatrix() * Gaudi::XYZPoint( 0, 0, 0 ) << std::endl
      << "* DoFs     : ";
  for ( DofMask::const_iterator j = m_dofMask.begin(), jEnd = m_dofMask.end(); j != jEnd; ++j ) {
    if ( ( *j ) ) lhs << dofs.at( std::distance( m_dofMask.begin(), j ) ) + " ";
  }
  lhs << std::endl;
  if ( !m_daughters.empty() ) {
    lhs << "* Daughters: ";
    for ( DaughterContainer::const_iterator idau = m_daughters.begin(); idau != m_daughters.end(); ++idau )
      lhs << ( *idau )->index() << " ";
    lhs << std::endl;
  }
  return lhs;
}

double LHCb::Alignment::Element::histoResidualMax() const {
  if ( name().find( "VP", 0 ) != std::string::npos ) return 0.1;
  if ( name().find( "UT", 0 ) != std::string::npos ) return 0.2;
  if ( name().find( "FT", 0 ) != std::string::npos ) return 0.5;
  if ( name().find( "Muon", 0 ) != std::string::npos ) return 10;
  if ( name().find( "OT", 0 ) != std::string::npos ) return 5.0;
  if ( name().find( "IT", 0 ) != std::string::npos ) return 0.5;
  if ( name().find( "TT", 0 ) != std::string::npos ) return 0.5;
  return 0.1;
}

std::vector<int> LHCb::Alignment::Element::redundantDofs() const {
  // first check that all elements of this alignable also appear as its daughters
  std::vector<int> rc;
  ElementContainer dauelements;
  for ( const auto* dau : daughters() )
    dauelements.insert( dauelements.end(), dau->detelements().begin(), dau->detelements().end() );
  std::sort( dauelements.begin(), dauelements.end() );
  ElementContainer myelements = detelements();
  std::sort( myelements.begin(), myelements.end() );
  if ( dauelements == myelements )
    for ( int i = 0; i < 6; ++i )
      if ( dofMask().isActive( i ) ) {
        bool foundall( true );
        for ( DaughterContainer::const_iterator idau = daughters().begin(); idau != daughters().end() && foundall;
              ++idau )
          foundall = foundall && ( *idau )->dofMask().isActive( i );
        if ( foundall ) rc.push_back( i );
      }
  return rc;
}
