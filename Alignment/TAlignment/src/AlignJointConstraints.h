/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "AlignKernel/AlEquations.h"
#include "Event/ChiSquare.h"
#include "TAlignment/AlignmentElement.h"

namespace LHCb::Alignment {

  struct JointConstraint {
    const LHCb::Alignment::Element* elementA{0};
    const LHCb::Alignment::Element* elementB{0};
    std::array<double, 3>           pivotA = {0, 0, 0};
    std::array<double, 6>           errors = {0, 0, 0, 0, 0, 0};
  };

  struct JointConstraintResidual {
    using Vector6 = AlParameters::TransformParameters;
    Vector6               residual;
    std::array<double, 6> errors = {0, 0, 0, 0, 0, 0};
    Gaudi::Transform3D    fromAlignmentAToCommon;
    Gaudi::Transform3D    fromAlignmentBToCommon;
    double                chi2() const;
    double                chi2( const AlParameters::DofMask& mask ) const;
  };

  class AlignJointConstraints {
  public:
    // Constructor called from AlignChisqConstraintTool
    AlignJointConstraints( const Elements& elements, const std::vector<std::string>& constraintdefs, MsgStream& log );
    /// Add survey and joint constraints to equations
    LHCb::ChiSquare addConstraints( Equations& equations, std::ostream& logmessage ) const;
    /// Retrieve the survey chi2 from a single element
    LHCb::ChiSquare chiSquare( const Element& element, bool activeonly = true ) const;
    /// Compute the joint residual
    JointConstraintResidual residual( const JointConstraint& element ) const;
    /// Compute the joint residual for an element
    std::optional<JointConstraintResidual> residual( const Element& element ) const;

  private:
    std::multimap<const Element*, JointConstraint> m_constraints;
  };
} // namespace LHCb::Alignment
