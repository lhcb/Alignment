/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AlignJointConstraints.h"
#include "ParsingHelpers.h"
#include "TAlignment/AlignmentElement.h"

namespace LHCb::Alignment {

  using namespace ParsingHelpers;

  // read the joint constraints
  AlignJointConstraints::AlignJointConstraints( const Elements& elements, const std::vector<std::string>& jointdefs,
                                                MsgStream& log ) {
    for ( const auto& configuredjoint : jointdefs ) {
      // first decode (this could go elsewhere)
      log << MSG::DEBUG << "Parsing joint constraint: " << configuredjoint << endmsg;
      std::vector<std::string> tokens = tokenize( configuredjoint, ":" );
      if ( tokens.size() != 4 && tokens.size() != 3 ) {
        log << MSG::ERROR << "Joint constraint has wrong number of tokens: " << configuredjoint << endmsg;
      } else {
        const auto      elementnameA = removechars( tokens[0], " " );
        const auto      elementnameB = removechars( tokens[1], " " );
        JointConstraint constraint;
        auto&           pivotA = constraint.pivotA;
        auto&           errors = constraint.errors;
        {
          auto errorsasstring = tokenize( tokens[2], " " );
          for ( size_t i = 0; i < 6; ++i ) errors[i] = boost::lexical_cast<double>( errorsasstring[i] );
        }
        if ( tokens.size() == 4 ) {
          auto pivotAstring = tokenize( tokens[3], " " );
          for ( size_t i = 0; i < 3; ++i ) pivotA[i] = boost::lexical_cast<double>( pivotAstring[i] );
        }
        // find the two elements
        const LHCb::Alignment::Element* elementA{0};
        const LHCb::Alignment::Element* elementB{0};
        for ( const auto* element : elements ) {
          // match the name of the alignable, or, if there is only one element, match the name of the condition
          if ( !elementA && ( element->name() == elementnameA || match( element->name(), elementnameA ) ) )
            elementA = element;
          if ( !elementB && ( element->name() == elementnameB || match( element->name(), elementnameB ) ) )
            elementB = element;
        }
        if ( !elementA )
          log << MSG::ERROR << "Cannot find element for joint constraint A: \'" << elementnameA << "\'" << endmsg;
        if ( !elementB )
          log << MSG::ERROR << "Cannot find element for joint constraint B: \'" << elementnameB << "\'" << endmsg;
        if ( elementA && elementB ) {
          constraint.elementA = elementA;
          constraint.elementB = elementB;
          m_constraints.emplace( elementA, constraint );
        }
      }
    }
  }

  double JointConstraintResidual::chi2() const {
    double chi2{0};
    for ( int i = 0; i < 6; ++i ) chi2 += residual[i] * residual[i] / ( errors[i] * errors[i] );
    return chi2;
  }

  double JointConstraintResidual::chi2( const AlParameters::DofMask& mask ) const {
    double chi2{0};
    for ( int i = 0; i < 6; ++i )
      if ( mask.isActive( i ) ) chi2 += residual[i] * residual[i] / ( errors[i] * errors[i] );
    return chi2;
  }

  std::optional<JointConstraintResidual> AlignJointConstraints::residual( const Element& element ) const {
    std::optional<JointConstraintResidual> res;
    const auto                             it = m_constraints.lower_bound( &element );
    if ( it != m_constraints.upper_bound( &element ) ) res = residual( it->second );
    return res;
  }

  JointConstraintResidual AlignJointConstraints::residual( const JointConstraint& joint ) const {
    // compute the 'joint' residual
    // - each constraint has two elements
    // - for each element we have defined a pivot point in the local frame
    // - we need to derivatives of the alignment constants at this point in the global frame to the alignment
    // constants in the alignment frame (jacobian)
    // - we have also defined an 'uncertainty' for each of the 6 dofs
    // - we then derive the 6-dimensional residual in the global frame (will this be a problem if modules are
    // tilted? should we rather choose the local frame of the first module?)
    // - then add contributions to the chi2

    const auto& pivotA   = joint.pivotA;
    const auto& errors   = joint.errors;
    const auto& elementA = joint.elementA;
    const auto& elementB = joint.elementB;
    const auto  frameA   = elementA->alignmentFrame();
    const auto  frameB   = elementB->alignmentFrame();

    // create the transform for the frame where we compare the parameters
    Gaudi::Transform3D nominalA           = Element::toGlobalMatrixMinusDelta( elementA->detelements().front() );
    const auto         commonframe        = nominalA * ROOT::Math::Translation3D{pivotA[0], pivotA[1], pivotA[2]};
    const auto         fromGlobalToCommon = commonframe.Inverse();

    // now we need the deltas in this frame, and the jacobians
    JointConstraintResidual res;
    res.fromAlignmentAToCommon       = fromGlobalToCommon * frameA;
    res.fromAlignmentBToCommon       = fromGlobalToCommon * frameB;
    const AlParameters deltaA        = elementA->currentDelta();                         // in alignment frame A
    const AlParameters deltaB        = elementB->currentDelta();                         // in alignment frame B
    const auto         deltaA_common = deltaA.transformTo( res.fromAlignmentAToCommon ); // in common frame
    const auto         deltaB_common = deltaB.transformTo( res.fromAlignmentBToCommon ); // in common frame
    // std::cout << "Delta A (align,common): " << deltaA.transformParameters() << " "
    // 	  << deltaA_common.transformParameters() << std::endl
    // 	  << "Delta B (align,common): " << deltaB.transformParameters() << " "
    // 	  << deltaB_common.transformParameters() << std::endl ;
    res.residual = deltaB_common.transformParameters() - deltaA_common.transformParameters();
    res.errors   = errors;
    return res;
  } // namespace

  LHCb::ChiSquare AlignJointConstraints::addConstraints( Equations& equations, std::ostream& logmessage ) const {
    size_t totalnumconstraints( 0 );
    double totalchisq( 0 );
    double totalchi2_by_parameter[6] = {0, 0, 0, 0, 0, 0};

    for ( const auto& joint : m_constraints ) {
      // First compute the residual
      const auto  res      = residual( joint.second );
      const auto& elementA = joint.second.elementA;
      const auto& elementB = joint.second.elementB;

      // Now create the chi2 and the derivatives and add.
      // There are serious issues with 'auto' and 'S-matrix expressions', so make sure to assign temporaries to
      // concrete types.
      using Vector6   = JointConstraintResidual::Vector6;
      using Matrix6x6 = AlParameters::Matrix6x6;

      // create the diagonal weight matrix
      AlParameters::TransformCovariance W;
      for ( int i = 0; i < 6; ++i ) W( i, i ) = 1 / ( res.errors[i] * res.errors[i] );

      const Vector6 Wresidual = W * res.residual;
      totalchisq += ROOT::Math::Similarity( res.residual, W );
      totalnumconstraints += 6;
      const Matrix6x6 jacobianA  = AlParameters::jacobian( res.fromAlignmentAToCommon );
      const Matrix6x6 jacobianAT = ROOT::Math::Transpose( jacobianA );
      const Matrix6x6 jacobianB  = AlParameters::jacobian( res.fromAlignmentBToCommon );
      const Matrix6x6 jacobianBT = ROOT::Math::Transpose( jacobianB );
      // std::cout << "Jacobian A: " << std::endl << jacobianA << std::endl
      // 	   << "Jacobian B: " << std::endl << jacobianB << std::endl ;

      // pay attention: 'jacobian' is not the derivative of the
      // residual. for that you need to add an extra minus sign to
      // 'A'. however, for some reason all over the alignment coee
      // we have defined the first derivative witht he wrong
      // sign. (that's the FIXME below.)
      ElementData& elemdataA = equations.element( elementA->index() );
      // FIX ME: sign is exactly opposite of what you expect. See also AlignAlgorithm.
      elemdataA.m_dChi2DAlpha += jacobianAT * Wresidual;
      elemdataA.m_d2Chi2DAlpha2 += ROOT::Math::Similarity( jacobianAT, W );
      ElementData& elemdataB = equations.element( elementB->index() );
      // FIX ME: sign is exactly opposite of what you expect. See also AlignAlgorithm.
      elemdataB.m_dChi2DAlpha -= jacobianBT * Wresidual;
      elemdataB.m_d2Chi2DAlpha2 += ROOT::Math::Similarity( jacobianBT, W );
      // don't forget the off-diagonal element.
      const auto rowindex = elementA->index();
      const auto colindex = elementB->index();
      if ( rowindex < colindex ) {
        elemdataA.m_d2Chi2DAlphaDBeta[colindex].add( -1 * jacobianAT * W * jacobianB );
      } else if ( rowindex > colindex ) {
        elemdataB.m_d2Chi2DAlphaDBeta[rowindex].add( -1 * jacobianBT * W * jacobianA );
      }
      // Keep track of chi2 for the different dofs
      for ( int i = 0; i < 6; ++i ) totalchi2_by_parameter[i] += res.residual( i ) * res.residual( i ) * W( i, i );
    }
    if ( totalnumconstraints > 0 ) {
      logmessage << "Total chi2 of joint constraints: " << totalchisq << "/" << totalnumconstraints << std::endl
                 << "chi2 values for each degree of freedom:" << std::endl
                 << "Tx_chi2: " << totalchi2_by_parameter[0] << "/" << totalnumconstraints << std::endl
                 << "Ty_chi2: " << totalchi2_by_parameter[1] << "/" << totalnumconstraints << std::endl
                 << "Tz_chi2: " << totalchi2_by_parameter[2] << "/" << totalnumconstraints << std::endl
                 << "Rx_chi2: " << totalchi2_by_parameter[3] << "/" << totalnumconstraints << std::endl
                 << "Ry_chi2: " << totalchi2_by_parameter[4] << "/" << totalnumconstraints << std::endl
                 << "Rz_chi2: " << totalchi2_by_parameter[5] << "/" << totalnumconstraints << std::endl;
    }
    return LHCb::ChiSquare( totalchisq, totalnumconstraints );
  }

  LHCb::ChiSquare AlignJointConstraints::chiSquare( const Element& element, bool activeonly ) const {
    // FIXME: exploit that the weight matrix is diagonal. if that changes, we need to update this code (and the stuff
    // above)
    double chi2{0};
    int    ndof{0};
    for ( auto it = m_constraints.lower_bound( &element ); it != m_constraints.upper_bound( &element ); ++it ) {
      const auto res = residual( it->second );
      for ( int idof = 0; idof < 6; ++idof )
        if ( !activeonly || element.dofMask().isActive( idof ) ) {
          const double W = 1 / ( res.errors[idof] * res.errors[idof] );
          chi2 += res.residual( idof ) * W * res.residual( idof );
          ndof += 1;
        }
    }
    return LHCb::ChiSquare{chi2, ndof};
  }

} // namespace LHCb::Alignment
