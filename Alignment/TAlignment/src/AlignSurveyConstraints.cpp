/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AlignSurveyConstraints.h"
#include "ParsingHelpers.h"
#include "TAlignment/AlignmentElement.h"

namespace LHCb::Alignment {

  // The first part of this deals with 'parsing' the inputs

  using namespace ParsingHelpers;

  namespace {
    struct ConfiguredSurveyData {
      ConfiguredSurveyData() {}
      std::string    name;
      Gaudi::Vector6 par;
      Gaudi::Vector6 err;
      double         pivot[3] = {0, 0, 0};
    };

    struct XmlSurveyData {
      std::string    name;
      Gaudi::Vector6 par;
      double         pivot[3] = {0, 0, 0};
    };

    struct NamedXmlUncertainty {
      std::string name;
      double      err[6] = {1, 1, 1, 1, 1, 1};
    };

    using ConfiguredSurveyContainer = std::vector<ConfiguredSurveyData>;
    using XmlContainer              = std::map<std::string, XmlSurveyData>;
    using XmlUncertaintyContainer   = std::vector<NamedXmlUncertainty>;

    XmlContainer parseXmlFiles( const std::vector<std::string>& filenames, MsgStream& log ) {
      XmlContainer xmldata;
      for ( const auto& filename : filenames ) {
        // I tried to use the standard xml parser, but single xml files
        // are not self-contained, so that will never work. Terefore, I
        // have written soemthing very stupid that should work for xml
        // files with alignment conditions.
        std::ifstream ifs( filename.c_str() );
        if ( !ifs.is_open() ) {
          log << MSG::ERROR << "Cannot open file: " << filename << endmsg;
          throw GaudiException( "Cannot open xml file", __PRETTY_FUNCTION__, StatusCode::FAILURE );
        }

        while ( ifs ) {
          std::string linestr = assembleConditionLine( ifs );
          if ( !linestr.empty() ) {
            size_t ipos( 0 ), jpos( 0 );
            ipos                      = linestr.find( "name", 0 );
            ipos                      = linestr.find_first_of( "\"", ipos + 1 );
            jpos                      = linestr.find_first_of( "\"", ipos + 1 );
            std::string conditionname = linestr.substr( ipos + 1, jpos - ipos - 1 );

            ipos                             = linestr.find( "dPosXYZ", jpos + 1 );
            ipos                             = linestr.find_first_of( ">", ipos );
            jpos                             = linestr.find_first_of( "<", ipos + 1 );
            std::vector<std::string> numbers = tokenize( linestr.substr( ipos + 1, jpos - ipos - 1 ), " " );
            assert( numbers.size() == 3 );
            std::vector<double> pospars( 3, 0 );
            for ( size_t i = 0; i < 3; ++i ) pospars[i] = extractdouble( numbers[i] );

            // std::cout << "position string: "
            //<< std::endl << numbers << std::endl ;
            // sscanf( numbers.c_str(),"%g %g %g", &dx,&dy,&dz) ;

            ipos    = linestr.find( "dRotXYZ", jpos + 1 );
            ipos    = linestr.find_first_of( ">", ipos );
            jpos    = linestr.find_first_of( "<", ipos + 1 );
            numbers = tokenize( linestr.substr( ipos + 1, jpos - ipos - 1 ), " " );
            assert( numbers.size() == 3 );
            std::vector<double> rotpars( 3, 0 );
            for ( size_t i = 0; i < 3; ++i ) rotpars[i] = extractdouble( numbers[i] );

            ipos = linestr.find( "pivotXYZ", jpos + 1 );
            std::vector<double> pivotpars( 3, 0 );
            if ( ipos != std::string::npos ) {
              ipos    = linestr.find_first_of( ">", ipos );
              jpos    = linestr.find_first_of( "<", ipos + 1 );
              numbers = tokenize( linestr.substr( ipos + 1, jpos - ipos - 1 ), " " );
              assert( numbers.size() == 3 );
              for ( size_t i = 0; i < 3; ++i ) pivotpars[i] = extractdouble( numbers[i] );
            }
            // now put the result in the dictionary
            XmlSurveyData& entry = xmldata[conditionname];
            entry.name           = conditionname;
            for ( int i = 0; i < 3; ++i ) {
              entry.par[i]     = pospars[i];
              entry.par[i + 3] = rotpars[i];
              entry.pivot[i]   = pivotpars[i];
            }
          }
        }
      }
      return xmldata;
    }

    ConfiguredSurveyContainer parseConfiguredSurveyConstraints( const std::vector<std::string>& constraints,
                                                                MsgStream&                      log ) {
      // there are two types of definitions, namely those that look like
      //   " pattern : valTx valTy valTz valRx valRy valRz : errTx errTy errTz errRx errRy errRz" --> 6 values and 6
      //   errors
      // and the old type
      //   " pattern : Tx : val +/- err " or even " pattern : Tx : err "
      // new pattern type with a pivot point, where the constraint is defined in the element frame
      //    " pattern : valTx valTy valTz valRx valRy valRz : errTx errTy errTz errRx errRy errRz : pivotx pivoty
      //    pivotz"
      //    --> 6 values and 6 and 3 values
      ConfiguredSurveyContainer container;
      for ( const auto& element : constraints ) {
        log << MSG::DEBUG << "Parsing constraint: " << element << endmsg;
        std::vector<std::string> tokens = tokenize( element, ":" );
        log << MSG::DEBUG << "Number of tokens: " << tokens.size() << endmsg;
        if ( tokens.size() != 3 && tokens.size() != 4 ) {
          log << MSG::ERROR << "constraint has wrong number of tokens: " << element << endmsg;
          throw GaudiException( "constraint has wrong number of tokens", __PRETTY_FUNCTION__, StatusCode::FAILURE );
        } else {
          std::string name  = removechars( tokens[0], " " );
          auto&       entry = container.emplace_back( ConfiguredSurveyData{} );
          entry.name        = name;

          // first see if definition is of the old type
          const std::vector<std::string> dofnames =
              boost::assign::list_of( "Tx" )( "Ty" )( "Tz" )( "Rx" )( "Ry" )( "Rz" );
          std::string dofstr = removechars( tokens[1], " ," );
          size_t      dof    = std::distance( dofnames.begin(), std::find( dofnames.begin(), dofnames.end(), dofstr ) );
          if ( dof < dofnames.size() ) {
            log << MSG::ERROR << "constraint value has wrong number of tokens: " << element << endmsg;
            throw GaudiException( "constraint has wrong number of tokens", __PRETTY_FUNCTION__, StatusCode::FAILURE );
          } else {
            std::vector<std::string> numbers = tokenize( tokens[1], " " );
            if ( numbers.size() != 6 ) {
              log << MSG::ERROR << "constraint has wrong number of deltas: " << tokens[1] << endmsg;
              throw GaudiException( "constraint has wrong number of deltas", __PRETTY_FUNCTION__, StatusCode::FAILURE );
            } else {
              for ( size_t i = 0; i < 6; ++i ) entry.par[i] = boost::lexical_cast<double>( numbers[i] );
            }

            numbers = tokenize( tokens[2], " " );
            if ( numbers.size() != 6 ) {
              log << MSG::ERROR << "constraint has wrong number of errors: " << tokens[2] << endmsg;
              throw GaudiException( "constraint has wrong number of errors", __PRETTY_FUNCTION__, StatusCode::FAILURE );
            } else {
              for ( size_t i = 0; i < 6; ++i ) entry.err[i] = boost::lexical_cast<double>( numbers[i] );
            }

            if ( tokens.size() == 4 ) {
              numbers = tokenize( tokens[3], " " );
              if ( numbers.size() != 3 ) {
                log << MSG::ERROR << "constraint has wrong number of pivot coordinates: \'" << tokens[3] << "\', "
                    << numbers << endmsg;
                throw GaudiException( "constraint has wrong number of pivot coordinates", __PRETTY_FUNCTION__,
                                      StatusCode::FAILURE );
              } else {
                for ( size_t i = 0; i < 3; ++i ) entry.pivot[i] = boost::lexical_cast<double>( numbers[i] );
              }
            }
          }
        }
      }
      return container;
    }

    XmlUncertaintyContainer parseXmlUncertainties( const std::vector<std::string>& patterns, MsgStream& log ) {

      XmlUncertaintyContainer container;
      for ( const auto& ipattern : patterns ) {
        // now still parse the patterns with XML uncertainties
        std::vector<std::string> tokens = tokenize( ipattern, ":" );
        if ( tokens.size() != 2 ) {
          log << MSG::ERROR << "xml uncertainty pattern has wrong number of tokens: " << ipattern << endmsg;
          throw GaudiException( "xml uncertainty pattern has wrong number of tokens", __PRETTY_FUNCTION__,
                                StatusCode::FAILURE );
        }
        NamedXmlUncertainty data;
        data.name              = removechars( tokens[0], " " );
        const auto valuetokens = tokenize( tokens[1], " " );
        if ( valuetokens.size() != 6 ) {
          log << MSG::ERROR << "xml uncertainty pattern has wrong number of errors: " << tokens[1] << endmsg;
          throw GaudiException( "xml uncertainty pattern has wrong number of errors", __PRETTY_FUNCTION__,
                                StatusCode::FAILURE );
        }
        for ( int i = 0; i < 6; ++i ) data.err[i] = boost::lexical_cast<double>( valuetokens[i] );
        container.emplace_back( data );
      }
      return container;
    }

    const NamedXmlUncertainty* findXmlUncertainty( const XmlUncertaintyContainer& container, const std::string& name,
                                                   MsgStream& log ) {
      // always take the last one:
      const NamedXmlUncertainty* rc{0};
      for ( const auto& entry : container ) {
        if ( match( name, entry.name ) ) rc = &entry;
      }
      if ( rc ) { log << MSG::DEBUG << "Matched " << name << " to " << rc->name << " " << rc->err << endmsg; }
      return rc;
    }
  } // namespace

  AlignSurveyConstraints::AlignSurveyConstraints( const Elements&                 elements,
                                                  const std::vector<std::string>& constraintnames,
                                                  const std::vector<std::string>& xmlfilenames,
                                                  const std::vector<std::string>& xmluncertaintynames,
                                                  MsgStream&                      log ) {
    // First do the parsing
    const auto xmldata          = parseXmlFiles( xmlfilenames, log );
    const auto xmluncertainties = parseXmlUncertainties( xmluncertaintynames, log );
    const auto configuredsurvey = parseConfiguredSurveyConstraints( constraintnames, log );

    log << MSG::INFO << "Number of entries in xml data: " << xmldata.size() << endmsg;
    log << MSG::INFO << "Number of entries specified by job-option: " << configuredsurvey.size() << endmsg;

    // read all survey parameters
    auto& surveypars = m_surveyParameters;
    for ( const auto& elemp : elements ) {
      const auto&       element = *elemp;
      ElementSurveyData newsurvey;

      // did we add information for this alignable explicitely? note
      // that we take the last one that matches.
      bool found = false;
      for ( auto it = configuredsurvey.begin(); it != configuredsurvey.end(); ++it )
        // match the name of the alignable, or, if there is only one element, match the name of the condition
        if ( element.name() == it->name || match( element.name(), it->name ) ) {
          log << MSG::DEBUG << "Matched element to configured survey" << element.name() << endmsg;
          newsurvey.par   = it->par;
          newsurvey.err   = it->err;
          newsurvey.pivot = Gaudi::XYZVector( it->pivot[0], it->pivot[1], it->pivot[2] );
          found           = true;
        }

      if ( !found ) {
        log << MSG::DEBUG << "Did not find any matching configured survey for alignable: " << element.name() << endmsg;

        // OK. Here is the new strategy which should also work for
        // groups of elements. Make an average of the xml found for all
        // daughters. Assign the errors based on the xml-error
        // patterns. Let's first look for that pattern.

        // for the error pattern we have the following rule:
        // - first we match the name of the alignable
        // - if there is only a single element, we then try to match the name of the condition
        // - if there is a single element, we then try to match the name of the element
        // extract the condition name, but only if this alignable has only one detector element

        // decltype needed for compatibility with DetDEsc and DD4hep
        // To be dropped when DetDesc is gone
        // The resulting type is basically AlignmentCondition*
        const auto* errors = findXmlUncertainty( xmluncertainties, element.name(), log );
        if ( errors == 0 && element.detelements().size() == 1 ) {
          if ( ( element.detelements().front().alignmentCondition() ) ) {
            std::string condname = element.detelements().front().alignmentConditionName();
#ifndef USE_DD4HEP
            // need to remove a leading slash
            condname = condname.substr( 1 );
#endif
            errors = findXmlUncertainty( xmluncertainties, condname, log );
          }
          if ( !errors ) errors = findXmlUncertainty( xmluncertainties, element.detelements().front().name(), log );
        }

        if ( !errors ) {
          log << MSG::ERROR << "Cannot find XML uncertainties for alignable: " << element.name() << endmsg;
        } else {
          for ( int i = 0; i < 6; ++i ) newsurvey.err[i] = errors->err[i];

          // loop over the detelements in this alignable to find the survey
          AlParameters::TransformParameters sumparameters;
          size_t                            numfound( 0 );
          for ( const auto& detelem : element.detelements() ) {
            // find this element in the XML catalogue
            std::string condname;
            if ( ( detelem.alignmentCondition() ) ) {
              condname = detelem.alignmentConditionName();
#ifndef USE_DD4HEP
              // need to remove a leading slash
              condname = condname.substr( 1 );
#endif
            }
            const auto it = xmldata.find( condname );
            if ( it == xmldata.end() ) {
              log << MSG::ERROR << "Cannot find condition \"" << condname << "\" for alignable " << element.name()
                  << " in survey dictionary." << endmsg;
            } else {
              log << MSG::DEBUG << "Found matching condition \"" << condname << "\" for alignable " << element.name()
                  << endmsg;
              ++numfound;
              const XmlSurveyData& survey = it->second;
              // if we have multiple elements, and each has their own survey, then we somehow need to make an average.
              // for that we transform the parameters from the survey frame of each element into the survey frame of
              // this element
              if ( element.detelements().size() == 1 ) {
                newsurvey.pivot = Gaudi::XYZVector{survey.pivot[0], survey.pivot[1], survey.pivot[2]};
                newsurvey.par   = survey.par;
              } else {
                Gaudi::Transform3D              nominalFrame = Element::toGlobalMatrixMinusDelta( detelem );
                AlParameters                    surveypars( survey.par );
                const ROOT::Math::Translation3D pivot( survey.pivot[0], survey.pivot[1], survey.pivot[2] );
                Gaudi::Transform3D              surveyFrame = nominalFrame * pivot;
                Gaudi::Transform3D fromSurveyToMotherSurvey = newsurvey.surveyFrame( element ).Inverse() * surveyFrame;
                AlParameters       surveyParameters         = surveypars.transformTo( fromSurveyToMotherSurvey );
                sumparameters += surveyParameters.transformParameters();
              }
            }
          }

          if ( numfound == element.detelements().size() ) {
            // use the average of the daughters
            if ( numfound > 1 ) {
              // now solve a peculiar problem: if we share xml parameters
              // with the daughters (because there is no xml for this
              // alignable) set the survey for these parameters to
              // 0. otherwise, it cannot converge.
              std::vector<int> shareddofs = element.redundantDofs();
              for ( auto idof : shareddofs ) sumparameters( idof ) = 0;
              // set this to the average
              newsurvey.par = sumparameters / numfound;
            }
            found = true;
          }
        }
      }
      if ( !found ) {
        log << MSG::WARNING << "Haven't been able to construct survey for " << element.name() << endmsg;
      } else {
        // issue a warning if any of the error is too small
        for ( int i = 0; i < 6; ++i )
          if ( !( newsurvey.err[i] > std::numeric_limits<float>::min() ) )
            log << MSG::ERROR << "Survey error is not positive: " << element.name() << " " << i << " "
                << newsurvey.err[i] << endmsg;
        surveypars[&element] = newsurvey;
      }
    }
  }

  LHCb::ChiSquare AlignSurveyConstraints::addConstraints( Equations& equations, std::ostream& logmessage ) const {
    size_t totalnumconstraints( 0 );
    double totalchisq( 0 );
    for ( const auto& survey : m_surveyParameters ) {
      // this translates the survey constraints to the alignment frame. for now I decided that we better translate the
      // alignment parameters to the survey frame.
      const auto&  elem     = survey.first;
      ElementData& elemdata = equations.element( elem->index() );
#undef APPLYSURVEYINALIGNMENTFRAME
#ifdef APPLYSURVEYINALIGNMENTFRAME
      const auto&                       surveypars   = survey.second.asParametersInAlignmentFrame( *elem );
      const auto                        currentdelta = elem->currentDelta();
      AlParameters::TransformParameters residual =
          surveypars.transformParameters() - currentdelta.transformParameters();
      AlParameters::TransformCovariance weight = surveypars.transformCovariance();
      weight.InvertChol();
      elemdata.m_dChi2DAlpha += weight * residual;
      elemdata.m_d2Chi2DAlpha2 += weight;
#else
      const auto&                   surveypars            = survey.second.asParametersInSurveyFrame();
      const auto                    surveyframe           = survey.second.surveyFrame( *elem );
      const auto                    fromAlignmentToSurvey = surveyframe.Inverse() * elem->alignmentFrame();
      const auto                    currentdelta          = elem->currentDelta().transformTo( fromAlignmentToSurvey );
      const AlParameters::Matrix6x6 jacobianT =
          ROOT::Math::Transpose( AlParameters::jacobian( fromAlignmentToSurvey ) );
      AlParameters::TransformParameters residual =
          surveypars.transformParameters() - currentdelta.transformParameters();
      AlParameters::TransformCovariance weight = surveypars.transformCovariance();
      weight.InvertChol();
      elemdata.m_dChi2DAlpha += jacobianT * weight * residual;
      elemdata.m_d2Chi2DAlpha2 += ROOT::Math::Similarity( jacobianT, weight );
#endif
      totalchisq += ROOT::Math::Similarity( residual, weight );
      totalnumconstraints += 6;
    } // end if elem.isActive
    logmessage << "Total chisquare of survey constraints: " << totalchisq << " / " << totalnumconstraints << std::endl;
    return LHCb::ChiSquare( totalchisq, totalnumconstraints );
  }

  LHCb::ChiSquare AlignSurveyConstraints::chiSquare( const Element& element, bool activeonly ) const {
    size_t     totalnumconstraints( 0 );
    double     totalchisq( 0 );
    const auto psurvey = surveyParameters( element );
    if ( psurvey ) {
      const AlParameters&               surveypars   = *psurvey;
      AlParameters                      currentdelta = element.currentDelta();
      AlParameters::TransformParameters residual =
          surveypars.transformParameters() - currentdelta.transformParameters();
      AlParameters::TransformCovariance weight = surveypars.transformCovariance();
      totalnumconstraints += 6;
      if ( activeonly ) {
        // this is extremely lazy: just set errors on inactive parameters 'big'
        const double largeerrors[6] = {1000, 1000, 1000, 1, 1, 1};
        for ( int idof = 0; idof < 6; ++idof )
          if ( !element.dofMask().isActive( idof ) ) {
            weight( idof, idof ) += largeerrors[idof] * largeerrors[idof];
            --totalnumconstraints;
          }
      }
      weight.InvertChol();
      totalchisq += ROOT::Math::Similarity( residual, weight );
    }
    return LHCb::ChiSquare( totalchisq, totalnumconstraints );
  }

} // namespace LHCb::Alignment
