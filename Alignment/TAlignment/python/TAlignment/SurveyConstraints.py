from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from DDDB.CheckDD4Hep import UseDD4Hep
from PyConf.application import ApplicationOptions

# Dictionary with all the available XMLfiles
XmlFilesDict = {
    'VP': {
        'data2023': ['Global_Design', 'Modules_dd4hep']
        if UseDD4Hep else ['Global_Design_detdesc', 'Modules'],
        '2023_dd4hep': ['Global_Design',
                        'Modules_dd4hep'],  # sensor TxTyRz survey values
        'design': ['Global_Design', 'Modules_Design']
        if UseDD4Hep else ['Global_Design_detdesc', 'Modules_Design_detdesc'],
        'module_sensor_survey': [
            'Global_Design', 'Modules_ModSensSurvey'
        ],  # module TxTyTzRz sensor TxTyRz survey values
        'module_sensor_survey_avg': [
            'Global_ModSensSurvey_avg', 'Modules_ModSensSurvey_avg'
        ],  # module and sensor TxTyTzRxRyRz survey values. Avgerage Tx and Ty of modules attributed to halves
    },
    'UT': {
        'MC': [],
        'data_alignment_20240604': [
            "HalfLayers_alignment_20240604",
            "Staves_alignment_20240604",
            "Faces_alignment_20240604",
            "Modules_alignment_20240604",
        ],
        'data_alignment_20240703': [
            "HalfLayers_alignment_20240703",
            "Staves_alignment_20240703",
            "Faces_alignment_20240703",
            "Modules_alignment_20240703",
        ],
        'data_alignment_20240706': [
            "HalfLayers_alignment_20240706",
            "Staves_alignment_20240706",
            "Faces_alignment_20240706",
            "Modules_alignment_20240706",
        ],
        'data_survey_YETS23_24_Layers_Staves': [
            "HalfLayers_survey_YETS23-24",
            "Staves_survey_YETS23-24",
            "Faces_ideal",
            "Modules_ideal",
        ]
    },
    'FT': {
        'MC': ['Modules_default_dd4hep']
        if UseDD4Hep else ['Modules_default_detdesc'],
        'data2023':
        (['CFrames_surveyInput_2023', 'Modules_default_dd4hep'] if UseDD4Hep
         else ['CFrames_surveyInput_2023_detdesc', 'Modules_default_detdesc']),
        'data20220809': ['Modules_surveyInput_20220809'],
        'data20221115': ['Modules_surveyInput_20221115'],  #beam angle fix
        'data20221115dd4hep': ['Modules_surveyInputdd4hep_20221115'],
    },
}


### This class is used to specify the version of the XML for the different constraints
class SurveyVersion:
    __slots__ = {'FT': '', 'VP': '', 'UT': '', 'Muon': ''}

    def __init__(self, version: str):
        if version == 'MC':
            self.VP = 'design'
            self.UT = 'MC'
            self.FT = 'MC'
            self.Muon = 'MC'
        elif version == 'data2023':
            self.VP = 'module_sensor_survey_avg'
            self.UT = 'data_survey_YETS23_24_Layers_Staves'
            self.FT = 'data2023'
            self.Muon = 'data2023'
        else:
            raise ValueError(
                f"SurveyVersion: cannot deal with input version={version}")


def surveyVersionFromOptions(options: ApplicationOptions):
    # this can be made more complicated if we want to switch by year etc
    return SurveyVersion('MC' if options.simulation else 'data2023')


class SurveyConstraints():

    __slots__ = {
        "Constraints":
        []  # List of survey constraints for particular alignment element
        ,
        "XmlFiles": [
        ]  # List of xml files with alignment constants from survey
        ,
        "XmlUncertainties": []  # List of uncertainties for survey from xml
        ,
        "DefaultSurveyDir":
        ""  # Directory from where to take survey constants
        ,
        "ElementJoints": [
        ]  # list of constraints to 'join' alignables (e.g. FT halfmodules) together
    }

    def __init__(self):
        self.Constraints = []
        self.XmlFiles = []
        self.XmlUncertainties = []
        self.ElementJoints = []
        import os
        self.DefaultSurveyDir = os.getenv("TALIGNMENTROOT") + "/surveyxml/"

    def defaultSurveyDir(self):
        return self.DefaultSurveyDir

    def VP(self, ver: str = 'design'):
        if not ver in XmlFilesDict['VP']:
            print('WARNING(SurveyConstraints): Survey constraint version ' +
                  ver + ' not found for Velo. Using \'design\'')
            ver = 'design'
        for f in XmlFilesDict['VP'][ver]:
            self.XmlFiles += [self.defaultSurveyDir() + "VP/" + f + ".xml"]

        VPGlobal = "VP" if UseDD4Hep else "VPSystem"
        VPHalves = "VP/.*VP(Left|Right)"
        VPModules = "VP/.*/Module.." if UseDD4Hep else "VP/.*/Module..WithSupport"
        VPSensors = "VP/.*ladder_." if UseDD4Hep else "VP/.*/Ladder."

        self.XmlUncertainties += [
            f"{VPGlobal} : 0.5 0.5 0.5 0.0001 0.0001 0.001",
            f"{VPHalves} : 0.5 0.5 0.5 0.0001 0.0001 0.001",
            f"{VPModules} : 0.05 0.05 0.1 0.001 0.001 0.001",
            f"{VPSensors} : 0.005 0.005 0.01 0.0002 0.0002 0.0001"
        ]

    def FT(self, ver='data2023', addHalfModuleJoints: bool = False):
        if type(ver) != str: ver = surveyVersionFromOptions(ver)
        if not ver in XmlFilesDict['FT']:
            print('WARNING(SurveyConstraints): Survey constraint version ' +
                  ver + ' not found for SciFi. Using \'latest\'')
            ver = 'latest'
        for f in XmlFilesDict['FT'][ver]:
            self.XmlFiles += [self.defaultSurveyDir() + "FT/" + f + ".xml"]

        FTSystem = "FT"
        FTStations = "FT/T."
        FTCFrames = "FT/T./(X1U|VX2)/HL."
        FTLayers = "FT/T./(X1|U|V|X2)" if UseDD4Hep else "FT/T./Layer(X1|U|V|X2)"
        FTHalfLayers = FTLayers + "/HL."
        FTModules = "FT/.*/HL./M." if UseDD4Hep else "FT/.*/Module."
        FTHalfModules = "FT/T./(X1|X2|U|V)/HL./Q./M." if UseDD4Hep else "FT/.*/Module."
        FTMats = "FT/.*Mat."
        self.XmlUncertainties += [
            f"{FTSystem}:     1 1 1 0.0003 0.0003 0.0003",
            f"{FTStations}:   1 1 1 0.0003 0.0003 0.0003",
            f"{FTHalfLayers}: 0.2 0.2 0.2 0.00008 0.0002 0.00008",
            f"{FTCFrames}:    0.2 0.2 0.2 0.00008 0.0002 0.00008",
            f"{FTModules}:    0.1 0.1 0.1 0.00004 0.0001 0.00004",
            f"{FTHalfModules} : 0.1 0.001 0.001 0.00008 0.0004 0.00008"
        ]

        # add halfmodules with a tight z constraint to the readout side
        # (rotations may be a bit too tight)
        #from TAlignment.Alignables import Alignables
        #self.XmlUncertainties += [f"{FTHalfModules} : 0.1 0.001 0.001 0.00008 0.0004 0.00008 : %s"
        #                         % Alignables()._FTHalfModuleReadoutSideTranslation ]

        # setup the module uncertainties depending on the data version
        ftmoduleuncertainties = "0.1 0.1 0.1 0.00004 0.0001 0.00004"
        if 'data' in ver:
            # SciFi test constraints v0.5
            if "202211" in ver:
                # Module/quarter uncertainties expanded
                ftmoduleuncertainties = "0.1 0.5 0.5 0.0002 0.0008 0.00005"
            elif "2023" in ver:
                #The position of the CFrames is constrained from the survey information, the modules are just constrained to their nominal positions within
                #the CFrames. This option should only be used when including the CFrames or HalfLayers as alignable objects, otherwise the module uncertainties
                #would be underestimated.
                ftmoduleuncertainties = "0.1 0.1 0.1 0.00004 0.0001 0.00004"
            else:
                # Module/quarter uncertainties estimated for 20220809 survey
                ftmoduleuncertainties = "0.2 0.2 0.2 0.00008 0.0004 0.00008"
        self.XmlUncertainties += [f"{FTModules}:{ftmoduleuncertainties}"]

        # unfortunately we need this also for data until we find a better way to deal with survey input
        self.Constraints += [
            f"{FTSystem}    : 0 0 0 0 0 0 : 1 1 1 0.0003 0.0003 0.0003",
            f"{FTStations}  : 0 0 0 0 0 0 : 0.5 0.5 0.5 0.001 0.001 0.001",
            f"{FTCFrames}   : 0 0 0 0 0 0 : 0.5 0.5 0.5 0.0001 0.0001 0.0001",
            f"{FTLayers}    : 0 0 0 0 0 0 : 0.5 0.5 0.5 0.0001 0.0001 0.0001",
            f"{FTHalfLayers}: 0 0 0 0 0 0 : 0.2 0.2 0.2 0.00008 0.0001 0.00008",
        ]

        # SciFi test constraints v0.1
        # translation constraints (somewhat) realistic
        if 'MC' in ver:
            self.Constraints += [
                # allow for an Rz of ~1mm/1m around the readout side (y=1213mm) of the half module
                f"{FTHalfModules} :  0 0 0 0 0 0 : 0.2 0.2 0.2 0.0002 0.0002 0.0002 : 0 +1213 0",
                f"{FTModules}     :  0 0 0 0 0 0 : 0.2 0.2 0.2 0.0002 0.0002 0.0002",
                f"{FTMats}        : 0 0 0 0 0 0 : 0.05 0.05 0.05 0.1 0.1 0.1"
            ]
        if addHalfModuleJoints:
            from TAlignment.Alignables import Alignables
            self.ElementJoints += Alignables().FTHalfModuleJoints()

    def UT(self, ver: str):
        if type(ver) != str: ver = surveyVersionFromOptions(ver)
        # for now, just constrain to the default geometry, probably not realistic
        if ver in XmlFilesDict['UT']:
            for f in XmlFilesDict['UT'][ver]:
                self.XmlFiles += [self.defaultSurveyDir() + "UT/" + f + ".xml"]
        else:
            print(
                'WARNING(SurveyConstraints): Survey constraint version %s not found for UT.'
                % ver)
        alignables = {
            "system": "UT",
            "sides": "UT/.*side",
            "halflayers": "UT/.*UT(aX|aU|bV|bX)",
            "staves": "UT/.*Stave.",
            "faces": "UT/.*Face.",
            "modules": "UT/.*Module."
        } if UseDD4Hep else {
            "system": "UT",
            "sides": "UT/UT(A|C)",
            "halflayers": "UT/UT(A|C)/UT(aX|aU|bV|bX)(A|C)sideLayer",
            "staves": "UT/.*/R.Stave.",
            "faces": "UT/.*(Back|Front)",
            "modules": "UT/.*/Module.{1,2}"
        }
        self.XmlUncertainties += [
            f"{alignables['system']} : 1.0 1.0 1.0 0.002 0.002 0.005",
            f"{alignables['sides']} : 1.0 1.0 1.0 0.002 0.002 0.005",
            f"{alignables['halflayers']} : 1.0 1.0 1.0 0.002 0.002 0.005",
            f"{alignables['staves']} : 0.5 0.5 0.5 0.001 0.001 0.001",
            f"{alignables['faces']} : 0.2 0.2 0.2 0.001 0.001 0.001",
            f"{alignables['modules']} : 0.1 0.1 0.1 0.0001 0.0001 0.0001"
        ]
        if 'data' in ver:
            if "survey" in ver:
                self.XmlUncertainties += [
                    f"{alignables['modules']} : 0.4 0.4 1.0 0.01 0.01 0.01",
                ]
            if "alignment" in ver:
                self.XmlUncertainties += [
                    f"{alignables['modules']} : 0.06 0.2 0.5 0.0 0.0 0.002",
                ]
                # this is needed until we have separate survey for system and sides
            self.Constraints += [
                f"{alignables['system']} : 0 0 0 0 0 0 : 0.5 0.5 0.5 0.0001 0.0001 0.0001",
                f"{alignables['sides']} : 0 0 0 0 0 0 : 0.5 0.5 0.5 0.0001 0.0001 0.0001"
            ]
        if 'MC' in ver:
            self.Constraints += [
                f"{alignables['system']} : 0 0 0 0 0 0 : 1.0 1.0 1.0 0.002 0.002 0.005",
                f"{alignables['sides']} : 0 0 0 0 0 0 : 1.0 1.0 1.0 0.002 0.002 0.005",
                f"{alignables['halflayers']} : 0 0 0 0 0 0 : 1.0 1.0 1.0 0.002 0.002 0.005",
                f"{alignables['staves']} : 0 0 0 0 0 0 :  0.5 0.5 0.5 0.001 0.001 0.001",
                f"{alignables['faces']} : 0 0 0 0 0 0 : 0.2 0.2 0.2 0.001 0.001 0.001",
                f"{alignables['modules']} : 0 0 0 0 0 0 : 0.1 0.1 0.1 0.0001 0.0001 0.0001",
            ]

    def MUON(self, ver: str = ''):
        MuonStation = "Muon/M.Station/M.(A|C)Side" if UseDD4Hep else "Muon/M./M.(A|C)Side"
        self.Constraints += [
            f"${MuonStation} :  0 0 0 0 0 0 : 1 1 3 0.001 0.005 0.001"
        ]

    def All(self, ver: SurveyVersion):
        if ver.VP: self.VP(ver.VP)
        if ver.UT: self.UT(ver.UT)
        if ver.FT: self.FT(ver.FT)
        if ver.Muon: self.MUON(ver.Muon)
        self.Constraints += [
            "Tracker : 0 0 0 0 0 0 : 10 10 10 0.1 0.1 0.1",
            "TStations : 0 0 0 0 0 0 : 10 10 10 0.1 0.1 0.1"
        ]


def surveyConstraintsFromOptions(options: ApplicationOptions):
    survey = SurveyConstraints()
    survey.All(surveyVersionFromOptions(options))
    return survey
