from __future__ import print_function

###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from DDDB.CheckDD4Hep import UseDD4Hep


class Alignables(list):
    WORLD = "/dd/Structure/LHCb" if not UseDD4Hep else "/world"

    def __init__(self, elements=None, dofs=""):
        self.m_dofs = ["Tx", "Ty", "Tz", "Rx", "Ry", "Rz", "None"]

        self.m_vpBase = self.WORLD + "/BeforeMagnetRegion/VP"
        self.m_vp = self.m_vpBase
        self.m_vpSideBase = self.m_vpBase + ("/MotionVP(Left|Right)"
                                             if UseDD4Hep else "")
        self.m_vpLeft = self.m_vpSideBase + "/VPLeft"
        self.m_vpRight = self.m_vpSideBase + "/VPRight"
        self.m_vpModules = self.m_vpSideBase + "/VP(Right|Left)/Module.{1,2}" + (
            "WithSupport" if not UseDD4Hep else "")
        self.m_vpModulesLeft = self.m_vpSideBase + "/VPLeft/Module.{1,2}" + (
            "WithSupport" if not UseDD4Hep else "")
        self.m_vpModulesRight = self.m_vpSideBase + "/VPRight/Module.{1,2}" + (
            "WithSupport" if not UseDD4Hep else "")
        self.m_vpSensors = self.m_vpModules + ("/Module.{1,2}/Ladder." if
                                               not UseDD4Hep else "/ladder_.")

        self.m_ut = self.WORLD + "/BeforeMagnetRegion/UT"
        self.m_utSides = self.m_ut + ('/(C|A)side' if UseDD4Hep else '/UT.')
        self.m_utLayers = self.m_utSides + ('/UT(aX|aU|bV|bX)'
                                            if UseDD4Hep else '/.*Layer')
        self.m_utStaves = self.m_utLayers + ('/Stave.'
                                             if UseDD4Hep else '/R.Stave.')
        self.m_utFaces = self.m_utStaves + ('/Face.' if UseDD4Hep else
                                            '/R.Stave.(Front|Back)')
        self.m_utModules = self.m_utFaces + '/Module(.|..)'

        self.m_tt = self.WORLD + "/BeforeMagnetRegion/TT"
        self.m_ttStations = ["TTa", "TTb"]
        self.m_ttLayers = [
            "TTa/TTaXLayer",
            "TTa/TTaULayer",
            "TTb/TTbVLayer",
            "TTb/TTbXLayer",
        ]

        self.m_muon = self.WORLD + "/DownstreamRegion/Muon"
        self.m_mustations = ["/M2", "/M3", "/M4", "/M5"]
        self.m_muhalfstationsAC = ["/M.ASide", "/M.CSide"]
        self.m_muhalfstations = ["/M.{1,2}Side"]
        self.m_muchambers = ["/R.{1,2}Side/Cham.{1,3}"]

        if elements and self.__validElements(elements):
            self.__append(elements, dofs)

    def __findDOF(self, dof, dofs):
        valid = ""
        if not dofs.find(dof) == -1:
            valid = dof

        return valid

    def __addDoFs(self, elements, dofs):
        valid = ""
        invalid = dofs
        tmp = []

        for i in range(len(self.m_dofs)):
            valid += self.__findDOF(self.m_dofs[i], invalid)
            invalid = invalid.replace(self.m_dofs[i], "")
        # remove eventual separators
        import re
        invalid = re.sub("[,:; ]", '', invalid)

        if valid:
            if isinstance(elements, list):
                for i in range(len(elements)):
                    tmp.append(elements[i] + ":%s" % valid)
            elif isinstance(elements, str):
                tmp.append(elements + ":%s" % valid)

        try:  ## asserting whether invalid is not empty
            assert (not invalid)
        except AssertionError:
            print("ERROR: Unknown degree of freedom(s) " + '"' + invalid + '"')
            print(
                "       Valid degrees of freedom are Tx, Ty, Tz, Rx, Ry and Rz"
            )
            return []

        return tmp

    def __validElements(self, elements):
        try:
            assert isinstance(elements, list) or isinstance(elements, str)
        except AssertionError:
            self.__elementsError(elements)
            return None

        ## Paranoia; extend also throws an exception
        if isinstance(elements, str):
            for i in range(len(elements)):
                try:
                    assert isinstance(elements[i], str)
                except AssertionError:
                    self.__elementsError(elements)
                    return None
        return True

    def __elementsError(self, elements):
        print("ERROR: Specify either a string representing a detector element")
        print(
            "       or a list of strings representing a list of detector elements"
        )
        print("elements = ", elements)

    def __append(self, elements, dofs):
        if dofs:
            tmp = self.__addDoFs(elements, dofs)
            self.extend(tmp)
        else:
            if isinstance(elements, list):
                self.extend(elements)
            elif isinstance(elements, str):
                self.append(elements)
            assert ValueError()

    ## Some predefined alignables

    ## Global ###############################################################################
    def Tracker(self, dofs=""):
        elements = []
        elements.append("Tracker : " + self.m_vp)
        elements.append("Tracker : " + self.m_ut)
        elements.append("Tracker : " + self._FT)
        #elements.append( "Tracker : " + self.m_muon )
        self.__append(elements, dofs)

    ## VP #################################################################################
    def VP(self, dofs=""):
        self.__append(self.m_vp, dofs)

    def VPModules(self, dofs=""):
        elements = []
        elements.append(self.m_vpModules)
        self.__append(elements, dofs)

    def VPModulesLeft(self, dofs=""):
        elements = []
        elements.append(self.m_vpModulesLeft)
        self.__append(elements, dofs)

    def VPModulesRight(self, dofs=""):
        elements = []
        elements.append(self.m_vpModulesRight)
        self.__append(elements, dofs)

    def VPSensors(self, dofs=""):
        elements = []
        elements.append(self.m_vpSensors)
        self.__append(elements, dofs)

    def VPRight(self, dofs=""):
        self.__append(self.m_vpRight, dofs)

    def VPLeft(self, dofs=""):
        self.__append(self.m_vpLeft, dofs)

    ## UT #################################################################################
    def UT(self, dofs=""):
        self.__append(self.m_ut, dofs)

    def UTSides(self, dofs=""):
        self.__append(self.m_utSides, dofs)

    def UTLayers(self, dofs=""):
        self.__append(self.m_utLayers, dofs)

    def UTStaves(self, dofs=""):
        self.__append(self.m_utStaves, dofs)

    def UTFaces(self, dofs=""):
        self.__append(self.m_utFaces, dofs)

    def UTModules(self, dofs=""):
        self.__append(self.m_utModules, dofs)

    ## FT #################################################################################
    _FT = WORLD + "/AfterMagnetRegion/T/FT"
    _FTStations = ["/T1", "/T2", "/T3"]
    _FTLayers = ["/LayerX1", "/LayerU", "/LayerV", "/LayerX2"
                 ] if not UseDD4Hep else ["/X1", "/U", "/V", "/X2"]
    _FTRealQuarters = [
        "/Quarter0", "/Quarter2", "/Quarter1", "/Quarter3"
    ] if not UseDD4Hep else ["/HL0/Q0", "/HL0/Q2", "/HL1/Q1", "/HL1/Q3"]
    _FTHalfLayerQuarters = {
        "/HL0": "/Quarter(0|2)" if not UseDD4Hep else "/HL0/Q(0|2)",
        "/HL1": "/Quarter(1|3)" if not UseDD4Hep else "/HL1/Q(1|3)"
    }
    _FTHalfLayers = {
        "/HL0": "/Quarter(0|2)" if not UseDD4Hep else "/HL0",
        "/HL1": "/Quarter(1|3)" if not UseDD4Hep else "/HL1"
    }
    _FTCFrameLayers = {
        "/X1U": ("/Layer" if not UseDD4Hep else "/") + "(X1|U)",
        "/VX2": ("/Layer" if not UseDD4Hep else "/") + "(V|X2)"
    }

    _FTModulesAll = [("/Module%d" if not UseDD4Hep else "/M%d") % i
                     for i in range(6)]
    _FTModulesShort = [("/Module%d" if not UseDD4Hep else "/M%d") % i
                       for i in range(5)]
    _FTMats = ["/Mat%d" % i for i in range(4)]
    _FTHalfModuleReadoutSideTranslation = "0 +1212.75 0"
    _FTHalfModuleMirrorSideTranslation = "0 -1212.75 0"

    _FTStationsRE = _FT + "/T."
    _FTLayersRE = _FTStationsRE + ("/"
                                   if UseDD4Hep else "/Layer") + "(X1|U|V|X2)"
    _FTHalfLayersRE = _FTLayersRE + ("/HL." if UseDD4Hep else ""
                                     )  # only works for DD4HEP
    _FTQuartersRE = _FTHalfLayersRE + ("/Q." if UseDD4Hep else "/Quarter.")
    _FTHalfModulesRE = _FTQuartersRE + ("/M." if UseDD4Hep else "/Module.")
    _FTMatsRE = _FTHalfModulesRE + "/Mat."

    def FT(self, dofs=""):
        self.__append(self._FT, dofs)

    def FTStations(self, dofs=""):
        self.__append(self._FTStationsRE, dofs)

    def FTLayers(self, dofs=""):
        self.__append(self._FTLayersRE, dofs)

    def FTHalfLayers(self, dofs=""):
        if UseDD4Hep: self.__append(self._FTHalfLayersRE, dofs)
        else:
            elements = []
            for station in self._FTStations:
                for layer in self._FTLayers:
                    for (side, halflayerquarters
                         ) in self._FTHalfLayerQuarters.items():
                        elements.append("FT" + station + layer + side + ":" +
                                        self._FT + station + layer +
                                        halflayerquarters)
            self.__append(elements, dofs)

    def FTQuarters(self, dofs=""):
        self.__append(self._FTQuartersRE, dofs)

    def FTCFrames(self, dofs=""):
        elements = []
        for station in self._FTStations:
            for (cframelayername, cframelayer) in self._FTCFrameLayers.items():
                for (side, halflayer) in self._FTHalfLayers.items():
                    elements.append("FT" + station + cframelayername + side +
                                    " : " + self._FT + station + cframelayer +
                                    halflayer)
        self.__append(elements, dofs)

    def FTCFrameLayers(self, dofs=""):
        elements = []
        for station in self._FTStations:
            for layer in self._FTLayers:
                for (side, halflayer) in self._FTHalfLayers.items():
                    elements.append("FT" + station + layer + side + " : " +
                                    self._FT + station + layer + halflayer)
        self.__append(elements, dofs)

    def FTModules(self, dofs=""):
        elements = []
        for station in self._FTStations:
            for layer in self._FTLayers:
                for (side,
                     halflayerquarters) in self._FTHalfLayerQuarters.items():
                    for module in self._FTModulesAll if station == "/T3" else self._FTModulesShort:
                        elements.append("FT" + station + layer + side +
                                        module + ":" + self._FT + station +
                                        layer + halflayerquarters + module)
        self.__append(elements, dofs)

    def FTHalfModules(self, dofs=""):
        self.__append(
            self._FTHalfModulesRE +
            (";(%s)" % self._FTHalfModuleReadoutSideTranslation), dofs)

    def FTHalfModuleJoints(self,
                           errors="0.01 0.01 0.0019 0.0004 0.0002 0.00017"):
        joints = []
        for station in self._FTStations:
            for layer in self._FTLayers:
                for module in self._FTModulesAll if station == "/T3" else self._FTModulesShort:
                    combinations = [("/HL0/Q0", "/HL0/Q2"),
                                    ("/HL1/Q1", "/HL1/Q3")] if UseDD4Hep else [
                                        ("/Quarter0", "/Quarter2"),
                                        ("/Quarter1", "/Quarter3")
                                    ]
                    for (Qtop, Qbot) in combinations:
                        modtop = 'FT' + station + layer + Qtop + module
                        modbot = 'FT' + station + layer + Qbot + module
                        joints.append(" : ".join([
                            modtop, modbot, errors,
                            self._FTHalfModuleMirrorSideTranslation
                        ]))
        return joints

    def FTMats(self, dofs=""):
        self.__append(self._FTMatsRE, dofs)

    ## MUON  ##############################################################################
    def MuonStations(self, dofs=""):
        elements = []
        for i in self.m_mustations:
            i = i + ("Station" if UseDD4Hep else "")
            elements.append(self.m_muon + i)
        self.__append(elements, dofs)

    def MuonHalfStations(self, dofs=""):
        elements = []
        for i in self.m_mustations:
            i = i + ("Station" if UseDD4Hep else "")
            for j in self.m_muhalfstations:
                elements.append(self.m_muon + i + j)
        self.__append(elements, dofs)

    def MuonHalfStationsAC(self, dofs=""):
        elements = []
        for i in self.m_mustations:
            i = i + ("Station" if UseDD4Hep else "")
            for j in self.m_muhalfstations:
                elements.append(self.m_muon + i + self.m_muhalfstationsAC[0])
                elements.append(self.m_muon + i + self.m_muhalfstationsAC[1])
        self.__append(elements, dofs)

    def MuonChambers(self, dofs=""):
        elements = []
        ## 5 Stations numbered from 1 to 5
        for i in self.m_mustations:
            ## There are 2 halves in each station
            for j in self.m_muhalfstations:
                ## Chambers
                if not UseDD4Hep:
                    for l in self.m_muchambers:
                        elements.append(self.m_muon + i + j + l)
        self.__append(elements, dofs)

    def MuonHalfStationsCside(self, dofs=""):
        elements = []
        for i in self.m_mustations:
            i = i + ("Station" if UseDD4Hep else "")
            elements.append(self.m_muon + i + self.m_muhalfstationsAC[1])
        self.__append(elements, dofs)

    def MuonHalfStationsAside(self, dofs=""):
        elements = []
        for i in self.m_mustations:
            i = i + ("Station" if UseDD4Hep else "")
            elements.append(self.m_muon + i + self.m_muhalfstationsAC[0])
        self.__append(elements, dofs)
