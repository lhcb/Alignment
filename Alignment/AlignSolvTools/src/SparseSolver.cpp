/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// Interface
#include "AlignmentInterfaces/IAlignSolvTool.h"

/** @class SparseSolver SparseSolver.h
 *
 *
 *  @author Wouter Hulsbergen
 *  @date   2007-07-24
 */

class SparseSolver : public extends<GaudiTool, IAlignSolvTool> {

public:
  /// Standard constructor
  using extends::extends;

  using IAlignSolvTool::compute; ///< avoids hiding the original function definitions
  /// Solves Ax = b
  StatusCode compute( AlSymMat& symMatrix, AlVec& vector ) const override;

private:
};

//-----------------------------------------------------------------------------
// Implementation file for class : SparseSolver
//
// 2007-07-24 : Jan Amoraal
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( SparseSolver )

#include "TDecompSparse.h"
#include "TMatrixDSparse.h"
#include "TMatrixDSym.h"
#include "TVectorD.h"

StatusCode SparseSolver::compute( AlSymMat& symMatrix, AlVec& vector ) const {
  // create a sparce matrix. I think that this is most efficient via
  // TMatrix, because the structure of the TSparseMatrix isn't
  // entirely self evident.

  size_t      dim = symMatrix.size();
  TMatrixDSym tsymMatrix( dim );
  TVectorD    tvector( dim );
  for ( size_t irow = 0; irow < dim; ++irow ) {
    tvector( irow ) = vector( irow );
    for ( size_t icol = 0; icol <= irow; ++icol ) tsymMatrix( irow, icol ) = symMatrix.fast( irow, icol );
  }

  TMatrixDSparse sparsematrix( tsymMatrix );
  info() << "Created sparse matrix. Number of non-zero elements is " << sparsematrix.NonZeros() << " out of "
         << dim * dim << endmsg;

  // solve
  int           verbose( 0 );
  TDecompSparse decompsparse( sparsematrix, verbose );
  bool          ierr = decompsparse.Solve( tvector );

  info() << "TDecompSparse::Solve returns: " << ierr << endmsg;

  // now copy the result back
  for ( size_t irow = 0; irow < dim; ++irow ) vector( irow ) = tvector( irow );

  // fill something arbitrary for the covariance matrix
  AlSymMat tmp( symMatrix.size() );
  for ( size_t irow = 0; irow < tmp.size(); ++irow ) {
    double c               = symMatrix.fast( irow, irow );
    tmp.fast( irow, irow ) = c != 0 ? 1 / c : 0;
  }
  symMatrix = tmp;
  return ierr ? StatusCode::FAILURE : StatusCode::SUCCESS;
}
