/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "DiagSolvToolBase.h"

class EigenDiagSolvTool : public DiagSolvToolBase {
public:
  using DiagSolvToolBase::DiagSolvToolBase;

  using IAlignSolvTool::compute; ///< avoids hiding the original function definitions

  StatusCode diagonalize( const AlSymMat& A, AlMat& U, AlMat& V, AlVec& S ) const override;
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( EigenDiagSolvTool )

StatusCode EigenDiagSolvTool::diagonalize( const AlSymMat& A, AlMat& U, AlMat& V, AlVec& S ) const {
  // There are several options in Eigen. We just start with one.
  Eigen::MatrixXd                eigenA = A.toMatrix();
  Eigen::BDCSVD<Eigen::MatrixXd> svdcalculator{eigenA, Eigen::ComputeFullU | Eigen::ComputeFullV};
  S = svdcalculator.singularValues();
  U = svdcalculator.matrixU();
  V = svdcalculator.matrixV();
  return StatusCode::SUCCESS;
}
