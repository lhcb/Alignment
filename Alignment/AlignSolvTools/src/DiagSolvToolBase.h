/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
// from Gaudi
#include "AlignmentInterfaces/IAlignSolvTool.h" // Interface
#include "GaudiAlg/GaudiTool.h"

/** @class DiagSolvTool DiagSolvTool.h AlignSolvTools/DiagSolvTool.h
 *
 *
 *  @author Adlene Hicheur
 *  @date   2007-06
 */

class DiagSolvToolBase : public extends<GaudiTool, IAlignSolvTool> {
public:
  /// Standard constructor
  using extends::extends;

  using IAlignSolvTool::compute; ///< avoids hiding the original function definitions

  ///< Call method to compute the solution, get symetric
  /// matrix and vector in input and replaces them by
  /// inverse matrix and solution vector in output.
  StatusCode compute( AlSymMat& A, AlVec& b ) const override {
    IAlignSolvTool::SolutionInfo info;
    return compute( A, b, info );
  }

  // Solves the system Ax=b for x. Return A=A^{-1} and
  // b=x. Furthermore, returns the total chi2 (b^T*A^{-1}*b) and the
  // largest contribution to the chi2 from a single mode.
  StatusCode compute( AlSymMat& A, AlVec& b, IAlignSolvTool::SolutionInfo& ) const override;

  StatusCode initialize() override;

  // Diagonalizes A symmetric matrix A such that A = U D V^T
  // * S is the vector with eigenvalues, the diagonal elements of D
  // * the columns of U are the eigen vectors of A
  // * for symmetric matrices a solution exists for which V=U, but ... both Eigen and Gsl do not obey that!
  virtual StatusCode diagonalize( const AlSymMat& A, AlMat& U, AlMat& V, AlVec& S ) const = 0;

protected:
  Gaudi::Property<size_t> m_numberOfPrintedEigenvalues{this, "NumberOfPrintedEigenvalues", 20};
  Gaudi::Property<double> m_eigenValueThreshold{this, "EigenValueThreshold", -1};
  Gaudi::Property<bool>   m_applyScaling{this, "ApplyScaling", false};
  Gaudi::Property<double> m_minEigenModeChisquare{this, "MinEigenModeChisquare", -1};
};
