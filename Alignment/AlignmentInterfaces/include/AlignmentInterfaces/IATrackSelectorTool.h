/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/IGeometryInfo.h"
#include "Event/Track.h"
#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_IATrackSelectorTool( "IATrackSelector", 2, 0 );

/**
 * Interface to a track selector tool for
 * the alignment framework
 * Created by: Johan Blouw, Physikalisches Institut, Heidelberg
 * modified 17.10.2007 by M.Deissenroth
 */
class IATrackSelectorTool : virtual public IAlgTool {
public:
  // Retrieve interface ID
  static const InterfaceID& interfaceID() { return IID_IATrackSelectorTool; }

  // Accept a track when it fullfills certain criteria
  virtual bool       accept( const LHCb::Track& aTrack, IGeometryInfo const& geometry ) = 0;
  virtual StatusCode Reset()                                                            = 0;

  virtual void PrintUniformTD() = 0;
};
