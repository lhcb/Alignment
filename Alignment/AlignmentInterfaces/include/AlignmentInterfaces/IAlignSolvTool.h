/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
// from STL
//#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// from AlignKernel
#include "AlignKernel/AlMat.h"
#include "AlignKernel/AlSymMat.h"
#include "AlignKernel/AlVec.h"

/** @class IAlignAlignTool IAlignAlignTool.h Kernel/IAlignAlignTool.h
 *
 *
 *  @author Adlene Hicheur
 *  @date   2006-09-20
 */

class IAlignSolvTool : public extend_interfaces<IAlgTool> {
public:
  DeclareInterfaceID( IAlignSolvTool, 2, 0 );

  // simple struct that stores some info on solution
  struct SolutionInfo final {
    double             totalChisq;
    double             maxChisqEigenMode;      // max chi2 contribution of a single mode
    double             minEigenValue;          // smallest eigenvalue
    size_t             weakestModeMaxPar;      // parameter that contributes strongest to weakest mode
    double             weakestModeMaxParCoef;  // coefficient of this parameter in eigenvector
    size_t             numNegativeEigenvalues; // number of negative eigenvalues (lagrange constraints)
    size_t             numSmallEigenvalues;    // number of eigenvalues smaller than one
    std::vector<float> eigenvalues;            // vector with all eigenvalues (or a subset)
  };

  // Solves the system Ax=b for x. Returns A=A^{-1} and b=x.
  virtual StatusCode compute( AlSymMat& A, AlVec& b ) const = 0;

  // Solves the system Ax=b for x. Returns A=A^{-1}, b=x and S =
  // eigenvalues of A.
  virtual StatusCode compute( AlSymMat& A, AlVec& b, AlVec& /*S*/ ) const { return compute( A, b ); }

  // Solves the system Ax=b for x. Return A=A^{-1} and
  // b=x. Furthermore, returns an object with some characteristics of
  // system/solution.
  virtual StatusCode compute( AlSymMat& A, AlVec& b, SolutionInfo& /*info*/ ) const {
    return compute( A, b ) ? StatusCode::SUCCESS : StatusCode::FAILURE;
  }
};
