/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "AlignKernel/AlEquations.h"

#include "Kernel/meta_enum.h"

#ifdef USE_DD4HEP
#  include "LbDD4hep/IDD4hepSvc.h"
#endif

#include "GaudiKernel/IAlgTool.h"

namespace LHCb::Alignment {
  /// Static ID object
  static const InterfaceID IID_IAlignUpdateTool( "LHCb::Alignment::IAlignUpdateTool", 0, 0 );

  class GetElementsToBeAligned;

  meta_enum_class( ConvergenceStatus, unsigned int, NotConverged = 0, Converged = 1, Unknown = 2 )

      class IAlignUpdateTool : virtual public IAlgTool {
  public:
    // Retrieve interface ID
    static const InterfaceID& interfaceID() { return IID_IAlignUpdateTool; }

    // Old interface
    virtual StatusCode process( const Equations& equations, const GetElementsToBeAligned& elementProvider, size_t iter,
                                size_t maxiter, ConvergenceStatus& status
#ifdef USE_DD4HEP
                                ,
                                LHCb::Det::LbDD4hep::IDD4hepSvc& detDataSvc
#endif
                                ) const = 0;
  };
} // namespace LHCb::Alignment
