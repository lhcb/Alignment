/***********************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb and ATLAS collaborations      *
*                                                                                   *
* This software is distributed under the terms of the Apache version 2 licence,     *
* copied verbatim in the file "LICENSE".                                            *
*                                                                                   *
* In applying this licence, CERN does not waive the privileges and immunities       *
* granted to it by virtue of its status as an Intergovernmental Organization        *
* or submit itself to any jurisdiction.                                             *
\***********************************************************************************/
#include <Gaudi/Accumulators.h>
#include <Gaudi/Accumulators/Histogram.h>
#include <Gaudi/BaseSink.h>
#include <LHCbAlgs/Consumer.h>

namespace Alignment::Tests::Histograms {

  // simple algo with one histo and one counter
  struct TestHistoAlg : LHCb::Algorithm::Consumer<void()> {
    using Consumer::Consumer;
    mutable Gaudi::Accumulators::Histogram<1> m_hist{this, "Histo", "Histo title", {1, 0, 1}};
    mutable Gaudi::Accumulators::Counter<>    m_counterhist{this, "Counter"};
    void                                      operator()() const override {
      ++m_hist[0.5];
      ++m_counterhist;
    }
  };
  DECLARE_COMPONENT( TestHistoAlg )

  // simple Sink printing every entity to stdout
  struct BasicSink : Gaudi::Monitoring::BaseSink {
    using BaseSink::BaseSink;
    void flush( bool ) override {
      applyToAllEntities( []( auto& ent ) { std::cout << nlohmann::json{ent} << "\n"; } );
    }
  };
  DECLARE_COMPONENT( BasicSink )

} // namespace Alignment::Tests::Histograms
