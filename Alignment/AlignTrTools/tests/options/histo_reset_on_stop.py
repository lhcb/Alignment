#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import ApplicationMgr
from Configurables import Alignment__Tests__Histograms__BasicSink as BasicSink, LHCb__Alignment__ResettingSink as ResettingSink

basicSink = BasicSink()
resettingSink = ResettingSink()
app = ApplicationMgr(
    EvtSel="NONE",
    TopAlg=["Alignment::Tests::Histograms::TestHistoAlg"],
    ExtSvc=[basicSink, resettingSink],
)

from GaudiPython.Bindings import AppMgr

appMgr = AppMgr()
appMgr.run(1)
appMgr.stop()
appMgr.run(1)
appMgr.exit()
