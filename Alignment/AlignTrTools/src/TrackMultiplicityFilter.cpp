/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class TrackMultiplicityFilter TrackMultiplicityFilter.h
 *
 *  Algorithm to filter events in which a track list is not empty
 *
 *  @author W. Hulsbergen
 *  @date   2008
 */

// Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include <string>

// track interfaces
#include "Event/FTCluster.h"

class TrackMultiplicityFilter : public GaudiAlgorithm {
public:
  // Constructors and destructor
  TrackMultiplicityFilter( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~TrackMultiplicityFilter();
  StatusCode execute() override;

private:
  size_t m_maxNumFTHits;
};

DECLARE_COMPONENT( TrackMultiplicityFilter )

TrackMultiplicityFilter::TrackMultiplicityFilter( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ) {
  // constructor
  declareProperty( "MaxNumFTHits", m_maxNumFTHits = 500 ); // PK-R3C Needs to be retuned with FT
}

TrackMultiplicityFilter::~TrackMultiplicityFilter() {}

StatusCode TrackMultiplicityFilter::execute() {
  setFilterPassed( true );
  std::string m_clusterLocation = LHCb::FTClusterLocation::Default;
  if ( exist<LHCb::FTClusters>( m_clusterLocation ) ) {
    LHCb::FTClusters* clusters = get<LHCb::FTClusters>( m_clusterLocation );
    if ( clusters && clusters->size() > m_maxNumFTHits ) setFilterPassed( false );
  }
  return StatusCode::SUCCESS;
}
