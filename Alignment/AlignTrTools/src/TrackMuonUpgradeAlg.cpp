/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ToolHandle.h"

// event
#include "Event/Track.h"
#include "Event/TrackFitResult.h"
#include <string>

class TrackMuonUpgradeAlg
    : public Gaudi::Functional::Transformer<LHCb::Tracks( const LHCb::Track::Range&, const LHCb::Tracks& )> {

public:
  TrackMuonUpgradeAlg( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"MuonInputLocation", LHCb::TrackLocation::Muon},
                      KeyValue{"TrackInputLocation", LHCb::TrackLocation::Default}},
                     KeyValue{"OutputLocation", "Rec/Track/LongMuons"} ){};

  LHCb::Tracks operator()( const LHCb::Track::Range& muontracks, const LHCb::Tracks& inputtracks ) const override;
};

DECLARE_COMPONENT( TrackMuonUpgradeAlg )

LHCb::Tracks TrackMuonUpgradeAlg::operator()( const LHCb::Track::Range& muontracks,
                                              const LHCb::Tracks&       inputtracks ) const {
  LHCb::Tracks outtracks;

  const LHCb::Tracks* besttracks( 0 );
  for ( const auto* muontrack : muontracks ) {
    const LHCb::Track* origtrack( 0 );

    if ( muontrack->ancestors().empty() ) {
      debug() << "Problem: muon track has no ancestors!" << endmsg;
      if ( besttracks == 0 ) {
        const LHCb::Tracks* tracks = &inputtracks;
        besttracks                 = tracks;
      }
      origtrack = static_cast<const LHCb::Track*>( besttracks->containedObject( muontrack->key() ) );
    } else {
      origtrack = muontrack->ancestors().front();
    }
    if ( origtrack && origtrack->type() == LHCb::Track::Types::Long ) {
      // for now, let's make a new track in a new container
      if ( outtracks.empty() ) {
        // clone
        auto track = new LHCb::Track( *origtrack );
        // add the muon hits
        track->addSortedToLhcbIDs( muontrack->lhcbIDs() );
        // invalidate the fit
        track->setFitResult( 0 );
        outtracks.insert( track );
      } else {
        // upgrade an existing track. to be done ...
      }
    }
  }

  return outtracks;
}
