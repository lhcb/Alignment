/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class AlignTrackCloneRemover AlignTrackCloneRemover.h
 *
 *  Merge different track lists.
 *
 *  @author Wouter Hulsbergen
 *  @date   05/01/2010
 */

#include "Event/Track.h"
#include "LHCbAlgs/MergingTransformer.h"
#include <string>

namespace LHCb {

  template <typename InputListType, typename OutputListType = InputListType>
  struct AlignTrackCloneRemover final
      : Algorithm::MergingTransformer<OutputListType( const Gaudi::Functional::vector_of_const_<InputListType>& )> {
    AlignTrackCloneRemover( const std::string& name, ISvcLocator* pSvcLocator )
        : Algorithm::MergingTransformer<OutputListType( const Gaudi::Functional::vector_of_const_<InputListType>& )>{
              name, pSvcLocator, {"InputLocations", {}}, {"OutputLocation", {}}} {}

    OutputListType operator()( const Gaudi::Functional::vector_of_const_<InputListType>& lists ) const override {

      // first create a single vector with all tracks
      const size_t                    N = std::accumulate( begin( lists ), end( lists ), size_t{0},
                                        []( const auto v, const auto& l ) { return v + l.size(); } );
      std::vector<const LHCb::Track*> alltracks;
      alltracks.reserve( N );
      for ( const auto& l : lists ) alltracks.insert( alltracks.end(), begin( l ), end( l ) );
      // sort them
      std::sort( begin( alltracks ), end( alltracks ), []( const auto& lhs, const auto& rhs ) {
        return lhs != rhs &&
               ( ( lhs->nDoF() > rhs->nDoF() ) ||
                 ( ( lhs->nDoF() == rhs->nDoF() ) &&
                   ( ( lhs->lhcbIDs().size() > rhs->lhcbIDs().size() ) ||
                     ( lhs->lhcbIDs().size() == rhs->lhcbIDs().size() && lhs->chi2() < rhs->chi2() ) ) ) );
      } );
      // now delete any overlapping tracks
      OutputListType out;
      for ( const auto& itr : alltracks ) {
        bool found = std::any_of( std::begin( out ), std::end( out ),
                                  [&itr]( auto& jtr ) { return itr->nCommonLhcbIDs( *jtr ); } );
        if ( !found ) out.insert( itr );
      }
      numtracks += int( alltracks.size() );
      numtracksremoved += int( alltracks.size() ) - int( out.size() );
      return out;
    }

    // Keep some counters
    mutable Gaudi::Accumulators::StatCounter<> numtracks{this, "Number of input tracks"};
    mutable Gaudi::Accumulators::StatCounter<> numtracksremoved{this, "Number of removed tracks"};
  };

  /// TODO (wh,10/2022): we can replace AlignTrackCloneRemover.cpp by TrackSelectionMerge, but that's for next time.
  using TrackSelectionMerger = AlignTrackCloneRemover<Track::Range, Track::Selection>;
  DECLARE_COMPONENT_WITH_ID( TrackSelectionMerger, "AlignTrackCloneRemover" )
} // namespace LHCb
