/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "AlignmentInterfaces/IATrackSelectorTool.h"

#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackInterfaces/ITrackCaloMatch.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"

#include "Event/FitNode.h"
#include "Event/Measurement.h"
#include "Event/Track.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/HashMap.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "boost/assign/list_of.hpp"
#include "boost/limits.hpp"
#include "boost/numeric/conversion/bounds.hpp"

#include <iomanip>
#include <string>

/**
 *  Header file for Tstation alignment : ATrackSelector
 *
 *  @author J. Blouw johan.blouw@cern.ch
 *  @date   31/09/2006
 */
class ATrackSelector : public GaudiTool, virtual public IATrackSelectorTool {

public:
  ATrackSelector( const std::string& type, const std::string& name, const IInterface* parent );

  /** Returns if the given track is selected or not
   *
   *  @param aTrack Reference to the Track to test
   *
   *  @return boolean indicating if the track is selected or not
   *  @retval true  Track is selected
   *  @retval false Track is rejected
   */
  StatusCode initialize() override;
  StatusCode finalize() override;
  bool       accept( const LHCb::Track& aTrack, IGeometryInfo const& geometry ) override;

  StatusCode Reset() override;

  void PrintUniformTD() override;

private:
  bool yCut( const LHCb::Track&, IGeometryInfo const& );
  void PrintSummary();

  // Interfaces:
  ITrackExtrapolator* m_extrapolator;
  ITrackCaloMatch*    m_trackenergy;

  bool   Unify( const LHCb::Track&, IGeometryInfo const& geometry );
  bool   uniformCut( int& );
  double m_minChi2Cut; // Min chi^2 cut
  double m_minPCut;    // Min p cut
  double m_minPtCut;   // Min pt cut
  int    m_charge;     // select particles with certain charge only
  double m_maxChi2Cut; // Max chi^2 cut
  double m_maxPCut;    // Max p cut
  double m_maxPtCut;   // Max pt cut
  double m_yMaxCut;
  double m_yMinCut;
  bool   m_weights;      // attempt to create uniform track distrubution in (OT)
  int    m_uniCut;       // module IDs larger than m_uniCut will be uniformed.
  double m_energyMinCut; // Min energy cut

  int m_wrong_charge, m_total, m_bad_chi2, m_bad_p, m_bad_pt;
  int m_few_hits, m_bad_energy, m_not_uniform;

  // Track types to accept
  typedef std::vector<std::string> TrackTypes;
  TrackTypes                       m_trTypes; // List of track types to select

  // Mapping type linking track types to selection boolean
  typedef GaudiUtils::HashMap<const LHCb::Track::Types, bool> SelTypes;
  SelTypes m_selTypes; // Mapping linking track types to selection boolean
  int      m_uniform[3][4][4][9];
  int      m_HitMatrix[4][9][9];
};

using namespace LHCb;

DECLARE_COMPONENT( ATrackSelector )

ATrackSelector::ATrackSelector( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {

  // interface
  declareInterface<IATrackSelectorTool>( this );

  // define track containers

  // cut options
  declareProperty( "MinPCut", m_minPCut = 0.0 );   // in GeV
  declareProperty( "MinPtCut", m_minPtCut = 0.0 ); // in GeV
  declareProperty( "MinChi2Cut", m_minChi2Cut = 0.0 );
  declareProperty( "Charge", m_charge = 0 );                                             // charge of particle selection
  declareProperty( "MaxPCut", m_maxPCut = boost::numeric::bounds<double>::highest() );   // in GeV
  declareProperty( "MaxPtCut", m_maxPtCut = boost::numeric::bounds<double>::highest() ); // in GeV
  declareProperty( "MaxChi2Cut", m_maxChi2Cut = boost::numeric::bounds<double>::highest() );
  declareProperty( "YCutMax_at_T1", m_yMaxCut = 9999.99 );
  declareProperty( "YCutMin_at_T1", m_yMinCut = -9999.99 );
  declareProperty( "UseWeights", m_weights = true );       // enable uniformization of track distribution
  declareProperty( "UniformCutOff", m_uniCut = 4 );        //  m_uniCut = 9 disables the uniformity.
  declareProperty( "MinEnergyCut", m_energyMinCut = 0.0 ); // in GeV
}

StatusCode ATrackSelector::initialize() {
  debug() << "Initialize track selector tool" << endmsg;
  // retrieve track-calo match tool
  if ( m_energyMinCut > 0. )
    m_trackenergy = tool<ITrackCaloMatch>( "TrackCaloMatch" );
  else
    m_trackenergy = NULL;

  StatusCode sc = Reset();
  if ( !sc ) {
    error() << "Error reseting uniformity distribution..." << endmsg;
    return StatusCode::FAILURE;
  }
  if ( m_uniCut < 1 || m_weights == false ) {
    warning() << "Resetting uniformity cut to m_uniCut = 1" << endmsg;
    m_uniCut = 1;
  } else {
    info() << "Attempting to create a uniform track distribution from module " << m_uniCut << " till module 9"
           << endmsg;
  }
  if ( m_yMinCut > -9999.99 || m_yMaxCut < 9999.99 ) {
    info() << "Using window " << m_yMinCut << " < y < " << m_yMaxCut << " to select tracks" << endmsg;
    m_extrapolator = tool<ITrackExtrapolator>( "TrackMasterExtrapolator" );
  } else
    m_extrapolator = NULL;
  // initialize track counters
  return StatusCode::SUCCESS;
}

StatusCode ATrackSelector::Reset() {
  for ( unsigned int i = 0; i < 3; i++ )     // stations
    for ( unsigned int j = 0; j < 4; j++ )   // quadrants
      for ( unsigned int k = 0; k < 4; k++ ) // layers
        for ( unsigned int l = 0; l < 9; l++ ) m_uniform[i][j][k][l] = 0;

  for ( unsigned int q = 0; q < 4; q++ )
    for ( unsigned int i = 0; i < 9; i++ )
      for ( unsigned int j = 0; j < 9; j++ ) m_HitMatrix[q][i][j] = 0;
  debug() << "Reset the uniform distribution!!!!" << endmsg;
  m_total = m_bad_chi2 = m_bad_p = m_bad_pt = m_few_hits = 0;
  m_bad_energy = m_not_uniform = 0;
  return StatusCode::SUCCESS;
}

StatusCode ATrackSelector::finalize() {
  debug() << "Finalize track selector tool" << endmsg;
  return StatusCode::SUCCESS;
}

bool ATrackSelector::accept( const LHCb::Track& aTrack, IGeometryInfo const& geometry ) {
  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Trying Track " << aTrack.key() << " " << aTrack.type() << endmsg; }
  m_total++;
  // simple cuts first

  // select positively and/or negatively charged tracks
  if ( m_charge != 0 ) {
    const int charge = aTrack.charge();
    if ( m_charge * charge < 0 ) {
      debug() << "Removing particle with charge " << charge << endmsg;
      m_wrong_charge++;
      return false;
    }
  }

  // chi-squared
  const double chi2 = aTrack.chi2PerDoF();
  // info() << "Into TrackSelector " <<chi2<<" "<<m_maxChi2Cut<< endmsg;
  if ( chi2 < m_minChi2Cut || chi2 > m_maxChi2Cut ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << " -> Chi^2 " << chi2 << " failed cut" << endmsg;
    m_bad_chi2++;
    // info()  << " -> Chi^2 " << chi2 << " failed cut" << endmsg;
    return false;
  }

  // cut p
  if ( m_maxPCut != -1 ) {
    const double p = aTrack.p();
    if ( p < m_minPCut || p > m_maxPCut ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << " -> P " << aTrack.p() << " failed cut" << endmsg;
      m_bad_p++;
      return false;
    }
  }

  // cut on pt
  if ( m_maxPtCut != -1 ) {
    const double pt = aTrack.pt();
    if ( pt < m_minPtCut || pt > m_maxPtCut ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << " -> Pt " << aTrack.pt() << " failed cut" << endmsg;
      m_bad_pt++;
      return false;
    }
  }

  // Generate uniform track distribution in x direction
  if ( m_weights )
    if ( !Unify( aTrack, geometry ) ) {
      debug() << "Not uniform... rejecting track" << endmsg;
      m_not_uniform++;
      return false;
    }
  // cut on energy deposited in calorimeters:
  if ( m_energyMinCut > 0.0 ) {
    double energy = m_trackenergy->energy( aTrack );
    debug() << "Energy on track = " << energy << endmsg;
    if ( energy < m_energyMinCut ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << " -> energy " << energy << " failed cut" << endmsg;
      m_bad_energy++;
      return false;
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << " -> Track selected" << endmsg;

  return true;
}

bool ATrackSelector::Unify( const LHCb::Track& aTrack, IGeometryInfo const& geometry ) {
  // check status of the fit:
  if ( aTrack.fitStatus() != LHCb::Track::FitStatus::Fitted || aTrack.nDoF() < 2 ) return false;
  if ( m_extrapolator && !yCut( aTrack, geometry ) ) {
    debug() << "No track extrapolator or does not fit yCut..." << endmsg;
    return false;
  }
  return true;
}

bool ATrackSelector::uniformCut( int& mod ) {
  unsigned int prev[9];
  for ( unsigned int t = 0; t < 9; t++ ) prev[t] = 0;
  unsigned int s = 0;
  // always return true when an outside module was hit
  if ( mod < m_uniCut ) return true;
  // Sum over quarters & layers
  for ( unsigned int q = 0; q < 4; q++ )
    for ( unsigned int l = 0; l < 4; l++ )
      for ( unsigned int m = 0; m < 9; m++ ) prev[m] += m_uniform[s][q][l][m];
  // check if the number of hits in this module
  // is more than what we want:
  if ( prev[mod - 1] >= (unsigned int)( 0.9 * prev[mod] ) ) {
    // debug() << "Keeping track..." << endmsg;
    return true;
  }
  //  debug() << "Removing track because it breaks uniformity!" << endmsg;
  return false;
}

bool ATrackSelector::yCut( const LHCb::Track& t, IGeometryInfo const& geometry ) {
  Gaudi::XYZPoint pRec;
  double          Z_position = 8630.0;
  m_extrapolator->position( t, Z_position, pRec, geometry ).ignore();
  info() << "Entry point = " << pRec << endmsg;
  return pRec.y() < m_yMaxCut && pRec.y() > m_yMinCut;
}

void ATrackSelector::PrintSummary() {
  info() << "------ Track Selection Summary ----------" << endmsg;
  info() << "------ Rejected number of tracks        " << endmsg;
  info() << "       Total nr. available : " << m_total << endmsg;
  if ( m_charge != 0 ) info() << "       wrong charge:       : " << m_wrong_charge << endmsg;
  info() << "       bad chi^2           : " << m_bad_chi2 << endmsg;
  if ( m_maxPCut != -1 ) info() << "       bad momentum        : " << m_bad_p << endmsg;
  if ( m_maxPtCut != -1 ) info() << "       bad tr. momentum    : " << m_bad_pt << endmsg;
  info() << "       insufficient hits   : " << m_few_hits << endmsg;
  if ( m_energyMinCut > 0.0 ) info() << "       insufficient energy : " << m_bad_energy << endmsg;
  info() << "       not uniform distro  : " << m_not_uniform << endmsg;
}

void ATrackSelector::PrintUniformTD() {
  this->PrintSummary();
  int HitMatrix[9][9];
  for ( unsigned int i = 0; i < 9; i++ )
    for ( unsigned int j = 0; j < 9; j++ ) HitMatrix[i][j] = 0;
  for ( unsigned int q = 0; q < 4; q++ )
    for ( unsigned int i = 0; i < 9; i++ )
      for ( unsigned int j = 0; j < 9; j++ ) HitMatrix[i][j] += m_HitMatrix[q][i][j];
  /*
  info() << "Total nr. of hits: " << tot << endmsg;
  info() << "Hit Correlation Matrix between modules in first and last layer in OT" << endmsg;
  info() << "Module nr:      0     1     2     3     4     5     6     7     8" << endmsg;
  info() <<   "         |-------------------------------------------------------|" << endmsg;
  for ( unsigned int i = 0; i < 9; i++ ) {
    info() << "   " << std::setw(5) << i << " | ";
    for ( unsigned int j = 0; j < 9; j++ ) {
      info() << " " << std::setprecision(5) << std::setw(5) << m_HitMatrix[j][i]/tot;
    }
    info() << endmsg;
  }
  */
  info() << "Hit Correlation Matrix between modules in first and last layer in OT" << endmsg;
  info() << "Module nr:      0     1     2     3     4     5     6     7     8" << endmsg;
  info() << "         |-------------------------------------------------------|" << endmsg;
  for ( unsigned int i = 0; i < 9; i++ ) {
    info() << "   " << std::setw( 5 ) << i << " | ";
    for ( unsigned int j = 0; j < 9; j++ ) { info() << " " << std::setw( 5 ) << HitMatrix[j][i]; }
    info() << endmsg;
  }

  for ( unsigned int q = 0; q < 4; q++ ) {
    info() << "Quadrant: " << q << endmsg;
    info() << "Hit Correlation Matrix between modules in first and last layer in OT" << endmsg;
    info() << "Module nr:      0     1     2     3     4     5     6     7     8" << endmsg;
    info() << "         |-------------------------------------------------------|" << endmsg;
    for ( unsigned int i = 0; i < 9; i++ ) {
      info() << "   " << std::setw( 5 ) << i << " | ";
      for ( unsigned int j = 0; j < 9; j++ ) { info() << " " << std::setw( 5 ) << m_HitMatrix[q][j][i]; }
      info() << endmsg;
    }
  }

  if ( m_weights ) {
    unsigned int prev[4][9];
    for ( unsigned int i = 0; i < 3; i++ ) // stations
      for ( unsigned int l = 0; l < 9; l++ ) prev[i][l] = 0;

    for ( unsigned int i = 0; i < 3; i++ ) { // stations
      info() << "Station nr. " << i << " : ";
      for ( unsigned int j = 0; j < 4; j++ ) { // quadrants
        info() << "Quadrant nr: " << j << endmsg;
        for ( unsigned int k = 0; k < 4; k++ ) { // layers
          info() << "Layer nr " << k << " : ";
          for ( unsigned int l = 0; l < 9; l++ ) info() << m_uniform[i][j][k][l] << " ";
          info() << endmsg;
        }
      }
    }
    for ( unsigned int i = 0; i < 3; i++ )     // stations
      for ( unsigned int j = 0; j < 4; j++ )   // quadrants
        for ( unsigned int k = 0; k < 4; k++ ) // layers
          for ( unsigned int l = 0; l < 9; l++ ) prev[i][l] += m_uniform[i][j][k][l];
    for ( unsigned int i = 0; i < 3; i++ ) { // stations
      info() << "Station " << i << endmsg;
      info() << "Hits : ";
      for ( unsigned int l = 0; l < 9; l++ ) info() << prev[i][l] << " ";
      info() << endmsg;
    }
  }
}
