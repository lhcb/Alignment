/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class AlignSaveTuple AlignSaveTuple Align/AlignTrTools/AlignSaveTuple.h
 *
 *  Provide a monitoring tool for T-Station alignment studies
 *
 *  @author L. Nicolas
 *  @date   13/06/2006
 */

//-----------------------------------------------------------------------------
// Implementation file for class : AlignSaveTuple
//
// Louis Nicolas, Adlene Hicheur (EPFL)
// Started: 06/06
///-----------------------------------------------------------------------------
//===========================================================================
// Includes
//===========================================================================
#include "Event/FitNode.h"
#include "Event/GhostTrackInfo.h"
#include "Event/MCParticle.h"
#include "Event/ODIN.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IMagneticFieldSvc.h"
#include "GaudiKernel/ToolHandle.h"
#include "Linker/LinkedTo.h"
#include "MCInterfaces/ITrackGhostClassification.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"

//===========================================================================
struct lessByID {
  bool operator()( const LHCb::LHCbID& obj1, const LHCb::LHCbID& obj2 ) const { return obj1.lhcbID() < obj2.lhcbID(); }
};
//===========================================================================
// collect hits shared with more than one track
//===========================================================================
std::vector<LHCb::LHCbID> getSharedHits( const LHCb::Tracks* tracks ) {
  std::vector<LHCb::LHCbID> sharedHits;
  // Reserving space for some vectors
  sharedHits.reserve( 1000 );

  for ( const LHCb::Track* aTrack : *tracks ) {
    for ( const LHCb::LHCbID aID : aTrack->lhcbIDs() ) {
      if ( std::any_of( tracks->begin(), tracks->end(),
                        [&]( const LHCb::Track* t ) { return t != aTrack && t->isOnTrack( aID ); } ) )
        sharedHits.push_back( aID );
    }
  }

  // sorting and stripping out duplicates
  std::sort( sharedHits.begin(), sharedHits.end(), lessByID{} );
  sharedHits.erase( std::unique( sharedHits.begin(), sharedHits.end() ), sharedHits.end() );
  return sharedHits;
}
//===========================================================================

class AlignSaveTuple : public GaudiTupleAlg {
public:
  // Constructors and destructor
  AlignSaveTuple( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  StatusCode execute() override;

private:
  typedef std::vector<double> Array;
  static const int            defValue = -999999;

  //======================================================================
  // Properties
  //======================================================================
  int m_nStations;

  int m_maxNHits;

  double m_nStripsTol;
  double m_nStrawsTol;

  std::string m_tracksPath;

  std::string m_ghostToolName;
  //======================================================================

  ToolHandle<ITrackExtrapolator> m_extrapolator;        // Interface to track extrapolator
  IMagneticFieldSvc*             m_pIMF;                ///< Pointer to the magn. field service
  ITrackGhostClassification*     m_ghostClassification; ///< Pointer to ghost tool

  bool m_mcData;
  long m_iEvent;

  //=============================================================================
};

//===========================================================================
// Declaration of the algorithm factory
DECLARE_COMPONENT( AlignSaveTuple )
//===========================================================================

//===========================================================================
// Constructor
//===========================================================================
AlignSaveTuple::AlignSaveTuple( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiTupleAlg( name, pSvcLocator ), m_extrapolator( "TrackParabolicExtrapolator" ) {

  // Ghost classification
  this->declareProperty( "GhostClassification", m_ghostToolName = "TTrackGhostClassification" );

  this->declareProperty( "TracksLocation", m_tracksPath = "Rec/Track/Best" );

  // Are we using MC or real data?
  this->declareProperty( "MCData", m_mcData = true );

  // 48 = 12 (layers) * 2 (ladder overlaps) * 2 (box overlaps)
  this->declareProperty( "IsolatedTrackNStripsTolerance", m_nStripsTol = 2 );
  this->declareProperty( "IsolatedTrackNStrawsTolerance", m_nStrawsTol = 1 );

  this->declareProperty( "MaxNHits", m_maxNHits = 40 );
}
//===========================================================================

//===========================================================================
// Initialize
//===========================================================================
StatusCode AlignSaveTuple::initialize() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  StatusCode sc = GaudiTupleAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return Error( "Failed to initialize" );

  // The extrapolator
  m_extrapolator.retrieve().ignore();

  // Retrieve the ghost classification tool
  m_ghostClassification = tool<ITrackGhostClassification>( m_ghostToolName, "GhostTool", this );

  // Get The Magnetic Field
  m_pIMF   = svc<IMagneticFieldSvc>( "MagneticFieldSvc", true );
  m_iEvent = 0;

  debug() << "AlignSaveTuple initialized successfully" << endmsg;

  return StatusCode::SUCCESS;
}
//===========================================================================

//===========================================================================
// Execute
//===========================================================================
StatusCode AlignSaveTuple::execute() {
  // Global Variables
  long   m_evtNr;
  long   m_runNr;
  int    m_eventMultiplicity;
  double m_ghostRate = defValue;

  // Track Variables
  int m_iGoodTrack;

  bool m_isGhostTrack             = false;
  int  m_ghostTrackClassification = defValue;

  int    m_nSharedHits;
  double m_fSharedHits;
  int    m_nHoles;

  double m_trackChi2PerDoF;
  double m_trackChi2Prob;
  double m_trackFitMatchChi2;

  double m_trackEta;

  double m_trackP;
  double m_trackPt;
  double m_trackErrP;
  double m_trackMCP  = defValue;
  double m_trackMCPt = defValue;

  double m_entryTX;
  double m_entryTY;
  double m_entryX;
  double m_entryY;

  // Hits Variables
  Array m_res;
  Array m_errRes;

  Array m_hitX;
  Array m_hitY;
  Array m_hitZ;

  int   m_nLadOverlaps;
  Array m_ladOvlapRes;
  Array m_ladOvlapLay;

  int   m_nBoxOverlaps;
  Array m_boxOvlapRes;
  Array m_boxOvlapSta;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "AlignSaveTuple starting execution" << endmsg;

  // Get the odin information
  const LHCb::ODIN* odin = get<LHCb::ODIN>( LHCb::ODINLocation::Default );

  // Get the tracks
  const LHCb::Tracks* tracks = get<LHCb::Tracks>( m_tracksPath );

  // Get the hits shared by more than one track
  auto sharedHits = getSharedHits( tracks );

  // Creating the tuple in memory
  Tuples::Tuple trackSelTuple = nTuple( "AllVariables" );

  // Get the association table
  auto const* links = get<LHCb::LinksByKey>( LHCb::LinksByKey::linkerName( m_tracksPath ) );

  auto isGhostTrack = [this, directTable = LinkedTo<LHCb::MCParticle>{links}]( const LHCb::Track* trk ) {
    //===========================================================================
    // Is track ghost or not?
    //===========================================================================
    if ( !directTable ) {
      this->Error( "Track MCParticle table missing. Are you running TrackAssociator before?" ).ignore();
      return false;
    }
    return directTable.range( trk ).empty();
  };

  //**********************************************************************
  // Global Variables
  //**********************************************************************
  m_runNr = odin->runNumber();
  m_evtNr = odin->eventNumber();
  ++m_iEvent;
  m_eventMultiplicity = tracks->size();
  //**********************************************************************

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Retrieved " << m_eventMultiplicity << " tracks." << endmsg; }

  // Loop over tracks - select some and make some plots
  m_iGoodTrack = 0;
  for ( auto iTracks = tracks->begin(); iTracks != tracks->end(); ++iTracks ) {
    LHCb::Track& aTrack = **iTracks;

    if ( m_mcData && iTracks == tracks->begin() ) {
      // Compute ghost rate
      m_ghostRate =
          (double)std::count_if( tracks->begin(), tracks->end(), [&]( const auto& t ) { return isGhostTrack( t ); } ) /
          m_eventMultiplicity;
    }

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "******************************************************" << endmsg << "Entering new good track"
              << endmsg;

    // Get the number of IT and OT hits
    //**********************************************************************
    // Tracks Variables
    //**********************************************************************
    // Ghost? ==> ghost classification
    if ( m_mcData ) {
      m_isGhostTrack             = isGhostTrack( &aTrack );
      m_ghostTrackClassification = defValue;
      LHCb::GhostTrackInfo ghostClassInfo;
      if ( m_isGhostTrack ) {
        m_ghostClassification->info( aTrack, ghostClassInfo ).ignore();
        m_ghostTrackClassification = ghostClassInfo.classification();
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Ghost classification = " << ghostClassInfo.classification() << endmsg;
      }
    }
    // Ghost Track Classification:
    // 0  = None
    // 1  = Real
    // 10 = Conversion
    // 11 = EM
    // 12 = FromPhi
    // 13 = HadronicInteraction
    // 14 = DecayInFlight
    // 20 = SpilloverAndNoise
    // 21 = GhostParent
    // 22 = InconsistentParts
    // 23 = Combinatoric

    m_nSharedHits = 0;
    m_fSharedHits = 0;
    m_nHoles      = 0;

    m_trackChi2PerDoF   = aTrack.chi2PerDoF();
    m_trackChi2Prob     = aTrack.probChi2();
    m_trackFitMatchChi2 = aTrack.info( LHCb::Track::AdditionalInfo::FitMatchChi2, 999999 );

    m_trackEta = aTrack.pseudoRapidity();

    m_trackP    = aTrack.p() / Gaudi::Units::GeV;
    m_trackPt   = aTrack.pt() / Gaudi::Units::GeV;
    m_trackErrP = sqrt( aTrack.firstState().errP2() ) / Gaudi::Units::GeV;
    if ( m_mcData ) {
      m_trackMCP  = defValue;
      m_trackMCPt = defValue;
    }

    // Track Slope and Position at 7500 mm
    LHCb::State aState = aTrack.closestState( 7500. );
    m_entryTX          = aState.tx();
    m_entryTY          = aState.ty();
    m_entryX           = aState.x();
    m_entryY           = aState.y();
    //**********************************************************************

    //**********************************************************************
    // Hits Variables
    //**********************************************************************
    m_res.clear();
    m_errRes.clear();

    m_hitX.clear();
    m_hitY.clear();
    m_hitZ.clear();

    m_nLadOverlaps = 0;
    m_ladOvlapRes.clear();
    m_ladOvlapLay.clear();

    m_nBoxOverlaps = 0;
    m_boxOvlapRes.clear();
    m_boxOvlapSta.clear();
    //**********************************************************************

    if ( m_mcData ) {
      //**********************************************************************
      // MCP and MCPt (get MCParticle linked to the track)
      //**********************************************************************
      // Retrieve the Linker table
      const LHCb::LinksByKey* links = get<LHCb::LinksByKey>( LHCb::LinksByKey::linkerName( m_tracksPath ) );

      // Get MCParticle linked by highest weight to Track and get its p and pt
      LHCb::MCParticle const* mcPart = LinkedTo<LHCb::MCParticle>{links}.range( &aTrack ).try_front();
      if ( mcPart ) {
        m_trackMCP  = mcPart->p() / Gaudi::Units::GeV;
        m_trackMCPt = mcPart->pt() / Gaudi::Units::GeV;
      }
      //**********************************************************************
    }

    for ( const auto& aID : aTrack.lhcbIDs() ) {
      //**********************************************************************
      // Number of shared hits in the track
      //**********************************************************************
      if ( std::binary_search( sharedHits.begin(), sharedHits.end(), aID, lessByID{} ) ) ++m_nSharedHits;
      //**********************************************************************
    }

    // Loop over the nodes to get the hits variables
    for ( const LHCb::FitNode* fNode : nodes( aTrack ) ) {

      // Only loop on hits with measurement
      if ( !fNode->hasMeasurement() ) continue;

      //**********************************************************************
      // Residuals and Residual Errors
      //**********************************************************************
      //     m_res.push_back(aNode.residual( ));
      //     m_errRes.push_back(aNode.errResidual( ));
      m_res.push_back( fNode->visit_r<double>(
          [&]( LHCb::FitNode::DimInfos<LHCb::Enum::nDim::Type::one> d ) { return d.unbiasedResidual( *fNode )[0]; },
          []( ... ) -> double { throw std::logic_error( "Alignment not implemented for 2D measurements" ); } ) );
      m_errRes.push_back( fNode->visit_r<double>(
          [&]( LHCb::FitNode::DimInfos<LHCb::Enum::nDim::Type::one> d ) {
            return d.errUnbiasedResidual( *fNode )[0][0];
          },
          []( ... ) -> double { throw std::logic_error( "Alignment not implemented for 2D measurements" ); } ) );
      //**********************************************************************

      //**********************************************************************
      // Hit Position
      //**********************************************************************
      Gaudi::XYZPoint hitPos = fNode->state().position();
      m_hitX.push_back( hitPos.X() );
      m_hitY.push_back( hitPos.Y() );
      m_hitZ.push_back( hitPos.Z() );
      //**********************************************************************
    }

    //**********************************************************************
    // Printing some informations in debug mode:
    //**********************************************************************
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "This track has " << 0 << " IT expected hits" << endmsg << "               " << m_nLadOverlaps
              << " IT ladder overlaps" << endmsg << "               " << m_nBoxOverlaps << " IT box overlaps" << endmsg
              << "               " << m_nHoles << " holes" << endmsg << "               " << m_nSharedHits
              << " shared hits"
              << " (= " << 100 * m_fSharedHits << " %)" << endmsg;
    //**********************************************************************

    //**********************************************************************
    // Fill the NTuple columns
    //**********************************************************************
    //     plot(variable, tag, "variable", min, max, nbins);
    // Global Variables
    trackSelTuple->column( "IEvent", m_iEvent ).ignore();
    trackSelTuple->column( "EvtNr", m_evtNr ).ignore();
    trackSelTuple->column( "RunNr", m_runNr ).ignore();
    trackSelTuple->column( "Multiplicity", m_eventMultiplicity ).ignore();
    if ( m_mcData ) trackSelTuple->column( "GhostRate", m_ghostRate ).ignore();

    trackSelTuple->column( "NVeLoClusters", 0 ).ignore();
    trackSelTuple->column( "NITClusters", 0 ).ignore();

    // Track Variables
    trackSelTuple->column( "ITrack", m_iGoodTrack ).ignore();

    if ( m_mcData ) {
      trackSelTuple->column( "IsAGhost", m_isGhostTrack ).ignore();
      trackSelTuple->column( "GhostTrackClassification", m_ghostTrackClassification ).ignore();
    }

    trackSelTuple->column( "NITHits", 0 ).ignore();
    trackSelTuple->column( "NOTHits", 0 ).ignore();
    trackSelTuple->column( "NSharedHits", m_nSharedHits ).ignore();
    trackSelTuple->column( "FSharedHits", m_fSharedHits ).ignore();
    trackSelTuple->column( "NNeighbouringHits", 0 ).ignore();
    trackSelTuple->column( "NHoles", m_nHoles ).ignore();

    trackSelTuple->column( "TrackChi2PerDoF", m_trackChi2PerDoF ).ignore();
    trackSelTuple->column( "TrackChi2Prob", m_trackChi2Prob ).ignore();
    trackSelTuple->column( "TrackFitMatchChi2", m_trackFitMatchChi2 ).ignore();

    trackSelTuple->column( "TrackEta", m_trackEta ).ignore();

    trackSelTuple->column( "TrackP", m_trackP ).ignore();
    trackSelTuple->column( "TrackPt", m_trackPt ).ignore();
    trackSelTuple->column( "TrackErrP", m_trackErrP ).ignore();
    if ( m_mcData ) {
      trackSelTuple->column( "TrackMCP", m_trackMCP ).ignore();
      trackSelTuple->column( "TrackMCPt", m_trackMCPt ).ignore();
    }
    // Track State closest to 7500. mm
    trackSelTuple->column( "TXAt7500", m_entryTX ).ignore();
    trackSelTuple->column( "TYAt7500", m_entryTY ).ignore();
    trackSelTuple->column( "XAt7500", m_entryX ).ignore();
    trackSelTuple->column( "YAt7500", m_entryY ).ignore();

    // Overlaps
    trackSelTuple
        ->farray( "LadOvlapRes", m_ladOvlapRes.begin(), m_ladOvlapRes.end(), "NLadOverlaps", int( m_maxNHits / 2 ) )
        .ignore();
    trackSelTuple
        ->farray( "LadOvlapLay", m_ladOvlapLay.begin(), m_ladOvlapLay.end(), "NLadOverlaps", int( m_maxNHits / 2 ) )
        .ignore();

    trackSelTuple->farray( "BoxOvlapRes", m_boxOvlapRes.begin(), m_boxOvlapRes.end(), "NBoxOverlaps", m_nStations )
        .ignore();
    trackSelTuple->farray( "BoxOvlapStation", m_boxOvlapSta.begin(), m_boxOvlapSta.end(), "NBoxOverlaps", m_nStations )
        .ignore();

    // Hits Variables
    trackSelTuple->farray( "Residuals", m_res.begin(), m_res.end(), "NResiduals", m_maxNHits ).ignore();
    trackSelTuple->farray( "ErrResiduals", m_errRes.begin(), m_errRes.end(), "NResiduals", m_maxNHits ).ignore();

    trackSelTuple->farray( "HitX", m_hitX.begin(), m_hitX.end(), "NResiduals", m_maxNHits ).ignore();
    trackSelTuple->farray( "HitY", m_hitY.begin(), m_hitY.end(), "NResiduals", m_maxNHits ).ignore();
    trackSelTuple->farray( "HitZ", m_hitZ.begin(), m_hitZ.end(), "NResiduals", m_maxNHits ).ignore();
    //**********************************************************************

    ++m_iGoodTrack;
    auto sc = trackSelTuple->write();
    if ( !sc ) return sc;
  }
  return StatusCode::SUCCESS;
}
//===========================================================================
