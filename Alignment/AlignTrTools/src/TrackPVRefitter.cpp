/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackSelector.h"
#include "TrackInterfaces/ITrackVertexer.h"

#include "Event/RecVertex.h"
#include "Event/Track.h"

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"

#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/ToolHandle.h"
#include "LHCbAlgs/Transformer.h"

#include <algorithm>

class TrackPVRefitter
    : public LHCb::Algorithm::Transformer<LHCb::RecVertices( const LHCb::RecVertices&, const DetectorElement& ),
                                          LHCb::DetDesc::usesConditions<DetectorElement>> {

public:
  TrackPVRefitter( const std::string& name, ISvcLocator* pSvcLocator );

  LHCb::RecVertices operator()( const LHCb::RecVertices& recvertices, const DetectorElement& geo ) const override;

private:
  ToolHandle<ITrackVertexer> m_vertexer{this, "TrackVertexer", "TrackVertexer"};
  ToolHandle<ITrackFitter>   m_trackfitter{this, "TrackFitter", ""};
  ToolHandle<ITrackSelector> m_trackselector{this, "TrackSelector", ""};
};

DECLARE_COMPONENT( TrackPVRefitter )

namespace {
  std::vector<const LHCb::Track*> myconvert( const SmartRefVector<LHCb::Track>& tracks ) {
    std::vector<const LHCb::Track*> rc;
    rc.reserve( tracks.size() );
    std::copy_if( tracks.begin(), tracks.end(), std::back_inserter( rc ),
                  []( const LHCb::Track* t ) { return t != nullptr; } );
    return rc;
  }
} // namespace

TrackPVRefitter::TrackPVRefitter( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"PVContainer", LHCb::RecVertexLocation::Primary},
                    KeyValue{"StandardGeometryTop", "/dd/Structure/LHCb"}},
                   KeyValue{"OutPVContainer", ""} ) {}

LHCb::RecVertices TrackPVRefitter::operator()( const LHCb::RecVertices& recvertices,
                                               const DetectorElement&   geo ) const {
  LHCb::RecVertices output;
  // for now I'll just create the track lists from the Best container
  for ( const auto& pv : recvertices ) {
    auto tracks = myconvert( pv->tracks() );

    // we select the tracks before we fit them, to save time
    if ( !m_trackselector.empty() ) {
      tracks.erase( std::remove_if( tracks.begin(), tracks.end(),
                                    [&]( const LHCb::Track* t ) { return !m_trackselector->accept( *t ); } ),
                    tracks.end() );
    }

    if ( !m_trackfitter.empty() ) {
      std::transform( tracks.begin(), tracks.end(), tracks.begin(), [&]( const LHCb::Track* t ) {
        StatusCode sc = m_trackfitter->operator()( const_cast<LHCb::Track&>( *t ), *geo.geometry() );
        return sc.isSuccess() ? t : nullptr;
      } );
      tracks.erase( std::remove( tracks.begin(), tracks.end(), nullptr ), tracks.end() );
    }

    if ( tracks.size() >= 2 ) {
      auto vertex = m_vertexer->fit( tracks, *geo.geometry() );
      vertex->setTechnique( pv->technique() );
      output.add( vertex.release() );
    }
  }

  return output;
}
