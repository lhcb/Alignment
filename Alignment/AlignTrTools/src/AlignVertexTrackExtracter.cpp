/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class AlignVertexTrackExtracter
 *
 *  Make a subselection of a track list
 *
 *  @author Wouter Hulsbergen
 *  @date   22/04/2024
 */

#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/SharedObjectsContainer.h"

class AlignVertexTrackExtracter
    : public Gaudi::Functional::Transformer<LHCb::Track::Selection( const LHCb::RecVertex::Range& )> {

public:
  AlignVertexTrackExtracter( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer{name, pSvcLocator, KeyValue{"InputVertices", ""}, KeyValue{"Output", ""}} {}
  LHCb::Track::Selection operator()( const LHCb::RecVertex::Range& vertices ) const override;
};

DECLARE_COMPONENT( AlignVertexTrackExtracter )

LHCb::Track::Selection AlignVertexTrackExtracter::operator()( const LHCb::RecVertex::Range& vertices ) const {
  // get all the tracks
  std::vector<const LHCb::Track*> alltracks;
  size_t                          ntracks = std::accumulate( vertices.begin(), vertices.end(), size_t( 0 ),
                                    []( size_t N, const auto& v ) { return N + v->tracks().size(); } );
  alltracks.reserve( ntracks );

  for ( const auto* v : vertices )
    for ( const auto& tr : v->tracks() )
      if ( tr ) alltracks.push_back( tr );

  // make sure all tracks are unique
  std::sort( alltracks.begin(), alltracks.end() );
  std::vector<const LHCb::Track*>::iterator it = std::unique( alltracks.begin(), alltracks.end() );
  alltracks.erase( it, alltracks.end() );

  // insert them in the output container
  LHCb::Track::Selection tracks;
  for ( const auto* tr : alltracks ) tracks.insert( tr );
  return tracks;
}
