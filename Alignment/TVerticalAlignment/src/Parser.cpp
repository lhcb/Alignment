/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "TVerticalAlignment/Parser.h"
#include <fstream>
#include <iostream>

// ClassImp(Parser);

//-----------------------------------------------------------------------------
// Implementation file for class : Parser
//
// 2011-07-12 : Frederic Guillaume Dupertuis
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================

//=============================================================================

Parser::Param_Result_Map* Parser::CreateParamResultMapFromFitFile( TString const& filename ) {
  cout << "Parsing file: " << filename << endl;
  std::ifstream NullCoord( filename.Data() );
  string        Str( "" );
  string        Str_Name( "" );
  TString       TStr_Value( "" );

  Param_Result_Map* Map = new Param_Result_Map();
  Param_Result*     ParamResult;

  while ( !NullCoord.eof() ) {
    ParamResult = new Param_Result();

    Str_Name   = "";
    TStr_Value = "";
    getline( NullCoord, Str );

    unsigned int i( 0 );
    for ( ; i < Str.size(); ++i ) {
      if ( Str[i] != ' ' )
        Str_Name += Str[i];
      else
        break;
    }

    if ( Str_Name == "" ) continue;
    ParamResult->Name = Str_Name;

    i++;
    for ( ; i < Str.size(); ++i ) {
      if ( Str[i] != ' ' )
        TStr_Value += Str[i];
      else
        break;
    }

    ParamResult->Value = TStr_Value.Atof();

    TStr_Value = "";

    i++;
    for ( ; i < Str.size(); ++i ) {
      if ( Str[i] != ' ' )
        TStr_Value += Str[i];
      else
        break;
    }

    ParamResult->Error = TStr_Value.Atof();

    ( *Map )[Str_Name] = ParamResult;
  }

  return Map;
}

Parser::XYZ_Pos_Map* Parser::CreateXYZPosMapFromFile( TString const& filename ) {
  std::ifstream NullCoord( filename.Data() );
  string        Str( "" );
  string        Str_Name( "" );
  TString       TStr_Value( "" );

  XYZ_Pos_Map* Map = new XYZ_Pos_Map();
  XYZ_Pos*     XYZPos;

  while ( !NullCoord.eof() ) {
    XYZPos = new XYZ_Pos();

    Str_Name   = "";
    TStr_Value = "";
    getline( NullCoord, Str );

    unsigned int i( 0 );
    for ( ; i < Str.size(); ++i ) {
      if ( Str[i] != ' ' )
        Str_Name += Str[i];
      else
        break;
    }

    if ( Str_Name == "" ) continue;
    XYZPos->Name = Str_Name;

    i++;
    i++;
    for ( ; i < Str.size(); ++i ) {
      if ( Str[i] != ',' )
        TStr_Value += Str[i];
      else
        break;
    }

    XYZPos->X = TStr_Value.Atof();

    TStr_Value = "";

    i++;
    for ( ; i < Str.size(); ++i ) {
      if ( Str[i] != ',' )
        TStr_Value += Str[i];
      else
        break;
    }

    XYZPos->Y = TStr_Value.Atof();

    TStr_Value = "";

    i++;
    for ( ; i < Str.size(); ++i ) {
      if ( Str[i] != ')' )
        TStr_Value += Str[i];
      else
        break;
    }

    XYZPos->Z = TStr_Value.Atof();

    ( *Map )[Str_Name] = XYZPos;
  }

  return Map;
}
