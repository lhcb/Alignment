/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#ifndef OT2DPLOT_H
#define OT2DPLOT_H

#include "OTNames.h"
#include "TH2D.h"
#include <map>
#include <string>

using namespace std;

/** @class OT2DPlot OT2DPlot.h macros/OT2DPlot.h
 *   *
 *  @author Frederic Guillaume Dupertuis
 *  @date   2010-11-23
 */
class OT2DPlot {
public:
  /// Standard constructor
  OT2DPlot(){};
  OT2DPlot( const char* name, const char* title, const char* plotType = "Module" );
  virtual ~OT2DPlot(){}; ///< Destructor

  void  Draw( Option_t* option = "" );
  void  Fill( string const& module, double const& value, bool const& qcombined = false );
  TH2D* Histogram() { return m_hmap; }

protected:
private:
  std::map<int, double> m_XMap;
  std::map<int, double> m_YMap;
  OTNames*              m_names;
  TString               m_plotType;

  int    m_NBinX;
  int    m_NBinY;
  double m_LowX;
  double m_LowY;
  double m_UpX;
  double m_UpY;

  TH2D* m_hmap;

  void InitMaps();
  void PlotLabels();
  void PlotBoxes();
  // ClassDef(OT2DPlot,1);
};

#endif
