/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ALIGNMENT_OTYALIGNMENT_OTYALIGNMENT_H
#define ALIGNMENT_OTYALIGNMENT_OTYALIGNMENT_H

// USER
#include "OT2DPlot.h"
#include "Parser.h"
// STL
#include <fstream>
#include <iostream>
#include <list>
#include <map>
#include <stdio.h>
#include <string>
#include <utility>
#include <vector>
// ROOT
#include "TCanvas.h"
#include "TColor.h"
#include "TCut.h"
#include "TF1.h"
#include "TFile.h"
#include "TFitResult.h"
#include "TGraphErrors.h"
#include "TH1I.h"
#include "TH2I.h"
#include "THStack.h"
#include "TKey.h"
#include "TLegend.h"
#include "TMath.h"
#include "TPad.h"
#include "TPaveStats.h"
#include "TProfile.h"
#include "TROOT.h"
#include "TString.h"
#include "TStyle.h"
#include "TSystem.h"
#include "TTree.h"

// RooFit
#include "RooAbsBinning.h"
#include "RooAbsDataStore.h"
#include "RooAddPdf.h"
#include "RooArgSet.h"
#include "RooBinning.h"
#include "RooCategory.h"
#include "RooChebychev.h"
#include "RooConstVar.h"
#include "RooCurve.h"
#include "RooCustomizer.h"
#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooDecay.h"
#include "RooEffProd.h"
#include "RooEfficiency.h"
#include "RooExponential.h"
#include "RooFitResult.h"
#include "RooGaussModel.h"
#include "RooGaussian.h"
#include "RooGenericPdf.h"
#include "RooHist.h"
#include "RooHistPdf.h"
#include "RooPlot.h"
#include "RooProdPdf.h"
#include "RooRealVar.h"
#include "RooSimultaneous.h"
#include "RooStats/SPlot.h"
#include "RooTrace.h"
#include "RooTruthModel.h"
#include "RooUnblindUniform.h"
#include "TAxis.h"
#include "TCanvas.h"

#include <boost/variant/get.hpp>
#include <boost/variant/variant.hpp>

#include "TVerticalAlignment/OT2DPlot.h"
#include "TVerticalAlignment/Parser.h"
#include "TVerticalAlignment/TVerticalAlignment.h"

using namespace std;
using namespace RooFit;

namespace Alignment {
  namespace TVerticalAlignment {

    class OTYAlignMagOff {
    public:
      // Constructors
      OTYAlignMagOff( TString filename, TString dbfilename, TString outputdir, bool constraint = false,
                      bool saveplots = false );
      // Destructor
      ~OTYAlignMagOff() {}

      // Methods
      void fit_efficiency();
      void plots();
      void glimpse_data();

    private:
      TVerticalAlignment* m_va;
      Param               m_param;
      TString             m_filename;
      TString             m_dbfilename;
      TString             m_outDirectory;
      bool                m_constraint;
      bool                m_saveplots;

      bool m_glimpse = false;

      OTNames*                      m_Names;
      std::map<std::string, double> m_GlobalXCoord;
      std::map<std::string, double> m_GlobalYCoord;

      ofstream m_YPosFitFile;
      ofstream m_LengthFitFile;
      ofstream m_DistanceFitFile;
      ofstream m_FitParameterFile;
      TString  m_outputname;

      std::map<std::string, double> CreateXMapFromFile();
      std::map<std::string, double> CreateYMapFromFile();
      void                          FitfromROOTFile();
      void                          GetMeanFromHisto( TString ModuleName );
      double                        GetShift( TString ModuleName, TString ModuleName2 );
      RooDataSet* GetRooDataSetFromTH1( TH1F* histoA1, TH1F* histoA1_exp, TH1F* histoA2, TH1F* histoA2_exp,
                                        TH1F* histoB1, TH1F* histoB1_exp, TH1F* histoB2, TH1F* histoB2_exp,
                                        RooRealVar y, RooRealVar weight, RooCategory cal_lr, RooCategory cat_AB,
                                        RooCategory cat_eff, double Distance, double Length, double EdgeScale );
    };

  } // namespace TVerticalAlignment
} // namespace Alignment

#endif
