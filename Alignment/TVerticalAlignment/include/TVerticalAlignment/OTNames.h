/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#ifndef OTNAMES_H
#define OTNAMES_H

#include <map>
#include <string>
#include <vector>

#include "TCanvas.h"
#include "TColor.h"
#include "TPad.h"
#include "TROOT.h"
#include "TString.h"
#include "TStyle.h"
#include "TSystem.h"
#include <iostream>

#include "RooDataHist.h"
#include "RooDataSet.h"
#include "RooFit.h"
#include "RooRealVar.h"
using namespace std;

/** @class OTNames OTNames.h macros/OTNames.h
 *   *
 *  @author Frederic Guillaume Dupertuis
 *  @date   2011-05-14
 */
class OTNames {
public:
  struct ModuleNames {
    int     UniqueModule;
    TString RAWNickname;
    TString Nickname;
    int     OTno;
    TString Layer;
    int     Quadrantno;
    int     Moduleno;
  };
  struct LayerNames {
    int     UniqueLayer;
    TString Nickname;
    int     OTno;
    TString Layer;
  };

  struct StationNames {
    int     UniqueStation;
    TString Nickname;
    int     OTno;
  };

  typedef std::vector<std::string>            Vector;
  typedef std::map<std::string, ModuleNames>  MapModuleNames;
  typedef std::map<std::string, LayerNames>   MapLayerNames;
  typedef std::map<std::string, StationNames> MapStationNames;

  /// Standard constructor
  OTNames();
  virtual ~OTNames(){}; ///< Destructor

  OTNames::Vector*      GetModules();
  OTNames::Vector*      GetLayers();
  OTNames::Vector*      GetStations();
  OTNames::ModuleNames* GetModuleNames( string const& module );
  int                   GetUniqueModule( string const& module );
  int                   GetUniqueLayer( string const& layer );
  int                   GetUniqueStation( string const& station );
  string                GetRAWModuleName( string const& module );
  string                GetLayerName( string const& module );
  string                GetStationName( string const& module );
  string                GetGlobalName();

protected:
private:
  void Init();
  void InitOTMapNames( int const& OTno, int const& Moduleno, int const& Quadrant, const char* Layer );

  Vector          m_Modules;
  Vector          m_Layers;
  Vector          m_Stations;
  MapModuleNames  m_MapModuleNames;
  MapLayerNames   m_MapLayerNames;
  MapStationNames m_MapStationNames;
  bool            m_init;

  // ClassDef(OTNames,1);
};

#endif
