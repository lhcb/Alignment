from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys
import glob


def read_yalign_IT(results_yalign, yalign):
    if os.path.isfile(results_yalign):
        with open(results_yalign, 'r') as myfile:
            for line in myfile:
                if not line.rstrip(): continue
                if line.rstrip().startswith('#'): continue
                box = line.split(":")[0].strip().replace('IT', 'ITT') + 'Box'
                if not box in yalign: continue
                value = line.split(":")[1].split("+-")[0].split()[-1]
                yalign[box] = round(float(value), 2)


def create_db_IT(xml_file_name, xml_file_withY):
    import xml.etree.ElementTree as ET
    import time
    file2write = open(xml_file_withY, 'w')
    if os.path.isfile(xml_file_name):
        with open(xml_file_name, 'r') as myfile:
            for line in myfile:
                if '<!--' in line:
                    if 'Author' in line or 'Version' in line:
                        file2write.write(line)
                    elif 'Description' in line:
                        file2write.write(
                            "<!-- Description:: vertical alignment corrections -->"
                            + '\n')
                    else:
                        file2write.write("<!-- " + time.strftime(
                            "%Y-%B-%d - %H:%M:%S", time.gmtime()) + " -->" +
                                         '\n')
                    continue
                if not line.rstrip():
                    file2write.write(line)
                    continue

                root = ET.fromstring(line.rstrip())
                boxname = root.attrib['name']

                if not boxname in yalign:
                    file2write.write(line)
                    continue

                for child in root:
                    if child.attrib['name'] == 'dPosXYZ':
                        x, y, z = child.text.split()
                        if 'TopBox' in boxname:
                            child.text = '{0} {1} {2}'.format(
                                x,
                                float(y) + yalign[boxname], z)
                        else:
                            child.text = '{0} {1} {2}'.format(
                                x,
                                float(y) - yalign[boxname], z)
                        file2write.write(ET.tostring(root) + '\n')
    file2write.close()


def GetListXmlFiles(fdir):
    flist = []
    for file in os.listdir(fdir):
        if file.endswith(".xml"):
            flist += [file]
    return flist


def GetXmlId(fname):
    Id = int(fname.split('/')[-1].replace('v', '').replace('.xml', ''))
    return Id


def FindLastVersion(fdir):
    flist = GetListXmlFiles(fdir)
    ids = []
    for l in flist:
        ids += [GetXmlId(l)]
        #ids += [int(l.replace('v','').replace('.xml',''))]
    return fdir + '/v%d.xml' % max(ids)


def FindLatestVersion(fdir):
    flist = GetListXmlFiles(fdir)
    newest = max(glob.iglob(fdir + '/*.xml'), key=os.path.getctime)
    return newest


def PrintLatestAlignment(xmlDir='/group/online/alignment'):
    DetDirs = {
        'TT': ['Modules', 'Global'],
        'IT': ['Modules', 'Global'],
        'OT': ['Modules', 'Global']
    }
    for det in DetDirs:
        for i in range(2):
            print(
                FindLastVersion(xmlDir + '/' + det + '/' + det +
                                DetDirs[det][i]))
    return


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Study signal efficiency.')
    parser.add_argument(
        '--correction',
        metavar='correction',
        action='store',
        type=str,
        default='/home/zhxu/Public/misalign_y_IT_boxes.txt',
        help='file name for the y alignment corrections')
    args = parser.parse_args()
    results_yalign = args.correction

    xmlDir = '/group/online/alignment'
    print('Latest alignment versions:')
    PrintLatestAlignment(xmlDir)

    print('Apply Y alignment corrections: ')
    yalign = {
        'ITT1TopBox': 0,
        'ITT1BottomBox': 0,
        'ITT1CSideBox': 0,
        'ITT1ASideBox': 0,
        'ITT2TopBox': 0,
        'ITT2BottomBox': 0,
        'ITT2CSideBox': 0,
        'ITT2ASideBox': 0,
        'ITT3TopBox': 0,
        'ITT3BottomBox': 0,
        'ITT3CSideBox': 0,
        'ITT3ASideBox': 0,
    }
    #-->read alignment parameters from a file
    sc = input("Import Y alignment corrections from file %s [yes/no]: " %
               results_yalign)
    if sc == 'no':
        results_yalign = input("Please enter the correct file name: ")
        if not os.path.isfile(results_yalign):
            print("%s is not a valid file!" % results_yalign)
            sys.exit()
    read_yalign_IT(results_yalign, yalign)
    print()
    print(' ----- ' * 20)
    for box, value in yalign.items():
        print('{0:15s} {1:.2f}'.format(box, value))

    sc = input("I comfirm these values are correct [yes/no]: ")
    if sc == 'no':
        sys.exit()
    print(' ----- ' * 20)
    #-->create a new xml file with the Y alignment corrections applied
    xml_file_name = FindLastVersion(os.path.join(xmlDir, 'IT/ITGlobal'))
    IdLast = GetXmlId(xml_file_name)
    IdNew = IdLast + 1
    xml_file_withY = os.path.join(xmlDir, 'IT/ITGlobal', 'v%d.xml' % IdNew)

    sc = input("Create the xml file %s [yes/no]: " % xml_file_withY)
    if sc == 'no':
        sys.exit()

    create_db_IT(xml_file_name, xml_file_withY)
    print('created new xml file: ', xml_file_withY)

    # print new alignment versions
    print('\nNew alignment versions:')
    PrintLatestAlignment(xmlDir)
