###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os, sys
from AlignmentOutputParser.AlPlotter import *
from AlignmentOutputParser.AlignOutput import *

fileName = '/afs/cern.ch/work/m/mamartin/Alignment2015/outputs/run154182_startfromITsurvey2/alignlog.txt'
aout = AlignOutput(fileName)
aout.Parse()
alIters = aout.AlignIterations
apl = AlPlotter(alIters)
apl.PlotLocalDeltaChi2()
