/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <fstream>
#include <map>
#include <vector>

namespace LHCb::Alignment {
  namespace FileIO {
    // Be careful: you NEVER want to put these in global scope

    // IO for a generic simple object
    template <typename T>
    std::ofstream& operator<<( std::ofstream& file, const T& object ) {
      // printf("Trying to write object with size: %u\n",sizeof( object )) ;
      // static_assert( std::is_trivially_copyable_v<T> );
      file.write( reinterpret_cast<const char*>( &object ), sizeof( object ) );
      return file;
    }
    template <typename T>
    std::ifstream& operator>>( std::ifstream& file, T& object ) {
      // printf("Trying to read object with size: %u\n",sizeof( object )) ;
      // static_assert( std::is_trivially_copyable_v<T> );
      file.read( reinterpret_cast<char*>( &object ), sizeof( object ) );
      return file;
    }

    // IO for a map
    template <typename Key, typename T, typename Compare, typename Allocator>
    std::ofstream& operator<<( std::ofstream& file, const std::map<Key, T, Compare, Allocator>& amap ) {
      size_t n = amap.size();
      // printf("Trying to write map with size: %d\n",int(n)) ;
      file << n;
      for ( const auto& [f, s] : amap ) file << f << s;
      return file;
    }
    template <typename Key, typename T, typename Compare, typename Allocator>
    std::ifstream& operator>>( std::ifstream& file, std::map<Key, T, Compare, Allocator>& amap ) {
      size_t n;
      file >> n;
      // printf("Trying to read map with size: %d\n",int(n)) ;
      Key key;
      T   obj;
      for ( size_t i = 0; i < n; ++i ) {
        file >> key >> obj;
        amap[key] = obj; // could be done more efficienctly
      }
      return file;
    }

    // IO for a vector
    template <typename T>
    std::ofstream& operator<<( std::ofstream& file, const std::vector<T>& v ) {
      size_t n = v.size();
      file << n;
      for ( typename std::vector<T>::const_iterator it = v.begin(); it != v.end(); ++it ) file << *it;
      return file;
    }
    template <typename T>
    std::ifstream& operator>>( std::ifstream& file, std::vector<T>& v ) {
      size_t n;
      file >> n;
      v.resize( n );
      for ( size_t i = 0; i < n; ++i ) file >> v[i];
      return file;
    }
  } // namespace FileIO
} // namespace LHCb::Alignment
