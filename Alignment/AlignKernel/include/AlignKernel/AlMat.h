/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include <Eigen/Dense>

/// Basically just a wrapper around the Eigen class, but with zero initialization
class AlMat : public Eigen::MatrixXd {
public:
  using Base = Eigen::MatrixXd;
  AlMat( Index n, Index m ) : Base( Base::Zero( n, m ) ) {}
  AlMat( size_t n, size_t m ) : Base( Base::Zero( n, m ) ) {}
  using Base::Base;
};
