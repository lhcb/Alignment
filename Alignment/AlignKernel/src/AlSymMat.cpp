/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "gsl/gsl_eigen.h"
#include "gsl/gsl_math.h"
#include "gsl/gsl_matrix.h"

#include "AlignKernel/AlSymMat.h"

AlSymMat AlSymMat::operator+( const AlSymMat& rhs ) const {
  auto b = *this;
  b += rhs;
  return b;
}

AlSymMat& AlSymMat::operator+=( const AlSymMat& rhs ) {

  if ( size() != rhs.size() ) {
    std::cerr << "operator+=: size do not match!" << std::endl;
    return *this;
  }

  for ( size_t i = 0; i < m_data.size(); ++i ) m_data[i] += rhs.m_data[i];
  return *this;
}

AlSymMat AlSymMat::operator-( const AlSymMat& rhs ) const {
  auto b = *this;
  b -= rhs;
  return b;
}

AlSymMat& AlSymMat::operator-=( const AlSymMat& rhs ) {

  if ( size() != rhs.size() ) {
    std::cerr << "operator+=: size do not match!" << std::endl;
    return *this;
  }

  for ( size_t i = 0; i < m_data.size(); ++i ) m_data[i] -= rhs.m_data[i];
  return *this;
}

AlSymMat& AlSymMat::operator*=( const double& d ) {
  for ( auto& e : m_data ) e *= d;
  return *this;
}

double AlSymMat::determinant() const {

  double deter = 1.;

  // get determinant using LU decomposition + Crout algorithm

  AlMat A = this->toMatrix();

  double     AMAX, DUM, SUM;
  int        CODE;
  double     TINY = 1.E-20;
  double     D;
  const auto N = rows();

  D    = 1.;
  CODE = 0;

  for ( size_t I = 0; I < N; ++I ) {
    AMAX = 0.;
    for ( size_t J = 0; J < N; ++J ) {
      if ( std::abs( A( I, J ) ) > AMAX ) AMAX = std::abs( A( I, J ) );
    }
    if ( AMAX < TINY ) {
      CODE = 1; // matrix is singular
      return 0;
    }
  }

  for ( size_t J = 0; J < N; J++ ) {

    for ( size_t I = 0; I < N; I++ ) {

      SUM         = A( I, J );
      size_t KMAX = ( I < J ) ? I : J;
      for ( size_t K = 0; K < KMAX; K++ ) SUM = SUM - A( I, K ) * A( K, J );
      A( I, J ) = SUM;
    }

    // find pivot and exchange if necessary

    size_t IMAX = J;
    for ( size_t I = J + 1; I < N; I++ ) {
      if ( std::abs( A( I, J ) ) > std::abs( A( IMAX, J ) ) ) IMAX = I;
    }

    if ( IMAX != J ) {
      for ( size_t K = 0; K < N; K++ ) {
        DUM          = A( IMAX, K );
        A( IMAX, K ) = A( J, K );
        A( J, K )    = DUM;
      }
      D = -D;
    }

    if ( J < N && ( std::abs( A( J, J ) ) > TINY ) ) {

      DUM = 1. / A( J, J );
      for ( size_t I = J + 1; I < N; I++ ) A( I, J ) = A( I, J ) * DUM;
    }
  }

  if ( CODE == 0 ) {
    deter = deter * D;
    for ( size_t I = 0; I < N; I++ ) { deter = deter * A( I, I ); }
    return deter;

  } else {
    return 0;
  }
}

int AlSymMat::diagonalize_GSL( AlVec& egval, AlMat& egvec ) const {

  const auto N = rows();

  // std::cout << " Debug...before gsl_matrix_alloc" << std::endl;

  gsl_matrix* m = gsl_matrix_alloc( N, N );

  // std::cout << " Debug...after gsl_matrix_alloc" << std::endl;

  //  if (1) {
  //    AlMat V(N,N);
  //    V.copyS((*this));
  //    m->data = V.ptr_data;
  for ( size_t ig = 0; ig < N; ig++ ) {
    for ( size_t jg = 0; jg < N; jg++ ) { gsl_matrix_set( m, ig, jg, ( *this )( ig, jg ) ); }
  }
  //    gsl_matrix_set(m,0,0,temp00);
  //  std::cout<<"gsl00: "<<gsl_matrix_get(m,0,0)<<std::endl;

  // }

  gsl_vector* eval = gsl_vector_alloc( N );
  gsl_matrix* evec = gsl_matrix_alloc( N, N );

  gsl_eigen_symmv_workspace* w  = gsl_eigen_symmv_alloc( N );
  int                        rc = gsl_eigen_symmv( m, eval, evec, w );

  gsl_eigen_symmv_free( w );

  // std::cout << " Debug...after diagonalization" << std::endl;

  // sort eigen values/eigen vectors in ascending order
  gsl_eigen_symmv_sort( eval, evec, GSL_EIGEN_SORT_VAL_ASC );

  for ( size_t ig = 0; ig < N; ++ig ) {
    egval( ig ) = gsl_vector_get( eval, ig );
    for ( size_t jg = 0; jg < N; ++jg ) { egvec( ig, jg ) = gsl_matrix_get( evec, ig, jg ); }
  }
  gsl_matrix_free( evec );
  gsl_vector_free( eval );

  return rc;
}
