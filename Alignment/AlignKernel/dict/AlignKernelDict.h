/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: AlignKernelDict.h,v 1.1 2009-07-06 12:40:02 wouter Exp $
#ifndef DICT_SOLVKERNELDICT_H
#define DICT_SOLVKERNELDICT_H 1

// this line is necessary to add ROOT::SMatrices to the dictionary
#define G__DICTIONARY

#include "AlignKernel/AlEquations.h"
#include "AlignKernel/AlMat.h"
#include "AlignKernel/AlSymMat.h"
#include "AlignKernel/AlVec.h"
#include "AlignKernel/OTMonoLayerAlignData.h"

#endif // DICT_SOLVKERNELDICT_H
