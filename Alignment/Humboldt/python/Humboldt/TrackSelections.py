###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.tonic import configurable
from PyConf.Tools import TrackSelector, TrackMasterFitter, TrackLinearExtrapolator, VPTrackSelector, TrackSimpleExtraSelector, TrackMasterExtrapolator, SimplifiedMaterialLocator
from PyConf.Algorithms import TrackEventFitter, Deprecated__TrackListRefiner as TrackListRefiner, LHCb__Converters__Track__v1__fromLHCbTrackVector as fromLHCbTrackVector, TrackSelectionToContainer, SharedTrackEventFitter

from RecoConf.hlt2_tracking import get_global_measurement_provider, kill_clones


def HighMomentumTTracks(input_tracks):
    myTrackSelector = TrackSelector(
        TrackTypes=["Ttrack"], MinPCut=50000, MaxChi2Cut=5)
    selected_tracks = TrackListRefiner(
        name='TrackListRefinerHighMomentumTTracks',
        inputLocation=input_tracks,
        Selector=myTrackSelector).outputLocation
    return selected_tracks


@configurable
def GoodLongTracks(input_tracks,
                   MinPCut=5000.,
                   MinPtCut=200.,
                   MaxChi2PerDoFMatch=5.):
    myTrackSelector = TrackSelector(
        TrackTypes=["Long"],
        MinPCut=MinPCut,
        MaxPCut=200000,
        MinPtCut=MinPtCut,
        MaxNTHoles=1,
        MaxChi2Cut=5,
        MaxChi2PerDoFMatch=MaxChi2PerDoFMatch,
        MaxChi2PerDoFVelo=5,
        MaxChi2PerDoFDownstream=5)
    selected_tracks = TrackListRefiner(
        name='TrackListRefinerGoodLongTracks',
        inputLocation=input_tracks,
        Selector=myTrackSelector).outputLocation
    return selected_tracks


def VPBackwardTracks(input_tracks, **kwargs):
    VPBackwardTrackSelector = TrackSelector(
        TrackTypes=["VeloBackward"], **kwargs)
    selected_BackwardTracks = TrackListRefiner(
        name='TrackListRefinerVPBackwardsTracks',
        inputLocation=input_tracks,
        Selector=VPBackwardTrackSelector).outputLocation
    return selected_BackwardTracks


def VPOverlapTracks(input_tracks, MaxChi2Dof=5):
    OverlapTrackSelector = TrackSelector(
        TrackTypes=["Velo", "VeloBackward", "Long"],
        MinNVeloALayers=2,
        MinNVeloCLayers=2,
        MaxChi2Cut=MaxChi2Dof)
    return TrackListRefiner(
        name="VPOverlapTracks",
        inputLocation=input_tracks,
        Selector=OverlapTrackSelector).outputLocation


def VPLongTracks(input_tracks):
    VPLongTrackSelector = TrackSelector(
        TrackTypes=["Long"],
        MinNVeloLayers=4,
        MinPCut=5000,
        MaxPCut=1000000.,
        MinPtCut=200,
        MaxChi2Cut=5,
        MaxChi2PerDoFVelo=5)
    return TrackListRefiner(
        inputLocation=input_tracks,
        Selector=VPLongTrackSelector).outputLocation


def VPGoodTracks(input_tracks, MinNVeloLayers=4, MaxChi2Dof=5):
    selector = TrackSelector(
        TrackTypes=["Velo", "VeloBackward", "Long", "Upstream"],
        MinNVeloLayers=MinNVeloLayers,
        MaxChi2Cut=MaxChi2Dof)
    return TrackListRefiner(
        name="VPGoodTracks", inputLocation=input_tracks,
        Selector=selector).outputLocation


def VPTileOverlapTracks(input_tracks, MinNVeloLayers=3, MaxChi2Dof=100):
    selector = VPTrackSelector(
        RequireTileOverlap=True,
        MinNVeloLayers=MinNVeloLayers,
        MaxChi2Cut=MaxChi2Dof)
    return TrackListRefiner(
        name="VPTileOverlapTracks",
        inputLocation=input_tracks,
        Selector=selector).outputLocation


def VPBeamHaloTracks(input_tracks, MinNVeloLayers=12, MaxChi2Dof=100):
    selector = VPTrackSelector(
        MinNVeloLayers=MinNVeloLayers, MaxChi2Cut=MaxChi2Dof)
    return TrackListRefiner(
        name="VPBeamHaloTracks", inputLocation=input_tracks,
        Selector=selector).outputLocation


def VPRareUsefulTracks(input_tracks):
    from PyConf.Algorithms import AlignTrackCloneRemover
    return AlignTrackCloneRemover(InputLocations=[
        VPOverlapTracks(input_tracks=input_tracks),
        VPTileOverlapTracks(input_tracks=input_tracks),
        VPBeamHaloTracks(input_tracks=input_tracks)
    ]).OutputLocation


def selectAndRefitVeloOnly(inputtracks):
    # first select tracks with velo hits
    veloselector = VPTrackSelector(
        TrackTypes=["Velo", "VeloBackward", "Long", "Upstream"],
        MinHits=5,
        MaxChi2Cut=5)
    selectedTracks = TrackListRefiner(
        inputLocation=inputtracks, Selector=veloselector).outputLocation
    # now reft them
    measprovider = get_global_measurement_provider(
        ignoreUT=True, ignoreFT=True, ignoreMuon=True)
    materiallocator = SimplifiedMaterialLocator()
    extrapolatorselector = TrackSimpleExtraSelector(
        ExtrapolatorName="TrackLinearExtrapolator")
    masterextrapolator = TrackMasterExtrapolator(
        ExtraSelector=extrapolatorselector,
        ApplyEnergyLossCorr=False,
        ApplyElectronEnergyLossCorr=False,
        MaterialLocator=materiallocator)
    trackfitter = TrackMasterFitter(
        MeasProvider=measprovider,
        MaterialLocator=materiallocator,
        Extrapolator=masterextrapolator,
        MaxUpdateTransports=1,
        MinNumVPHitsForOutlierRemoval=8,  # default value it too low
        MakeMeasurements=True,
        MakeNodes=True  # need to force a refit
    )
    veloonlyfitter = SharedTrackEventFitter(
        TracksInContainer=selectedTracks, Fitter=trackfitter)
    return veloonlyfitter.TracksOutContainer
