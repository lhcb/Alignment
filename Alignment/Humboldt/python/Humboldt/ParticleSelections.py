###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import MeV, GeV, mm, ps
from PyConf.tonic import configurable


# dummy empty particle container to use for mandatory input of alignment
def DummyParticles():
    from PyConf.Algorithms import ParticlesEmptyProducer
    dummyparticles = ParticlesEmptyProducer().Output
    return dummyparticles


@configurable
def defaultHLT1D0Selection(vertices_v2,
                           mass_min=1845 * MeV,
                           mass_max=1885 * MeV,
                           trackptmin=1. * GeV,
                           trackpmin=5. * GeV):
    import Functors as F
    from RecoConf.standard_particles import make_has_rich_long_pions, make_has_rich_long_kaons
    from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
    from Functors.math import in_range

    loosepions = make_has_rich_long_pions()
    loosekaons = make_has_rich_long_kaons()

    minIP = 0.07 * mm
    minIPChi2 = 16  # make sure tracks have not been used in PVs

    pions = ParticleFilter(
        loosepions,
        F.FILTER(
            F.require_all(
                F.PT > trackptmin, F.P > trackpmin, F.PID_K < 0.,
                F.MINIPCHI2CUT(IPChi2Cut=minIPChi2, Vertices=vertices_v2),
                F.MINIPCUT(IPCut=minIP, Vertices=vertices_v2))))

    kaons = ParticleFilter(
        loosekaons,
        F.FILTER(
            F.require_all(
                F.PT > trackptmin, F.P > trackpmin, F.PID_K > 5.,
                F.MINIPCHI2CUT(IPChi2Cut=minIPChi2, Vertices=vertices_v2),
                F.MINIPCUT(IPCut=minIP, Vertices=vertices_v2))))

    particles = [pions, kaons]

    combination_code = F.require_all(
        in_range(1760 * MeV, F.MASS, 1960 * MeV),
        F.SUM(F.PT) > 2000 * MeV,
        F.MAXDOCACUT(0.1 * mm),
    )
    vertex_code = F.require_all(
        F.CHI2DOF < 10., F.OWNPVETA > 2, F.OWNPVETA < 5,
        in_range(mass_min, F.MASS,
                 mass_max), F.END_VZ > -341. * mm, F.OWNPVIPCHI2 < 10,
        F.OWNPVDLS > 5, F.OWNPVLTIME > 0.450 * ps, F.OWNPVDIRA > 0.9995)

    combinedD0 = ParticleCombiner([pions, kaons],
                                  name="AlignHLTD0_{hash}",
                                  DecayDescriptor="[D0 -> pi+ K-]cc",
                                  CombinationCut=combination_code,
                                  CompositeCut=vertex_code,
                                  PrimaryVertices=vertices_v2)
    return combinedD0


@configurable
def defaultDetachedJpsi(vertices_v2, trackptmin=1. * GeV, trackpmin=6. * GeV):
    #TODO: get particles from HLT SelReports instead of rebuilding
    # NOTE: selections match those in HLT... etc

    import Functors as F
    from RecoConf.standard_particles import make_ismuon_long_muon
    from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
    from Functors.math import in_range

    loosemuons = make_ismuon_long_muon()

    minIP = 0.07 * mm
    maxvertexchi2 = 10
    minPIDmu = 0
    maxdoca = 0.2 * mm
    min_bpvfdchi2 = 5
    minDira = 0.999
    minSumPt = 2000 * MeV
    maxMuonchi2Corr = 1.8

    #F.PID_MU > minPIDmu,

    muons = ParticleFilter(
        loosemuons,
        F.FILTER(
            F.require_all(F.PT > trackptmin, F.P > trackpmin,
                          F.MUONCHI2CORR() < maxMuonchi2Corr,
                          F.MINIPCUT(IPCut=minIP, Vertices=vertices_v2))))

    particles = [muons, muons]

    combination_code = F.require_all(
        in_range(2950 * MeV, F.MASS, 3250 * MeV), F.MAXDOCACUT(maxdoca),
        F.SUM(F.PT) > minSumPt)
    vertex_code = F.require_all(F.CHI2DOF < maxvertexchi2,
                                F.BPVETA(vertices_v2) > 2,
                                F.BPVETA(vertices_v2) < 5,
                                F.BPVFDCHI2(vertices_v2) > min_bpvfdchi2,
                                F.BPVDIRA(vertices_v2) > minDira)

    detatchedJpsi = ParticleCombiner([muons, muons],
                                     name="AlignHLTdetachedJPsi",
                                     DecayDescriptor="J/psi(1S) -> mu+ mu-",
                                     CombinationCut=combination_code,
                                     CompositeCut=vertex_code)

    return detatchedJpsi


@configurable
def defaultUpsilonMuMu(pvs, trackptmin=4. * GeV, trackpmin=10. * GeV):
    #TODO: get particles from HLT SelReports instead of rebuilding
    # NOTE: selections match those in HLT... etc

    import Functors as F
    from RecoConf.standard_particles import make_long_muons
    from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
    from Functors.math import in_range

    loosemuons = make_long_muons()
    maxIPchi2 = 10
    maxvertexchi2 = 10
    maxdoca = 0.5

    muons = ParticleFilter(
        loosemuons,
        F.FILTER(
            F.require_all(F.PT > trackptmin, F.P > trackpmin,
                          F.MINIPCHI2(pvs) < maxIPchi2)))

    combination_code = F.require_all(
        in_range(8800 * MeV, F.MASS, 11000 * MeV), F.MAXDOCACUT(maxdoca))
    vertex_code = F.require_all(
        F.CHI2DOF < maxvertexchi2,
        in_range(9000 * MeV, F.MASS, 10800 * MeV),
        # Think carefully about whether this is the right functor
        #        F.BPVETA(vertices_v2) > 2,
        #        F.BPVETA(vertices_v2) < 5
    )

    defaultZ = ParticleCombiner([muons, muons],
                                name="MonitoringUpsilon",
                                DecayDescriptor="Z0 -> mu+ mu-",
                                CombinationCut=combination_code,
                                CompositeCut=vertex_code)

    return defaultZ


def defaultHLTZ(vertices_v2):
    #TODO: get particles from HLT SelReports instead of rebuilding
    # NOTE: selections match those in HLT... etc

    import Functors as F
    from RecoConf.standard_particles import make_long_muons
    from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
    from Functors.math import in_range

    loosemuons = make_long_muons()

    ptmin = 300 * MeV
    pmin = 6000 * MeV
    minIPchi2 = 0
    maxvertexchi2 = 25
    maxdoca = 0.2

    muons = ParticleFilter(
        loosemuons,
        F.FILTER(
            F.require_all(
                F.PT > ptmin, F.P > pmin,
                F.MINIPCHI2CUT(IPChi2Cut=minIPchi2, Vertices=vertices_v2))))

    particles = [muons, muons]

    combination_code = F.require_all(
        in_range(78690 * MeV, F.MASS, 103690 * MeV),
        #        F.Z > -300 * mm,
        F.MAXDOCACUT(maxdoca))
    vertex_code = F.require_all(
        F.CHI2DOF < maxvertexchi2,
        # Think carefully about whether this is the right functor
        #        F.BPVETA(vertices_v2) > 2,
        #        F.BPVETA(vertices_v2) < 5
    )

    defaultZ = ParticleCombiner([muons, muons],
                                name="AlignHLTZ",
                                DecayDescriptor="Z0 -> mu+ mu-",
                                CombinationCut=combination_code,
                                CompositeCut=vertex_code)

    return defaultZ


def defaultZ0(vertices_v2):
    #TODO: get particles from HLT SelReports instead of rebuilding
    # NOTE: selections match those in HLT... etc

    import Functors as F
    from RecoConf.standard_particles import make_ismuon_long_muon
    from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
    from Functors.math import in_range

    loosemuons = make_ismuon_long_muon()

    ptmin = 10 * GeV
    pmax = 2000 * GeV
    maxIPchi2 = 100
    maxvertexchi2 = 25
    maxdoca = 0.2

    muons = ParticleFilter(
        loosemuons,
        F.FILTER(
            F.require_all(F.PT > ptmin, F.P < pmax,
                          F.BPVIPCHI2(vertices_v2) < maxIPchi2)))

    combination_code = F.require_all(
        in_range(78690 * MeV, F.MASS, 103690 * MeV),
        F.SDOCA(1, 2) < maxdoca)
    vertex_code = F.require_all(F.CHI2DOF < maxvertexchi2)
    defaultZ = ParticleCombiner([muons, muons],
                                name="DefaultZ0",
                                DecayDescriptor="Z0 -> mu+ mu-",
                                CombinationCut=combination_code,
                                CompositeCut=vertex_code)

    return defaultZ
