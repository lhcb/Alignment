###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.application import configure, make_odin
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.hlt2_tracking import make_VPClus_hits
from DDDB.CheckDD4Hep import UseDD4Hep
from PyConf.tonic import configurable
from RecoConf.event_filters import require_gec
from .ParticleSelections import DummyParticles
from .VertexSelections import DummyPVs
from PyConf.Tools import (AlignUpdateTool, EigenDiagSolvTool)
from PyConf.Algorithms import (TrackFitMatchMonitor, TrackMonitor,
                               TrackVPOverlapMonitor, AlignAlgorithm,
                               TrackVertexMonitor, AlignTrackCloneRemover,
                               VPTrackMonitor, TrackPV2HalfMonitor)


@configurable
def createAlignAlgorithm(tracks,
                         elements,
                         updatetool,
                         name='AlignAlgorithm',
                         updateInFinalize=False,
                         outputlevel=3,
                         nIterations=1,
                         usecorrelations=True,
                         chi2Outlier=10000,
                         outputDataFile='',
                         onlineMode=False,
                         runList=None,
                         useLocalFrame=True,
                         xmlWriters=None,
                         inputDataFiles=None,
                         pvs=None,
                         particles=None,
                         odin=None):

    if runList is None:
        runList = []
    if xmlWriters is None:
        xmlWriters = []
    if inputDataFiles is None:
        inputDataFiles = []
    if particles is None:
        particles = DummyParticles()
    if pvs is None:
        pvs = DummyPVs()
    if odin is None:
        odin = make_odin()
    return AlignAlgorithm(
        name=name,
        VertexLocation=pvs,
        ODINLocation=odin,
        TracksLocation=tracks,
        ParticleLocation=particles,
        UpdateTool=updatetool,
        Elements=list(elements),
        UpdateInFinalize=updateInFinalize,
        OutputLevel=outputlevel,
        NumberOfIterations=nIterations,
        UseCorrelations=usecorrelations,
        Chi2Outlier=chi2Outlier,
        OutputDataFile=outputDataFile,
        OnlineMode=onlineMode,
        RunList=runList,
        UseLocalFrame=useLocalFrame,
        XmlWriters=xmlWriters,
        InputDataFiles=inputDataFiles)


@configurable
def createAlignUpdateTool(name='updateTool',
                          surveyConstraints=None,
                          matrixSolverTool=None,
                          lagrangeConstraints=None,
                          minNumberOfHits=10,
                          usePreconditioning=True,
                          logFile="alignlog.txt",
                          useWeightedAverage=False,
                          isPublic=True):
    if lagrangeConstraints is None:
        lagrangeConstraints = []
    if matrixSolverTool is None:
        matrixSolverTool = EigenDiagSolvTool(public=True)
    updatetool = AlignUpdateTool(
        name=name,
        MatrixSolverTool=matrixSolverTool,
        SurveyConstraints=surveyConstraints.Constraints
        if surveyConstraints else [],
        SurveyXmlUncertainties=surveyConstraints.XmlUncertainties
        if surveyConstraints else [],
        SurveyXmlFiles=surveyConstraints.XmlFiles if surveyConstraints else [],
        ElementJointConstraints=surveyConstraints.ElementJoints
        if surveyConstraints else [],
        LagrangeConstraints=lagrangeConstraints,
        MinNumberOfHits=minNumberOfHits,
        UsePreconditioning=usePreconditioning,
        LogFile=logFile,
        UseWeightedAverage=useWeightedAverage,
        public=isPublic,
        OutputLevel=3)
    return updatetool


def runAlignment(options,
                 surveyConstraints,
                 lagrangeConstraints,
                 alignmentTracks,
                 elementsToAlign,
                 odin=None,
                 alignmentPVs=None,
                 particles=None,
                 monitorList=None,
                 filters=None,
                 apply_gec=True,
                 updateInFinalize=False,
                 usePrKalman=False):

    # make sure to merge track lists, if multiple lists are provided
    if monitorList is None:
        monitorList = []
    if filters is None:
        filters = []
    alignTrackLists = alignmentTracks
    if not isinstance(alignTrackLists, list):
        alignTrackLists = [alignmentTracks]
    # make sure to merge them and removes clones
    alignmentTracks = AlignTrackCloneRemover(
        InputLocations=alignTrackLists).OutputLocation

    # automatically create monitoring algorithms for each input track list
    velo_light_clusters = make_VPClus_hits()

    for trklist in alignTrackLists:
        monitorList.append(
            TrackMonitor(
                name="AlignMoni_TrackMonitor",
                TracksInContainer=trklist,
                allow_duplicate_instances_with_distinct_names=True))
        monitorList.append(
            TrackFitMatchMonitor(
                name="AlignMoni_TrackFitMatchMonitor", TrackContainer=trklist))
        monitorList.append(
            TrackVPOverlapMonitor(
                name="AlignMoni_TrackVPOverlapMonitor",
                TrackContainer=trklist))
        monitorList.append(
            TrackPV2HalfMonitor(
                name='AlignMoni_TrackPV2HalfMonitor',
                TrackContainer=trklist,
                ODINLocation=make_odin(),
                limPx=10.0,
                limPy=10.0,
                limPz=1500.))
        cur_vp_track_monitor = VPTrackMonitor
        monitorList.append(
            cur_vp_track_monitor(
                name="AlignMoni_VPTrackMonitor",
                TrackContainer=trklist,
                ClusterContainer=velo_light_clusters))

    # create monitor for PVs
    if alignmentPVs:
        monitorList.append(
            TrackVertexMonitor(
                name="AlignMoni_VertexMonitor",
                PVContainer=alignmentPVs,
                TrackContainer=alignmentTracks))

    # configure an instance of AlignChisqConstraintTool
    updatetool = createAlignUpdateTool(
        surveyConstraints=surveyConstraints,
        lagrangeConstraints=lagrangeConstraints)

    alignment = createAlignAlgorithm(
        pvs=alignmentPVs,
        tracks=alignmentTracks,
        odin=odin,
        particles=particles,
        elements=elementsToAlign,
        updatetool=updatetool,
        updateInFinalize=updateInFinalize)

    alignment_node = CompositeNode(
        "alignmentNode", [alignment] + monitorList,
        combine_logic=NodeLogic.NONLAZY_OR,
        force_order=True)
    if apply_gec:
        filters.append(require_gec())

    top_node = CompositeNode(
        "filterNode",
        filters + [alignment_node],
        combine_logic=NodeLogic.LAZY_AND,
        force_order=True)

    configure(options, top_node, public_tools=[])


def getXMLWriterList(detectors,
                     prefix='xml',
                     precision=10,
                     online=False,
                     removePivot=True):
    topLevelElements = {
        'VP':
        "/dd/Structure/LHCb/BeforeMagnetRegion/VP"
        if not UseDD4Hep else "/world/BeforeMagnetRegion/VP",
        'UT':
        "/dd/Structure/LHCb/BeforeMagnetRegion/UT"
        if not UseDD4Hep else "/world/BeforeMagnetRegion/UT",
        'FT':
        "/dd/Structure/LHCb/AfterMagnetRegion/T/FT",
        'Muon':
        "/dd/Structure/LHCb/DownstreamRegion/Muon",
        'Ecal':
        "/dd/Structure/LHCb/DownstreamRegion/Ecal"
    }
    subElements = {
        'VP': {
            'Global': [0, 1],
            'Modules': [2, 4]
        },
        'UT': {
            'Detectors': [0, 1, 2],
            'Staves': [3, 4],
            'Sensors': [5]
        },
        'FT': {
            'FTSystem': [0, 1, 2, 3],
            'Modules': [4],
            'Mats': [5]
        },
        'Muon': {
            'Global': [0, 1, 2],
            'Modules': [2]
        },
        'Ecal': {
            'alignment': []
        }
    }
    xmlWriterList = []
    if not isinstance(detectors, list):
        detectors = [detectors]
    for detector in detectors:
        topLevelElement = topLevelElements[detector]
        for element, depths in subElements[detector].items():
            depth = "/".join([str(d) for d in depths])
            curString = (f'{prefix}/Conditions/'
                         f'{detector}/Alignment/{element}.xml:'
                         f'{topLevelElement}:{precision}:{depth}:'
                         f'{int(online)}:{int(removePivot)}')
            xmlWriterList.append(curString)
    return xmlWriterList
