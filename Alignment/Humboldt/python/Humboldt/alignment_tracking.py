###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import MeV, GeV, mm
from functools import partial

from PyConf.tonic import configurable
from PyConf.application import make_odin
from PyConf.Algorithms import (
    PrKalmanFilter_Velo, TrackSelectionMerger, PatPVLeftRightFinderMerger,
    PrKalmanFilter, PrKalmanFilter_noUT, PrStoreUTHitEmptyProducer,
    TrackMonitor, FTTrackMonitor, TrackVertexMonitor, TrackParticleMonitor,
    ParticleMassMonitor, PrKalmanFilterToolExampleAlgo_V1V1,
    PrKalmanFilterToolExampleAlgo_V1V1_noUT, TrackEventFitter,
    AlignTrackCloneRemover, MuonHitsToMuonPads, MuonPadsToMuonClusters,
    MuonClustersToNNMuonTracks, MakeMuonTracks, MakeMuonTracks_PrKF,
    MuonTrackSelector, TrackMuonMatching, FTMatCalibrationMonitor,
    AlignVertexTrackExtracter, RecVertexEmptyProducer)
from PyConf.Algorithms import VoidFilter
from PyConf.Algorithms import Deprecated__TrackListRefiner as TrackListRefiner, TrackSelectionToContainer
from PyConf.utilities import DISABLE_TOOL
from RecoConf.event_filters import require_pvs
from DDDB.CheckDD4Hep import UseDD4Hep
import Functors as F

from PyConf.Tools import (TrackLinearExtrapolator, TrackMasterExtrapolator,
                          TrackMasterFitter, TrackSimpleExtraSelector,
                          KalmanFilterTool, MuonMeasurementProvider,
                          VPMeasurementProvider, FTMeasurementProvider,
                          TrackSelector, FTTrackSelector)

####

from RecoConf.legacy_rec_hlt1_tracking import (
    all_velo_track_types,
    make_VeloClusterTrackingSIMD_hits,
    make_VeloClusterTrackingSIMD,
    make_reco_pvs,
    make_PatPV3DFuture_pvs,
    get_global_clusters_on_track_tool,
    get_global_clusters_on_track_tool_only_velo,
    make_PrStorePrUTHits_hits,
)
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.muonid import make_muon_hits
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_light_reco_pr_kf_without_UT, make_light_reco_pr_kf
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks, make_TrackBestTrackCreator_tracks,
    make_hlt2_tracks, get_global_measurement_provider, get_track_master_fitter,
    get_global_materiallocator, make_VPClus_hits, make_PrStoreSciFiHits_hits,
    make_hlt2_tracks_without_UT)

from RecoConf.core_algorithms import make_unique_id_generator

####

from Humboldt.TrackSelections import VPBackwardTracks, VPGoodTracks, GoodLongTracks, VPRareUsefulTracks
from Humboldt.VertexSelections import VPPrimaryVertices
from Humboldt.ParticleSelections import defaultHLT1D0Selection, defaultDetachedJpsi, defaultUpsilonMuMu, defaultZ0


def make_pvs(inputtracks):
    return PatPVLeftRightFinderMerger(InputTracks=inputtracks).OutputVertices


def make_empty_ut_hit_for_meas_provider():
    """Creates an empty container of UT hits, used for the no UT scenario.
    Necessary to run with DD4hep while Ut is not there yet. Could be moved to Moore

    Returns:
            DataHandle: PrStoreUTHitEmptyProducer' Output.

    """
    return PrStoreUTHitEmptyProducer().Output


@configurable
def make_fitted_velotracks(
        usePrKalman=True,
        MaxOutlierIterations=2,
        MaxChi2Dof=5,
        MinNVeloLayers=4,
        clusters_on_track_tool=get_global_clusters_on_track_tool_only_velo,
        ApplyMaterialCorrections=True):
    with make_VeloClusterTrackingSIMD.bind(
            use_full=True, SkipForward=4, SeedingWindow=6):
        vp_hits = make_VeloClusterTrackingSIMD_hits()
        velo_tracks = all_velo_track_types()
    backward_velo_tracks = velo_tracks["Pr::backward"]
    forward_velo_tracks = velo_tracks["Pr"]

    if usePrKalman:
        kf_template = partial(
            PrKalmanFilter_Velo,
            MaxOutlierIterations=MaxOutlierIterations,
            HitsVP=vp_hits,
            FillFitResult=True,
            ClassicSmoothing=True,
            MinNumVPHitsForOutlierRemoval=4,
            TrackAddClusterTool=clusters_on_track_tool(),
            VeloTrackPT=400. if ApplyMaterialCorrections else 1.e9,
            ReferenceExtrapolator=TrackLinearExtrapolator(),
            InputUniqueIDGenerator=make_unique_id_generator())

        fitted_velo_bwd = kf_template(
            name="PrKalmanFilter_VeloBwd",
            Input=backward_velo_tracks).OutputTracks
        fitted_velo_fwd = kf_template(
            name="PrKalmanFilter_VeloFwd",
            Input=forward_velo_tracks).OutputTracks

        selected_bwd_tracks = VPBackwardTracks(
            fitted_velo_bwd,
            MaxChi2Cut=MaxChi2Dof,
            MinNVeloLayers=MinNVeloLayers)
        selected_fwd_tracks = VPGoodTracks(
            fitted_velo_fwd,
            MaxChi2Dof=MaxChi2Dof,
            MinNVeloLayers=MinNVeloLayers)
        velotracks = TrackSelectionMerger(
            InputLocations=[selected_bwd_tracks, selected_fwd_tracks
                            ]).OutputLocation
    else:
        measprovider = get_global_measurement_provider(
            ignoreUT=True,
            ignoreFT=True,
            ignoreMuon=True,
            velo_hits=make_VPClus_hits,
            ut_hits=make_empty_ut_hit_for_meas_provider)
        #materiallocator = SimplifiedMaterialLocator() this will not work with dd4hep
        extrapolatorselector = TrackSimpleExtraSelector(
            ExtrapolatorName="TrackLinearExtrapolator")
        masterextrapolator = TrackMasterExtrapolator(
            ExtraSelector=extrapolatorselector,
            ApplyEnergyLossCorr=False,
            ApplyElectronEnergyLossCorr=False)
        trackfitter = TrackMasterFitter(
            MeasProvider=measprovider,
            Extrapolator=masterextrapolator,
            MaxUpdateTransports=3,
            MinNumVPHitsForOutlierRemoval=4,
            MaxNumberOutliers=MaxOutlierIterations,
            TrackAddClusterTool=clusters_on_track_tool(),
            MakeMeasurements=True,
            UseClassicalSmoother=True,
            MakeNodes=True,  # need to force a refit
            FastMaterialApproximation=True,
            ApplyMaterialCorrections=ApplyMaterialCorrections)
        veloonlyfitter = TrackEventFitter(
            TracksInContainer=velo_tracks["v1"], Fitter=trackfitter)
        velotracks = VPGoodTracks(
            veloonlyfitter.TracksOutContainer,
            MaxChi2Dof=MaxChi2Dof,
            MinNVeloLayers=MinNVeloLayers)

    return velotracks


def make_align_vp_input(usePrKalman=True,
                        usePVs=True,
                        MaxOutlierIterations=2,
                        MaxChi2Dof=5,
                        MinNVeloLayers=4,
                        ApplyMaterialCorrections=True):
    align_tracks = make_fitted_velotracks(
        usePrKalman=usePrKalman,
        MaxOutlierIterations=MaxOutlierIterations,
        MaxChi2Dof=MaxChi2Dof,
        MinNVeloLayers=MinNVeloLayers,
        ApplyMaterialCorrections=ApplyMaterialCorrections)
    align_pvs = make_pvs(
        align_tracks) if usePVs else RecVertexEmptyProducer().Output
    return align_tracks, align_pvs


def make_align_input_besttracks(usePrKalman=True,
                                MaxChi2Dof=5,
                                MinNVeloLayers=4):
    # it seems that we have little choice but just to set up the entire reco sequence ourselves. the moore stuff is close to impossible to work with.
    # note that the PVs reconstructed by TrackBeamLineVertexFinderSoA do not store the list of associated tracks. Use PatPV3DFuture instead
    with make_VeloClusterTrackingSIMD.bind(use_full=True,SkipForward=4,SeedingWindow=6),\
        PrKalmanFilter_noUT.bind(FillFitResult=True,ClassicSmoothing=True):
        hlt2_tracks = make_hlt2_tracks_without_UT(use_pr_kf=usePrKalman)

        # this is a bit funny, but we need fitted velo tracks. so we filter from the best track container what we need, then add our own fitted velo tracks
        goodlongtracks = GoodLongTracks(hlt2_tracks['BestLong']['v1'])
        goodvelotracks = make_fitted_velotracks(
            usePrKalman=usePrKalman,
            MaxChi2Dof=MaxChi2Dof,
            MinNVeloLayers=MinNVeloLayers)
        # combine using AlignTrackCloneRemover
        goodfittedtracks = AlignTrackCloneRemover(
            name="AlignBestCloneRemover",
            InputLocations=[goodlongtracks, goodvelotracks]).OutputLocation

    # now make the PVs with these tracks
    pvs = make_pvs(goodfittedtracks)

    return goodfittedtracks, pvs


def monitor_SciFi_tracks(track_group,
                         tracks_name,
                         includeQuarters=False,
                         VerboseMode=False,
                         minHits=10):
    monitorlist = []
    myFTTrackMonitor = FTTrackMonitor(
        name="FTTrackMonitor_" + tracks_name,
        TracksInContainer=track_group,
        VerboseMode=VerboseMode)
    monitorlist.append(myFTTrackMonitor)
    myTrackMonitor = TrackMonitor(
        name="TrackMonitor_" + tracks_name,
        TracksInContainer=track_group,
        allow_duplicate_instances_with_distinct_names=True)
    monitorlist.append(myTrackMonitor)

    if includeQuarters:
        Q0Selector = FTTrackSelector(
            MinHitsCSide=minHits, MinHitsBottomHalf=minHits)
        Q1Selector = FTTrackSelector(
            MinHitsASide=minHits, MinHitsBottomHalf=minHits)
        Q2Selector = FTTrackSelector(
            MinHitsCSide=minHits, MinHitsTopHalf=minHits)
        Q3Selector = FTTrackSelector(
            MinHitsASide=minHits, MinHitsTopHalf=minHits)

        for selector, quartername in zip(
            [Q0Selector, Q1Selector, Q2Selector, Q3Selector],
            ["_Q0", "_Q1", "_Q2", "_Q3"]):
            selected_tracks = TrackListRefiner(
                inputLocation=track_group, Selector=selector).outputLocation
            monitorlist = monitorlist + monitor_SciFi_tracks(
                selected_tracks, tracks_name + quartername)

    return monitorlist


@configurable
def make_scifi_tracks_and_particles_prkf(FilterOnParticles=False,
                                         UseJPsi=True,
                                         UseD0=True,
                                         UseZ0=True):
    with reconstruction.bind(from_file=False),\
     PrKalmanFilter.bind(FillFitResult=True,ClassicSmoothing=True),\
     make_light_reco_pr_kf.bind(skipRich=False, skipCalo=False, skipMuon=False),\
     make_muon_hits.bind(geometry_version=3),\
     make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
     make_VeloClusterTrackingSIMD.bind(use_full=True, SkipForward=4, SeedingWindow=6),\
     hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf):

        # this makes PVs from all reasonable velo tracks using the special PV reco for alignment
        goodvelotracks = make_fitted_velotracks(ApplyMaterialCorrections=False)
        pvs = make_pvs(goodvelotracks)

        # to speed things up, we use only a subset of the PVs, and make another selection of velo tracks useful for alignment
        reco = hlt2_reconstruction()
        best_tracks = reco['LongTracks']
        goodlongtracks = GoodLongTracks(best_tracks)
        selected_pvs = VPPrimaryVertices(pvs)
        selectedpvvelotracks = AlignVertexTrackExtracter(
            InputVertices=selected_pvs).Output
        rarevelotracks = VPRareUsefulTracks(goodvelotracks)
        selected_tracks = AlignTrackCloneRemover(InputLocations=[
            goodlongtracks, rarevelotracks, selectedpvvelotracks
        ])

        odin = make_odin()
        allpvs = reco["PVs"]
        filters = [require_pvs(allpvs)]
        particlelists = []

        D0_monitoring = defaultHLT1D0Selection(
            allpvs, mass_min=1760 * MeV, mass_max=1960 * MeV)
        if UseD0: particlelists.append(defaultHLT1D0Selection(allpvs))

        from RecoConf.algorithms_thor import ParticleFilter
        from Functors.math import in_range
        jpsi_monitoring = defaultDetachedJpsi(allpvs)
        jpsi_alignment = ParticleFilter(
            jpsi_monitoring, F.FILTER(
                in_range(3050 * MeV, F.MASS, 3150 * MeV)))
        if UseJPsi:
            particlelists.append(
                ParticleFilter(
                    jpsi_monitoring,
                    F.FILTER(in_range(3050 * MeV, F.MASS, 3150 * MeV))))

        z0_monitoring = defaultZ0(allpvs)
        if UseZ0:
            particlelists.append(
                ParticleFilter(z0_monitoring,
                               F.FILTER(in_range(80 * GeV, F.MASS,
                                                 100 * GeV))))

        upsilon_monitoring = defaultUpsilonMuMu(pvs)

        if FilterOnParticles:
            filters.append(
                VoidFilter(
                    Cut=F.SIZE(D0_monitoring) + F.SIZE(jpsi_monitoring) +
                    F.SIZE(z0_monitoring) + F.SIZE(upsilon_monitoring) > 0))

        alignmentTracks = TrackSelectionMerger(
            InputLocations=[selected_tracks]).OutputLocation

        monitorlist = []
        #monitorlist = monitorlist + monitor_SciFi_tracks(
        #    alignmentTracks, "AlignGoodLongTracks", includeQuarters=True)
        monitorlist = monitorlist + monitor_SciFi_tracks(
            best_tracks, "BestHLT2Tracks", includeQuarters=True)
        myTrackVertexMonitor = TrackVertexMonitor(
            TrackContainer=alignmentTracks, PVContainer=pvs)
        monitorlist.append(myTrackVertexMonitor)
        monitorlist.append(
            TrackMonitor(
                name="GoodVeloTracksMonitor",
                TracksInContainer=goodvelotracks))
        from PyConf.Algorithms import TrackVPOverlapMonitor
        monitorlist.append(
            TrackVPOverlapMonitor(
                name="GoodVeloTracksOverlapMonitor",
                TrackContainer=goodvelotracks))

        myParticleMonitor = ParticleMassMonitor(
            name="defaultD0MassMonitor",
            ParticleType=["D0", "Pi", "K"],
            Particles=D0_monitoring,
            MassMean=1864.84 * MeV,
            MassSigma=18 * MeV,
            Bins=70,
            MinPt=800 * MeV,
            MaxPt=5000 * MeV)
        monitorlist.append(myParticleMonitor)
        if UseDD4Hep:  # TrackParticleMonitor does not work in the detdesc build anymore
            monitorlist += [
                TrackParticleMonitor(
                    name="D0Monitor",
                    InputLocation=D0_monitoring,
                    MinMass=1800.,
                    MaxMass=1920.,
                    ParticleSpecies="D0"),
                TrackParticleMonitor(
                    name="JpsiMonitor",
                    InputLocation=jpsi_monitoring,
                    MinMass=2950.,
                    MaxMass=3250.,
                    ParticleSpecies="Psi"),
                TrackParticleMonitor(
                    name="UpsilonMonitor",
                    InputLocation=upsilon_monitoring,
                    MinMass=9000.,
                    MaxMass=10800.,
                    ParticleSpecies="Ups"),
                TrackParticleMonitor(
                    name="Z0Monitor",
                    InputLocation=z0_monitoring,
                    MinMass=75000.,
                    MaxMass=110000.,
                    ParticleSpecies="Z0"),
            ]
        matcalibrationMonitor = FTMatCalibrationMonitor(
            name="FTMatCalibrationMonitor",
            TracksInContainer=selected_tracks,
            ProduceYamlFile=True,
            SubtractMatAverage=False,
            OutputFileName="matContraction.yml")
        monitorlist.append(matcalibrationMonitor)

        from PyConf.Algorithms import (
            TrackEcalMatchMonitor, LHCb__Converters__Calo__Cluster__v1__fromV2
            as ClusterConverter)
        ecalclusters = reco['AllCaloHandles']['ecalClusters']
        old_ecal = ClusterConverter(InputClusters=ecalclusters)
        old_ecalClusters = old_ecal.OutputClusters
        old_ecalDigits = old_ecal.OutputDigits
        monitorlist += [
            TrackEcalMatchMonitor(
                CaloClustersLocation=old_ecalClusters,
                TrackLocation=best_tracks)
        ]

        from PyConf.Algorithms import ParticleContainerMerger
        particles = None
        if len(particlelists) == 1:
            particles = particlelists[0]
        elif len(particlelists) > 1:
            particles = ParticleContainerMerger(InputContainers=particlelists)
        return alignmentTracks, selected_pvs, particles, odin, monitorlist, filters


def monitor_muon_tracks(track_group, tracks_name):
    monitorlist = []
    """
    myTrackMonitor = TrackMonitor(
        name="MuonTrackMonitor_" + tracks_name,
        TracksInContainer=track_group,
        VerboseMode=True)
    monitorlist.append(myTrackMonitor)
    """
    myTrackMonitor = TrackMonitor(
        name="TrackMonitor_" + tracks_name, TracksInContainer=track_group)
    monitorlist.append(myTrackMonitor)

    return monitorlist


def make_muon_tracks_prkf():
    max_chi2ndof_pre_outlier_removal = 999.
    max_chi2ndof = 8.
    min_chi2_outlier = 25.

    with reconstruction.bind(from_file=False),\
         make_light_reco_pr_kf_without_UT.bind(skipRich=False, skipCalo=False, skipMuon=False),\
         make_muon_hits.bind(geometry_version=3),\
         make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
         hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf_without_UT),\
         PrKalmanFilter_noUT.bind(FillFitResult=True, ClassicSmoothing=True),\
         make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=max_chi2ndof, max_chi2ndof_pre_outlier_removal=max_chi2ndof_pre_outlier_removal, min_chi2_outlier=min_chi2_outlier),\
         make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=max_chi2ndof):

        reco = reconstruction()
        best_tracks = reco['Ttracks']

        #select tracks
        myTrackSelector = TrackSelector(TrackTypes=["Ttrack"])
        LongTracks_selection = TrackListRefiner(
            inputLocation=best_tracks, Selector=myTrackSelector).outputLocation

        # needed because TrackMuonMatching asks for a TrackContainer, not TrackSelection. There might be a better way to do this
        LongTracks = TrackSelectionToContainer(
            InputLocation=LongTracks_selection, OutputLevel=5).OutputLocation

        # muon decoding
        #from RecoConf.hlt1_muonid import make_muon_hits

        #if UseDD4Hep:
        #    muonHits = make_muon_hits()  # DATA
        #else:
        #    muonHits = make_muon_hits(geometry_version=3)  # MC
        muonHits = make_muon_hits()
        #+++++++++++++++++++++++++++++++++++ Configure fitter and measurmenet provider
        withmuon_provider = partial(
            get_global_measurement_provider,
            vp_provider=VPMeasurementProvider,
            ut_provider=DISABLE_TOOL,
            ft_provider=FTMeasurementProvider,
            muon_provider=MuonMeasurementProvider,
            ignoreVP=False,
            ignoreUT=True,
            ignoreFT=False,
            ignoreMuon=False)

        kf_tool_template = partial(
            KalmanFilterTool,
            MaxChi2PreOutlierRemoval=
            max_chi2ndof_pre_outlier_removal,  # which values to use here?
            MinChi2Outlier=min_chi2_outlier,
            MaxChi2=max_chi2ndof,
            TrackAddClusterTool=get_global_clusters_on_track_tool(),
            UniqueIDGenerator=make_unique_id_generator(),
            ReferenceExtrapolator=TrackMasterExtrapolator(
                MaterialLocator=get_global_materiallocator()))

        kf_tool_with_fitresult = kf_tool_template(
            FillFitResult=True, ClassicSmoothing=True)

        fitter = partial(
            get_track_master_fitter,
            get_measurement_provider=withmuon_provider)
        #++++++++++++++++++++

        muonPads = MuonHitsToMuonPads(
            HitContainerLocation=muonHits, OutputLevel=5)
        muonClusters = MuonPadsToMuonClusters(
            PadContainerLocation=muonPads.PadContainer,
            OutputLevel=5,
            MaxPadsPerStation=500)
        NNMuonTracks = MuonClustersToNNMuonTracks(
            ClusterContainerLocation=muonClusters.ClusterContainer,
            OutputLevel=5,
            MaxNeurons=8000,
            AddXTalk=True,
            AssumeCosmics=False,
            AssumePhysics=True)

        muontracks = MakeMuonTracks_PrKF(
            MuonTracksLocation=NNMuonTracks.NNMuonTracksContainer,
            MuonHitsLocation=muonHits,
            FitTracks=True,
            BField=True,
            SkipBigClusters=True,
            KalmanFilterTool=kf_tool_template(),
            OutputLevel=5).TracksOutputLocation

        selected_muon_tracks = MuonTrackSelector(TracksInputContainer=muontracks,\
                                                 MuonPcut=6000.,\
                                                 inCaloAcceptance=True,\
                                                 noOverlap=False,\
                                                 OutputLevel=5,\
                                                 MuonChisquareCut=5,\
                                                 minHitStation=3).TracksOutputContainer

        matched_tracks_without_UT = TrackMuonMatching(
            MuonTracksLocation=selected_muon_tracks,
            TracksLocation=LongTracks,
            AllCombinations=False,
            trackType="Ttrack",
            MatchAtFirstMuonHit=True,
            MatchChi2Cut=20.0,
            FitTracks=False,
            OutputLevel=5,
            Fitter=fitter()).TracksOutputLocation
        #alignmentTracks = matched_tracks_without_UT

        vp_hits = make_VeloClusterTrackingSIMD_hits()
        #ut_hits = make_PrStorePrUTHits_hits()
        ft_hits = make_PrStoreSciFiHits_hits()

        # ================== Fit Long+Muon Track

        # NOTE: This algorithm was not intended to be actually used...
        fitted_matched_tracks_without_UT = PrKalmanFilterToolExampleAlgo_V1V1_noUT(
            HitsVP=vp_hits,
            HitsFT=ft_hits,
            HitsMuon=muonHits,
            TrackFitter=kf_tool_with_fitresult,
            name="PrKalmanFilterToolLongMuonV1_noUT",
            Input=matched_tracks_without_UT,
            OutputLevel=5).OutputTracks

        alignmentTracks = fitted_matched_tracks_without_UT

        monitorlist = []
        monitorlist = monitorlist + monitor_muon_tracks(
            alignmentTracks, "AlignMuonLongTracks")

        return alignmentTracks, monitorlist
