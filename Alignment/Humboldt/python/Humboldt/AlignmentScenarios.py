###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from TAlignment.Alignables import Alignables
from TAlignment.SurveyConstraints import SurveyConstraints, SurveyVersion
from DDDB.CheckDD4Hep import UseDD4Hep


# scenario used for prompt alignment
class AlignmentScenario():
    __slots__ = {
        "Name": ""  # name of the scenario
        ,
        "Elements": []  # list with alignment elements
        ,
        "LagrangeConstraints": []  # list with lagrange constraints
        ,
        "SurveyConstraints": SurveyConstraints()  # survey constraints
        ,
        "SubDetectors": []  # list of subdetectors written to xml
    }

    def __init__(self, aName="AlignmentScenario", **kwargs):
        for (a, b) in self.__slots__.items():
            setattr(self, a, b)
        self.Name = aName
        for (a, b) in kwargs:
            setattr(self, a, b)
        #self.Elements = Elements
        #self.LagrangeConstraints = lagrangeconstraints
        #self.SurveyConstraints = surveyconstraints
        #self.SubDetectors = subdetectors


def toDofList(dofstring):
    dofs = []
    for dof in ["Tx", "Ty", "Tz", "Rx", "Ry", "Rz"]:
        if dof in dofstring: dofs.append(dof)
    return dofs


def configureVPAlignment(setup=None,
                         globaldofs=None,
                         halfdofs=None,
                         alignLeftHalf=True,
                         alignRightHalf=True,
                         moduledofs=None,
                         sensordofs=None,
                         survey_version='2023data',
                         useLagrangeConstraints=True,
                         useSensorLagrangeConstraints=False):

    if setup is None: setup = AlignmentScenario("VPAlignment")
    setup.SubDetectors += ['VP']

    elements = Alignables()
    if globaldofs: elements.VP(globaldofs)
    if halfdofs:
        elements.VPLeft(halfdofs if alignLeftHalf else "None")
        elements.VPRight(halfdofs if alignRightHalf else "None")
    if moduledofs: elements.VPModules(moduledofs)
    if sensordofs: elements.VPSensors(sensordofs)
    setup.Elements += list(elements)

    if survey_version:
        setup.SurveyConstraints.VP(ver=survey_version)

    # make sure that the velo stays where it was. Important note: the
    # dofs here must match the ones that we actually align for. If you
    # specify too many, things will go rather wrong.
    constraints = []
    if useLagrangeConstraints:
        if halfdofs and alignLeftHalf and alignRightHalf:
            constraints.append(
                "VPHalfAverage : .*?VP(Left|Right) : %s : delta " % ' '.join(
                    toDofList(halfdofs)))

        if moduledofs and moduledofs != "None":
            velomoduleconstraints = ""
            if halfdofs:
                for dof in ["Tx", "Ty", "Tz", "Rx", "Ry", "Rz"]:
                    if dof in moduledofs and dof in halfdofs:
                        velomoduleconstraints += f" {dof} Sz{dof} Sz2{dof}"
            velomoduleconstraints += " : survey"
            for side in ['Right', 'Left']:
                constraints.append(
                    "VPInternal%s : .*?%s/Module(..WithSupport|..): %s" %
                    (side, side, velomoduleconstraints))

    if useSensorLagrangeConstraints:
        if sensordofs and sensordofs != "None":
            velosensorconstraints = ""
            if moduledofs:
                for dof in ["Tx", "Ty", "Tz", "Rx", "Ry", "Rz"]:
                    if dof in sensordofs and dof in moduledofs:
                        velosensorconstraints += f" {dof} "
            velosensorconstraints += " : survey"
            # now we need this constraint for every single module
            # perhaps it is better to rely on the survey constraints
            for imod in range(52):
                constraints.append(
                    "VPModule%02d : VP/.*?/Module%02d/ladder.* : %s" %
                    (imod, imod, velosensorconstraints))
    setup.LagrangeConstraints = constraints
    return setup


def configureUTAlignment(setup=None,
                         layerdofs='TxTz',
                         stavedofs='TxRz',
                         facedofs='',
                         moduledofs='TxRz',
                         survey_version='data_survey_YETS23_24_Layers_Staves'):
    if setup is None: setup = AlignmentScenario("UTAlignment")
    setup.SubDetectors += ['UT']
    elements = Alignables()
    # FIXME: alignment of UT in detdesc does not work, possibly because reconstruction does not pick up alignment constants
    if UseDD4Hep:
        if layerdofs: elements.UTLayers(layerdofs)
        if stavedofs: elements.UTStaves(stavedofs)
        if facedofs: elements.UTFaces(facedofs)
        if moduledofs: elements.UTModules(moduledofs)
    elif layerdofs or stavedofs or facedocs or moduledofs:
        print(
            "WARNING: UT alignment does not work in DETDESC. will disable UT alignment DOFS"
        )
        if layerdofs: elements.UTLayers('None')
        if stavedofs: elements.UTStaves('None')
        if facedofs: elements.UTFaces('None')
        if moduledofs: elements.UTModules('None')

    setup.Elements += list(elements)

    # add to survey
    if survey_version: setup.SurveyConstraints.UT(ver=survey_version)
    return setup


def configureFTAlignment(setup=None,
                         cframedofs=None,
                         layerdofs='Tz',
                         halflayerdofs='Tx',
                         moduledofs='TxRz',
                         halfmoduledofs=None,
                         survey_version='data2023'):
    if setup is None: setup = AlignmentScenario("FTAlignment")
    setup.SubDetectors += ['FT']
    elements = Alignables()
    if cframedofs: elements.FTCFrames(cframedofs)
    if layerdofs: elements.FTLayers(layerdofs)
    if halflayerdofs: elements.FTHalfLayers(halflayerdofs)
    if moduledofs: elements.FTModules(moduledofs)
    if halfmoduledofs: elements.FTHalfModules(halfmoduledofs)
    setup.Elements += list(elements)

    # add to survey
    addHalfModuleJoints = halfmoduledofs and halfmoduledofs != "None"
    if survey_version:
        setup.SurveyConstraints.FT(
            ver=survey_version, addHalfModuleJoints=addHalfModuleJoints)
    return setup


def configureRun3Alignment(survey_version, fixQOverPBias=True):
    # just a first implementation to test some essential things

    setup = AlignmentScenario("Run3Alignment")
    setup.SubDetectors += ['VP', 'UT', 'FT']

    # define the alignment elements
    elements = Alignables()
    elements.VP("None")
    elements.VPRight("Tx")
    elements.VPLeft("Tx")
    elements.VPModules("Tx")
    elements.FTHalfLayers("Tx")
    elements.FTModules("Tx")
    elements.UTLayers("Tx")
    setup.Elements = elements

    # add some survey constraints (just fixing to nominal, right now)
    setup.SurveyConstraints.All(survey_version)

    # make sure that the velo stays where it was
    constraints = []
    constraints.append("VeloHalfAverage : VP/VP(Left|Right) : Tx")
    # fix the global shearing in the velo
    constraints.append("VeloModuleShearing : VP/VPLeft/Module.{1,2}" +
                       ("WithSupport : Tx" if not UseDD4Hep else " : Tx"))
    # fix the q/p scale by not moving T in X. note that you do not
    # want to do this if you use D0 in the alignment
    if fixQOverPBias:
        constraints.append("FT3X : FT/T3/LayerX2 : Tx")
    # there is still a spectrometer weak mode left, but I cannot find it :-(
    setup.LagrangeConstraints = constraints

    return setup


# define additional scenarios below
def configureVPHalfAlignment(halfdofs="TxTyTzRxRyRz",
                             survey_version='2023data'):
    '''
    This should be the default alignment for the Automatic alignment procedure
    Align 2-halves for all degree of freedom
    Constrain the global Velo position
    '''
    setup = AlignmentScenario("VPHalfAlignment")
    configureVPAlignment(
        setup, halfdofs=halfdofs, survey_version=survey_version)
    return setup


def configureVPModuleAlignment(globaldofs: str = "None",
                               halfdofs: str = "TxTyTzRxRyRz",
                               moduledofs: str = "TxTyRz",
                               survey_version: str = '2023data'):
    '''
    This should be the default alignment for the Automatic alignment procedure
    Align 2-halves for all degree of freedom and
    Modues only for only the main degrees of freedom Tx Ty Rz
    Constrain the global Velo position and two modules in each half
    '''
    setup = AlignmentScenario("VPModuleAlignment")
    configureVPAlignment(
        setup,
        globaldofs=globaldofs,
        halfdofs=halfdofs,
        moduledofs=moduledofs,
        survey_version=survey_version)
    return setup


def configureVPSensorAlignment(globaldofs="None",
                               halfdofs="None",
                               moduledofs="None",
                               sensordofs="TxTyRz",
                               survey_version='2023data'):
    setup = AlignmentScenario("VPSensorAlignment")
    configureVPAlignment(
        setup,
        globaldofs=globaldofs,
        halfdofs=halfdofs,
        moduledofs=moduledofs,
        sensordofs=sensordofs,
        survey_version=survey_version)
    return setup


def configureGlobalAlignment(survey_version: SurveyVersion,
                             VPglobaldofs: str = 'RxRyRz',
                             VPhalfdofs: str = 'TxTyTzRxRyRz',
                             VPmoduledofs: str = 'TxTyTzRz',
                             VPsensordofs: str = 'TxTyTzRz',
                             UTlayerdofs: str = 'TxTz',
                             UTstavedofs: str = 'TxRz',
                             UTmoduledofs: str = 'TxRz',
                             FTcframedofs: str = 'Tx',
                             FTlayerdofs: str = 'Tz',
                             FThalflayerdofs: str = 'Tx',
                             FTmoduledofs: str = 'TxRz',
                             FThalfmoduledofs: str = 'TxRz'):
    # this aligns the velo relative to FT and UT, including all survey constraints
    setup = AlignmentScenario('FullTrackerAlignment')
    configureVPAlignment(
        setup,
        survey_version=survey_version.VP,
        globaldofs=VPglobaldofs,
        halfdofs=VPhalfdofs,
        moduledofs=VPmoduledofs,
        sensordofs=VPsensordofs)
    configureUTAlignment(
        setup,
        survey_version=survey_version.UT,
        layerdofs=UTlayerdofs,
        stavedofs=UTstavedofs,
        moduledofs=UTmoduledofs)
    configureFTAlignment(
        setup=setup,
        cframedofs=FTcframedofs,
        layerdofs=FTlayerdofs,
        halflayerdofs=FThalflayerdofs,
        moduledofs=FTmoduledofs,
        halfmoduledofs=FThalfmoduledofs,
        survey_version=survey_version.FT)

    # constrain some weak 'scaling' modes in FT
    for (side, halflayer) in [('Left', 'HL0'), ('Right', 'HL1')]:
        setup.LagrangeConstraints.append(
            "T3X2%s : FT/T3/(Layer|)X2/%s/M(odule|). : Tx Rz SxTx SxRz : survey"
            % (side, halflayer))
    return setup
