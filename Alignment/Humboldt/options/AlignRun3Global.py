###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from PyConf.application import configure_input
from DDDB.CheckDD4Hep import UseDD4Hep

options.ntuple_file = "testmonitoring.root"
options.histo_file = "testmonitoringhist.root"
options.print_freq = 100
# set options above this line!

configure_input(options)

# only configure data flow after this line !

from Humboldt.alignment_tracking import make_scifi_tracks_and_particles_prkf
alignmentTracks, alignmentPVs, particles, odin, monitors, filters = make_scifi_tracks_and_particles_prkf(
)

#define elements and degrees of freedom to be aligned
from Humboldt.AlignmentScenarios import configureGlobalAlignment
from TAlignment.SurveyConstraints import surveyVersionFromOptions
config = configureGlobalAlignment(
    survey_version=surveyVersionFromOptions(options))

# constrain the global z scale
config.LagrangeConstraints.append("T3X2Z : FT/T3/(Layer|)X2 : Tz : survey")

# get some extra residual histograms
from PyConf.Algorithms import AlignAlgorithm
AlignAlgorithm.global_bind(FillHistos=True)

from Humboldt.utils import runAlignment, createAlignAlgorithm, getXMLWriterList
with createAlignAlgorithm.bind(
        outputDataFile="humb-fulltracker.dat",
        updateInFinalize=False,
        onlineMode=False,
        xmlWriters=[] if UseDD4Hep else getXMLWriterList(config.SubDetectors)):
    runAlignment(
        options,
        surveyConstraints=config.SurveyConstraints,
        lagrangeConstraints=config.LagrangeConstraints,
        alignmentTracks=alignmentTracks,
        alignmentPVs=alignmentPVs,
        particles=particles,
        odin=odin,
        filters=filters,
        elementsToAlign=list(config.Elements),
        monitorList=monitors,
        usePrKalman=True)
