###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration file to test running on MC sample
"""
from Moore import options
from DDDB.CheckDD4Hep import UseDD4Hep
import os

options.set_input_and_conds_from_testfiledb(
    'upgrade_Sept2022_minbias_0fb_md_xdigi')
###### Bu2JpsiK
#from glob import glob
#files = glob("/eos/lhcb/hlt2/LHCb/0000256145/Run_*.mdf")
#files = glob(
#    "/eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00183853/0000/*.digi") + glob(
#        "/eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00185366/0000/*.digi")

#print(files)

#options.input_files = files  #50:200
#input_files = options.input_files

from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)

#=================

options.event_store = 'EvtStoreSvc'
options.histo_file = "MC_histo.root"
options.simulation = True

# multithreading not working while creating tuples
options.n_threads = 1
options.evt_max = -1
options.scheduler_legacy_mode = False

from PyConf.application import configure_input
configure_input(options)

from Humboldt.alignment_tracking import make_muon_tracks_prkf
alignmentTracks, monitorlist = make_muon_tracks_prkf()

from PyConf.Algorithms import TrackMonitor, TrackMuonMatchMonitor

#define elements and degrees of freedom to be aligned
from TAlignment.Alignables import Alignables
elements = Alignables()
dofs = "TxTy"
elements.MuonHalfStationsAC(dofs)

# add survey constraints
from TAlignment.SurveyConstraints import SurveyConstraints
surveyconstraints = SurveyConstraints()
surveyconstraints.MUON()

#+++++++++++++++++++++++++++++++++++++++++
from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    xml_writer_list = []
else:
    from Humboldt.utils import getXMLWriterList
    xml_writer_list = getXMLWriterList(['Muon'], prefix='humb-muon/')

from Humboldt.utils import runAlignment
from Humboldt.utils import createAlignUpdateTool, createAlignAlgorithm, getXMLWriterList
from Humboldt.options import usePrKalman
with createAlignUpdateTool.bind(
        logFile="alignlog_Muon.txt"), createAlignAlgorithm.bind(
            xmlWriters=xml_writer_list):
    runAlignment(
        options,
        surveyConstraints=surveyconstraints,
        lagrangeConstraints=[],
        alignmentTracks=alignmentTracks,
        elementsToAlign=elements,
        monitorList=monitorlist,
        usePrKalman=usePrKalman)
