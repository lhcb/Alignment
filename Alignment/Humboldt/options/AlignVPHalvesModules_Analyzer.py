###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from PyConf.application import configure_input
from Humboldt.utils import createAlignAlgorithm, runAlignment

options.histo_file = "testmonitoringhist.root"
options.event_store = 'EvtStoreSvc'

# set options above this line!

configure_input(options)

# only configure data flow after this line !
from Humboldt.options import usePrKalman
from Humboldt.alignment_tracking import make_align_vp_input
alignmentTracks, alignmentPVs = make_align_vp_input(usePrKalman=usePrKalman)

#define elements and degrees of freedom to be aligned
from Humboldt.AlignmentScenarios import configureVPModuleAlignment
from TAlignment.SurveyConstraints import surveyVersionFromOptions
config = configureVPModuleAlignment(
    halfdofs="TxTyTz",
    moduledofs="TxTy",
    survey_version=surveyVersionFromOptions(options).VP)

with createAlignAlgorithm.bind(
        outputDataFile="humb-vp-halves-modules-derivs",
        updateInFinalize=False,
        onlineMode=True):
    runAlignment(
        options,
        surveyConstraints=config.SurveyConstraints,
        lagrangeConstraints=config.LagrangeConstraints,
        alignmentTracks=alignmentTracks,
        elementsToAlign=config.Elements,
        alignmentPVs=alignmentPVs,
        usePrKalman=usePrKalman)
