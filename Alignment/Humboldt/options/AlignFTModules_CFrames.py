###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os
from Moore import options
from PyConf.application import configure_input
from DDDB.CheckDD4Hep import UseDD4Hep
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

from glob import glob
files = glob("/calib/align/LHCb/Tracker/0000269045/Run_*.mdf")

options.input_files = files
options.evt_max = -1
#options.n_threads = 35
options.input_type = 'MDF'
#options.input_type = 'Online'
options.simulation = False
options.data_type = 'Upgrade'
options.event_store = 'EvtStoreSvc'
options.histo_file = "GoodLongTracks_histo.root"
# multithreading not working while creating tuples
options.scheduler_legacy_mode = False

#Set detector conditions and tags
options.geometry_version = "trunk"
#options.conditions_version = "master"
options.conditions_version = "master"
dd4hep = DD4hepSvc()
dd4hep.DetectorList = [
    '/world', 'VP', 'FT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal', 'Muon'
]  # Remove the UT

configure_input(options)

from Humboldt.utils import runAlignment

from Humboldt.alignment_tracking import make_scifi_tracks_and_particles_prkf
alignmentTracks, alignmentPVs, particles, odin, monitors, filters = make_scifi_tracks_and_particles_prkf(
)

from TAlignment.Alignables import Alignables
elements = Alignables()
elements.FTCFrames("TxRz")
elements.FTHalfModules("TxRz")

# add survey constraints
from TAlignment.SurveyConstraints import SurveyConstraints, surveyVersionFromOptions
surveyconstraints = SurveyConstraints()
surveyconstraints.FT(
    ver=surveyVersionFromOptions(options), addHalfModuleJoints=True)

# Include Lagrange constraints
constraints = []
constraints.append("CFramesGlobalConstraint : FT/T.*/(X1U|VX2)/HL. : Tx Rz")
constraints.append(
    "ModulesGlobalConstraint : FT/T./(X1|X2|U|V)/HL./Q./M. : Tx Rz")
constraints.append("BackFramesFixed : FT/T3/X2/HL.*/M. : Tx Rz")

# xml writers
if UseDD4Hep:
    xml_writer_list = []
else:
    from Humboldt.utils import getXMLWriterList
    xml_writer_list = getXMLWriterList('FT', prefix='humb-ft-modules-d0/')

# Run alignment
from Humboldt.utils import createAlignUpdateTool, createAlignAlgorithm, getXMLWriterList
with createAlignUpdateTool.bind(logFile="alignlog_ft_modules_d0_prkalman.txt"
                                ), createAlignAlgorithm.bind(
                                    xmlWriters=xml_writer_list):
    runAlignment(
        options,
        surveyConstraints=surveyconstraints,
        lagrangeConstraints=constraints,
        alignmentTracks=alignmentTracks,
        alignmentPVs=alignmentPVs,
        particles=particles,
        odin=odin,
        filters=filters,
        elementsToAlign=elements,
        monitorList=monitors)
