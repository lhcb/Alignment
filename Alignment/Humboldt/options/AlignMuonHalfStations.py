###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from DDDB.CheckDD4Hep import UseDD4Hep
import os

# ========>  online cluster
run_list_v8 = [270352, 270451, 270484, 270537, 270634]
run_number = run_list_v8[4]
path = f'/calib/align/LHCb/Muon/0000{run_number}'
test_files_data = os.listdir(path)
test_files_data = [f'{path}/{a}' for a in test_files_data]
options.input_files = test_files_data

options.input_raw_format = 0.5
input_files = options.input_files

options.input_type = 'MDF'
options.simulation = False
options.data_type = 'Upgrade'

print("#############################")
print(input_files)
print("#############################")

# set DDDB and CondDB info
options.geometry_version = "run3/trunk"
options.conditions_version = 'alignment2023'  # NEW

from RecoConf.decoders import default_ft_decoding_version
default_ft_decoding_version.global_bind(value=6)

options.event_store = 'EvtStoreSvc'
options.ntuple_file = "testmonitoring.root"
options.histo_file = "testmonitoringhist.root"

# set options above this line!
from PyConf.application import configure_input
from PyConf.application import configure
configure_input(options)

from Humboldt.alignment_tracking import make_muon_tracks_prkf
alignmentTracks = make_muon_tracks_prkf()

#define elements and degrees of freedom to be aligned
from TAlignment.Alignables import Alignables
elements = Alignables()
dofs = "TxTy"
elements.MuonHalfStationsAC(dofs)

# add survey constraints
from TAlignment.SurveyConstraints import SurveyConstraints
surveyconstraints = SurveyConstraints()
surveyconstraints.MUON()

if UseDD4Hep:
    xml_writer_list = []
else:
    from Humboldt.utils import getXMLWriterList
    xml_writer_list = getXMLWriterList('Muon', prefix='humb-muon/')

from Humboldt.utils import runAlignment
from Humboldt.utils import createAlignUpdateTool, createAlignAlgorithm, getXMLWriterList
from Humboldt.options import usePrKalman
with createAlignUpdateTool.bind(
        logFile="alignlog_Muon.txt"), createAlignAlgorithm.bind(
            xmlWriters=xml_writer_list):
    runAlignment(
        options,
        surveyConstraints=surveyconstraints,
        lagrangeConstraints=[],
        alignmentTracks=alignmentTracks,
        elementsToAlign=elements,
        usePrKalman=usePrKalman)
