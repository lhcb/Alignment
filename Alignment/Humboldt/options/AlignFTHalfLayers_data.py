###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration file to test running on data.
"""
import re
from PyConf.application import configure_input
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD, VPRetinaFullClusterDecoder, PrKalmanFilter, PrKalmanFilter_Downstream, PrKalmanFilter_noUT

from RecoConf.reconstruction_objects import reconstruction
from RecoConf.legacy_rec_hlt1_tracking import (
    make_VeloClusterTrackingSIMD, make_velo_full_clusters, make_reco_pvs,
    make_PatPV3DFuture_pvs)
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_light_reco_pr_kf_without_UT
from RecoConf.hlt2_tracking import (
    get_global_measurement_provider, make_PrKalmanFilter_noUT_tracks,
    make_PrKalmanFilter_Seed_tracks, make_PrKalmanFilter_Velo_tracks,
    make_TrackBestTrackCreator_tracks, get_UpgradeGhostId_tool_no_UT,
    make_hlt2_tracks)
from RecoConf.muonid import make_muon_hits
from RecoConf.calorimeter_reconstruction import make_digits, make_calo

from Moore import options, run_moore

from Humboldt.utils import runAlignment, createAlignUpdateTool, createAlignAlgorithm, getXMLWriterList
from Humboldt.alignment_tracking import make_scifi_tracks_and_particles_prkf
from TAlignment.SurveyConstraints import SurveyConstraints, surveyVersionFromOptions

from glob import glob

options.input_type = 'MDF'
options.simulation = False
options.data_type = 'Upgrade'

# set DDDB and CondDB info
options.geometry_version = "run3/trunk"
CONDDBTag = "master"
options.conditions_version = CONDDBTag

files = glob("/eos/lhcb/hlt2/LHCb/0000256145/Run_*.mdf")
options.input_files = files[0:10]  #50:200

options.event_store = 'EvtStoreSvc'
options.histo_file = "GoodLongTracks_histo.root"
# multithreading not working while creating tuples
options.n_threads = 8
options.evt_max = -1

options.scheduler_legacy_mode = False

configure_input(options)

alignmentTracks, alignmentPVs, particles, odin, monitors, filters = make_scifi_tracks_and_particles_prkf(
)

from TAlignment.Alignables import Alignables
elements = Alignables()
dofs = "TxRzTz"
elements.FTHalfLayers(dofs)

# add survey constraints
surveyconstraints = SurveyConstraints()
surveyconstraints.FT(surveyVersionFromOptions(options).FT)

# define Lagrange constraints
constraints = []

with createAlignUpdateTool.bind(
        logFile="alignlog_ft_modules_d0_prkalman.txt"
), createAlignAlgorithm.bind(
        xmlWriters=getXMLWriterList('FT', prefix='humb-ft-modules-d0/')):
    runAlignment(
        options,
        surveyConstraints=surveyconstraints,
        lagrangeConstraints=constraints,
        alignmentTracks=alignmentTracks,
        alignmentPVs=alignmentPVs,
        particles=particles,
        odin=odin,
        filters=filters,
        elementsToAlign=elements,
        monitorList=monitors)
