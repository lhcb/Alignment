###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.application import configure_input, make_odin
from PyConf.Algorithms import Deprecated__TrackListRefiner as TrackListRefiner, SharedTrackEventFitter, VeloClusterTrackingSIMDFull, RecVertexEmptyProducer, TrackSelectionMerger, TrackMonitor, TrackFitMatchMonitor, TrackVertexMonitor, VPTrackMonitor
from PyConf.Tools import VPTrackSelector, TrackMasterFitter, TrackLinearExtrapolator, TrackSimpleExtraSelector, TrackMasterExtrapolator, SimplifiedMaterialLocator

from RecoConf.decoders import default_ft_decoding_version
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.legacy_rec_hlt1_tracking import make_reco_pvs, make_PatPV3DFuture_pvs, make_VeloClusterTrackingSIMD, make_VPClus_hits
from RecoConf.hlt2_tracking import get_global_measurement_provider
from RecoConf.hlt2_global_reco import make_default_reconstruction
from RecoConf.mc_checking import vphits_resolution_checker
from Moore import options

from Humboldt.ParticleSelections import DummyParticles
from Humboldt.TrackSelections import VPBackwardsTracks, VPLongTracks, VPOverlapTracks
from Humboldt.VertexSelections import VPPrimaryVertices
from Humboldt.utils import createAlignUpdateTool, createAlignAlgorithm, getXMLWriterList, runAlignment

from TAlignment.Alignables import Alignables
from TAlignment.SurveyConstraints import SurveyConstraints

default_ft_decoding_version.global_bind(value=6)
options.set_input_and_conds_from_testfiledb('upgrade_DC19_01_MinBiasMD')
options.input_files = list(set(options.input_files))  # remove dups
input_files = options.input_files
input_files = [a for a in input_files if 'eoslhcb.cern.ch' in a]
input_files.sort()
options.input_files = input_files
options.evt_max = 10
options.ntuple_file = "testmonitoring.root"
options.histo_file = "testmonitoringhist.root"

# set options above this line!

configure_input(options)


# only configure data flow after this line !
def selectAndRefitVeloOnly(inputtracks):
    # first select tracks with velo hits
    veloselector = VPTrackSelector(
        TrackTypes=["Velo", "Long", "Upstream"], MinHits=5, MaxChi2Cut=5)
    selectedTracks = TrackListRefiner(
        inputLocation=inputtracks, Selector=veloselector).outputLocation
    # now reft them
    measprovider = get_global_measurement_provider(
        ignoreUT=True, ignoreFT=True, ignoreMuon=True)
    materiallocator = SimplifiedMaterialLocator()
    extrapolatorselector = TrackSimpleExtraSelector(
        ExtrapolatorName="TrackLinearExtrapolator")
    masterextrapolator = TrackMasterExtrapolator(
        ExtraSelector=extrapolatorselector,
        ApplyEnergyLossCorr=False,
        ApplyElectronEnergyLossCorr=False,
        MaterialLocator=materiallocator)
    trackfitter = TrackMasterFitter(
        MeasProvider=measprovider,
        MaterialLocator=materiallocator,
        Extrapolator=masterextrapolator,
        MaxUpdateTransports=1,
        MinNumVPHitsForOutlierRemoval=8,  # default value it too low
        MakeMeasurements=True,
        MakeNodes=True  # need to force a refit
    )
    veloonlyfitter = SharedTrackEventFitter(
        TracksInContainer=selectedTracks, Fitter=trackfitter)
    return veloonlyfitter.TracksOutContainer


# at the moment, define tracks and PVs by hand. Could be changed to centrally defined selection in the future
def getAlignmentTracksAndPVs():
    # note that the PVs reconstructed by TrackBeamLineVertexFinderSoA do not store the list of associated tracks. Use PatPV3DFuture instead
    with reconstruction.bind(from_file=False), make_reco_pvs.bind(
            make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs
    ), make_VeloClusterTrackingSIMD.bind(
            algorithm=VeloClusterTrackingSIMDFull, SkipForward=4,
            SeedingWindow=6), make_default_reconstruction.bind(
                usePatPVFuture=True):
        #make tracks and PVs
        # TODO: in principle the PVs should be an optional input for the alignment
        reco = reconstruction()
        hlt2_tracks = reco["Tracks"]
        best_tracks = hlt2_tracks
        pvs = reco["PVs"]

        # make a subselection of tracks with velo hits, then refit them with a well-configured master fitter
        veloonlyfittedtracks = best_tracks  #selectAndRefitVeloOnly( best_tracks )

        odin = make_odin()
        # create input particles, use dummy particles for now
        # TODO: in principle these should be composite particles and be an optional input for the alginment
        particles = DummyParticles()

        #select tracks and PVs
        selected_LongTracks = VPLongTracks(veloonlyfittedtracks)
        selected_BackwardTracks = VPBackwardsTracks(veloonlyfittedtracks)
        selected_OverlapTracks = VPOverlapTracks(veloonlyfittedtracks)

        alignmentTracks = TrackSelectionMerger(InputLocations=[
            selected_LongTracks, selected_BackwardTracks,
            selected_OverlapTracks
        ]).OutputLocation

        selected_pvs = VPPrimaryVertices(pvs)

        # add track and vertex monitoring
        # TODO: should only use selected PVs
        myTrackMonitor = TrackMonitor(
            TracksInContainer=alignmentTracks, PrimaryVertices=pvs)
        myTrackFitMatchMonitor = TrackFitMatchMonitor(
            TrackContainer=alignmentTracks)
        # TODO: check vertices
        myTrackVertexMonitor = TrackVertexMonitor(
            TrackContainer=alignmentTracks, PVContainer=pvs)
        myVPTrackMonitor = VPTrackMonitor(
            TrackContainer=best_tracks, ClusterContainer=make_VPClus_hits())
        monitorlist = [
            myTrackMonitor, myTrackFitMatchMonitor, myTrackVertexMonitor,
            myVPTrackMonitor,
            vphits_resolution_checker()
        ]
        return alignmentTracks, selected_pvs, particles, odin, monitorlist


alignmentTracks, alignmentPVs, particles, odin, monitors = getAlignmentTracksAndPVs(
)

#define elements and degrees of freedom to be aligned
elements = Alignables()
dofs = "TxTyTzRxRyRz"
dofsmodules = "TxTyTzRxRyRz"
#elements.VPLeft(dofs)
#elements.VPRight(dofs)
#elements.VPModules(dofsmodules)
#elements.Tracker( dofs )
elements.VPLeft(dofs)
elements.VPRight(dofs)
elements.UT(dofs)
elements.FT(dofs)

# add survey constraints
surveyconstraints = SurveyConstraints()

# define Lagrange constraints
constraints = []

with createAlignUpdateTool.bind(
        logFile="alignlog.txt"), createAlignAlgorithm.bind(
            xmlWriters=getXMLWriterList(
                'VP', prefix='humb-vp-halves-modules/')):
    runAlignment(
        options,
        surveyConstraints=surveyconstraints,
        lagrangeConstraints=constraints,
        alignmentTracks=alignmentTracks,
        alignmentPVs=alignmentPVs,
        particles=particles,
        odin=odin,
        elementsToAlign=elements,
        monitorList=monitors,
        updateInFinalize=True)
