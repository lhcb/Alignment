###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from PyConf.application import configure_input
from DDDB.CheckDD4Hep import UseDD4Hep
from Humboldt.AlignmentScenarios import configureVPSensorAlignment
from TAlignment.SurveyConstraints import surveyVersionFromOptions

configure_input(options)

from Humboldt.utils import runAlignment

from Humboldt.options import usePrKalman
from Humboldt.alignment_tracking import make_align_vp_input
alignmentTracks, alignmentPVs = make_align_vp_input(usePrKalman=usePrKalman)

#define elements and degrees of freedom to be aligned
config = configureVPSensorAlignment(
    survey_version=surveyVersionFromOptions(options).VP)

from Humboldt.utils import createAlignUpdateTool, createAlignAlgorithm, getXMLWriterList

if UseDD4Hep:
    xml_writer_list = []
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    DD4hepSvc().DetectorList = ['/world', 'VP']

else:
    from Humboldt.utils import getXMLWriterList
    xml_writer_list = getXMLWriterList(config.SubDetectors)

with createAlignAlgorithm.bind(xmlWriters=xml_writer_list):
    runAlignment(
        options,
        surveyConstraints=config.SurveyConstraints,
        lagrangeConstraints=config.LagrangeConstraints,
        alignmentTracks=alignmentTracks,
        elementsToAlign=config.Elements,
        alignmentPVs=alignmentPVs,
        usePrKalman=usePrKalman)
