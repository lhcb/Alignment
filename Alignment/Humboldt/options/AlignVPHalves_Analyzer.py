###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from PyConf.application import configure_input
from Humboldt.utils import createAlignAlgorithm, runAlignment
from DDDB.CheckDD4Hep import UseDD4Hep
from RecoConf.event_filters import require_pvs
from Humboldt.options import usePrKalman
from Humboldt.alignment_tracking import make_align_vp_input
from Humboldt.AlignmentScenarios import configureVPHalfAlignment

if UseDD4Hep:
    # overlay_path = "./OverlayRoot"
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    DD4hepSvc().UseConditionsOverlay = True

    dd4hepSvc = DD4hepSvc()
    dd4hepSvc.DetectorList = ['/world', 'VP']  #removes the UT

options.histo_file = "testmonitoringhist.root"
if UseDD4Hep:
    options.simulation = False

options.event_store = 'EvtStoreSvc'

# set options above this line!

configure_input(options)

# only configure data flow after this line !

alignmentTracks, alignmentPVs = make_align_vp_input(usePrKalman=usePrKalman)

#define elements and degrees of freedom to be aligned
from TAlignment.SurveyConstraints import surveyVersionFromOptions
config = configureVPHalfAlignment(
    halfdofs="TxTyTz", survey_version=surveyVersionFromOptions(options).VP)

filter_pvs = []
filter_pvs.append(require_pvs(alignmentPVs))

with createAlignAlgorithm.bind(
        outputDataFile="humb-vp-halves-modules-derivs",
        updateInFinalize=False,
        onlineMode=True):
    runAlignment(
        options,
        surveyConstraints=config.SurveyConstraints,
        lagrangeConstraints=config.LagrangeConstraints,
        alignmentTracks=alignmentTracks,
        elementsToAlign=config.Elements,
        filters=filter_pvs,
        alignmentPVs=alignmentPVs,
        usePrKalman=usePrKalman)
