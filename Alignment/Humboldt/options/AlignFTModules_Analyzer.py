###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from PyConf.application import configure_input
from Humboldt.utils import createAlignAlgorithm, runAlignment
from DDDB.CheckDD4Hep import UseDD4Hep
from Humboldt.alignment_tracking import make_scifi_tracks_and_particles_prkf
alignmentTracks, alignmentPVs, particles, odin, monitors, filters = make_scifi_tracks_and_particles_prkf(
)
from TAlignment.Alignables import Alignables

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    DD4hepSvc().UseConditionsOverlay = True

options.histo_file = "monitoringhist_SciFi.root"
if UseDD4Hep:
    options.simulation = False
if options.getProp("input_type") != "Online":
    options.set_input_and_conds_from_testfiledb(
        'upgrade_Sept2022_minbias_0fb_md_mdf')
    options.evt_max = 10000

options.event_store = 'EvtStoreSvc'

# set options above this line!

configure_input(options)

#define elements and degrees of freedom to be aligned

elements = Alignables()
elements.FTHalfModules("TxRxRz")

# add survey constraints
from TAlignment.SurveyConstraints import surveyConstraintsFromOptions
surveyconstraints = surveyConstraintsFromOptions(options)

constraints = []

with createAlignAlgorithm.bind(
        outputDataFile="humb-ft-modules-derivs",
        updateInFinalize=False,
        onlineMode=True):
    runAlignment(
        options,
        surveyConstraints=surveyconstraints,
        lagrangeConstraints=constraints,
        alignmentTracks=alignmentTracks,
        elementsToAlign=elements,
        alignmentPVs=alignmentPVs,
        usePrKalman=True,
        filters=filters)
