###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from pathlib import Path
from Configurables import AlignOnlineIterator, ApplicationMgr, DDDBConf
from Moore import options
from PyConf.application import configure_input
from Humboldt.utils import createAlignUpdateTool
from DDDB.CheckDD4Hep import UseDD4Hep
from TAlignment.Alignables import Alignables

if options.getProp("input_type") != "Online":
    if UseDD4Hep:
        from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
        Path("OverlayRoot").mkdir(exist_ok=True)
        DD4hepSvc(
            ConditionsOverlayInitPath="OverlayRoot", UseConditionsOverlay=True)

options.input_type = 'NONE'

from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
dd4hepSvc = DD4hepSvc()
dd4hepSvc.DetectorList = [
    '/world', 'VP', 'FT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal', 'Muon'
]  #removes the UT

options.geometry_version = "run3/trunk"
CONDDBTag = "master"
options.conditions_version = CONDDBTag
options.simulation = not UseDD4Hep

options.input_files = []
# options.histo_file = "testmonitoringhist.root"
# set options above this line!
configure_input(options)

# Copied from PyConf.application.configure_input
# TODO also do this in configure_input when input_type = 'NONE'
DDDBConf(Simulation=options.simulation, DataType=options.data_type)

#define elements and degrees of freedom to be aligned
elements = Alignables()
elements.FTHalfModules("TxRz")

# add survey constraints
from TAlignment.SurveyConstraints import SurveyConstraints, surveyVersionFromOptions
surveyconstraints = SurveyConstraints()
surveyconstraints.FT(
    ver=surveyVersionFromOptions(options), addHalfModuleJoints=True)

# define Lagrange constraints
constraints = []
constraints.append("BackFramesFixed : FT/T3/X2/HL.*/M. : Tx Rz")

myUpdateTool = createAlignUpdateTool(
    surveyConstraints=surveyconstraints,
    lagrangeConstraints=constraints,
    logFile="alignlog.txt")

_, configurable_tools = myUpdateTool.configuration().apply()
update_tool_conf = next(
    (x for x in configurable_tools if x.name() == 'ToolSvc.updateTool'), None)
assert update_tool_conf.name() == 'ToolSvc.updateTool'

myAlignIterator = AlignOnlineIterator(
    Elements=list(elements),
    DerivativeFiles=[
        os.path.join(
            os.getenv("PREREQUISITE_0", ""), "humb-ft-modules-derivs")
    ],
    UpdateTool=update_tool_conf.getFullName() + ":PUBLIC")

myAlignIterator.SubDetectors = ['FT']
myAlignIterator.RunType = 'SCIFI'
myAlignIterator.AlignElements = ['HalfLayer/Modules']
app = ApplicationMgr(EvtMax=0, EvtSel='NONE', TopAlg=[])
app.EventLoop = myAlignIterator

# NOTE: when running online, locations for the overlay, derivatives and
# log file are overridden in MooreOnlineConf/options/align_iterator.py
