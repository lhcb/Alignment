###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import re
from glob import glob

logfiles = glob('splititertest/Iter1/alignlog.txt') + glob(
    'AlignmentResults/Iter1/alignlog*.txt')

logExists = len(logfiles) > 0
if logExists:
    with open(logfiles[-1]) as f:
        log = f.read()
    res = re.findall(r'Convergence status: (\S+)', log)
    if res:
        status = res[-1]
        if (status != 'Converged'):
            causes.append('No convergence reached in second iteration')
    else:
        causes.append('Could not find Convergence data in log file')
else:
    causes.append('Alignment log file not produced')
