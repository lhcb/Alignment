###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
options.set_input_and_conds_from_testfiledb('expected_2024_BsToJpsiPhi_xdigi')
options.evt_max = 10
options.ntuple_file = "testmonitoring.root"
options.histo_file = "testmonitoringhist.root"

# this is needed until someone fixes the dd4hep simulation database
# note that the numbers depend on the simulation version
from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    dx = 1.092
    dy = 0.474
    condval = "!alignment{ position: [%f * mm, %f * mm, 0 * mm], rotation: [0,0,0]}" % (
        dx, dy)
    DD4hepSvc().ConditionsOverride = {
        "/world/BeforeMagnetRegion/VP/MotionVPLeft:alignment_delta": condval,
        "/world/BeforeMagnetRegion/VP/MotionVPRight:alignment_delta": condval
    }
