###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import os, re
from DDDB.CheckDD4Hep import UseDD4Hep

logExists = os.path.isfile("alignlog.txt")
if UseDD4Hep:
    xmlExists = os.path.isfile(
        "default-yml-dir/Conditions/VP/Alignment/Modules.yml"
    ) and os.path.isfile("default-yml-dir/Conditions/VP/Alignment/Global.yml")
else:
    xmlExists = os.path.isfile(
        "xml/Conditions/VP/Alignment/Modules.xml") and os.path.isfile(
            "xml/Conditions/VP/Alignment/Global.xml")
if not logExists:
    causes.append('alignment log file not produced')

if not xmlExists:
    causes.append('XML/YML files with constants not produced')

with open('alignlog.txt') as f:
    log = f.read()

status = re.findall(r'Convergence status: (\S+)', log)[-1]

result['alignlog'] = result.Quote(log)
if status != 'Converged':
    causes.append('No convergence reached')
