/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ALIGNMENT_ALIGNMENTMONITORING_PARSECONSTANTS_H
#define ALIGNMENT_ALIGNMENTMONITORING_PARSECONSTANTS_H

#include <map>
#include <string>
#include <vector>

namespace Alignment {
  namespace AlignmentMonitoring {
    class ParseConstants {
    public:
      // Constructor
      ParseConstants();
      // Destructor
      ~ParseConstants() {}
      // Methods
      std::map<std::string, double> Parse( const char* filename );
      std::map<std::string, double> Parse( const std::vector<std::string> filenames );

    private:
    };
  } // namespace AlignmentMonitoring
} // namespace Alignment

#endif
