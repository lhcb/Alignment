/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ALIGNMENT_ALIGNMENTMONITORING_MONITORINGAPPLICATION_H
#define ALIGNMENT_ALIGNMENTMONITORING_MONITORINGAPPLICATION_H 1

// ALIGNMENTMONITORING
#include "AlignmentMonitoring/HistHelper.h"
#include "AlignmentMonitoring/MonitoringPage.h"
// STL
#include <string>
#include <vector>
// ROOT
#include "TString.h"

namespace Alignment {
  namespace AlignmentMonitoring {
    class MonitoringApplication {
    public:
      // Constructor
      MonitoringApplication( const char* outputName = "plots" );
      // Destructor
      ~MonitoringApplication() {}

      // Methods
      void addPage( MonitoringPage* page ) { m_pages.push_back( page ); }
      void addFile( const TString& filenametag );
      void drawPage( TString pagename, bool normalize = false );
      void draw( bool print = false, bool normalize = false );
      void draw( TCanvas* c1, bool normalize = false );

    private:
      std::string                  m_outputName;
      std::vector<MonitoringPage*> m_pages;
      std::vector<TString>         m_filenames;
      std::vector<TString>         m_tags;
      HistHelper                   m_hhelp;
    };
  } // namespace AlignmentMonitoring
} // namespace Alignment

#endif
