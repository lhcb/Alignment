/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef NAMETTSECTOR_H
#define NAMETTSECTOR_H

#include <string>

/** @class  NameTTSector
 *  @author Zhirui Xu
 *  @date   2015.10.16
 */

namespace Alignment {
  namespace AlignmentMonitoring {
    class NameTTSector {
    public:
      // standard constructor
      NameTTSector( const std::string name );
      virtual ~NameTTSector(){};

      int         GetUniqueSector();
      int         GetNumberOfSensors();
      int         GetSectorID();
      int         GetChannel();
      std::string GetStation();
      std::string GetLayer();
      std::string GetRegion();

    protected:
    private:
      std::string m_station;
      std::string m_layer;
      std::string m_region;
      std::string m_sector;

      int m_uniqueSector;
      int m_sectorID;
      int m_numberOfSensors;

      int  m_channel;
      void ConvertName2Channel();
    };

  } // namespace AlignmentMonitoring
} // namespace Alignment

#endif
