#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

##########################
###   Options parser   ###
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description="Macro to make plots to monitor automatic VELO alignment")
    parser.add_argument(
        '-r', '--run', help='run number, default is the last one')
    parser.add_argument(
        '--alignlog',
        help='location of alignlog.txt, default is guessed by run number')
    parser.add_argument(
        '--histoFiles',
        help=
        'location of histograms files, expect two paths, old ad new align histos, default is guessed by run number',
        nargs=2)
    parser.add_argument('-o', '--outFile', help='output file name')
    parser.add_argument(
        '--online',
        help='Setup monitoring job to send the histograms to the presenter',
        action='store_true')
    parser.add_argument(
        '--makehist', help='Setup all histos', action='store_true')
    args = parser.parse_args()

##########################

import os, sys, fnmatch, re, copy, importlib
import ROOT as r
from GaudiPython import gbl
from AlignmentOutputParser.AlignOutput import *
from AlignmentMonitoring.MultiPlot import MultiPlot
from AlignmentMonitoring.OnlineUtils import findLastRun, findAlignlog, findHistos
import AlignmentMonitoring.RootUtils
from AlignmentMonitoring.RootUtils import getExpression, getDofDeltas, getDofDeltaConvergence, makeGraph, makeHisto, getDrawnCanvas, makeGraphHisto, getHisto, makeLineHisto
importlib.reload(sys.modules['AlignmentMonitoring.RootUtils'])
import AlignmentMonitoring.RootUtils
from AlignmentMonitoring.RootUtils import getExpression, getDofDeltas, getDofDeltaConvergence, makeGraph, makeHisto, getDrawnCanvas, makeGraphHisto, getHisto, makeLineHisto

r.gROOT.SetBatch(True)

activity = 'Velo'


def plotsCompare(Pages, files_histo, outputFile_name, normalize=True):
    inFiles = {key: r.TFile(path) for key, path in files_histo.items()}
    for title, paths in Pages:
        mps = []
        for cont, _path in enumerate(paths):
            histo_args = {}
            wantMeanLines = False
            if type(_path) == str:
                path = _path
            else:
                try:
                    path = _path[0]
                    histo_args = copy.deepcopy(_path[1])
                    wantMeanLines = _path[2]
                except IndexError:
                    pass
            if 'vlines' in histo_args:
                histo_args['vlines_colors'] = {
                    val: r.kBlack
                    for val in histo_args['vlines']
                }
                histo_args['vlines_colors'][0] = r.kGreen + 2
                histo_args['vlines_styles'] = {
                    val: 3
                    for val in histo_args['vlines']
                }
                histo_args['vlines_styles'][0] = 5
                histo_args['vlines_width'] = {
                    val: 2
                    for val in histo_args['vlines']
                }  #{0 : 2}
            drawLegend = (cont == 0)
            histos_obj = {key: inFiles[key].Get(path) for key in inFiles}
            if 'title' not in histo_args:
                histo_args['title'] = histos_obj['old'].GetTitle()
            isProfile = isinstance(histos_obj['old'], r.TProfile)
            is1DHistogram = False
            if isinstance(histos_obj['old'], r.TProfile):
                histos = {
                    key: (r.TProfile)(inFiles[key].Get(path))
                    for key in inFiles
                }
            elif isinstance(histos_obj['old'], r.TH1D):
                is1DHistogram = True
                histos = {
                    key: (r.TH1D)(inFiles[key].Get(path))
                    for key in inFiles
                }
            elif isinstance(histos_obj['old'], r.TH1F):
                is1DHistogram = True
                histos = {
                    key: (r.TH1F)(inFiles[key].Get(path))
                    for key in inFiles
                }
            elif isinstance(histos_obj['old'], r.TH2D):
                histos = {
                    key: (r.TProfile)(inFiles[key].Get(path).ProfileX(
                        "DummyTitle", 1, -1, "g"))
                    for key in inFiles
                }
                #for key in inFiles:
                #    histos[key].GetYaxis().SetRangeUser(-0.1,0.1)
            elif isinstance(histos_obj['old'], r.TH2F):
                histos = {
                    key: (r.TProfile)(inFiles[key].Get(path).ProfileX(
                        "DummyTitle", 1, -1, "g"))
                    for key in inFiles
                }
                #for key in inFiles:
                #    histos[key].GetYaxis().SetRangeUser(-0.1,0.1)
            else:
                histos = {key: inFiles[key].Get(path) for key in inFiles}
            print('histos.values()', histos.values())
            if normalize:
                for histo in histos.values():
                    if is1DHistogram:
                        if histo.GetEntries() != 0.0:
                            histo.Scale(1. / histo.GetEntries())
                        else:
                            histo.Scale(1. / 1.)
            kind = 'p' if isProfile else 'h'
            histo_args.pop('name', None)
            mps.append(
                MultiPlot(
                    path, kind=kind, drawLegend=drawLegend, **histo_args))
            mps[-1].Add(histos['old'], 'old', color=r.kRed, markerStyle=1)
            try:
                mps[-1].Add(
                    histos['new'],
                    'new',
                    color=r.kBlue,
                    lineStyle=2,
                    markerStyle=1)
            except KeyError:
                pass
            if isinstance(histos['old'], r.TProfile):
                mps[-1].DrawOption = 'nostack hist p'
            else:
                mps[-1].DrawOption = 'nostack hist'
            if wantMeanLines:
                mps[-1].AddLine(
                    histos['old'].GetMean(), 'v', color=r.kRed, style=1)
                mps[-1].AddLine(
                    histos['new'].GetMean(), 'v', color=r.kBlue, style=2)
        c1 = getDrawnCanvas(mps)
        c1.Print(outputFile_name)


def getLines(filename, x1, y1, x2, y2):
    f = r.TFile(filename, "RECREATE")
    line = r.TLine(x1, y1, x2, y2)
    c = r.TCanvas("linecanvas", "Line", 800, 600)
    line.Draw()
    c.Write()
    line.Write()
    f.Close()


def getLinesall(filename, x1, y1, x2, y2, x11, y11, x21, y21):
    f = r.TFile(filename, "RECREATE")
    line = r.TLine(x1, y1, x2, y2)
    line1 = r.TLine(x11, y11, x21, y21)
    c = r.TCanvas("linecanvas", "Line", 800, 600)
    line.Draw()
    line1.Draw()
    c.Write()
    line.Write()
    #c1 = r.TCanvas("linecanvas1","Line1",800,600)
    #c.Write()
    line1.Write()
    f.Close()


def publishHistosCompare(Pages,
                         files_histo,
                         monSvc,
                         folder_name='MoniOnlineAligVP'):
    print(files_histo)
    histo_paths = sum([i[1] for i in Pages], [])
    print(i[1] for i in Pages)
    for histo_path in histo_paths:
        path, histo_args = histo_path[:2]
        inFiles = {key: r.TFile(path) for key, path in files_histo.items()}
        histos = {key: inFiles[key].Get(path) for key in inFiles}
        if 'title' not in histo_args:
            histo_args['title'] = histos['old'].GetTitle()
        if 'name' not in histo_args:
            histo_args['name'] = '_'.join(path.split('/')[-2:])
        histos['new'].SetTitle(histo_args['title'])
        histos['old'].SetTitle(histo_args['title'])
        histos['new'].SetName(histo_args['name'])
        histos['old'].SetName(histo_args['name'] + '_reference')

        if isinstance(histos['old'], r.TProfile) or isinstance(
                histos['old'], r.TGraph):
            histos['old'] = histos['old'].ProjectionX(histos['old'].GetName())
            histos['new'] = histos['new'].ProjectionX(histos['new'].GetName())

        monSvc.publishHistogram(folder_name, histos['new'], add=False)
        monSvc.publishHistogram(folder_name, histos['old'], add=False)


def getHistos(Pages, files_histo, outfilename, name, taskname):
    rootfile = r.TFile.Open(files_histo[name])
    f = r.TFile.Open(outfilename, "RECREATE")
    dir = r.TDirectory()
    dir = f.mkdir(taskname)
    dir.cd()
    histo_paths = sum([i[1] for i in Pages], [])
    histos = []
    for i in range(0, len(histo_paths)):
        hist = rootfile.Get(histo_paths[i][0])
        if isinstance(hist, r.TH2D) or isinstance(hist, r.TH2F):
            profiled_hist = hist.ProfileX((histo_paths[i][0]).split('/')[-1],
                                          1, -1, "g")
            hist = profiled_hist
        hist.Write()

    f.Write()
    f.Close()


Pages = [
    ("Long track properties and PV position", [
        [
            "AlignMoni_TrackMonitor/Velo/chi2_per_ndof",
            {
                'rangeX': [0.0, 5.0],
                'title': " track #chi^{2}/ndof",
                'name': "Velo_chi2ndof"
            }, True
        ],
        [
            "AlignMoni_TrackMonitor/Velo/eta",
            {
                'title': "track #eta",
                'name': "Velo_eta"
            }
        ],
        [
            "AlignMoni_TrackMonitor/Velo/phi",
            {
                'title': "track #phi",
                'name': "Velo_phi"
            }
        ],
        [
            "AlignMoni_VertexMonitor/PV x position",
            {
                'title': "PV x position",
                'name': "PV x position"
            }
        ],
        [
            "AlignMoni_VertexMonitor/PV y position",
            {
                'rangeX': [0.0, 2.0],
                'title': "PV y position",
                'name': "PV y position"
            }, True
        ],
        [
            "AlignMoni_VertexMonitor/PV z position",
            {
                'title': "PV z position",
                'name': "PV z position"
            }
        ],
    ]),
    ("VELO related quantities", [
        [
            "AlignMoni_TrackMonitor/Velo/chi2PerDofVelo",
            {
                'title': '(7) Velo segment #chi^{2}/ndof'
            }
        ],
        [
            "AlignMoni_TrackMonitor/Velo/VPXresidualPull",
            {
                'title': '(8) Velo x residual pull'
            }
        ],
        [
            "AlignMoni_TrackMonitor/Velo/VPYresidualPull",
            {
                'title': '(9) Velo y residual pull'
            }
        ],
        [
            "AlignMoni_VertexMonitor/PV chisquare per dof",
            {
                'title': '(10) PV #chi^{2}/ndof'
            }
        ],
    ]),
    ("VELO 2-halves alignment", [
        [
            "AlignMoni_TrackPV2HalfMonitor/Left-Right PV delta x",
            {
                'title': '(12) Left-Right PV delta x',
                'vlines': [0, -.008, .008],
                'rangeX': [-.04, .04]
            }, True
        ],
        [
            "AlignMoni_TrackPV2HalfMonitor/Left-Right PV delta y",
            {
                'title': '(13) Left-Right PV delta y',
                'vlines': [0, -.008, .008],
                'rangeX': [-.04, .04]
            }, True
        ],
        [
            "AlignMoni_TrackPV2HalfMonitor/Left-Right PV delta z",
            {
                'title': '(14) Left-Right PV delta z',
                'vlines': [0, -.05, .05],
                'rangeX': [-.3, .3]
            }, True
        ],
    ]),
    ("Residual Plots", [
        [
            "AlignMoni_VPTrackMonitor/Forward/TracksAll/BiasedResidualXModule",
            {
                'title': '(15) X Residual vs Module',
            }
        ],
        [
            "AlignMoni_VPTrackMonitor/Forward/TracksAll/BiasedResidualYModule",
            {
                'title': '(16) Y Residual vs Module',
            }
        ],
        [
            "AlignMoni_TrackMonitor/Velo/VPXResidual",
            {
                'title': '(17) Velo x residual'
            }
        ],
        [
            "AlignMoni_TrackMonitor/Velo/VPYResidual",
            {
                'title': '(18) Velo y residual'
            }
        ],
    ]),
    ("Overlap Residual Plots", [
        [
            "AlignMoni_TrackVPOverlapMonitor/x overlap residual CSO-NSI",
            {
                'title': '(19) CSO-NSI x overlap residual vs module number',
            }
        ],
        [
            "AlignMoni_TrackVPOverlapMonitor/x overlap residual CLI-NSI",
            {
                'title': '(20) CLI-NSI x overlap residual vs module number',
            }
        ],
        [
            "AlignMoni_TrackVPOverlapMonitor/x overlap residual CLI-NLO",
            {
                'title': '(21) CLI-NLO x overlap residual vs module number',
            }
        ],
        [
            "AlignMoni_TrackVPOverlapMonitor/module",
            {
                'title': '(22) module number distribution for overlap hits',
            }
        ],
    ]),
]

if __name__ == '__main__':

    if not (args.alignlog and args.histoFiles):
        run = args.run if args.run else findLastRun(activity)
    alignlog = args.alignlog if args.alignlog else findAlignlog(activity, run)
    files_histo = {
        'old': args.histoFiles[0],
        'new': args.histoFiles[1]
    } if args.histoFiles else findHistos(activity, run)

    # read alignlog
    aout = AlignOutput(alignlog)
    aout.Parse()
    print('I am reading the Alignlog!!')

    c = r.TCanvas('c', 'c')
    outputFile_name = args.outFile if args.outFile else 'MoniPlots.pdf'
    c.Print(outputFile_name + '[')

    # Plots compare before-after
    plotsCompare(Pages, files_histo, outputFile_name)

    #mp_overlapx = []
    #
    #directoryname = "VPOverlapTracksTrackVPOverlapMonitor/overlapresidual_X"
    #filename = {key: r.TFile(path) for key, path in files_histo.items()}
    #num_stations = 26
    #mean = getMeanOverlapResidual(
    #    filename.get('old').GetName(), directoryname, num_stations)[0]
    #err = getMeanOverlapResidual(
    #    filename.get('old').GetName(), directoryname, num_stations)[1]
    #gr0 = makeGraph(mean, err)
    #
    #mean1 = getMeanOverlapResidual(
    #    filename.get('new').GetName(), directoryname, num_stations)[0]
    #err1 = getMeanOverlapResidual(
    #    filename.get('new').GetName(), directoryname, num_stations)[1]
    #gr01 = makeGraph(mean1, err1)
    #
    #mp_overlapx.append(
    #    MultiPlot(
    #        '1',
    #        title=
    #        '(15) Mean x Overlap Residuals vs station num;Station number; Mean x Overlap Residual [#mum] ',
    #        kind='g',
    #        #hlines=[0, -1.5, 1.5, -5, 5],
    #        #hlines_colors={
    #        #    -4: r.kGreen + 3,
    #        #    4: r.kGreen + 3
    #        #},
    #        rangeY=[-50, 50]))
    #mp_overlapx[0].DrawOption = 'ap'
    #mp_overlapx[0].Add(gr01, "new")
    #mp_overlapx[0].Add(gr0, "old")
    #
    #if args.online:
    #    monSvc.publishHistogram(
    #        folder_name,
    #        makeGraphHisto('old', 'Mean Ov Res [#mum]', mean, err),
    #        add=False)
    #    monSvc.publishHistogram(
    #        folder_name,
    #        makeGraphHisto('new', 'Mean Ov Res [#mum]', mean1, err1),
    #        add=False)
    #
    #mp_overlapy = []
    #
    #directorynamey = "VPOverlapTracksTrackVPOverlapMonitor/overlapresidual_Y"
    #meany = getMeanOverlapResidual(
    #    filename.get('old').GetName(), directorynamey, num_stations)[0]
    #erry = getMeanOverlapResidual(
    #    filename.get('old').GetName(), directorynamey, num_stations)[1]
    #gr0y = makeGraph(meany, erry)
    #
    #mean1y = getMeanOverlapResidual(
    #    filename.get('new').GetName(), directorynamey, num_stations)[0]
    #err1y = getMeanOverlapResidual(
    #    filename.get('new').GetName(), directorynamey, num_stations)[1]
    #gr01y = makeGraph(mean1, err1)
    #
    #mp_overlapy.append(
    #    MultiPlot(
    #        '2',
    #        title=
    #        '(15) Mean y Overlap Residuals vs station num;Station number; Mean y Overlap Residual [#mum]',
    #        kind='g',
    #        #hlines=[0, -1.5, 1.5, -5, 5],
    #        #hlines_colors={
    #        #    -4: r.kGreen + 3,
    #        #    4: r.kGreen + 3
    #        #},
    #        rangeY=[-30, 30]))
    #mp_overlapy[0].DrawOption = 'ap'
    #mp_overlapy[0].Add(gr01y, "new")
    #mp_overlapy[0].Add(gr0y, "old")
    #if args.online:
    #    monSvc.publishHistogram(
    #        folder_name,
    #        makeGraphHisto('old', 'Mean Ov Res [#mum]', meany, erry),
    #        add=False)
    #    monSvc.publishHistogram(
    #        folder_name,
    #        makeGraphHisto('new', 'Mean Ov Res [#mum]', mean1y, err1y),
    #        add=False)
    #
    #mp1s = [mp_overlapx[0], mp_overlapy[0]]
    #c1 = getDrawnCanvas(mp1s)
    #c1.Print(outputFile_name)

    mps = []

    taskname = "VeloAlignmentMonitoring"
    location_online = "/hist/Savesets/ByRun/VeloAlignmentMonitoring/"
    directory = str(args.run)[:3] + "000"
    parent_dir = location_online + str(args.run)[:2] + "0000"
    path = os.path.join(parent_dir, directory)
    if not os.path.isdir(path):
        os.makedirs(path)
    filename_new = taskname + "-run" + str(args.run) + ".root"

    location_reference = "/hist/Reference/VeloAlignmentMonitoring/"
    filename_old = "default_1.root"
    filename_reference = "default_" + str(args.run) + ".root"

    histsnew = getHistos(Pages, files_histo,
                         parent_dir + "/" + directory + "/" + filename_new,
                         "new", taskname)
    histsold = getHistos(Pages, files_histo, location_reference + filename_old,
                         "old", taskname)
    histsreference = getHistos(Pages, files_histo,
                               location_reference + filename_reference, "old",
                               taskname)
    f1 = r.TFile.Open(location_reference + filename_old, "UPDATE")
    f2 = r.TFile.Open(location_reference + filename_reference, "UPDATE")

    #f1.GetDirectory(taskname)
    #f1.cd(taskname)
    #hist1x = makeGraphHisto(
    #    'Mean Overlap X res - old',
    #    'Mean Overlap X residuals old vs iteration;Iteration;Mean Overlap X residuals [#mum]',
    #    mean, err)
    #hist1y = makeGraphHisto(
    #    'Mean Overlap Y res - old',
    #    'Mean Overlap Y residuals old vs iteration;Iteration;Mean Overlap Y residuals [#mum]',
    #    meany, erry)
    #f1.Write()
    #f1.Close()
    ##hist1y.SetDirectory("VeloAlignment/")

    f = r.TFile.Open(parent_dir + "/" + directory + "/" + filename_new,
                     "UPDATE")
    f.GetDirectory(taskname)
    f.cd(taskname)

    for expression, title in [
        ('TrChi2nDof', '(17) Track #chi^{2}/dof'),
        ('VChi2nDof', '(18) Vertex #chi^{2}/dof'),
        ('NormalisedChi2Change', '(19) #Delta #chi^{2}/dof'),
        ('vertices', '(20) Number of vertices'),
        ('tracks', '(21) Number of tracks'),
        ('hits', '(22) Number of hits'),
    ]:

        if args.makehist:
            hists = makeGraphHisto(expression, title,
                                   getExpression(aout, expression))
            hists.SetDirectory(0)
            hists.Write()

        gr = makeGraph(getExpression(aout, expression))
        mps.append(
            MultiPlot(
                expression,
                title='{0};Iteration;{0}'.format(title),
                histos={expression: gr},
                kind='g',
                drawLegend=False))
        mps[-1].DrawOption = 'alp'

    ##mps += [mp_overlapx[0], mp_overlapy[0]]
    c1 = getDrawnCanvas(mps)
    c1.Print(outputFile_name)

    # 2 halves convergence plots
    c.cd()
    mp1 = MultiPlot(
        '1',
        title='(20 a) Convergence Halves;Iteration;Variation [#mum]',
        kind='g',
        hlines=[0, -1.5, 1.5, -5, 5],
        hlines_colors={
            -4: r.kGreen + 3,
            4: r.kGreen + 3
        },
        rangeY=[-10, 10])
    mp1.DrawOption = 'alp'
    mp1.Add(
        makeGraph(*getDofDeltaConvergence(
            aout, dof='Tx', alignable='VP/MotionVPLeft/VPLeft')), 'Tx')
    mp1.Add(
        makeGraph(*getDofDeltaConvergence(
            aout, dof='Ty', alignable='VP/MotionVPLeft/VPLeft')), 'Ty')

    mp1z = MultiPlot(
        '1z',
        title='(21 b) Convergence Halves;Iteration;Variation [#mum]',
        kind='g',
        hlines=[0, -1.5, 1.5, -5, 5],
        hlines_colors={
            -4: r.kGreen + 3,
            4: r.kGreen + 3
        },
        rangeY=[-100, 100])
    mp1z.DrawOption = 'alp'
    mp1z.Add(
        makeGraph(*getDofDeltaConvergence(
            aout, dof='Tz', alignable='VP/MotionVPLeft/VPLeft')), 'Tz')

    #Rotations for the halves are not enabled when VELO is open, enable this again when the configuration is changed
    #mp2 = MultiPlot(
    #    '2',
    #    title='(21 c) Convergence Halves;Iteration;Variation [#murad]',
    #    kind='g',
    #    hlines=[0, -4, 4],
    #    rangeY=[-25, 25])
    #mp2.DrawOption = 'alp'
    #mp2.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Rx', alignable='VP/MotionVPLeft/VPLeft')), 'Rx')
    #mp2.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Ry', alignable='VP/MotionVPLeft/VPLeft')), 'Ry')

    #mp3 = MultiPlot(
    #    '3',
    #    title='(22 d) Convergence Halves Rz;Iteration;Variation [#murad]',
    #    kind='g',
    #    drawLegend=False,
    #    hlines=[0, -30, 30],
    #    rangeY=[-100, 100],
    #    histos={
    #        'Rz1':
    #        makeGraph(*getDofDeltaConvergence(
    #            aout, dof='Rz', alignable='VP/MotionVPLeft/VPLeft'))
    #    },
    #)
    #mp3.DrawOption = 'alp'
    if args.online:
        monSvc.publishHistogram(
            folder_name,
            makeGraphHisto(
                'Rz', 'Convergence Rz Halves;Iteration;Variation [#murad]',
                getDofDeltaConvergence(
                    aout, dof='Rz', alignable='VP/MotionVPLeft/VPLeft')[0]),
            add=False)

    c1 = getDrawnCanvas([mp1, mp1z])  #, mp2, mp3])
    #mps += [mp_overlapx[0], mp_overlapy[0], mp1, mp1z, mp2, mp3]
    #c1 = getDrawnCanvas(mps)
    c1.Print(outputFile_name)

    ## # Modules
    #c.cd()
    #mp1 = MultiPlot(
    #    '1',
    #    title='Convergence Modules;Iteration;Variation [#mum]',
    #    kind='g',
    #    hlines=[0, -2, 2],
    #    rangeY=[-10, 10])
    #mp1.DrawOption = 'alp'
    #mp1.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Tx', alignable='VP/VPLeft/Module01WithSupport')),
    #    '00-Tx',
    #    style=1)
    #mp1.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Ty', alignable='VP/VPLeft/Module01WithSupport')),
    #    '00-Ty',
    #    style=-1)
    #mp1.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Tx', alignable='VP/VPLeft/Module25WithSupport')),
    #    '25',
    #    style=2)
    #mp1.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Ty', alignable='VP/VPLeft/Module25WithSupport')),
    #    style=-2)
    #mp1.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Tx', alignable='VP/VPLeft/Module41WithSupport')),
    #    '41',
    #    style=3)
    #mp1.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Ty', alignable='VP/VPLeft/Module41WithSupport')),
    #    style=-3)
    #mp1.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Tx', alignable='VP/VPLeft/Module51WithSupport')),
    #    '51',
    #    style=4)
    #mp1.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Ty', alignable='VP/VPLeft/Module51WithSupport')),
    #    style=-4)
    #
    #mp2 = MultiPlot(
    #    '2',
    #    title='Convergence Modules Rz;Iteration;Variation [#murad]',
    #    kind='g',
    #    hlines=[0, -100, 100],
    #    rangeY=[-300, 300])
    #mp2.DrawOption = 'alp'
    #mp2.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Rz', alignable='VP/VPLeft/Module01WithSupport')),
    #    '01',
    #    style=1)
    #mp2.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Rz', alignable='VP/VPLeft/Module25WithSupport')),
    #    '25',
    #    style=2)
    #mp2.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Rz', alignable='VP/VPLeft/Module41WithSupport')),
    #    '41',
    #    style=3)
    #mp2.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Rz', alignable='VP/VPLeft/Module51WithSupport')),
    #    '51',
    #    style=4)
    #
    #mp3 = MultiPlot(
    #    '3',
    #    title='Convergence Modules;Iteration;Variation [#mum]',
    #    kind='g',
    #    hlines=[0, -2, 2],
    #    rangeY=[-10, 10])
    #mp3.DrawOption = 'alp'
    #mp3.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Tx', alignable='VP/VPRight/Module00WithSupport')),
    #    '00-Tx',
    #    style=1)
    #mp3.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Ty', alignable='VP/VPRight/Module00WithSupport')),
    #    '00-Ty',
    #    style=-1)
    #mp3.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Tx', alignable='VP/VPRight/Module26WithSupport')),
    #    '26',
    #    style=2)
    #mp3.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Ty', alignable='VP/VPRight/Module26WithSupport')),
    #    style=-2)
    #mp3.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Tx', alignable='VP/VPRight/Module42WithSupport')),
    #    '42',
    #    style=3)
    #mp3.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Ty', alignable='VP/VPRight/Module42WithSupport')),
    #    style=-3)
    #mp3.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Tx', alignable='VP/VPRight/Module50WithSupport')),
    #    '50',
    #    style=4)
    #mp3.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Ty', alignable='VP/VPRight/Module50WithSupport')),
    #    style=-4)
    #
    #mp4 = MultiPlot(
    #    '4',
    #    title='Convergence Modules Rz;Iteration;Variation [#murad]',
    #    kind='g',
    #    hlines=[0, -100, 100],
    #    rangeY=[-300, 300])
    #mp4.DrawOption = 'alp'
    #mp4.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Rz', alignable='VP/VPRight/Module00WithSupport')),
    #    '00',
    #    style=1)
    #mp4.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Rz', alignable='VP/VPRight/Module26WithSupport')),
    #    '26',
    #    style=2)
    #mp4.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Rz', alignable='VP/VPRight/Module42WithSupport')),
    #    '42',
    #    style=3)
    #mp4.Add(
    #    makeGraph(*getDofDeltaConvergence(
    #        aout, dof='Rz', alignable='VP/VPRight/Module50WithSupport')),
    #    '50',
    #    style=4)

    # # Modules histo
    #mp5 = MultiPlot(
    #    '5',
    #    title='Variation All Modules;Variation [#mum];',
    #    kind='h',
    #    vlines=[-2, 2])
    #mp5.Add(
    #    makeHisto(
    #        'Tx5',
    #        getDofDeltas(
    #            aout,
    #            dof='Tx',
    #            regexes_alignables=['VP/VP(Left|Right)/Module..WithSupport']),
    #        range=[-10, 10],
    #        nBins=30), 'Tx')
    #mp5.Add(
    #    makeHisto(
    #        'Ty5',
    #        getDofDeltas(
    #            aout,
    #            dof='Ty',
    #            regexes_alignables=['VP/VP(Left|Right)/Module..WithSupport']),
    #        range=[-10, 10],
    #        nBins=30), 'Ty')
    #
    #mp6 = MultiPlot(
    #    '6',
    #    title='Variation All Modules Rz;Variation [#murad];',
    #    kind='h',
    #    vlines=[-100, 100],
    #    drawLegend=False)
    #mp6.Add(
    #    makeHisto(
    #        'Rz6',
    #        getDofDeltas(
    #            aout,
    #            dof='Rz',
    #            regexes_alignables=['VP/VP(Left|Right)/Module..WithSupport']),
    #        range=[-300, 300],
    #        nBins=30))
    #
    ##line = TLine(0,1,5,1)

    #c1 = getDrawnCanvas([mp1, mp3, mp5, mp2, mp4, mp6])
    #c1 = getDrawnCanvas([mp1, mp2, mp3])
    #c1.Print(outputFile_name)

    c.Print(outputFile_name + ']')

    #l = r.TList()
    #l2 = r.TList()
    #histo_paths = sum([i[1] for i in Pages], [])
    #print("we are in histopaths")
    #print(i[1] for i in Pages)
    #paths = []
    #hists = []
    #for histo_path in histo_paths:
    #   path, histo_args = histo_path[:2]
    #paths.append(path)
    #print(path)
    #getHisto(filename.get("old").GetName(),path, "histfile1.root")

    #print(histo_args)
    #  inFiles = {key: r.TFile(path) for key, path in files_histo.items()}
    #  histospath = {key: inFiles[key].Get(path) for key in inFiles}
    #  print(histospath)
    #  hists.append(histospath["old"])
    #l2.Add(histospath["old"])
    #hist = histospath["old"]
    #l2.Write("histlist1",r.TObject.kSingleKey)
    #print(len(hists))

    #filename_old = "testoldv1.root"
    #histsnew = getHistos(Pages, files_histo, filename_new, "new")
    #histsold = getHistos(Pages, files_histo, filename_old, "old")
    #f1 = r.TFile.Open(filename_old, "UPDATE")
    #hist1x = makeGraphHisto('Mean Overlap X res - old', 'Mean Overlap X residuals old vs iteration;Iteration;Mean Overlap X residuals [#mum]',mean, err)
    #hist1y = makeGraphHisto('Mean Overlap Y res - old', 'Mean Overlap Y residuals old vs iteration;Iteration;Mean Overlap Y residuals [#mum]',meany, erry)
    #f1.Write()
    #f1.Close()
    #f = r.TFile.Open(filename_new, "UPDATE")

    #l1.Write("alignlogvalues",r.TObject.kSingleKey)
    if args.makehist:
        print("Making the VELO convergence histograms")
        #hist2x = makeGraphHisto(
        #    'Mean Overlap X res - new',
        #    'Mean Overlap X residuals new vs iteration;Iteration;Mean Overlap X residuals [#mum]',
        #    mean1, err1)
        #hist2y = makeGraphHisto(
        #    'Mean Overlap Y res - new',
        #    'Mean Overlap Y residuals new vs iteration;Iteration;Mean Overlap Y residuals [#mum]',
        #    mean1y, err1y)

        hist3 = makeGraphHisto(
            'Tx', 'Convergence Tx Halves;Iteration;Variation [#mum]',
            getDofDeltaConvergence(
                aout, dof='Tx', alignable='VP/MotionVPLeft/VPLeft')[0])
        hist4 = makeGraphHisto(
            'Ty', 'Convergence Ty Halves;Iteration;Variation [#mum]',
            getDofDeltaConvergence(
                aout, dof='Ty', alignable='VP/MotionVPLeft/VPLeft')[0])
        hist5 = makeGraphHisto(
            'Tz', 'Convergence Tz Halves;Iteration;Variation [#mum]',
            getDofDeltaConvergence(
                aout, dof='Tz', alignable='VP/MotionVPLeft/VPLeft')[0])

        #Rotations are not enables when the VELO is open, enable them again when the configuration is changed
        hist6 = makeGraphHisto(
            'Rx', 'Convergence Rx Halves;Iteration;Variation [#murad]',
            getDofDeltaConvergence(
                aout, dof='Rx', alignable='VP/MotionVPLeft/VPLeft')[0])
        hist7 = makeGraphHisto(
            'Ry', 'Convergence Ry Halves;Iteration;Variation [#murad]',
            getDofDeltaConvergence(
                aout, dof='Ry', alignable='VP/MotionVPLeft/VPLeft')[0])
        hist8 = makeGraphHisto(
            'Rz', 'Convergence Rz Halves;Iteration;Variation [#murad]',
            getDofDeltaConvergence(
                aout, dof='Rz', alignable='VP/MotionVPLeft/VPLeft')[0])

        #modules

        #hist9 = makeGraphHisto(
        #    'Tx - Mod01',
        #    'Convergence Tx Module 01;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Tx', alignable='VP/VPLeft/Module01WithSupport')[0])
        #hist10 = makeGraphHisto(
        #    'Ty - Mod01',
        #    'Convergence Ty Module 01;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Ty', alignable='VP/VPLeft/Module01WithSupport')[0])
        #hist11 = makeGraphHisto(
        #    'Tx - Mod25',
        #    'Convergence Tx Module 25;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Tx', alignable='VP/VPLeft/Module25WithSupport')[0])
        #hist12 = makeGraphHisto(
        #    'Ty - Mod25',
        #    'Convergence Ty Module 25;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Ty', alignable='VP/VPLeft/Module25WithSupport')[0])
        #hist13 = makeGraphHisto(
        #    'Tx - Mod41',
        #    'Convergence Tx Module 41;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Tx', alignable='VP/VPLeft/Module41WithSupport')[0])
        #hist14 = makeGraphHisto(
        #    'Ty - Mod41',
        #    'Convergence Ty Module 41;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Ty', alignable='VP/VPLeft/Module41WithSupport')[0])
        #hist15 = makeGraphHisto(
        #    'Tx - Mod51',
        #    'Convergence Tx Module 51;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Tx', alignable='VP/VPLeft/Module51WithSupport')[0])
        #hist16 = makeGraphHisto(
        #    'Ty - Mod51',
        #    'Convergence Ty Module 51;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Ty', alignable='VP/VPLeft/Module51WithSupport')[0])
        #hist17 = makeGraphHisto(
        #    'Rz - Mod01',
        #    'Convergence Rz Module 01;Iteration;Variation [#murad]',
        #    getDofDeltaConvergence(
        #        aout, dof='Rz', alignable='VP/VPLeft/Module01WithSupport')[0])
        #hist18 = makeGraphHisto(
        #    'Rz - Mod25',
        #    'Convergence Rz Module 25;Iteration;Variation [#murad]',
        #    getDofDeltaConvergence(
        #        aout, dof='Rz', alignable='VP/VPLeft/Module25WithSupport')[0])
        #hist19 = makeGraphHisto(
        #    'Rz - Mod41',
        #    'Convergence Rz Module 41;Iteration;Variation [#murad]',
        #    getDofDeltaConvergence(
        #        aout, dof='Rz', alignable='VP/VPLeft/Module41WithSupport')[0])
        #hist20 = makeGraphHisto(
        #    'Rz - Mod51',
        #    'Convergence Rz Module 51;Iteration;Variation [#murad]',
        #    getDofDeltaConvergence(
        #        aout, dof='Rz', alignable='VP/VPLeft/Module51WithSupport')[0])
        #
        #hist21 = makeGraphHisto(
        #    'Tx - Mod00',
        #    'Convergence Tx Module 00;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Tx', alignable='VP/VPRight/Module00WithSupport')[0])
        #hist22 = makeGraphHisto(
        #    'Ty - Mod00',
        #    'Convergence Ty Module 00;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Ty', alignable='VP/VPRight/Module00WithSupport')[0])
        #hist23 = makeGraphHisto(
        #    'Tx - Mod26',
        #    'Convergence Tx Module 26;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Tx', alignable='VP/VPRight/Module26WithSupport')[0])
        #hist24 = makeGraphHisto(
        #    'Ty - Mod26',
        #    'Convergence Ty Module 26;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Ty', alignable='VP/VPRight/Module26WithSupport')[0])
        #hist25 = makeGraphHisto(
        #    'Tx - Mod42',
        #    'Convergence Tx Modules 42;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Tx', alignable='VP/VPRight/Module42WithSupport')[0])
        #hist26 = makeGraphHisto(
        #    'Ty - Mod42',
        #    'Convergence Ty Module 42;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Ty', alignable='VP/VPRight/Module42WithSupport')[0])
        #hist27 = makeGraphHisto(
        #    'Tx - Mod50',
        #    'Convergence Tx Module 50;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Tx', alignable='VP/VPRight/Module50WithSupport')[0])
        #hist28 = makeGraphHisto(
        #    'Ty - Mod50',
        #    'Convergence Ty Module 50;Iteration;Variation [#mum]',
        #    getDofDeltaConvergence(
        #        aout, dof='Ty', alignable='VP/VPRight/Module50WithSupport')[0])
        #hist29 = makeGraphHisto(
        #    'Rz - Mod00',
        #    'Convergence Rz Module 00;Iteration;Variation [#murad]',
        #    getDofDeltaConvergence(
        #        aout, dof='Rz', alignable='VP/VPRight/Module00WithSupport')[0])
        #hist30 = makeGraphHisto(
        #    'Rz - Mod26',
        #    'Convergence Rz Module 26;Iteration;Variation [#murad]',
        #    getDofDeltaConvergence(
        #        aout, dof='Rz', alignable='VP/VPRight/Module26WithSupport')[0])
        #hist31 = makeGraphHisto(
        #    'Rz - Mod42',
        #    'Convergence Rz Module 42;Iteration;Variation [#murad]',
        #    getDofDeltaConvergence(
        #        aout, dof='Rz', alignable='VP/VPRight/Module42WithSupport')[0])
        #hist32 = makeGraphHisto(
        #    'Rz - Mod50',
        #    'Convergence Rz Module 50;Iteration;Variation [#murad]',
        #    getDofDeltaConvergence(
        #        aout, dof='Rz', alignable='VP/VPRight/Module50WithSupport')[0])
        #
        #hist33 = makeHisto(
        #    'Variation Tx all modules',
        #    getDofDeltas(
        #        aout,
        #        dof='Tx',
        #        regexes_alignables=['VP/VP(Left|Right)/Module..WithSupport']),
        #    range=[-10, 10],
        #    nBins=30)
        #hist34 = makeHisto(
        #    'Variation Ty all modules',
        #    getDofDeltas(
        #        aout,
        #        dof='Ty',
        #        regexes_alignables=['VP/VP(Left|Right)/Module..WithSupport']),
        #    range=[-10, 10],
        #    nBins=30)
        #hist35 = makeHisto(
        #    'Variation Rz all modules',
        #    getDofDeltas(
        #        aout,
        #        dof='Rz',
        #        regexes_alignables=['VP/VP(Left|Right)/Module..WithSupport']),
        #    range=[-10, 10],
        #    nBins=30)

        valuesx = [1, 1]
        valuesy = [0, 0]
        f.Write()

    #Define lines on plots to be saved in a separate histogram:
    #First specify location to save the lines online
    #location = "/hist/Reference/VeloAlignmentMonitoring/"

    rootfile_iter1 = r.TFile.Open(files_histo["new"])

    pvxyline1 = getLines(
        location_reference + "pvxylines1.root",
        rootfile_iter1.Get("AlignMoni_VertexMonitor/PV x position").GetMean(),
        0.0,
        rootfile_iter1.Get("AlignMoni_VertexMonitor/PV x position").GetMean(),
        300000.0)

    pvxyline1 = getLines(
        location_reference + "pvxylines2.root",
        rootfile_iter1.Get("AlignMoni_VertexMonitor/PV y position").GetMean(),
        0.0,
        rootfile_iter1.Get("AlignMoni_VertexMonitor/PV y position").GetMean(),
        300000.0)

    pvxyline3 = getLines(
        location_reference + "pvxylines3.root",
        rootfile_iter1.Get("AlignMoni_VertexMonitor/PV z position").GetMean(),
        0.0,
        rootfile_iter1.Get("AlignMoni_VertexMonitor/PV z position").GetMean(),
        300000.0)

    pvleftrightxyline1 = getLines(
        location_reference + "pvleftrightxylines1.root",
        rootfile_iter1.Get(
            "AlignMoni_TrackPV2HalfMonitor/Left-Right PV delta x").GetMean(),
        0.0,
        rootfile_iter1.Get(
            "AlignMoni_TrackPV2HalfMonitor/Left-Right PV delta x").GetMean(),
        300000.0)

    pvleftrightxyline2 = getLines(
        location_reference + "pvleftrightxylines2.root",
        rootfile_iter1.Get(
            "AlignMoni_TrackPV2HalfMonitor/Left-Right PV delta y").GetMean(),
        0.0,
        rootfile_iter1.Get(
            "AlignMoni_TrackPV2HalfMonitor/Left-Right PV delta y").GetMean(),
        300000.0)

    pvleftrightxyline3 = getLines(
        location_reference + "pvleftrightxylines3.root",
        rootfile_iter1.Get(
            "AlignMoni_TrackPV2HalfMonitor/Left-Right PV delta z").GetMean(),
        0.0,
        rootfile_iter1.Get(
            "AlignMoni_TrackPV2HalfMonitor/Left-Right PV delta z").GetMean(),
        300000.0)

    lines2 = getLinesall(location_reference + "lines2.root", -0.008, 0.0,
                         -0.008, 3000.0, 0.008, 0.0, 0.008, 3000.0)
