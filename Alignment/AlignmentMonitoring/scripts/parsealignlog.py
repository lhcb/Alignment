#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import numpy as np
import matplotlib.pyplot as plt
import re
from scanf import scanf


def stringreplace(astring, insub, outsub, pos):
    return astring.replace(insub, outpub)


def ElementColor(name):
    if "FT" in name:
        if "HL0" in name: return 'blue'
        if "HL1" in name: return 'red'
    if "VP" in name:
        if "Left" in name: return 'red'
        if "Right" in name: return 'blue'
    if "UT" in name:
        if "Aside" in name: return 'red'
        if "Cside" in name: return 'blue'
    return 'black'


def ElementMarkerStyle(name):
    if "ladder" in name: return "x"
    if "M" in name: return "+"
    #markers = ["." , "," , "o" , "v" , "^" , "<", ">"]
    #if "ladder" in name:
    return "o"


#import copy
#parvecF = np.zeros( (6,), dtype=np.float32 )
#parvecB = 6*[True]


class ParSet:
    def __init__():
        self._nhits = 0
        self._vec = np.zeros((6, ), dtype=np.float32)


class AElement:
    def __init__(self, name):
        self._name = name
        self._pos = np.zeros((3, ), dtype=np.float32)
        self._parsign = np.zeros((6, ), dtype=np.float32)
        self._parintot = np.zeros((6, ), dtype=np.float32)
        self._parouttot = np.zeros((6, ), dtype=np.float32)
        self._parin = np.zeros((6, ), dtype=np.float32)
        self._parout = np.zeros((6, ), dtype=np.float32)
        self._parinsurveyframe = np.zeros((6, ), dtype=np.float32)
        self._survey = np.zeros((6, ), dtype=np.float32)
        self._surveyerr = -1 * np.ones((6, ), dtype=np.float32)
        self._par = np.zeros((6, ), dtype=np.float32)
        self._partot = np.zeros((6, ), dtype=np.float32)
        self._err = np.zeros((6, ), dtype=np.float32)
        self._uncorrelatederr = np.zeros((6, ), dtype=np.float32)
        self._first = 6 * [True]
        self._dchi2 = 0
        self._insufficienthits = False

    def seen(self, ipar):
        return not self._first[ipar]

    def dx(self, ipar, mode=5):
        if mode == 0: return self._parout[ipar] - self._parin[ipar]
        if mode == 1: return self._parout[ipar]
        if mode == 2: return self._parouttot[ipar] - self._parintot[ipar]
        if mode == 3:
            return self._parout[ipar] - self._parin[ipar] + self._parintot[ipar]
        if mode == 4: return self._parin[ipar]
        if mode == 5: return self._parout[ipar] - self._survey[ipar]
        if mode == 6: return self._survey[ipar]
        if mode == 7: return self._parin[ipar] - self._survey[ipar]
        if mode == 8: return self._parouttot[ipar] - self._survey[ipar]
        if mode == 9: return self._parinsurveyframe[ipar] - self._survey[ipar]
        if mode == 10: return self._partot[ipar]
        return 0.0

    def z(self):
        return self._pos[2]


def modelabel(mode):
    labels = [
        'delta', 'final', 'delta tot', 'final tot', 'initial',
        'difference to survey', 'survey', 'initial to survey',
        'total to survey', 'difference to survey', 'global'
    ]
    return labels[mode]


def parname(ipar):
    labels = ["Tx", "Ty", "Tz", "Rx", "Ry", "Rz"]
    return labels[ipar]


class IterationData():
    def __init__(self, iteration):
        self._iteration = iteration
        self._elements = {}


#Global position: (-4.44089e-16,1.11022e-16,0) Average position of hits: (-12.0829,-7.06214,223.868)
#Number of tracks/hits/outliers seen: 1120303 2287826 0
#Local-to-global diagonal: 1 1 1 1 1 1
#Tx  : curtot= -2.98        cur= -2.98        delta= 0.05639      +/- 0.0005198    [ 0.0004655    ]  gcc= 0.4449
#Ty  : curtot= 0.5777       cur= 0.5777       delta= 0.02288      +/- 0.0002915    [ 0.0002843    ]  gcc= 0.2204
#Tz  : curtot= 0.1474       cur= 0.1474       delta= 0.01961      +/- 0.001676     [ 0.001667     ]  gcc= 0.101
#Rx  : curtot= -8.885e-05   cur= -8.885e-05   delta= -2.594e-05   +/- 2.142e-06    [ 2.114e-06    ]  gcc= 0.1605
#Ry  : curtot= -6.627e-05   cur= -6.627e-05   delta= -7.695e-05   +/- 2.542e-06    [ 2.28e-06     ]  gcc= 0.4422
#Rz  : curtot= -0.0005078   cur= -0.0005078   delta= 0.0005222    +/- 3.493e-05    [ 3.443e-05    ]  gcc= 0.1696
#contribution to hit uncertainty (absolute/relative): 2.662e-05 0.002122
#local delta chi2 / dof: 1.903e+04 / 6
#survey chi2 / dof (before/after): 37.16 14.19 / 6
#align pars: -2.924, 0.5991, 0.1667, -0.0001148, -0.0001433, 1.439e-05
#survey pars:   -1.86, 0, 0, 0, 0, 0
#survey uncertainties: 1, 0.2, 0.2, 0.0001, 0.0001, 0.0001


def floatpattern():
    return "(-?\d*\.?\d+([eE][-+]?\d+)?"


def namedfloatpattern(name):
    #return "(?P<%s>([-+]?\d*\.?\d+( |e[-+]\d+)))" % name
    #return "(?P<%s>([+-]?\d+\.?\d*))"%name
    #return "(P<%s>(-?\d*\.?\d+(?:[eE][-+]?\d+)))"%name
    return "(?P<%s>(-?\d*\.?\d+([eE][-+]?\d+)?))" % name


def parindex(parname):
    if "Tx" in parname: return 0
    if "Ty" in parname: return 1
    if "Tz" in parname: return 2
    if "Rx" in parname: return 3
    if "Ry" in parname: return 4
    if "Rz" in parname: return 5


def parsealignlogfile(filename, mergeiterations=True):
    alignablepattern = re.compile("Alignable: (?P<name>(.*))")
    iterationpattern = re.compile("Iteration: (?P<iteration>(\d*))")
    parlinepattern = re.compile(
        "(?P<parname>(Tx|Ty|Tz|Rx|Ry|Rz))" +
        str(".* curtot= %s" % namedfloatpattern("curtot")) +
        str(".* cur= %s" % namedfloatpattern("cur")) +
        str(".* delta= %s" % namedfloatpattern("delta")) +
        str(".* \+/\- %s" % namedfloatpattern("deltaerr")) + ".* ")
    nfp = namedfloatpattern
    positionpattern = re.compile("Global position: \(%s,%s,%s\)" %
                                 (nfp("posx"), nfp("posy"), nfp("posz")))

    elementcontainer = {}
    iteration = None
    alignablename = None
    iterations = {}
    if mergeiterations:
        iteration = IterationData("all")
        iterations["all"] = iteration

    infile = open(filename)
    for line in infile:
        #print("parsing line",line)
        #if line[0] == '*': continue
        if not mergeiterations:
            matches = iterationpattern.search(line)
            if matches:
                iterationname = matches.group('iteration')
                iteration = IterationData(iterationname)
                iterations[iterationname] = iteration
                #print("found iteration %s" % iterationname)

        matches = alignablepattern.search(line)
        if matches:
            alignablename = matches.group('name')
            if alignablename not in iteration._elements:
                iteration._elements[alignablename] = AElement(alignablename)
            element = iteration._elements[alignablename]

        matches = parlinepattern.search(line)
        if matches:
            ipar = parindex(matches.group('parname'))
            element._partot[ipar] = float(matches.group('curtot'))
            element._par[ipar] = float(matches.group('cur')) + float(
                matches.group('delta'))
            element._err[ipar] = float(matches.group('deltaerr'))
            element._parout[ipar] = float(matches.group('cur')) + float(
                matches.group('delta'))
            if element._first[ipar]:
                element._parin[ipar] = float(matches.group('cur'))
            element._first[ipar] = False
            #print( line)
            #print( "'%s'" % matches.group('cur'))
            #print("float value %d=%f" % (ipar, float(matches.group('cur'))),
            #      element._par[ipar], ipar, element._name)
        elif "curtot" in line:
            print("problem parsing line", line)
            lidwqdqw

        matches = positionpattern.search(line)
        if matches:
            element._pos[0] = matches.group('posx')
            element._pos[1] = matches.group('posy')
            element._pos[2] = matches.group('posz')
        if "align pars:" in line:
            alignpars = scanf("align pars: %f, %f, %f, %f, %f, %f", line)
            for (i, v) in enumerate(alignpars):
                element._parin[i] = v
                if element._par[i] == 0: element._par[i] = v
                element._parinsurveyframe[i] = v
        if "align pars in survey frame:" in line:
            alignpars = scanf(
                "align pars in survey frame: %f, %f, %f, %f, %f, %f", line)
            if alignpars:
                for (i, v) in enumerate(alignpars):
                    element._parinsurveyframe[i] = v
        if "survey pars" in line:
            surveypars = scanf("survey pars: %f, %f, %f, %f, %f, %f", line)
            for (i, v) in enumerate(surveypars):
                element._survey[i] = v
        if "Local-to-global diagonal:" in line:
            element._parsign = scanf(
                "Local-to-global diagonal: %f %f %f %f %f %f", line)

    #for elemname, elem in iterations['0']._elements.items():
    #    print("elem=",elem._name,elem._pos, elem._parin)

    print("Found elements:")
    for elemname, elem in iterations[list(
            iterations.keys())[-1]]._elements.items():
        print(elemname)

    return iterations


def drawVersusZ(filename, mode, pars, pattern=".*", **kwargs):

    iterations = parsealignlogfile(filename)
    print("iterations:", iterations)

    namepattern = re.compile(pattern)

    # get series of z values, dx values and colors
    for ipar in pars:
        z = []
        dx = []
        dxerr = []
        col = []
        style = []
        for elemname, elem in iterations[list(
                iterations.keys())[-1]]._elements.items():
            if (mode == 7 or mode == 4 or elem.seen(ipar)) and (
                    pattern == None or namepattern.fullmatch(elem._name)):
                z.append(elem.z())
                dx.append(elem._parsign[ipar] * elem.dx(ipar, mode))
                dxerr.append(elem._err[ipar])
                col.append(ElementColor(elemname))
                style.append(ElementMarkerStyle(elemname))
        if 'noerrors' in kwargs and kwargs['noerrors']:
            dxerr = len(dx) * [0]
        # create the plot
        print(z)
        print(dx)
        fig, axis = plt.subplots(1, 1)
        fig.set_size_inches(8, 4)
        for (x, y, yerr, c, marker) in zip(z, dx, dxerr, col, style):
            axis.errorbar(x=x, y=y, yerr=yerr, c=c, marker=marker)
            #print("c",c)
            #axis.scatter(x=x,y=y,c=c,marker=marker)
        fig.suptitle(modelabel(mode))
        axis.set_ylabel(parname(ipar))
        ylims = plt.ylim()
        plt.ylim([min(ylims[0], -ylims[1]), max(-ylims[0], ylims[1])])
        fig.savefig("fig_%s_%d.png" % (parname(ipar), mode))

    plt.show()


if __name__ == "__main__":
    from optparse import OptionParser
    parser = OptionParser(usage="%prog [options] alignlog.txt")
    parser.add_option(
        "-v",
        "--verbose",
        action="store_true",
        dest="verbose",
        default=False,
        help="verbose")
    parser.add_option("-m", "--mode", type="int", default=5)
    parser.add_option("-p", "--pars", type="str", default="TxTyTzRxRyRz")
    parser.add_option("--pattern", type="str", default=".*")
    parser.add_option(
        "-e", "--noerrors", action="store_true", help="don't plot errors")
    (opts, args) = parser.parse_args()

    if len(args) != 1:
        print("please provide one input file")
    else:
        filename = args[0]
        pars = []
        if "Tx" in opts.pars: pars.append(0)
        if "Ty" in opts.pars: pars.append(1)
        if "Tz" in opts.pars: pars.append(2)
        if "Rx" in opts.pars: pars.append(3)
        if "Ry" in opts.pars: pars.append(4)
        if "Rz" in opts.pars: pars.append(5)
        drawVersusZ(
            filename,
            pattern=opts.pattern,
            mode=opts.mode,
            pars=pars,
            noerrors=opts.noerrors)
'''    
    if infile:
        iterations = readAlignableFromFile(infile)
        for (key,data) in iterations.items():
            print( data._elements["VP/VPRight"]._partot[0] )


    resolverpositions = [
        (250854,28.150,-25.850,0.45),
        (250857,21.080,-18.90,0.45),
        (250861,14.103,-11.898,0.45),
        (250863,7.112,-4.888,0.466),
        (250874,3.155,-0.845,0.456),
        (250878,2.181,0.181,0.464),
#        (250884,1.739,0.738,0.467),
        (250886,1.306,1.304,0.479),
        (250895,1.306,1.304,0.479) ]

    alignconstants = []
    alignconstanterrs = []
    import copy
        
    for ( runnr, resolverA, resolverC, resolverY ) in resolverpositions :
        filename = "%d/Iter4/alignlog_vp_halves_modules.txt" % runnr
        iterations = readAlignableFromFile(open(filename))
        for (key,data) in iterations.items():
            #print(runnr, resolverA, resolverC, data._elements["VP/VPRight"]._partot[0],
            #          resolverC - data._elements["VP/VPRight"]._partot[0], data._elements["VP/VPRight"]._partot[4]*1000  )

            alignpars = copy.deepcopy(data._elements["VP/VPRight"]._par)
            #print(alignpars)
            alignpars[0] -= resolverC
            alignpars[1] -= resolverY
            #print(alignpars)
            #dwqdqw
            alignconstants.append( alignpars )
            alignconstanterrs.append( copy.deepcopy( data._elements["VP/VPRight"]._err ) )
            print(runnr, resolverA, resolverC, alignconstants[-1])

    print(alignconstants)
            
    # make some plots
    
    fig, axes = plt.subplots(2,3)
    axes = [ item for sublist in axes for item in sublist ]
    fig.set_size_inches(16,9)
    fig.subplots_adjust(left=0.10,right=0.98,bottom=0.10,top=0.95,wspace = 0.3,hspace=0.3)
    titles = ['Tx','Ty','Tz','Rx','Ry','Rz']
    
    # Tx as function of opening
    N = len(alignconstants)
    opening = [ A - C for (run,A,C,Y) in resolverpositions ]
    labels  = [ str(run) for (run,A,C,Y) in resolverpositions ]
    for i in range(6):
        axes[i].set_title(titles[i])
        axes[i].set_xlabel("opening [mm]")
        axes[i].set_ylabel(titles[i])
        #axes[i].errorbar( opening,  [ a[i] for a in alignconstants ], yerr=[ a[i] for a in alignconstanterrs],fmt="o")

        points = []
        for (o,a,b) in zip( opening,alignconstants,alignconstanterrs):
            points.append(axes[i].errorbar( [o],[a[i]],yerr=[b[i]],fmt="o"))
        if i==0:
            axes[i].legend( points, labels)
            
        #axes[i].scatter( opening,  [ a[i] for a in alignconstants ] )
    fig.savefig("alignconstants.pdf")
    plt.show()
        
'''
