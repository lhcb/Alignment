#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

##########################
###   Options parser   ###
from __future__ import print_function
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description="Macro to make plots to monitor automatic Tracker alignment"
    )
    parser.add_argument(
        '-r', '--run', help='run number, default is the last one')
    parser.add_argument(
        '--alignlog',
        help='location of alignlog.txt, default is guessed by run number')
    parser.add_argument(
        '--histoFiles',
        help=
        'location of histograms files, expect two paths, old ad new align histos, default is guessed by run number',
        nargs=2)
    parser.add_argument('-o', '--outFile', help='output file name')
    parser.add_argument(
        '--online',
        help='Setup monitoring job to send the histograms to the presenter',
        action='store_true')
    parser.add_argument(
        '--makehist', help='Setup all histos', action='store_true')
    args = parser.parse_args()
##########################

from GaudiPython import gbl
import ROOT as r
#import os
import os, sys, fnmatch, re, copy, importlib
from AlignmentMonitoring import OnlineUtils, RootUtils
importlib.reload(sys.modules['AlignmentMonitoring.RootUtils'])
import numpy as np

AlMon = gbl.Alignment.AlignmentMonitoring


def ConfigureMonApp(outputName, pages):
    r.gStyle.SetOptTitle(1)
    monApp = AlMon.MonitoringApplication(outputName)
    for key in sorted(pages.keys()):
        name = pages[key]['name']
        hists = pages[key]['hists']
        hists_sa = RootUtils.wrapStringArray(hists)
        if len(hists_sa) < 10:
            almon = AlMon.MonitoringPage(name, hists_sa)
            for hist in hists:
                configs = Configs[name][hist]
                if ('title' in configs): almon.setTitle(hist, configs['title'])
                if ('mean' in configs): almon.setMLine(hist, configs['mean'])
                if ('vlines' in configs):
                    almon.setVLine(
                        hist,
                        RootUtils.wrapNumericArray(configs['vlines'],
                                                   'double'))
                if ('hlines' in configs):
                    almon.setHLine(
                        hist,
                        RootUtils.wrapNumericArray(configs['hlines'],
                                                   'double'))
                if ('yrange' in configs):
                    almon.setYRange(hist, configs['yrange'][0],
                                    configs['yrange'][1])
                if ('xrange' in configs):
                    almon.setXRange(hist, configs['xrange'][0],
                                    configs['xrange'][1])
                if ('reference' in configs):
                    almon.setReferenceLine(hist, configs['reference'][0],
                                           configs['reference'][1])
                if ('lines' in configs):
                    tLines = r.std.vector('TLine*')()
                    for x1, y1, x2, y2 in configs['lines']:
                        tLines.push_back(r.TLine(x1, y1, x2, y2))
                    almon.setLines(hist, tLines)
            monApp.addPage(almon)
        else:
            print('Too many histograms per page requested!')
    return monApp


Pages = {
    1: {
        'name':
        "FT TrackMonitor",
        'hists': [
            "AlignMoni_FTTrackMonitor_PrKalman/UnbiasedResidualModules",
            "AlignMoni_FTTrackMonitor_PrKalman/txT1",
            "AlignMoni_FTTrackMonitor_PrKalman/xT1",
            "AlignMoni_FTTrackMonitor_PrKalman/yT1",
            "AlignMoni_FTTrackMonitor_PrKalman/slopesT1",
            "AlignMoni_FTTrackMonitor_PrKalman/posT1",
        ]
    },
    2: {
        'name':
        "FT TrackMonitor",
        'hists': [
            "AlignMoni_FTTrackMonitor_PrKalman/txT3",
            "AlignMoni_FTTrackMonitor_PrKalman/xT3",
            "AlignMoni_FTTrackMonitor_PrKalman/yT3",
            "AlignMoni_FTTrackMonitor_PrKalman/slopesT3",
            "AlignMoni_FTTrackMonitor_PrKalman/posT3",
        ]
    },
    3: {
        'name':
        "TrackFitMatchMonitor",
        'hists': [
            "AlignMoni_TrackFitMatchMonitor/curvatureRatioTToLongVsQoP",
            "AlignMoni_TrackFitMatchMonitor/curvatureRatioVeloUTToLongVsQoP",
            "AlignMoni_TrackFitMatchMonitor/curvatureRatioTToLongVsTx",
            "AlignMoni_TrackFitMatchMonitor/curvatureRatioTToLong",
            "AlignMoni_TrackFitMatchMonitor/curvatureRatioTToLongPull",
        ]
    },
    4: {
        'name':
        "TrackMonitor",
        'hists': [
            "AlignMoni_TrackMonitor/Long/nFTHits",
            "AlignMoni_TrackMonitor/Long/chi2ProbVsMom",
            "AlignMoni_TrackMonitor/Long/chi2ProbVsEta",
            "AlignMoni_TrackMonitor/Long/chi2ProbVsPhi",
            "AlignMoni_TrackMonitor/Long/chi2ProbMatchVsMom",
            "AlignMoni_TrackMonitor/Long/chi2ProbDownstreamVsMom",
            "AlignMoni_TrackMonitor/Long/chi2ProbVeloVsMom",
        ]
    },
    5: {
        'name':
        "FTMatCalibrationMonitor",
        'hists': [
            "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat4",
            "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat85",
            "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat165",
            "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat245",
            "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat325",
            "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat405",
            "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat485",
            "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat565",
            "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat645",
            "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat725",
            "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat805",
            "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat885",
        ]
    },
    6: {
        'name':
        "D0MassMonitor",
        'hists': [
            "AlignMoni_defaultD0MassMonitor/D0_MassvsPdiff",
            "AlignMoni_defaultD0MassMonitor/D0_mass",
        ]
    },
}

Configs = {
    "FT TrackMonitor": {
        "AlignMoni_FTTrackMonitor_PrKalman/UnbiasedResidualModules": {
            'title': '(1) Unbiased residual in each module'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/txT1": {
            'title': '(3) tx T1'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/xT1": {
            'title': '(4) x in T1'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/yT1": {
            'title': '(5) y in T1'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/slopesT1": {
            'title': '(6) slopes in FTStation T1'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/posT1": {
            'title': '(7) extrapolated track position in FTStation T1'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/txT3": {
            'title': '(8) tx T3'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/xT3": {
            'title': '(9) x in T3'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/yT3": {
            'title': '(9) y in T3'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/slopesT3": {
            'yrange': [-0.5, 0.5],
            'xrange': [-0.5, 0.5],
            'title': '(10) slopes in FTStation T3'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/posT3": {
            'title': '(11) extrapolated track position in FTStation T3'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/unbiasedresidualVsXY_profileL0": {
            'title': '(12) unbiasedresidualVsXY L0'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/unbiasedresidualVsXY_profileL1": {
            'title': '(13) unbiasedresidualVsXY L1'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/unbiasedresidualVsXY_profileL2": {
            'title': '(14) unbiasedresidualVsXY L2'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/unbiasedresidualVsXY_profileL3": {
            'title': '(15) unbiasedresidualVsXY L3'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/unbiasedresidualVsXY_profileL4": {
            'title': '(16) unbiasedresidualVsXY L4'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/unbiasedresidualVsXY_profileL5": {
            'title': '(17) unbiasedresidualVsXY L5'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/unbiasedresidualVsXY_profileL6": {
            'title': '(18) unbiasedresidualVsXY L6'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/unbiasedresidualVsXY_profileL7": {
            'title': '(19) unbiasedresidualVsXY L7'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/unbiasedresidualVsXY_profileL8": {
            'title': '(20) unbiasedresidualVsXY L8'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/unbiasedresidualVsXY_profileL9": {
            'title': '(21) unbiasedresidualVsXY L9'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/unbiasedresidualVsXY_profileL10": {
            'title': '(22) unbiasedresidualVsXY L10'
        },
        "AlignMoni_FTTrackMonitor_PrKalman/unbiasedresidualVsXY_profileL11": {
            'title': '(23) unbiasedresidualVsXY L11'
        },
    },
    "TrackFitMatchMonitor": {
        "AlignMoni_TrackFitMatchMonitor/curvatureRatioTToLongVsQoP": {
            'title': '(1) curvatureRatioTToLong vs QoP'
        },
        "AlignMoni_TrackFitMatchMonitor/curvatureRatioVeloUTToLongVsQoP": {
            'title': '(2) curvatureRatioVeloUTToLong vs QoP'
        },
        "AlignMoni_TrackFitMatchMonitor/curvatureRatioTToLongVsTx": {
            'title': '(3) curvatureRatioTToLong vs Tx'
        },
        "AlignMoni_TrackFitMatchMonitor/curvatureRatioTToLong": {
            'title': '(4) curvatureRatioTToLong'
        },
        "AlignMoni_TrackFitMatchMonitor/curvatureRatioTToLongPull": {
            'title': '(5) curvatureRatioTToLongPull'
        },
    },
    "TrackMonitor": {
        "AlignMoni_TrackMonitor/Long/nFTHits": {
            'title': '(1) Number of FT hits'
        },
        "AlignMoni_TrackMonitor/Long/chi2ProbVsMom": {
            'yrange': [0.0, 1.0],
            'title': '(2) chi2Prob vs Mom'
        },
        "AlignMoni_TrackMonitor/Long/chi2ProbVsEta": {
            'yrange': [0.0, 1.0],
            'title': '(3) chi2Prob vs Eta'
        },
        "AlignMoni_TrackMonitor/Long/chi2ProbVsPhi": {
            'yrange': [0.0, 1.0],
            'title': '(4) chi2Prob vs Phi'
        },
        "AlignMoni_TrackMonitor/Long/chi2ProbMatchVsMom": {
            'yrange': [0.0, 1.0],
            'title': '(5) chi2Prob vs Mom'
        },
        "AlignMoni_TrackMonitor/Long/chi2ProbDownstreamVsMom": {
            'yrange': [0.0, 1.0],
            'title': '(6) Chi2Prob vs Mom'
        },
        "AlignMoni_TrackMonitor/Long/chi2ProbVeloVsMom": {
            'yrange': [0.0, 1.0],
            'title': '(7) Chi2ProbVelo vs Mom'
        },
    },
    "FTMatCalibrationMonitor": {
        "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat4":
        {
            'title': '(1) unbiased residual per Channel, mat Mat4'
        },
        "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat85":
        {
            'title': '(2) unbiased residual per Channel, mat Mat85'
        },
        "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat165":
        {
            'title': '(3) unbiased residual per Channel, mat Mat165'
        },
        "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat245":
        {
            'title': '(4) unbiased residual per Channel, mat Mat245'
        },
        "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat325":
        {
            'title': '(5) unbiased residual per Channel, mat Mat325'
        },
        "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat405":
        {
            'title': '(6) unbiased residual per Channel, mat Mat405'
        },
        "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat485":
        {
            'title': '(7) unbiased residual per Channel, mat Mat485'
        },
        "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat565":
        {
            'title': '(8) unbiased residual per Channel, mat Mat565'
        },
        "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat645":
        {
            'title': '(9) unbiased residual per Channel, mat Mat645'
        },
        "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat725":
        {
            'title': '(10) unbiased residual per Channel, mat Mat725'
        },
        "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat805":
        {
            'title': '(11) unbiased residual per Channel, mat Mat805'
        },
        "AlignMoni_FTMatCalibrationMonitor/DeltaUnbiasedresidualPerChannelGlobalMat885":
        {
            'title': '(12) unbiased residual per Channel, mat Mat885'
        },
    },
    "D0MassMonitor": {
        "AlignMoni_defaultD0MassMonitor/D0_MassvsPdiff": {
            'title': '(1) D0 mass vs Pdiff'
        },
        "AlignMoni_defaultD0MassMonitor/D0_mass": {
            'title': '(2) D0 mass'
        }
    }
}


def publishHistosCompare(Configs,
                         files_histo,
                         monSvc,
                         folder_name='MoniOnlineAligTracker'):
    histo_paths = sum([i.items() for i in Configs.values()], [])
    for histo_path in histo_paths:
        path, histo_args = histo_path[:2]
        inFiles = {key: r.TFile(path) for key, path in files_histo.items()}
        histos = {key: inFiles[key].Get(path) for key in inFiles}
        if 'title' not in histo_args:
            histo_args['title'] = histos['old'].GetTitle()
        if 'name' not in histo_args:
            histo_args['name'] = '_'.join(path.split('/')[-2:])
        histos['new'].SetTitle(histo_args['title'])
        histos['old'].SetTitle(histo_args['title'])
        histos['new'].SetName(histo_args['name'])
        histos['old'].SetName(histo_args['name'] + '_reference')

        if isinstance(histos['old'], r.TProfile) or isinstance(
                histos['old'], r.TGraph):
            histos['old'] = histos['old'].ProjectionX(histos['old'].GetName())
            histos['new'] = histos['new'].ProjectionX(histos['new'].GetName())

        monSvc.publishHistogram(folder_name, histos['new'], add=False)
        monSvc.publishHistogram(folder_name, histos['old'], add=False)


def getHistos(Configs, files_histo, outfilename, name):
    rootfile = r.TFile.Open(files_histo[name])
    f = r.TFile.Open(outfilename, "RECREATE")
    histo_paths = sum([list(i.items()) for i in Configs.values()], [])
    histos = []
    for i in range(0, len(histo_paths)):
        print("histo_paths[i]")
        print(histo_paths[i][0])
        hist = rootfile.Get(histo_paths[i][0])
        hist.Write()
    f.Write()
    f.Close()


from AlignmentOutputParser.AlignOutput import *
from matplotlib.backends.backend_pdf import PdfPages
from AlignmentMonitoring.MultiPlot import MultiPlot
r.gROOT.SetBatch(True)

if __name__ == '__main__':

    activity = 'FT'

    run = args.run if args.run else OnlineUtils.findLastRun(activity)

    #-->read histograms from the newest .root files
    files_histo = {
        'old': args.histoFiles[0],
        'new': args.histoFiles[1]
    } if args.histoFiles else OnlineUtils.findHistos(activity, run)
    print(files_histo)

    if args.online:
        #################################################################
        # Initialize monitoring Job to send plots to the presenter
        #################################################################
        from Configurables import MonitoringJob
        from Monitoring.MonitoringJob import start
        mj = MonitoringJob()
        # JobName should be the same as the saver
        mj.JobName = "MoniOnlineAlig"
        mj.Sender = True
        mj.Saver = False
        # Start our main job
        gaudi, monSvc = start()
        folder_name = 'MoniOnlineAligTracker'
        #################################################################

    outputFile_name = args.outFile if args.outFile else 'MoniPlots.pdf'

    c = r.TCanvas('c', 'c')
    c.Print(outputFile_name + '[')
    #-->plot the histograms
    monApp = ConfigureMonApp(outputFile_name, Pages)
    for label, f in files_histo.items():
        monApp.addFile(label + ':' + f)
    monApp.draw(c, False)

    if args.online:
        publishHistosCompare(
            Configs=Configs,
            files_histo=files_histo,
            monSvc=monSvc,
            folder_name=folder_name)

    #-->read alignlog.txt file
    c = r.gPad.GetCanvas()
    r.gPad.SetGrid(0, 0)
    alignlog = args.alignlog if args.alignlog else OnlineUtils.findAlignlog(
        activity, run)
    print(alignlog)
    aout = AlignOutput(alignlog)
    aout.Parse()
    mps = []

    taskname = "SciFiAlignmentMonitoring"
    location_online = "/hist/Savesets/ByRun/SciFiAlignmentMonitoring/"
    directory = str(args.run)[:3] + "000"
    parent_dir = location_online + str(args.run)[:2] + "0000"
    path = os.path.join(parent_dir, directory)
    if not os.path.isdir(path):
        os.makedirs(path)
    filename_new = taskname + "-run" + str(args.run) + ".root"

    location_reference = "/hist/Reference/SciFiAlignmentMonitoring/"
    filename_old = "default_1.root"
    filename_reference = "default_" + str(args.run) + ".root"

    histsnew = getHistos(Configs, files_histo,
                         parent_dir + "/" + directory + "/" + filename_new,
                         "new")
    histsold = getHistos(Configs, files_histo,
                         location_reference + filename_old, "old")
    histsreference = getHistos(Configs, files_histo,
                               location_reference + filename_reference, "old")

    f = r.TFile.Open(parent_dir + "/" + directory + "/" + filename_new,
                     "UPDATE")

    for expression, title in [
        ('TrChi2nDof', '(22) Track #chi^{2}/dof'),
        ('NormalisedChi2Change', '(23) #Delta #chi^{2}/dof'),
        ('vertices', '(20) Number of vertices'),
        ('tracks', '(21) Number of tracks'),
        ('hits', '(22) Number of hits'),
    ]:

        if args.makehist:
            hists = RootUtils.makeGraphHisto(
                expression, title, RootUtils.getExpression(aout, expression))
            hists.SetDirectory(0)
            hists.Write()

        gr = RootUtils.makeGraph(RootUtils.getExpression(aout, expression))
        mps.append(
            MultiPlot(
                expression,
                title='{0};Iteration;{0}'.format(title),
                histos={expression: gr},
                kind='g',
                drawLegend=False))
        mps[-1].DrawOption = 'alp'

    # Plots for SciFi stations (not normally aligned, therefore commented)
    c.cd()
    '''
    mp1 = MultiPlot(
        '1',
        title='(24) Convergence FT T1;Iteration;Variation [#mum]',
        kind='g',
        hlines=[0, -100, 100],
        hlines_colors={
            -4: r.kGreen + 3,
            4: r.kGreen + 3
        },
        rangeY=[-250, 250])
    mp1.DrawOption = 'alp'

    mp1.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tz', alignable='FT/T1')), 'Tz')
    mp1.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T1')), 'Tx')

    if args.online:
        monSvc.publishHistogram(
            folder_name,
            RootUtils.makeGraphHisto(
                'FT/T1_Tz', 'Convergence FT/T1 Tz;Iteration;Variation [#mum]',
                RootUtils.getDofDeltaConvergence(
                    aout, dof='Tz', alignable='FT/T1')[0]),
            add=False)
        monSvc.publishHistogram(
            folder_name,
            RootUtils.makeGraphHisto(
                'FT/T1_Tx', 'Convergence FT/T1 Tx;Iteration;Variation [#mum]',
                RootUtils.getDofDeltaConvergence(
                    aout, dof='Tx', alignable='FT/T1')[0]),
            add=False)

    mp2 = MultiPlot(
        '2',
        title='(25) Convergence FT T1 Layer UCSide ;Iteration;Variation [#mum]',
        kind='g',
        hlines=[0, -150, 150],
        rangeY=[-250, 250])
    mp2.DrawOption = 'alp'
    mp2.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tz', alignable='FT/T1LayerUCSide')), 'Tz')
    mp2.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T1LayerUCSide')), 'Tx')

    if args.online:
        monSvc.publishHistogram(
            folder_name,
            RootUtils.makeGraphHisto(
                'FT/T1LayerUCSide_Tz',
                'Convergence FT/T1LayerUCSide Tz;Iteration;Variation [#mum]',
                RootUtils.getDofDeltaConvergence(
                    aout, dof='Tz', alignable='FT/T1LayerUCSide')[0]),
            add=False)
        monSvc.publishHistogram(
            folder_name,
            RootUtils.makeGraphHisto(
                'FT/T1LayerUCSide_Tx',
                'Convergence FT/T1LayerUCSide Tx;Iteration;Variation [#mum]',
                RootUtils.getDofDeltaConvergence(
                    aout, dof='Tx', alignable='FT/T1LayerUCSide')[0]),
            add=False)
    '''
    # c1 = getDrawnCanvas([mp1, mp2, mp3])

    ##To remove convergence plots for stations and layers
    ##mps += [mp1, mp2]

    c1 = RootUtils.getDrawnCanvas(mps)
    c1.Print(outputFile_name)

    # HalfModules
    c.cd()

    #Convergence halfmodules Tx
    #T1 Module 0
    mp3 = MultiPlot(
        '2',
        title='(26) Convergence FT T1 Module 0 Tx;Iteration;Variation [#mum]',
        kind='g',
        hlines=[0, -150, 150],
        rangeY=[-250, 250])
    mp3.DrawOption = 'alp'
    mp3.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T1/X1/HL0/Q0/M0')),
        'FT/T1/X1/HL0/Q0/M0')
    mp3.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T1/X1/HL1/Q1/M0')),
        'FT/T1/X1/HL1/Q1/M0')
    mp3.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T1/X1/HL0/Q2/M0')),
        'FT/T2/X1/HL0/Q2/M0')
    mp3.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T2/X1/HL1/Q3/M0')),
        'FT/T2/X1/HL1/Q3/M0')

    mp4 = MultiPlot(
        '2',
        title='(26) Convergence FT T1 Module 2 Tx;Iteration;Variation [#mum]',
        kind='g',
        hlines=[0, -150, 150],
        rangeY=[-250, 250])
    mp4.DrawOption = 'alp'
    mp4.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T1/X1/HL0/Q0/M2')),
        'FT/T1/X1/HL0/Q0/M2')
    mp4.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T1/X1/HL1/Q1/M2')),
        'FT/T1/X1/HL1/Q1/M2')
    mp4.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T1/X1/HL0/Q2/M2')),
        'FT/T1/X1/HL0/Q2/M2')
    mp4.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T2/X1/HL1/Q3/M2')),
        'FT/T2/X1/HL1/Q3/M2')

    #c1 = RootUtils.getDrawnCanvas([mp3, mp4])
    #c1.Print(outputFile_name)

    #c.cd()

    #T1 Module 4
    mp5 = MultiPlot(
        '2',
        title='(26) Convergence FT T1 Module 4 Tx;Iteration;Variation [#mum]',
        kind='g',
        hlines=[0, -150, 150],
        rangeY=[-250, 250])
    mp5.DrawOption = 'alp'
    mp5.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T1/X1/HL0/Q0/M4')),
        'FT/T1/X1/HL0/Q0/M4')
    mp5.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T1/X1/HL1/Q1/M4')),
        'FT/T1/X1/HL1/Q1/M4')
    mp5.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T1/X1/HL0/Q2/M4')),
        'FT/T1/X1/HL0/Q2/M4')
    mp5.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T1/X1/HL1/Q3/M4')),
        'FT/T1/X1/HL1/Q3/M4')

    #T3 Module 0
    mp6 = MultiPlot(
        '2',
        title='(26) Convergence FT T3 Module 0 Tx;Iteration;Variation [#mum]',
        kind='g',
        hlines=[0, -150, 150],
        rangeY=[-250, 250])
    mp6.DrawOption = 'alp'
    mp6.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T3/X1/HL0/Q0/M0')),
        'FT/T3/X1/HL0/Q0/M0')
    mp6.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T3/X1/HL1/Q1/M0')),
        'FT/T3/X1/HL1/Q1/M0')
    mp6.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T3/X1/HL0/Q2/M0')),
        'FT/T3/X1/HL0/Q2/M0')
    mp6.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T3/X1/HL1/Q3/M0')),
        'FT/T3/X1/HL1/Q3/M0')

    #T3 Module 2
    mp7 = MultiPlot(
        '2',
        title='(26) Convergence FT T3 Module 2 Tx;Iteration;Variation [#mum]',
        kind='g',
        hlines=[0, -150, 150],
        rangeY=[-250, 250])
    mp7.DrawOption = 'alp'
    mp7.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T3/X1/HL0/Q0/M2')),
        'FT/T3/X1/HL0/Q0/M2')
    mp7.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T3/X1/HL1/Q1/M2')),
        'FT/T3/X1/HL1/Q1/M2')
    mp7.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T3/X1/HL0/Q2/M2')),
        'FT/T1/X1/HL0/Q2/M2')
    mp7.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T3/X1/HL1/Q3/M2')),
        'FT/T3/X1/HL1/Q3/M2')

    #T3 Module 4
    mp8 = MultiPlot(
        '2',
        title='(26) Convergence FT T3 Module 4 Tx;Iteration;Variation [#mum]',
        kind='g',
        hlines=[0, -150, 150],
        rangeY=[-250, 250])
    mp8.DrawOption = 'alp'
    mp8.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T3/X1/HL0/Q0/M4')),
        'FT/T3/X1/HL0/Q0/M4')
    mp8.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T3/X1/HL1/Q1/M4')),
        'FT/T3/X1/HL1/Q1/M4')
    mp8.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T3/X1/HL0/Q2/M4')),
        'FT/T3/X1/HL0/Q2/M4')
    mp8.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tx', alignable='FT/T3/X1/HL1/Q3/M4')),
        'FT/T3/X1/HL1/Q3/M4')
    '''
    mp6 = MultiPlot(
        '2',
        title=
        '(26) Convergence FT Module3 and Module 4 Tz;Iteration;Variation [#mum]',
        kind='g',
        hlines=[0, -150, 150],
        rangeY=[-250, 250])
    mp6.DrawOption = 'alp'
    mp6.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tz', alignable='FT/T1LayerX2Quarter(1|3)Module4')),
        'T1LayerX2Quarter(1|3)Mod4')
    mp6.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tz', alignable='FT/T1LayerX2Quarter(0|2)Module4')),
        'T1LayerX2Quarter(0|2)Mod4')
    mp6.Add(
        RootUtils.makeGraph(*RootUtils.getDofDeltaConvergence(
            aout, dof='Tz', alignable='FT/T3LayerX2Quarter(0|2)Module3')),
        'T3LayerX2Quarter(0|2)Mod3')

    if args.online:
        monSvc.publishHistogram(
            folder_name,
            RootUtils.makeGraphHisto(
                'FT/T1LayerX2Quarter(1|3)Module4_Tz',
                'Convergence FT/T1LayerX2Quarter(1|3)Module4 Tz;Iteration;Variation [#mum]',
                RootUtils.getDofDeltaConvergence(
                    aout,
                    dof='Tz',
                    alignable='FT/T1LayerX2Quarter(1|3)Module4')[0]),
            add=False)
        monSvc.publishHistogram(
            folder_name,
            RootUtils.makeGraphHisto(
                'FT/T1LayerX2Quarter(0|2)Module4_Tz',
                'Convergence FT/T1LayerX2Quarter(0|2)Module4 Tz;Iteration;Variation [#mum]',
                RootUtils.getDofDeltaConvergence(
                    aout,
                    dof='Tz',
                    alignable='FT/T1LayerX2Quarter(0|2)Module4')[0]),
            add=False)
        monSvc.publishHistogram(
            folder_name,
            RootUtils.makeGraphHisto(
                'FT/T3LayerX2Quarter(0|2)Module3_Tz',
                'Convergence FT/T3LayerX2Quarter(0|2)Module3 Tz;Iteration;Variation [#mum]',
                RootUtils.getDofDeltaConvergence(
                    aout,
                    dof='Tz',
                    alignable='FT/T3LayerX2Quarter(0|2)Module3')[0]),
            add=False)
    '''
    #c1 = RootUtils.getDrawnCanvas([mp5, mp6])
    c1 = RootUtils.getDrawnCanvas([mp3, mp4, mp5, mp6, mp7, mp8])
    c1.Print(outputFile_name)

    c.Print(outputFile_name + ']')

    ##########################
    #l1.Write("alignlogvalues",r.TObject.kSingleKey)

    if args.makehist:
        print("making the histograms for halfmodules convergence")
        '''
        hist1 = RootUtils.makeGraphHisto(
            'T1_Tz', 'Convergence FT/T1 Tz;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tz', alignable='FT/T1')[0])
        hist2 = RootUtils.makeGraphHisto(
            'T1_Tx', 'Convergence FT/T1 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T1')[0])
        hist3 = RootUtils.makeGraphHisto(
            'T1LayerUCSide_Tz',
            'Convergence FT/T1LayerUCSide Tz;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tz', alignable='FT/T1LayerUCSide')[0])
        hist4 = RootUtils.makeGraphHisto(
            'T1LayerUCSide_Tx',
            'Convergence FT/T1LayerUCSide Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T1LayerUCSide')[0])
        '''
        #T1
        hist5 = RootUtils.makeGraphHisto(
            'FTT1X1HL0Q0M0_Tx',
            'Convergence FT/T1/X1/HL0/Q0/M0 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T1/X1/HL0/Q0/M0')[0])
        hist6 = RootUtils.makeGraphHisto(
            'FTT1X1HL1Q1M0_Tx',
            'Convergence FT/T1/X1/HL1/Q1/M0 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T1/X1/HL1/Q1/M0')[0])
        hist7 = RootUtils.makeGraphHisto(
            'FTT1X1HL0Q2M0_Tx',
            'Convergence FT/T1/X1/HL0/Q2/M0 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T1/X1/HL0/Q2/M0')[0])
        hist8 = RootUtils.makeGraphHisto(
            'FTT1X1HL1Q3M0_Tx',
            'Convergence FT/T1/X1/HL1/Q3/M0 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T1/X1/HL1/Q3/M0')[0])
        hist9 = RootUtils.makeGraphHisto(
            'FTT1X1HL0Q0M2_Tx',
            'Convergence FT/T1/X1/HL0/Q0/M2 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T1/X1/HL0/Q0/M2')[0])
        hist10 = RootUtils.makeGraphHisto(
            'FTT1X1HL1Q1M2_Tx',
            'Convergence FT/T1/X1/HL1/Q1/M2 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T1/X1/HL1/Q1/M2')[0])
        hist11 = RootUtils.makeGraphHisto(
            'FTT1X1HL0Q2M2_Tx',
            'Convergence FT/T1/X1/HL0/Q2/M2 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T1/X1/HL0/Q2/M2')[0])
        hist12 = RootUtils.makeGraphHisto(
            'FTT1X1HL1Q3M2_Tx',
            'Convergence FT/T2/X1/HL1/Q3/M2 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T1/X1/HL1/Q3/M2')[0])
        hist13 = RootUtils.makeGraphHisto(
            'FTT1X1HL0Q0M4_Tx',
            'Convergence FT/T1/X1/HL0/Q0/M4 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T1/X1/HL0/Q0/M4')[0])
        hist14 = RootUtils.makeGraphHisto(
            'FTT1X1HL1Q1M4_Tx',
            'Convergence FT/T1/X1/HL1/Q1/M4 Tz;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T1/X1/HL1/Q1/M4')[0])
        hist15 = RootUtils.makeGraphHisto(
            'FTT1X1HL0Q2M4_Tx',
            'Convergence FT/T1/X1/HL0/Q2/M4 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T1/X1/HL0/Q2/M4')[0])
        hist16 = RootUtils.makeGraphHisto(
            'FTT2X1HL1Q3M4_Tx',
            'Convergence FT/T2/X1/HL1/Q3/M4 Tz;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T2/X1/HL1/Q3/M4')[0])
        #T3
        hist17 = RootUtils.makeGraphHisto(
            'FTT3X1HL0Q0M0_Tx',
            'Convergence FT/T3/X1/HL0/Q0/M0 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T3/X1/HL0/Q0/M0')[0])
        hist18 = RootUtils.makeGraphHisto(
            'FTT3X1HL1Q1M0_Tx',
            'Convergence FT/T3/X1/HL1/Q1/M0 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T3/X1/HL1/Q1/M0')[0])
        hist19 = RootUtils.makeGraphHisto(
            'FTT3X1HL0Q2M0_Tx',
            'Convergence FT/T3/X1/HL0/Q2/M0 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T3/X1/HL0/Q2/M0')[0])
        hist20 = RootUtils.makeGraphHisto(
            'FTT3X1HL1Q3M0_Tx',
            'Convergence FT/T3/X1/HL1/Q3/M0 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T3/X1/HL1/Q3/M0')[0])
        hist21 = RootUtils.makeGraphHisto(
            'FTT3X1HL0Q0M2_Tx',
            'Convergence FT/T3/X1/HL0/Q0/M2 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T3/X1/HL0/Q0/M2')[0])
        hist22 = RootUtils.makeGraphHisto(
            'FTT3X1HL1Q1M2_Tx',
            'Convergence FT/T3/X1/HL1/Q1/M2 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T3/X1/HL1/Q1/M2')[0])
        hist23 = RootUtils.makeGraphHisto(
            'FTT3X1HL0Q2M2_Tx',
            'Convergence FT/T3/X1/HL0/Q2/M2 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T3/X1/HL0/Q2/M2')[0])
        hist24 = RootUtils.makeGraphHisto(
            'FTT3X1HL1Q3M2_Tx',
            'Convergence FT/T3/X1/HL1/Q3/M2 Tz;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T3/X1/HL1/Q3/M2')[0])
        hist25 = RootUtils.makeGraphHisto(
            'FTT3X1HL0Q0M4_Tx',
            'Convergence FT/T3/X1/HL0/Q0/M4 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T3/X1/HL0/Q0/M4')[0])
        hist26 = RootUtils.makeGraphHisto(
            'FTT3X1HL1Q1M4_Tx',
            'Convergence FT/T3/X1/HL1/Q1/M4 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T3/X1/HL1/Q1/M4')[0])
        hist27 = RootUtils.makeGraphHisto(
            'FTT3X1HL0Q2M4_Tx',
            'Convergence FT/T3/X1/HL0/Q2/M4 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T3/X1/HL0/Q2/M4')[0])
        hist28 = RootUtils.makeGraphHisto(
            'FTT3X1HL1Q3M4_Tx',
            'Convergence FT/T3/X1/HL1/Q3/M4 Tx;Iteration;Variation [#mum]',
            RootUtils.getDofDeltaConvergence(
                aout, dof='Tx', alignable='FT/T3/X1/HL1/Q3/M4')[0])

        print("done with histograms for halfmodules convergence")
        f.Write()

    f.Close()
