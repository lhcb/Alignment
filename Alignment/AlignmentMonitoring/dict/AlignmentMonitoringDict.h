/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DICT_ALIGNMENTMONITORINGDICT_H
#define DICT_ALIGNMENTMONITORINGDICT_H 1

// Include files
#include "AlignmentMonitoring/AlignmentMonitoring.h"
#include "AlignmentMonitoring/CompareConstants.h"
#include "AlignmentMonitoring/HistHelper.h"
#include "AlignmentMonitoring/MonitoringApplication.h"
#include "AlignmentMonitoring/MonitoringPage.h"
#include "AlignmentMonitoring/NameITSector.h"
#include "AlignmentMonitoring/NameTTSector.h"
#include "AlignmentMonitoring/ParseConstants.h"
#include "AlignmentMonitoring/Utilities.h"

#endif // DICT_ALIGNMENTMONITORINGDICT_H
