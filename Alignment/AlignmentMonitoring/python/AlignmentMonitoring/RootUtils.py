###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

#!/usr/bin/env python

import os, sys, fnmatch, re, copy, importlib
import ROOT as r


def getExpression(aout, expression):
    '''
    For this to work one must have already done
    aout = AlignOutput(fileName)
    aout.Parse()

    return list values

    expression in [Chi2, DeltaChi2, DeltaChi2nDofs, DeltaChi2overNDofs, LocalDeltaChi2, MaxModeChi2, NormalisedChi2Change, TrChi2nDof, VChi2nDof]
    '''
    print("Num vxs")
    print(getattr(i, expression) for i in aout.AlignIterations)
    return [getattr(i, expression) for i in aout.AlignIterations]


def getDofDeltas(aout, dof='Tx', regexes_alignables=None):
    '''
    For this to work one must have already done
    aout = AlignOutput(fileName)
    aout.Parse()

    return list values of difference after-before for all the alignables which satisfy one of the regexes
    '''
    if regexes_alignables is None:
        regexes_alignables = ['VP/VP(Left|Right)/Module..']
    import re
    #regexes_alignables=['VP/VP(Left|Right)/Module..']
    deltas = []
    for alignable in aout.AlignIterations[0].Alignables:
        match = [
            re.match(regex + '$', alignable) for regex in regexes_alignables
        ]
        if match != [None] * len(match):  # there is at least a match
            print("here 1")
            deltas.append(
                sum([
                    j.Delta for j in sum([
                        i.Alignables[alignable].AlignmentDOFs
                        for i in aout.AlignIterations
                    ], []) if j.Name == dof
                ]))
    if 'T' in dof:
        deltas = [i * 1000 for i in deltas]  # Translations in um
    elif 'R' in dof:
        deltas = [i * 1000000 for i in deltas]  # Rotations in urad
    print("in getdofdeltas")
    print(deltas)
    return deltas


def getDofDeltaConvergence(aout, dof='Tx', alignable='VP/VPLeft'):
    '''
    For this to work one must have already done
    aout = AlignOutput(fileName)
    aout.Parse()

    return list values and list errors
    '''
    #print(aout)
    #alignable='VP/VPLeft'
    #print("in getdof")
    #print(alignable)
    #print(sum([i.Alignables[alignable].AlignmentDOFs for i in aout.AlignIterations]))
    #print(aout.AlignIterations)
    #print("in getdof1")
    #print([i.Alignables[alignable].AlignmentDOFs for i in aout.AlignIterations])
    list_delta = [(j.Delta, j.DeltaErr) for j in sum(
        [i.Alignables[alignable].AlignmentDOFs
         for i in aout.AlignIterations], []) if j.Name == dof]
    if 'T' in dof:
        delta = [(i[0] * 1000, i[1] * 1000)
                 for i in list_delta]  # Translations in um
    elif 'R' in dof:
        delta = [(i[0] * 1000000, i[1] * 1000000)
                 for i in list_delta]  # Rotations in urad
    return [i[0] for i in delta], [i[1] for i in delta]


def getMeanOverlapResidual(filename, directoryname, num_stations):
    '''
    return list values and list errors
    '''
    print("filenames")
    print(filename)
    rootfile = r.TFile.Open(filename)
    mean = []
    meanerr = []

    for i in range(0, num_stations):
        dir = rootfile.GetDirectory(directoryname)
        mean.append(dir.Get(str(i)).GetMean() * 1000)
        meanerr.append(dir.Get(str(i)).GetMeanError() * 1000)

    print("mean 1")
    print(mean)

    print("mean err 1")
    print(meanerr)

    return mean, meanerr


def getHisto(filename, histoname, outfilename):
    '''
    return histogram
    '''
    print("filenames")
    print(filename)
    rootfile = r.TFile.Open(filename)
    f = r.TFile.Open(outfilename, "RECREATE")
    print(histoname)
    histo = rootfile.Get(histoname)
    histo.Write()


def wrapStringArray(string_list):
    sa = r.std.vector(r.std.string)()
    for s in string_list:
        sa.push_back(s)
    return sa


def wrapNumericArray(num_list, data_type):
    na = r.std.vector(data_type)()
    for n in num_list:
        na.push_back(n)
    return na


def getDrawnCanvas(drawables):

    if len(drawables) == 1:
        canvas_divsions = [1]
    elif len(drawables) == 2:
        canvas_divsions = [2]
    elif len(drawables) in [3, 4]:
        canvas_divsions = [2, 2]
    elif len(drawables) in [5, 6]:
        canvas_divsions = [3, 2]
    elif len(drawables) in [7, 8, 9]:
        canvas_divsions = [3, 3]

    c = r.TCanvas()

    c.Divide(*canvas_divsions)

    for i in range(len(drawables)):
        c.cd(i + 1)
        drawables[i].Draw()

    c.Update()
    return c


def makeGraph(values, errors=None):
    import numpy as np
    x = np.array([float(i) for i in range(len(values))])
    ex = np.array([0.0 for i in range(len(x))])
    y = np.array([float(i) for i in values])
    if errors:
        ey = np.array([float(i) for i in errors])
    else:
        ey = np.array([0.0 for i in range(len(x))])
    return r.TGraphErrors(len(x), x, y, ex, ey)


def makeline(valuesx, valuesy, errors=None):
    import numpy as np
    x = np.array([float(i) for i in range(len(valuesx))])
    ex = np.array([0.0 for i in range(len(x))])
    y = np.array([float(i) for i in valuesy])
    if errors:
        ey = np.array([float(i) for i in errors])
    else:
        ey = np.array([0.0 for i in range(len(x))])
    return r.TGraphErrors(len(x), x, y, ex, ey)


def makeHisto(name, values, title=None, range=None, nBins=100):
    if range is None:
        range = [None, None]
    if range[0] == None: range[0] = min(values)
    if range[1] == None: range[1] = max(values) + 1
    if title == None: title = name
    h = r.TH1D(name, title, nBins, *range)
    for i in values:
        h.Fill(i)
    return h


def makeGraphHisto(name, title, values, errors=None):
    '''
    Take values and errors and make TH1D that contains graph
    '''
    h = r.TH1D(name, title, len(values), 0, len(values))
    for i in range(len(values)):
        h.SetBinContent(i + 1, values[i])
        if errors:
            h.SetBinError(i + 1, errors[i])
        else:
            h.SetBinError(i + 1, 0)
    return h.Clone()


def makeLineHisto(name, title, valuesx, valuesy, errors=None):
    '''
    Take values and errors and make TH2D that contains graph
    '''
    h = r.TH2D(name, title, len(valuesx), 0, len(valuesx), len(valuesy), 0,
               len(valuesy))
    for i in range(len(valuesx)):
        for j in range(len(valuesy)):
            h.Fill(valuesx[i], valuesy[i])
            if errors:
                h.SetBinError(i + 1, errors[i], j + 1, errors[j])
                #h.SetBinError(i + 1, errors[i])
            else:
                h.SetBinError(i + 1, 0, j + 1, 0)
                #h.SetBinError(i + 1, 0)
    return h.Clone()
