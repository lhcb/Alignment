#!/bin/python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from GaudiPython import gbl
AlMon = gbl.Alignment.AlignmentMonitoring

#hfile = '/afs/cern.ch/work/m/mamartin/public/Tests/TrackingPlots_2015_Velov5_Trackerv8-5000ev-histos.root'
#hfile = 'monitoring/Align-r156916-Online-VELOv2ITv4TTv4OTv4-20000ev-histos.root'
#outputdir = "test/r156916/ITv4TTv4OTv4"
hfile = 'monitoring/Align-r156916-Online-20000ev-histos.root'
outputdir = "test/r156916/Velov5_Trackerv8"
aMon = AlMon.AlignmentMonitoring(hfile, outputdir)
#aMon = AlMon.AlignmentMonitoring(hfile)
aMon.SetVerbosity(1)
aMon.Run()
#raw_input("press Enter to continue")
