#!/bin/python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from GaudiPython import gbl
AlMon = gbl.Alignment.AlignmentMonitoring

alDir = "/afs/cern.ch/user/m/mamartin/public/tests/alignment/"
versions = {
    'ITG': {
        'set': 'IT/ITGlobal',
        'ver': 'v10'
    },
    'ITM': {
        'set': 'IT/ITModules',
        'ver': 'v7'
    },
    'TTG': {
        'set': 'TT/TTGlobal',
        'ver': 'v9'
    },
    'TTM': {
        'set': 'TT/TTModules',
        'ver': 'v9'
    },
    'OTG': {
        'set': 'OT/OTGlobal',
        'ver': 'v9'
    },
    'OTM': {
        'set': 'OT/OTModules',
        'ver': 'v9'
    }
}
cc = {}
for key, args in versions.items():
    cc[key] = AlMon.CompareConstants(args['set'], args['ver'], alDir)
    cc[key].Compare('v14')
    cc[key].PrintWarnings(2)  #1: OK, 2: WARNING, 3:SEVERE
    cc[key].SaveHists('output/hists_' + key + '.root')
