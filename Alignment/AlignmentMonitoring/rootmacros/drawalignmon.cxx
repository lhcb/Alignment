/*****************************************************************************\
* (c) Copyright 2000-2014 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <TString.h>
#include <vector>

TString TrackMonitor          = "AlignMoni_TrackMonitor/";
TString TrackVertexMonitor    = "AlignMoni_VertexMonitor/";
TString TrackPV2HalfMonitor   = "AlignMoni_TrackPV2HalfMonitor/";
TString TrackVPOverlapMonitor = "AlignMoni_TrackVPOverlapMonitor/";
TString D0Monitor             = "D0Monitor/";
TString JpsiMonitor           = "JpsiMonitor/";
TString UpsilonMonitor        = "UpsilonMonitor/";
TString Z0Monitor             = "Z0Monitor/";
TString TrackFitMatchMonitor  = "AlignMoni_TrackFitMatchMonitor/";

void addParticlePages( MonitoringApplication& monapp, const TString& particle, const TString& monitor,
                       bool addByQuadrant = false ) {
  monapp.addPage( new MonitoringPage( particle + " properties", monitor + "mass", monitor + "masspull",
                                      monitor + "chi2", monitor + "eta", monitor + "phimatt", monitor + "asym" ) );

  monapp.addPage(
      new MonitoringPage( particle + " daughters", monitor + "trackChi2", monitor + "trackPt", monitor + "trackP" ) );

  monapp.addPage( new MonitoringPage( particle + " mass vs kinematics", monitor + "massVersusMomDif",
                                      monitor + "massVersusEtaDif", monitor + "massVersusOpenAngle",
                                      monitor + "massVersusPhi", monitor + "massVersusPhiMatt",
                                      monitor + "massVersusMom" ) );
  if ( addByQuadrant )
    monapp.addPage( new MonitoringPage( particle + " mass by quadrant", monitor + "massQ4", monitor + "massQ1",
                                        monitor + "massQ3", monitor + "massQ2" ) );
}

MonitoringApplication* configureMonApp() {
  MonitoringApplication* monapp = new MonitoringApplication();

  monapp->addPage( new MonitoringPage( "tracks", TrackMonitor + "nTracks", TrackMonitor + "trackType",
                                       TrackMonitor + "/TrackMultiplicityFine", TrackMonitor + "history" ) );

  monapp->addPage( new MonitoringPage( "Velo track properties", TrackMonitor + "Velo/nVPHits",
                                       TrackMonitor + "Velo/phi", TrackMonitor + "Velo/eta",
                                       TrackMonitor + "Velo/chi2_per_ndof"
                                       //				      TrackMonitor+"Velo/HitVeloLayers"
                                       ) );
  monapp->addPage( new MonitoringPage( "Velo backward track properties", TrackMonitor + "VeloBackward/nVPHits",
                                       TrackMonitor + "VeloBackward/phi", TrackMonitor + "VeloBackward/eta",
                                       TrackMonitor + "VeloBackward/chi2_per_ndof" ) );
  //				      TrackMonitor+"VeloBackward/HitVeloLayers") ) ;

  monapp->addPage( new MonitoringPage( "residuals on velo tracks (top:Forward;bottom:Backward)",
                                       TrackMonitor + "Velo/VPXResidual", TrackMonitor + "Velo/VPYResidual",
                                       TrackMonitor + "VeloBackward/VPXResidual",
                                       TrackMonitor + "VeloBackward/VPYResidual" ) );

  monapp->addPage( new MonitoringPage( "residual pull on velo tracks (top:Forward;bottom:Backward)",
                                       TrackMonitor + "Velo/VPXresidualPull", TrackMonitor + "Velo/VPYresidualPull",
                                       TrackMonitor + "VeloBackward/VPXresidualPull",
                                       TrackMonitor + "VeloBackward/VPYresidualPull" ) );

  monapp->addPage( new MonitoringPage( "residuals on long tracks", TrackMonitor + "Long/VPXResidual",
                                       TrackMonitor + "Long/VPYResidual", TrackMonitor + "Long/FTResidual",
                                       TrackMonitor + "Long/UTResidual" ) );

  monapp->addPage( new MonitoringPage( "residual pull on long tracks", TrackMonitor + "Long/VPXresidualPull",
                                       TrackMonitor + "Long/VPYresidualPull", TrackMonitor + "Long/FTresidualPull",
                                       TrackMonitor + "Long/UTresidualPull" ) );

  monapp->addPage( new MonitoringPage( "hit layers on long tracks", TrackMonitor + "Long/HitVeloALayers",
                                       TrackMonitor + "Long/HitVeloCLayers", TrackMonitor + "Long/HitUTLayers",
                                       TrackMonitor + "Long/HitFTLayers" ) );

  monapp->addPage( new MonitoringPage( "long track chisquares", TrackMonitor + "Long/chi2_per_ndof",
                                       TrackMonitor + "Long/chi2PerDofMatch",
                                       TrackMonitor + "Long/chi2PerDofDownstream", TrackMonitor + "Long/chi2PerDofVelo",
                                       TrackMonitor + "Long/noutliers", TrackMonitor + "Long/numiter" ) );

  monapp->addPage( new MonitoringPage(
      "long track chisqprob versus phi", TrackMonitor + "Long/chi2ProbVsPhi", TrackMonitor + "Long/chi2ProbVeloVsPhi",
      TrackMonitor + "Long/chi2ProbMatchVsPhi", TrackMonitor + "Long/chi2ProbDownstreamVsPhi" ) );

  monapp->addPage( new MonitoringPage( "long track chisqprob versus momentum", TrackMonitor + "Long/chi2ProbVsMom",
                                       TrackMonitor + "Long/chi2ProbVeloVsMom",
                                       TrackMonitor + "Long/chi2ProbMatchVsMom",
                                       TrackMonitor + "Long/chi2ProbDownstreamVsMom" ) );

  monapp->addPage( new MonitoringPage( "PV", TrackVertexMonitor + "NumPrimaryVertices",
                                       TrackVertexMonitor + "NumTracksPerPV", TrackVertexMonitor + "NumBackTracksPerPV",
                                       TrackVertexMonitor + "PV x position", TrackVertexMonitor + "PV y position",
                                       TrackVertexMonitor + "PV z position" ) );

  monapp->addPage( new MonitoringPage(
      "PV chi-squares", TrackVertexMonitor + "NumLongTracksPerPV", TrackVertexMonitor + "PV chisquare per dof",
      TrackVertexMonitor + "PV forward chisquare per dof", TrackVertexMonitor + "PV backward chisquare per dof",
      TrackVertexMonitor + "PV long chisquare per dof" ) );

  monapp->addPage(
      new MonitoringPage( "left-right PV position from TrackPV2HalfMonitor", TrackPV2HalfMonitor + "Left PV x position",
                          TrackPV2HalfMonitor + "Left PV y position", TrackPV2HalfMonitor + "Right PV x position",
                          TrackPV2HalfMonitor + "Right PV y position" ) );

  monapp->addPage( new MonitoringPage(
      "left-right PV delta from TrackPV2HalfMonitor", TrackPV2HalfMonitor + "Left-Right PV delta x",
      TrackPV2HalfMonitor + "Left-Right PV delta y", TrackPV2HalfMonitor + "Left-Right PV delta z",
      TrackPV2HalfMonitor + "PV left-right delta x versus z", TrackPV2HalfMonitor + "PV left-right delta y versus z",
      TrackPV2HalfMonitor + "Right PV Num of track" ) );

  monapp->addPage( new MonitoringPage(
      "Delta left-right from TrackVertexMonitor", TrackVertexMonitor + "PV left-right delta x",
      TrackVertexMonitor + "PV left-right delta y", TrackVertexMonitor + "PV left-right delta z",
      TrackVertexMonitor + "PV left-right delta x versus z", TrackVertexMonitor + "PV left-right delta y versus z",
      TrackVertexMonitor + "PV left-right delta z versus z" ) );

  monapp->addPage( new MonitoringPage(
      "Delta forward-backward from TrackVertexMonitor", TrackVertexMonitor + "PV forward-backward delta x",
      TrackVertexMonitor + "PV forward-backward delta y", TrackVertexMonitor + "PV forward-backward delta z",
      TrackVertexMonitor + "PV forward-backward delta x versus z",
      TrackVertexMonitor + "PV forward-backward delta y versus z",
      TrackVertexMonitor + "PV forward-backward delta z versus z" ) );

  monapp->addPage( new MonitoringPage(
      "Delta beamline from TrackVertexMonitor", TrackVertexMonitor + "PV-beamline delta x",
      TrackVertexMonitor + "PV-beamline delta y", TrackVertexMonitor + "PV-beamline delta x versus z",
      TrackVertexMonitor + "PV-beamline delta y versus z" ) );

  addParticlePages( *monapp, "D0", D0Monitor );
  addParticlePages( *monapp, "Jpsi", JpsiMonitor );
  addParticlePages( *monapp, "Upsilon", UpsilonMonitor );
  addParticlePages( *monapp, "Z0", Z0Monitor );

  monapp->addPage( new MonitoringPage(
      "Velo-T match", TrackFitMatchMonitor + "Velo-T/dx vs qop", TrackFitMatchMonitor + "Velo-T/dx vs tx",
      TrackFitMatchMonitor + "Velo-T/dx vs ty", TrackFitMatchMonitor + "Velo-T/dx pull vs qop",
      TrackFitMatchMonitor + "Velo-T/dx pull vs tx", TrackFitMatchMonitor + "Velo-T/dx pull vs ty" ) );

  monapp->addPage( new MonitoringPage( "Velo-T match: curvature", TrackFitMatchMonitor + "curvatureRatioTToLong",
                                       TrackFitMatchMonitor + "curvatureRatioVeloUTToLong",
                                       TrackFitMatchMonitor + "curvatureRatioTToLongVsQoP",
                                       TrackFitMatchMonitor + "curvatureRatioVeloUTToLongVsQoP" ) );

  monapp->addPage( new MonitoringPage(
      "Velo-T match: curvature (II)", TrackFitMatchMonitor + "curvatureRatioTToLongVsTxPos",
      TrackFitMatchMonitor + "curvatureRatioVeloUTToLongVsTxPos", TrackFitMatchMonitor + "curvatureRatioTToLongVsTxNeg",
      TrackFitMatchMonitor + "curvatureRatioVeloUTToLongVsTxNeg" ) );
  /*
  monapp->addPage(new MonitoringPage("Velo-T match: position of kick",
                                     TrackFitMatchMonitor+"/kickZ",
                                     TrackFitMatchMonitor+"/kickZVsX",
                                     TrackFitMatchMonitor+"/kickDeltay")) ;

   monapp->addPage(new MonitoringPage("T-TT match",
                                     TrackFitMatchMonitor+"/T-TT/dx pull vs tx",
                                     TrackFitMatchMonitor+"/T-TT/dtx pull vs tx",
                                     TrackFitMatchMonitor+"/T-TT/dy pull vs tx",
                                     TrackFitMatchMonitor+"/T-TT/dx pull vs ty",
                                     TrackFitMatchMonitor+"/T-TT/dtx pull vs ty",
                                     TrackFitMatchMonitor+"/T-TT/dy pull vs ty") ) ;

    monapp->addPage(new MonitoringPage("T-TT match (II)",
                                       TrackFitMatchMonitor+"/T-TT/dx pull",
                                       TrackFitMatchMonitor+"/T-TT/dy pull",
                                       TrackFitMatchMonitor+"/T-TT/dtx pull",
                                       TrackFitMatchMonitor+"/T-TT/dty pull") ) ;
  */

  monapp->addPage( new MonitoringPage( "A/C side overlap histograms (all stations)",
                                       TrackVPOverlapMonitor + "x A-C overlap residual",
                                       TrackVPOverlapMonitor + "y A-C overlap residual" ) );

  monapp->addPage( new MonitoringPage(
      "Tile overlap histograms (all modules)", TrackVPOverlapMonitor + "x overlap residual CLI-NLO",
      TrackVPOverlapMonitor + "x overlap residual CLI-NSI", TrackVPOverlapMonitor + "x overlap residual CSO-NSI",
      TrackVPOverlapMonitor + "y overlap residual CLI-NLO", TrackVPOverlapMonitor + "y overlap residual CLI-NSI",
      TrackVPOverlapMonitor + "y overlap residual CSO-NSI" ) );

  monapp->addPage( new MonitoringPage(
      "Velo A-C tracks", TrackVPOverlapMonitor + "breakpoint delta-x", TrackVPOverlapMonitor + "breakpoint delta-tx",
      TrackVPOverlapMonitor + "breakpoint delta-x vs z", TrackVPOverlapMonitor + "breakpoint delta-y",
      TrackVPOverlapMonitor + "breakpoint delta-ty", TrackVPOverlapMonitor + "breakpoint delta-y vs z" ) );

  return monapp;
}

void drawall( const std::vector<TString>& thefiles, bool normalize = false ) {
  MonitoringApplication* monapp = configureMonApp();
  for ( size_t i = 0; i < thefiles.size(); ++i ) monapp->addFile( thefiles[i] );
  monapp->draw( "plots", normalize );
}

void drawall( TString file1, TString file2 = "", TString file3 = "", TString file4 = "", TString file5 = "",
              TString file6 = "", TString file7 = "", TString file8 = "", bool normalize = false ) {
  MonitoringApplication* monapp = configureMonApp();
  monapp->addFile( file1 );
  if ( file2 != TString( "" ) ) monapp->addFile( file2 );
  if ( file3 != TString( "" ) ) monapp->addFile( file3 );
  if ( file4 != TString( "" ) ) monapp->addFile( file4 );
  if ( file5 != TString( "" ) ) monapp->addFile( file5 );
  if ( file6 != TString( "" ) ) monapp->addFile( file6 );
  if ( file7 != TString( "" ) ) monapp->addFile( file7 );
  if ( file8 != TString( "" ) ) monapp->addFile( file8 );
  monapp->draw( "plots", normalize );
}

void drawalignmon( const char* filename ) { drawall( filename ); }

void drawpage( TString pagename, TString file1, TString file2 = "", TString file3 = "", TString file4 = "",
               bool normalize = false ) {
  MonitoringApplication* monapp = configureMonApp();
  monapp->addFile( file1 );
  if ( file2 != TString( "" ) ) monapp->addFile( file2 );
  if ( file3 != TString( "" ) ) monapp->addFile( file3 );
  if ( file4 != TString( "" ) ) monapp->addFile( file4 );
  monapp->drawPage( pagename, normalize );
}

void drawall( int lastiter, int firstiter = 0, bool normalize = false ) {
  MonitoringApplication* monapp = configureMonApp();
  char                   tmp[256];
  sprintf( tmp, "Iter%d:Iter%d/histograms.root", lastiter, lastiter );
  monapp->addFile( TString( tmp ) );
  if ( lastiter != firstiter ) {
    sprintf( tmp, "Iter%d:Iter%d/histograms.root", firstiter, firstiter );
    monapp->addFile( TString( tmp ) );
  }
  // drawall( files );
  monapp->draw( "plots", normalize );
}

void drawalliters( std::vector<int> iterations, bool normalize = false ) {
  MonitoringApplication* monapp = configureMonApp();
  char                   tmp[256];
  for ( const auto& it : iterations ) {
    // sprintf(tmp,"Iter%d:Iter%d/histograms.root",it,it) ;
    sprintf( tmp, "Iter%d:Iter%d/alignmonitoringhist_new.root", it, it );
    monapp->addFile( TString( tmp ) );
  }
  monapp->draw( "plots", normalize );
}

void drawhisto( TString histo, TString file1, TString file2 = "", TString file3 = "", TString file4 = "",
                bool normalize = false ) {
  MonitoringApplication* monapp = new MonitoringApplication();
  monapp->addFile( file1 );
  // if(file2 != TString("") )
  monapp->addFile( file2 );
  // if(file3 != TString("") )
  monapp->addFile( file3 );
  // if(file4 != TString("") )
  monapp->addFile( file4 );
  monapp->addPage( new MonitoringPage( histo, histo ) );
  monapp->drawPage( histo, normalize );
}

void drawtileoverlapresiduals() {
  gStyle->SetHistLineWidth( 2 );
  TString histonames[] = {
      TrackVPOverlapMonitor + "x overlap residual CLI-NLO", TrackVPOverlapMonitor + "x overlap residual CLI-NSI",
      TrackVPOverlapMonitor + "x overlap residual CSO-NSI", TrackVPOverlapMonitor + "y overlap residual CLI-NLO",
      TrackVPOverlapMonitor + "y overlap residual CLI-NSI", TrackVPOverlapMonitor + "y overlap residual CSO-NSI"};
  TString filenames[] = {"Iter6/alignmonitoringhist_new.root", "Iter16/alignmonitoringhist_new.root"};
  auto    c1          = new TCanvas( "c2", "", 900, 600 );
  c1->Divide( 3, 2 );

  int ipad = 0;
  for ( const auto& hisname : histonames ) {
    c1->cd( ++ipad );
    // char hisname[256] ;
    auto h2xa = getH2( filenames[0], hisname );
    auto h2xb = getH2( filenames[1], hisname );
    auto h1a  = h2xa->ProjectionY( "tmpa" );
    auto h1b  = h2xb->ProjectionY( "tmpb" );
    // sprintf(hisname,"%s A-C overlap residual",s) ;
    // h1a->SetTitle(hisname) ;
    h1b->SetTitle( h2xa->GetTitle() );
    h1b->SetLineColor( 2 );
    h1b->DrawCopy();
    h1a->DrawCopy( "sames" );
  }
  c1->Print( "tileoverlap.pdf" );
}
