/*****************************************************************************\
* (c) Copyright 2000-2014 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TF1.h"
#include "TH1.h"
#include "TH2.h"
#include "TList.h"
#include "TString.h"
#include "TTree.h"

#include "TCanvas.h"
#include "TFile.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TPaveStats.h"
#include "TPostScript.h"
#include "TProfile.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TSystem.h"

#include "drawutils.h"

#include <cmath>
#include <iostream>

TString fixName( const TString& input ) {
  TString name = input;
  name.ReplaceAll( "+", "_" );
  name.ReplaceAll( "/", "_" );
  name.ReplaceAll( "(", "_" );
  name.ReplaceAll( ")", "_" );
  name.ReplaceAll( " ", "_" );
  name.ReplaceAll( "(", "" );
  name.ReplaceAll( ")", "" );
  name.ReplaceAll( "\\", "" );
  name.ReplaceAll( "/", "" );
  name.ReplaceAll( "+", "" );
  name.ReplaceAll( "-", "" );
  name.ReplaceAll( "*", "" );
  return name;
}

TCanvas* getCanvas( const TString& name, const TString& title, int w = 700, int h = 500 ) {
  TCanvas* rc = dynamic_cast<TCanvas*>( gROOT->GetListOfCanvases()->FindObject( name ) );
  if ( rc == 0 ) rc = new TCanvas( name, title, w, h );
  rc->SetTitle( title );
  rc->Clear();
  return rc;
}

void createPage( TCanvas* canvas, const char* title, int nx, int ny ) {
  double titlepady = 0.95;
  canvas->Clear();
  canvas->cd();
  char padname[1024];
  sprintf( padname, "%s_titlepad", canvas->GetName() );
  TPad* titlepad = new TPad( padname, padname, 0, titlepady, 1, 1 );
  titlepad->Draw();
  titlepad->cd();
  TText* titletext = new TText( 0.5, 0.5, title );
  titletext->SetNDC();
  titletext->SetTextSize( 0.8 );
  titletext->SetTextAlign( 22 );
  titletext->Draw();
  canvas->cd();

  // double ymargin(0),xmargin(0) ;
  double dy = 1 / Double_t( ny );
  double dx = 1 / Double_t( nx );
  int    n( 0 );
  for ( int iy = 0; iy < ny; ++iy ) {
    double y2 = titlepady * ( 1 - iy * dy );
    double y1 = titlepady * ( 1 - iy * dy - dy );
    for ( int ix = 0; ix < nx; ++ix ) {
      double x1 = ix * dx;
      double x2 = x1 + dx;
      ++n;
      sprintf( padname, "%s_%d", canvas->GetName(), n );
      TPad* pad = new TPad( padname, padname, x1, y1, x2, y2 );
      pad->SetNumber( n );
      pad->SetGridx();
      pad->SetGridy();
      pad->Draw();
    }
  }
  // for(int i=0; i<nx * ny; ++i) {
  // canvas->cd(i+1) ;
  //  gPad->DrawFrame(0,0,1,1) ;
  //}
}

TCanvas* createPage( const char* name, const char* title, int nx, int ny, int w, int h ) {
  TCanvas* canvas = getCanvas( name, title, w, h );
  createPage( canvas, title, nx, ny );
  return canvas;
}

TFile* getFile( const TString& filename ) {
  TFile* file( 0 );
  file = dynamic_cast<TFile*>( gROOT->GetListOfFiles()->FindObject( filename ) );
  if ( !file ) file = TFile::Open( filename );
  return file;
}

TObject* getObject( const TString& filename, const TString& directory, const TString& hisname ) {
  TDirectory* curdir = gDirectory;
  TObject*    h1( 0 );
  TFile*      file{0};
  if ( filename.Length() > 0 ) {
    file = getFile( filename );
  } else {
    file = gDirectory->GetFile();
  }
  if ( file && file->IsOpen() ) {
    if ( directory.Length() > 0 )
      file->cd( directory );
    else
      file->cd();
    h1 = gDirectory->Get( hisname );
  } else {
    std::cout << "Cannot find file: " << filename << std::endl;
  }
  if ( h1 == 0 )
    std::cout << "Cannot find "
              << "object \'" << hisname << "\' "
              << "in dir \'" << directory << "\' "
              << "in file " << filename << "\' " << std::endl;
  if ( curdir ) curdir->cd();
  return h1;
}

TObject* getObject( const TString& filename, const TString& name ) {
  int     ipos    = name.Last( '/' );
  TString dirname = ipos >= 0 ? TString( name.Data(), ipos ) : "";
  TString hisname = ipos >= 0 ? TString( name.Data() + ipos + 1 ) : name;
  return getObject( filename, dirname, hisname );
}

struct ObjectPath {
  TString dirname;
  TString hisname;
  TString filename;
  void    extract( const TString& path ) {
    int ipos = path.Last( '/' );
    dirname  = ipos >= 0 ? TString( path.Data(), ipos ) : "";
    hisname  = ipos >= 0 ? TString( path.Data() + ipos + 1 ) : path;
    filename = "";
    ipos     = dirname.Index( ".root" );
    if ( ipos >= 0 ) {
      filename = TString( dirname.Data(), ipos + 5 );
      dirname  = TString( dirname.Data() + ipos + 6 );
    }
  }
  ObjectPath( const TString& path ) { extract( path ); }
  ObjectPath( const TString& _filename, const TString& _dirname, const TString& _hisname )
      : filename{_filename}, dirname{_dirname}, hisname{_hisname} {}
};

TObject* getObject( const TString& name ) {
  ObjectPath path{name};
  return getObject( path.filename, path.dirname, path.hisname );
}

TH1* getH1( const TString& filename, const TString& hisname ) {
  TObject* obj = getObject( filename, hisname );
  TH1*     h1  = dynamic_cast<TH1*>( obj );
  if ( h1 ) { h1->UseCurrentStyle(); }
  return h1;
}

TH1* getH1( const TString& hisname ) {
  TObject* obj = getObject( hisname );
  TH1*     h1  = dynamic_cast<TH1*>( obj );
  if ( h1 ) { h1->UseCurrentStyle(); }
  return h1;
}

TH1* getH1Special( const TString& filename, const TString& hisname ) {
  int  index = hisname.Index( "[-]" );
  TH1* rc{0};
  if ( index >= 0 ) {
    // stupid pattern matching
    TString hisname1{hisname.Data(), index};
    TString hisname2{hisname.Data() + index + 3};
    TH1*    h1_1 = getH1( filename, hisname1 );
    TH1*    h1_2 = getH1( filename, hisname2 );
    TH1*    h1   = dynamic_cast<TH1*>( h1_1->Clone( hisname1 + "Diff" ) );
    h1->SetTitle( TString{h1_1->GetTitle()} + " minus " + h1_2->GetTitle() );
    h1->Add( h1_2, -1.0 );
    rc = h1;
  } else {
    rc = getH1( filename, hisname );
  }
  return rc;
}

TH2* getH2( const TString& filename, const TString& hisname ) {
  TH2* h2 = dynamic_cast<TH2*>( getObject( filename, hisname ) );
  if ( h2 ) { h2->UseCurrentStyle(); }
  return h2;
}

TProfile* getProfile1D( const TString& filename, const TString& hisname ) {
  TProfile* h2 = dynamic_cast<TProfile*>( getObject( filename, hisname ) );
  if ( h2 ) { h2->UseCurrentStyle(); }
  return h2;
}

bool is1D( TH1* h1 ) { return h1 && h1->GetNbinsY() == 1 && dynamic_cast<TProfile*>( h1 ) == 0; }

bool isProfile1D( TH1* h1 ) { return h1 && h1->GetNbinsY() == 1 && dynamic_cast<TProfile*>( h1 ) != 0; }

TH2* is2D( TH1* h1 ) { return dynamic_cast<TH2*>( h1 ); }

double max1D( const TH1& h1 ) {
  int    firstbin = h1.GetXaxis()->GetFirst();
  int    lastbin  = h1.GetXaxis()->GetLast();
  double rc       = -1e9;
  // for a profile, only consider bins with at least 0.1% of the entries
  const TProfile* pr = dynamic_cast<const TProfile*>( &h1 );
  const int       N  = h1.GetEntries();
  for ( int ibin = firstbin; ibin <= lastbin; ++ibin )
    if ( !pr || pr->GetBinEntries( ibin ) > 0.001 * N )
      rc = std::max( rc, h1.GetBinContent( ibin ) ); //+ h1.GetBinError(ibin) ) ;
  return rc;
}

double min1D( const TH1& h1 ) {
  int             firstbin = h1.GetXaxis()->GetFirst();
  int             lastbin  = h1.GetXaxis()->GetLast();
  double          rc       = 1e9;
  const TProfile* pr       = dynamic_cast<const TProfile*>( &h1 );
  for ( int ibin = firstbin; ibin <= lastbin; ++ibin )
    if ( !pr || pr->GetBinEntries( ibin ) > 0 )
      rc = std::min( rc, h1.GetBinContent( ibin ) ); //-h1.GetBinError(ibin) ) ;
  return rc;
}

TPaveStats* updatePaveStats( const TH1& h1, int ny = 0 ) {
  TPaveStats* st = (TPaveStats*)( h1.FindObject( "stats" ) );
  if ( st ) {
    st->SetLineColor( h1.GetLineColor() );
    st->SetTextColor( h1.GetLineColor() );
    if ( ny != 0 ) {
      double y1 = st->GetY1NDC();
      double y2 = st->GetY2NDC();
      double dy = ny * ( y2 - y1 );
      st->SetY1NDC( y1 - dy );
      st->SetY2NDC( y2 - dy );
    }
  }
  return st;
}

int colorFromIndex( int index ) {
  int colors[] = {1, 2, kBlue + 1, kGreen + 3, 51, kOrange + 2, 6, 7, 8, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41};
  return colors[index];
}

void setStyleFromIndex( TH1& h1, int styleindex ) {
  const int color = colorFromIndex( styleindex );
  h1.SetLineColor( color );
  h1.SetMarkerColor( color );
  h1.SetMarkerStyle( 20 );
  // h1->SetMarkerSize(0.5) ;
  if ( h1.GetListOfFunctions() ) {
    TIter    iter( h1.GetListOfFunctions() );
    TObject* obj;
    while ( ( obj = iter.Next() ) ) {
      TF1* f1 = dynamic_cast<TF1*>( obj );
      if ( f1 ) f1->SetLineColor( color );
    }
  }

  if ( is1D( &h1 ) ) {
    int fillstyles[] = {3004, 3005, 3006, 3007, 3305, 3306};
    h1.SetFillStyle( styleindex <= 4 ? fillstyles[styleindex] : 0 );
    h1.SetFillColor( color );
  }
}

TH1* drawH2( const TString& filename, const TString& hisname, const char* drawopt = "", int color = 1 ) {
  TH1* h1 = getH1( filename, hisname );
  TH2* h2 = dynamic_cast<TH2*>( h1 );
  TH1* rc = 0;
  if ( h2 ) {
    TH1*    tmp = h2;
    TString stringopt( drawopt );
    TH1*    owned( 0 );
    char    tmpname[1024];
    sprintf( tmpname, "%s-%s_tmp", filename.Data(), hisname.Data() );
    if ( stringopt.Contains( "ProfX" ) ) {
      owned = tmp = h2->ProfileX( tmpname );
      stringopt.ReplaceAll( "ProfX", "" );
    } else if ( stringopt.Contains( "ProjX" ) ) {
      owned = tmp = h2->ProjectionX( tmpname );
      stringopt.ReplaceAll( "ProjX", "" );
    } else if ( stringopt.Contains( "ProjY" ) ) {
      TObjArray*  subStrL  = TPRegexp( ".*Proj.\[\\d+,\\d+].*" ).MatchS( stringopt );
      const Int_t nrSubStr = subStrL->GetLast() + 1;
      int         firstbin{0}, lastbin{h2->GetNbinsX()};
      if ( nrSubStr == 2 ) {
        firstbin = ( (TObjString*)subStrL->At( 0 ) )->GetString().Atoi();
        lastbin  = ( (TObjString*)subStrL->At( 1 ) )->GetString().Atoi();
      }
      std::cout << "Tried to match pattern: " << firstbin << " " << lastbin << std::endl;
      owned = tmp = h2->ProjectionY( tmpname, firstbin, lastbin );
      stringopt.ReplaceAll( "ProjY", "" );
    }
    tmp->SetTitle( h2->GetTitle() );
    tmp->UseCurrentStyle();
    tmp->SetLineColor( color );
    tmp->SetMarkerColor( color );
    rc = tmp->DrawCopy( stringopt );
    delete owned;
  }
  return rc;
}

TH1* drawH1( TH1* h1, const char* drawopt, int styleindex, bool normalize ) {
  TString drawoptstring( drawopt );
  // poor person's parsing
  if ( drawoptstring.Contains( "range" ) ) {
    int   index = drawoptstring.Index( TString( "range" ) );
    float xmin( h1->GetXaxis()->GetXmin() ), xmax( h1->GetXaxis()->GetXmax() );
    sscanf( drawoptstring.Data() + index, "range[%f,%f]", &xmin, &xmax );
    h1->GetXaxis()->SetRangeUser( xmin, xmax );
    drawoptstring.Remove( index, drawoptstring.Index( "]" ) + 1 - index );
  }

  if ( normalize && is1D( h1 ) ) {
    double integral = h1->Integral();
    if ( integral > 0 && fabs( integral - 1 ) > 1e-6 ) {
      if ( h1->GetSumw2() == 0 ) h1->Sumw2();
      h1->Scale( 1 / integral );
    }
  }
  if ( is2D( h1 ) ) {
    h1->GetDirectory()->cd();
    if ( drawoptstring.Contains( "ProjY" ) ) {
      auto h2    = dynamic_cast<TH2*>( h1 );
      int  index = drawoptstring.Index( TString( "ProjY" ) );
      int  firstbin{0}, lastbin{h2->GetNbinsX()};
      sscanf( drawoptstring.Data() + index, "ProjY[%d,%d]", &firstbin, &lastbin );
      firstbin = std::max( 0, firstbin );
      lastbin  = std::min( h2->GetNbinsX(), lastbin );
      char hisname[1024];
      sprintf( hisname, "%s_%s_%d_%d", h2->GetName(), h2->GetDirectory()->GetFile()->GetName(), firstbin, lastbin );
      std::cout << "Hisname: " << hisname << std::endl;
      auto htmp = h2->ProjectionY( fixName( TString( hisname ) ), firstbin, lastbin );
      htmp->SetDirectory( gROOT );
      h1 = htmp->DrawCopy( "hist" );
      if ( firstbin == lastbin ) {
        char title[256];
        sprintf( title, "Bin %d", firstbin );
        h1->SetTitle( title );
      }
    } else {
      // create a temporary name
      TString name = TString( h1->GetDirectory()->GetFile()->GetName() ) + "_" + h1->GetName() + "_pr";
      TH1*    htmp = is2D( h1 )->ProfileX( name );
      h1           = htmp->DrawCopy( drawoptstring );
      delete htmp;
    }
  } else {
    h1 = h1->DrawCopy( drawoptstring );
  }
  setStyleFromIndex( *h1, styleindex );
  return h1;
}

TH1* drawH1( const TString& filename, const TString& hisname, const char* drawopt = "", int styleindex = 0,
             bool normalize = false ) {
  // TString specialopt ;
  // TString realhisname = hisname ;
  // if( hisname.Contains(":" ) ) {
  //   std::cout << "Treating special options in histo name: " << hisname << std::endl ;
  //   int index = hisname.Index(":") ;
  //   specialopt = hisname ;
  //   specialopt.Remove(index+1,0) ;
  //   realhisname.Remove(hisname.Length()-index,index) ;
  //   std::cout << "hisname: \"" << hisname << "\"" << std::endl ;
  //   std::cout << "specialopt: \"" << specialopt<< "\"" << std::endl ;
  // }
  TH1* h1 = getH1Special( filename, hisname );
  if ( h1 ) {
    h1 = drawH1( h1, drawopt, styleindex, normalize );
  } else {
    std::cout << "cannot find histo: " << hisname << std::endl;
  }
  return h1;
}

TH1* drawH1( const TString& hisname, const char* drawopt = "", int styleindex = 1, bool normalize = false ) {
  ObjectPath path{hisname};
  return drawH1( path.filename, path.dirname + "/" + path.hisname, drawopt, styleindex, normalize );
}

struct InputFile {
  TString name;
  TString title;
  int     styleindex;
  InputFile( TString aname, TString atitle, int astyleindex )
      : name( aname ), title( atitle ), styleindex( astyleindex ) {}
  InputFile( TString aname, int astyleindex ) : name( aname ), title( aname ), styleindex( astyleindex ) {}
  InputFile() {}
};

struct InputFiles : public std::vector<InputFile> {
  template <typename T>
  InputFiles( std::initializer_list<T> inputs ) {
    int index = 0;
    for ( const auto& input : inputs ) this->push_back( std::move( InputFile{input, index++} ) );
  }
  InputFiles() {}
};

TH1* drawH1( const InputFiles& files, const TString& hisname, TString drawopt = "", bool normalize = false ) {
  TH1* rc = drawH1( files[0].name, hisname, drawopt, files[0].styleindex, false );
  if ( rc ) {
    double norm = rc->Integral();

    if ( drawopt == "" ) {
      // drawopt = is1D(rc) ? "hist" : (isProfile1D(rc) ? "err" : "box") ;
      drawopt = is1D( rc ) ? "err" : ( isProfile1D( rc ) ? "err" : "box" );
      rc->SetDrawOption( drawopt );
      if ( isProfile1D( rc ) ) rc->SetStats( 0 );
    }

    double minval = min1D( *rc );
    double maxval = max1D( *rc );
    if ( rc->GetEntries() > 0 && !( minval < maxval ) ) {
      std::cout << "Something wrong with min/maxvals: " << rc->GetName() << " " << rc->GetEntries() << " " << minval
                << " " << maxval << std::endl;
    }

    for ( unsigned int i = 1; i < files.size(); ++i )
      if ( files[i].name.Length() > 0 ) {
        TH1* h1 = drawH1( files[i].name, hisname, drawopt + " sames", files[i].styleindex, false );
        if ( h1 ) {
          if ( isProfile1D( rc ) )
            rc->SetStats( 0 );
          else {
            TPaveStats* st = (TPaveStats*)h1->FindObject( "stats" );
            if ( st ) st->SetTextColor( h1->GetMarkerColor() );
          }
          if ( is1D( h1 ) && normalize ) {
            if ( h1->GetSumw2() == 0 ) h1->Sumw2();
            h1->Scale( norm / h1->Integral() );
          }
          maxval = std::max( maxval, max1D( *h1 ) );
          minval = std::min( minval, min1D( *h1 ) );
        }
      }

    if ( is1D( rc ) ) {
      if ( !gPad->GetLogy() ) {
        if ( maxval > max1D( *rc ) ) rc->SetMaximum( 1.1 * maxval );
        rc->SetMinimum( 0 );
      } else {
        rc->SetMaximum( 2 * maxval );
        rc->SetMinimum( minval > 0 ? 0.5 * minval : 0.5 );
      }
    } else if ( isProfile1D( rc ) ) {
      double miny( 0 ), maxy( 0 );
      if ( minval < 0 && maxval > 0 ) {
        // symmetrize
        maxy = 1.1 * std::max( -minval, maxval );
        miny = -maxy;
      } else {
        maxy = maxval + 0.1 * ( maxval - minval );
        miny = minval - 0.1 * ( maxval - minval );
        if ( minval >= 0 && miny < 0 ) miny = 0;
      }
      if ( maxy > miny ) {
        // if( maxy > rc->GetMaximum() ) rc->SetMaximum(maxy) ;
        // if( miny < rc->GetMinimum() ) rc->SetMinimum(miny) ;
        if ( !gPad->GetLogy() ) {
          rc->SetMaximum( maxy );
          rc->SetMinimum( miny );
        } else {
          rc->SetMaximum( 2 * maxy );
          rc->SetMinimum( miny > 0 ? 0.5 * miny : 0.5 );
        }
      }
    }
  } else {
    gPad->Clear();
  }

  return rc;
}

TH1* drawH2( std::vector<TString> files, TString hisname, TString drawopt ) {
  TH1* rc = drawH2( files[0], hisname, drawopt );
  for ( unsigned int i = 1; i < files.size(); ++i )
    drawH2( files[i], hisname, drawopt + " sames", colorFromIndex( i ) );
  return rc;
}

void drawLegend( const std::vector<InputFile>& files, double x = 0.6, double y = 0.9, double textsize = 0.04 ) {
  TLatex text;
  text.SetNDC();
  text.SetTextSize( textsize );
  for ( size_t i = 0; i < files.size(); ++i ) {
    if ( files[i].title != TString() ) {
      text.SetTextColor( colorFromIndex( files[i].styleindex ) );
      text.DrawText( x, y - 0.05 * i, files[i].title );
    }
  }
}

TH1* drawH1( const std::vector<TString>& hisnames, TString drawopt = "", bool normalize = false ) {
  // first get pointers to the histograms
  std::vector<TH1*> histos;
  for ( const auto& p : hisnames ) {
    auto h1 = getH1( p );
    if ( h1 ) histos.push_back( h1 );
  }
  int  index{0};
  TH1* rc = drawH1( histos.front(), drawopt, index, false );
  if ( rc ) {
    double norm = rc->Integral();

    if ( drawopt == "" ) {
      // drawopt = is1D(rc) ? "hist" : (isProfile1D(rc) ? "err" : "box") ;
      drawopt = is1D( rc ) ? "err" : ( isProfile1D( rc ) ? "err" : "box" );
      rc->SetDrawOption( drawopt );
      if ( isProfile1D( rc ) ) rc->SetStats( 0 );
    }

    double            minval = min1D( *rc );
    double            maxval = max1D( *rc );
    std::vector<TH1*> drawnhistos;
    drawnhistos.push_back( rc );
    for ( unsigned int i = 1; i < histos.size(); ++i ) {
      ++index;
      TH1* h1 = drawH1( histos[i], drawopt + " sames", index, false );
      if ( h1 ) {
        if ( isProfile1D( rc ) )
          rc->SetStats( 0 );
        else {
          TPaveStats* st = (TPaveStats*)h1->FindObject( "stats" );
          if ( st ) st->SetTextColor( h1->GetLineColor() );
        }
        if ( is1D( h1 ) && normalize ) {
          if ( h1->GetSumw2() == 0 ) h1->Sumw2();
          h1->Scale( norm / h1->Integral() );
        }
        maxval = std::max( maxval, max1D( *h1 ) );
        minval = std::min( minval, min1D( *h1 ) );
        drawnhistos.push_back( h1 );
      }
    }

    if ( is1D( rc ) ) {
      if ( !gPad->GetLogy() ) {
        if ( maxval > max1D( *rc ) ) rc->SetMaximum( 1.1 * maxval );
        rc->SetMinimum( 0 );
      } else {
        rc->SetMaximum( 2 * maxval );
        rc->SetMinimum( minval > 0 ? 0.5 * minval : 0.5 );
      }
    } else if ( isProfile1D( rc ) ) {
      double miny( 0 ), maxy( 0 );
      if ( minval < 0 && maxval > 0 ) {
        // symmetrize
        maxy = 1.1 * std::max( -minval, maxval );
        miny = -maxy;
      } else {
        maxy = maxval + 0.1 * ( maxval - minval );
        miny = minval - 0.1 * ( maxval - minval );
        if ( minval >= 0 && miny < 0 ) miny = 0;
      }
      // if( maxy > rc->GetMaximum() ) rc->SetMaximum(maxy) ;
      // if( miny < rc->GetMinimum() ) rc->SetMinimum(miny) ;
      if ( !gPad->GetLogy() ) {
        rc->SetMaximum( maxy );
        rc->SetMinimum( miny );
      } else {
        rc->SetMaximum( 2 * maxval );
        rc->SetMinimum( minval > 0 ? 0.5 * minval : 0.5 );
      }
    }
    gPad->Update();
    for ( size_t i = 0; i < drawnhistos.size(); ++i ) updatePaveStats( *drawnhistos[i], i );
    gPad->Modified();
  } else {
    gPad->Clear();
  }
  return rc;
}

/*******************************************************************************/

class MonitoringPage {

private:
  TString              m_title;
  std::vector<TString> m_h;

public:
  MonitoringPage( const char* atitle, const char* h1 = 0, const char* h2 = 0, const char* h3 = 0, const char* h4 = 0,
                  const char* h5 = 0, const char* h6 = 0, const char* h7 = 0, const char* h8 = 0, const char* h9 = 0,
                  const char* h10 = 0 )
      : m_title( atitle ) {
    // should use a 'variadic' function here
    if ( h1 ) m_h.push_back( h1 );
    if ( h2 ) m_h.push_back( h2 );
    if ( h3 ) m_h.push_back( h3 );
    if ( h4 ) m_h.push_back( h4 );
    if ( h5 ) m_h.push_back( h5 );
    if ( h6 ) m_h.push_back( h6 );
    if ( h7 ) m_h.push_back( h7 );
    if ( h8 ) m_h.push_back( h8 );
    if ( h9 ) m_h.push_back( h9 );
    if ( h10 ) m_h.push_back( h10 );
  }

  MonitoringPage( const char* atitle, const std::vector<TString>& h ) : m_title{atitle}, m_h{h} {}

  /*

  MonitoringPage(const char* atitle,
                 const char* histos... )
    : m_title(atitle)
  {
    // should use a 'variadic' function here
    // see: https://en.cppreference.com/w/cpp/utility/variadic
    // but this only works if you also specify the number of arguments. (see various discussions).
    va_list args;
    va_start(args, histos);
    const char* thishisto = histos ;
    while( thishisto ) {
      std::cout << thishisto << std::endl ;
      thishisto = va_arg(args, const char*);
      m_h.push_back( thishisto ) ;
    }
    va_end(args);
  }
  */
  // void
  //   draw(const std::vector<TString>& filenames, bool print ) {
  //     TCanvas* c1 = getCanvas(m_title,m_title) ;
  //     draw( filenames, c1 ) ;

  //     if(print) c1->Print(m_title + ".eps") ;
  //   }

  void draw( const InputFiles& files, TCanvas* canvas, bool normalize, TString globaldrawopt = "" ) {
    std::cout << "In MonitoringPage::draw: " << m_title << " " << canvas << std::endl;
    size_t ny = 2;
    if ( m_h.size() > 6 ) ny = std::min( std::max( 2, int( std::sqrt( m_h.size() ) ) ), 4 );
    size_t nx = std::max( int( m_h.size() / ny ), 2 );
    if ( nx * ny < m_h.size() ) ++nx;
    if ( nx * ny < m_h.size() ) {
      std::cout << "shit: " << m_h.size() << " " << nx << " " << ny << std::endl;
      return;
    }
    if ( m_h.size() == 1 ) ny = nx = 1;
    TCanvas* c1 = canvas ? canvas : getCanvas( m_title, m_title );
    createPage( c1, m_title, nx, ny );
    for ( size_t i = 0; i < m_h.size(); ++i ) {
      c1->cd( i + 1 );
      std::cout << "drawing: " << m_h[i] << std::endl;

      TString hisname     = m_h[i];
      TString thisdrawopt = "";
      int     ipos        = m_h[i].First( ":" );
      if ( ipos > 0 && ipos < m_h[i].Sizeof() ) {
        hisname     = TString( m_h[i].Data(), ipos );
        thisdrawopt = TString( m_h[i].Data() + ipos + 1 );
      }
      if ( thisdrawopt.Contains( "log" ) ) {
        gPad->SetLogy();
        thisdrawopt.Replace( thisdrawopt.Index( "log" ), 3, " " );
      }
      if ( globaldrawopt.Length() > 0 ) { thisdrawopt += " " + globaldrawopt; }
      drawH1( files, hisname, thisdrawopt.Data(), normalize );
    }
  }

  TString GetName() const { return m_title; }
};

/******************************************************************************/

class MonitoringApplication {
public:
  MonitoringApplication() {}
  void addPage( MonitoringPage* page ) { m_pages.push_back( page ); }
  void addFile( const TString& filenametag ) {
    InputFile file;
    int       ipos = filenametag.First( ":" );
    file.name      = filenametag;
    if ( ipos > 0 && ipos < filenametag.Sizeof() ) {
      file.title = TString( filenametag.Data(), ipos );
      file.name  = TString( filenametag.Data() + ipos + 1 );
      file.name.ReplaceAll( " ", "" );
      file.name.ReplaceAll( "//", '/' );
    }
    file.styleindex = m_filenames.size();
    m_filenames.push_back( file );
  }
  void addFile( const TString& filenametag, int styleindex ) {
    addFile( filenametag );
    m_filenames.back().styleindex = styleindex;
  }

  void drawPage( TString pagename, bool normalize = false ) {
    bool found = false;
    for ( size_t ipage = 0; ipage < m_pages.size() && !found; ++ipage )
      if ( m_pages[ipage]->GetName() == pagename ) {
        found = true;
        m_pages[ipage]->draw( m_filenames, 0, normalize );
        gPad->GetCanvas()->cd( 1 );
        drawLegend( m_filenames, 0.65, 0.85 );
        gPad->GetCanvas()->Print( "c1.eps" );
      }
    if ( !found ) {
      std::cout << "Cannot find page called \'" << pagename << "\'. Please choose from:" << std::endl;
      for ( size_t ipage = 0; ipage < m_pages.size(); ++ipage ) std::cout << m_pages[ipage]->GetName() << std::endl;
    }
  }

  void draw( TString pdfname = "plots", bool normalize = false, TString drawopt = "" ) {
    TPostScript* ps = new TPostScript( pdfname + ".ps", 112 );
    ps->Range( 26, 18 );
    TCanvas* c1 = new TCanvas( "LHCbMonitoring", "" );

    for ( size_t ipage = 0; ipage < m_pages.size(); ++ipage ) {
      std::cout << "page: " << m_pages[ipage]->GetName() << std::endl;
      m_pages[ipage]->draw( m_filenames, c1, normalize, drawopt );
      gPad->GetCanvas()->cd( 1 );
      drawLegend( m_filenames, 0.6, 0.85 );
      if ( c1 ) c1->Update();
    }
    if ( ps ) {
      ps->Close();
      gSystem->Exec( "ps2pdf " + pdfname + ".ps ;  rm " + pdfname + ".ps" );
    }
  }

private:
  std::vector<MonitoringPage*> m_pages;
  InputFiles                   m_filenames;
};

static MonitoringApplication* gMonitoringApplication = new MonitoringApplication();

void drawHistos( TString histname, const char* fileA, const char* fileB = 0, const char* fileC = 0 ) {
  MonitoringPage page( histname, histname );
  InputFiles     files;
  files.push_back( InputFile( fileA, 1 ) );
  if ( fileB ) files.push_back( InputFile( fileB, 2 ) );
  if ( fileC ) files.push_back( InputFile( fileC, 3 ) );
  page.draw( files, 0, 0 );
}
