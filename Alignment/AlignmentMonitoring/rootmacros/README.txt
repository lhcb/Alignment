To use the 'drawalignmon' script to plot histograms, you can use

shell> root drawutils.cxx drawalignmon.cxx\(\"histos.root\"\)

When you want to have more controls, run functions from the root command line, e.g.

root> .L drawutils.cxx
root> .L drawalignment.cxx
root> drawall( "before alignment:Iter0/histograms.root","after alignment:Iter5/histograms.root")

good luck!
