/*****************************************************************************\
* (c) Copyright 2000-2014 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DRAWUTILS
#define DRAWUTILS

class TCanvas;
class TTree;
class TString;
class TH1;

TCanvas* createPage( const char* name, const char* title, int nx, int ny, int w = 700, int h = 500 );

TTree* getTree( const TString& filename, const TString& hisname );

TH1* createH1( TTree& tree, const TString& var, int nbins, double xmin, double xmax, const char* sel = "" );

TEventList* makeEventList( TTree& tree, TCut sel, TString name = "" );
#endif
